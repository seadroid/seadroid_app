﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using fifi.Client.DataObjects;
using fifi.Utils;
using Newtonsoft.Json;

namespace fifi.Client.Interfaces
{
   public class numverifyApi
    {


       
        
        private string baseurl = "http://apilayer.net/api";
        private HttpClient Hclient;
        private string accesskey = "c0aeb5fafcaed3193e26e76383ce8186";

        public numverifyApi()
        {           
            Hclient = new HttpClient();
                  
        }

        public async Task<VerifiedPhoneNumber> VerifyNumber(string Number)
        {
            try
            {
                string RequestUrl = "/validate?access_key=" + accesskey + "&number=" + Number;

                var method = new HttpMethod("GET");
                var request = new HttpRequestMessage(method, baseurl + RequestUrl);
                var HRM = await Hclient.SendAsync(request);
                var result = JsonConvert.DeserializeObject<VerifiedPhoneNumber>(await HRM.Content.ReadAsStringAsync());
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine("NumverifyAPI " + e);
                throw;
            }
          


        }



    }
}
