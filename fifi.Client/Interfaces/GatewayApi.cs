﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Security.Tokens;
using System.Text;
using System.Threading.Tasks;
using fifi.Client.DataObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace fifi.Client.Interfaces
{
    public class GatewayApi
    {
        private string Token = "qNgQlFm9SL29YLjmeP_h64uI4Prwo8NByULh9xsP1TAF7pKYsPOGhi8A8pVx0oRS";
        private string baseurl = "http://badssl.gatewayapi.com/rest/";
        private HttpClient Hclient;
        private HttpWebRequest webRequest;

        public GatewayApi()
        {
            Hclient = new HttpClient();
            
        
        }


        public async Task<bool> sendSms(string message, string phonenumber)
        {

            try
            {                                                       
                 var mydata = new JObject {{"sender","SEADROID"}, {"message", message}, {"recipients", new JArray { new JObject { { "msisdn", phonenumber }}}}};             
                 var content = new StringContent(mydata.ToString(), Encoding.UTF8, "application/json");         
                 var HRM = await Hclient.PostAsync(baseurl + "/mtsms?token=" + Token , content);
                  var result =  HRM.IsSuccessStatusCode;
                              
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);            
            }        

            return false;
        }
              
    }
}