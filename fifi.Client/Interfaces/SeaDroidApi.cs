﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using fifi.Client;
using fifi.Client.DataObjects;
using fifi.Client.helper;
using fifi.Utils;
using fifi.Utils.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.SecureStorage;


namespace fifi.Client.Interfaces
{
    public class SeaDroidApi : DelegatingHandler
    {
        private bool success;
        private string Secret = "aYPjXwgSVhAh7oB2dzGbJKTdnrOI4VzCDtNKiXbp";
        private string baseurl = "https://seadroidlife.com/api";
        private HttpClient Hclient;

        public SeaDroidApi()
        {
            Hclient = new HttpClient();


            Hclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            try
            {
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        #region Request setup

        private async Task<string> MakeRequest(string Type, string RequestUrl)
        {
          


            var Email = CrossSecureStorage.Current.GetValue("userEmail");
            var Password = CrossSecureStorage.Current.GetValue("userPassword");

            Hclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                Settings.Current.AuthToken);
            var method = new HttpMethod(Type);
        

            try
            {
                HttpResponseMessage hrm = await Hclient.SendAsync(new HttpRequestMessage(method, baseurl + RequestUrl));

                string result = await hrm.Content.ReadAsStringAsync();

                try
                {
                    var Errors = JsonConvert.DeserializeObject<ApiError>(result);

                    if (!Errors.success && Errors.errors != null)
                    {
                        if (!string.IsNullOrEmpty(Errors.errors.message))
                        {
                            var RefreshResult = await RefreshToken();
                            if (!RefreshResult)
                            {
                                var LoginResult = await PostLogin(Email, Password);
                                var ServiceAuth = JsonConvert.DeserializeObject<ServiceAuth>(LoginResult);
                                Settings.Current.AuthToken = ServiceAuth.AccessToken;
                                Settings.Current.RefreshAuthToken = ServiceAuth.RefreshToken;
                            }

                            Hclient = new HttpClient();
                            Hclient.DefaultRequestHeaders.Authorization =
                                new AuthenticationHeaderValue("Bearer", Settings.Current.AuthToken);
                            hrm = await Hclient.SendAsync(new HttpRequestMessage(method, baseurl + RequestUrl));
                        }
                    }
                    result = await hrm.Content.ReadAsStringAsync();
                    return result;
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }

                return result;
            }


            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// checks if the current acess token is valid
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns>bool</returns>
        private async Task<bool> CheckAcessToken(string email, string password)
        {
            var request = new HttpRequestMessage(new HttpMethod("GET"), baseurl + "/user");
            var postclient = new HttpClient();
            postclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                Settings.Current.AuthToken);
            var HRM = await postclient.SendAsync(request);
            var result = await HRM.Content.ReadAsStringAsync();
            TokenExpiredMessage TokenExpired = JsonConvert.DeserializeObject<TokenExpiredMessage>(result);

            if (TokenExpired.message != null)
            {
                return false;
            }
            return true;
        }


        private async Task<bool> RefreshToken()
        {
            try
            {
                var request = new HttpRequestMessage(new HttpMethod("POST"),
                    baseurl + "/login");
                var body = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("client_id", "1"),
                    new KeyValuePair<string, string>("client_secret", Secret),
                    new KeyValuePair<string, string>("refresh_token", Settings.Current.RefreshAuthToken),
                    new KeyValuePair<string, string>("grant_type", "refresh_token")
                };

                request.Content = new FormUrlEncodedContent(body);
                var postclient = new HttpClient();
                var HRM = await postclient.SendAsync(request);


                if (HRM.IsSuccessStatusCode)
                {
                    var result = await HRM.Content.ReadAsStringAsync();
                    var serviceauth = JsonConvert.DeserializeObject<ServiceAuth>(result);
                    Settings.Current.AuthToken = serviceauth.AccessToken;
                    Settings.Current.RefreshAuthToken = serviceauth.RefreshToken;
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }

        #endregion

        #region Masters

        /// <summary>
        /// gets the masters belonging to the user making the request
        /// </summary>
        /// <returns>a list of masters</returns>
        public async Task<string> GetUserMasters()
        {
            string result = await MakeRequest("GET", "/masters");
            return result;
        }


        /// <summary>
        ///  adds the users id to the master corrosponding to the masterserial in the database
        /// </summary>
        /// <param name="masterserial"></param>
        /// <returns></returns>
        public async Task<string> AssignMaster(string masterserial)
        {
            string result = await MakeRequest("POST", "/masters/" + masterserial);
            return result;
        }


        /// <summary>
        ///  Removes the master from the user
        /// </summary>
        /// <param name="master_id"></param>
        /// <returns></returns>
        public async Task<string> RemoveMaster(string master_id)
        {
            string result = await MakeRequest("PATCH", "/masters/" + master_id + "?user_id=0");
            return result;
        }

        /// <summary>
        /// assings the Boat with the given id to the master
        /// </summary>
        /// <param name="Boat_id"></param>
        /// <param name="master_id"></param>
        /// <returns></returns>
        public async Task<string> AssingBoatToMaster(int Boat_id, int master_id)
        {
            string result = await MakeRequest("PATCH", "/masters/" + master_id + "?boat_id=" + Boat_id);
            return result;
        }

        #endregion

        #region User

        /// <summary> 
        /// creates a new user 
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="country_id"></param>
        /// <returns>HTTP result code</returns>
        public async Task<string> CreateUser(string firstname, string lastname, string email, string password,
            string phoneNumber, int country_id)
        {
            string RequestUrl = "/register?firstname=" + firstname + "&lastname=" + lastname + "&email=" + email +
                                "&password=" + password + "&mobile=" + phoneNumber + "&country_id=" + country_id;
            // string RequestUrl = "/register";
            var method = new HttpMethod("POST");
            var request = new HttpRequestMessage(method, baseurl + RequestUrl);
            request.Content = new StringContent("");
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var HRM = await Hclient.SendAsync(request);
            var result = await HRM.Content.ReadAsStringAsync();
            return result;
        }

        /// <summary>
        ///  Login
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns> an acess token and a expire given in seconds </returns>
        public async Task<string> PostLogin(string email, string password)
        {
            try
            {
                var request = new HttpRequestMessage(new HttpMethod("POST"),
                    baseurl + "/login");
                var body = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("username", email),
                    new KeyValuePair<string, string>("password", password),
                    new KeyValuePair<string, string>("client_id", "1"),
                    new KeyValuePair<string, string>("client_secret", Secret),
                    new KeyValuePair<string, string>("grant_type", "password")
                };

                request.Content = new FormUrlEncodedContent(body);
                var postclient = new HttpClient();
                var HRM = await postclient.SendAsync(request);
                var result = await HRM.Content.ReadAsStringAsync();
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// gets the userprofile of the use who sends the request
        /// </summary>
        /// <returns>the users userprofile</returns>
        public async Task<string> GetUserProfile()
        {
            var request = new HttpRequestMessage(new HttpMethod("GET"), baseurl + "/users/auth");
            var postclient = new HttpClient();
            postclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                Settings.Current.AuthToken);
            var HRM = await postclient.SendAsync(request);
            var result = await HRM.Content.ReadAsStringAsync();
            return result;
        }

        /// <summary>
        /// Gets all users in the database
        /// </summary>
        /// <returns>a list of all users</returns>
        public async Task<string> GetUsers()
        {
            var request = await MakeRequest("GET", "/users");
            return request;
        }


        /// <summary>
        ///  Gets the users corrosponding to the user_id givin
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns>a user object</returns>
        public async Task<string> GetSpecificUser(string user_id)
        {
            var request = await MakeRequest("GET", "/users/" + user_id);
            return request;
        }

        /// <summary>
        /// sets the firstname, lastname, image and email of a user
        /// </summary>
        /// <param name="image"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<string> PatchUserInfo(string user_id, string firstname, string lastname)
        {
            var request = await MakeRequest("PATCH",
                "/users/" + user_id + "?firstname=" + firstname + "&lastname=" + lastname);
            return request;
          
        }


        /// <summary>
        ///  verifies 
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns></returns>
        public async Task<string> VerifyUser(string user_id)
        {
            var result = await MakeRequest("PATCH", "/users/" + user_id + "?verified=1");
            return result;
        }


        public async Task<string> UploadImange(string image, string user_id)
        {
            try
            {
                Hclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                    Settings.Current.AuthToken);
                var method = new HttpMethod("POST");
                var request = new HttpRequestMessage(method, baseurl + "/users/" + user_id + "/upload");

                var body = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("image", image),
                };
                request.Content = new MyFormUrlEncodedContent(body);

                //request.Content = new StringContent("");
                //request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage hrm = await Hclient.SendAsync(request);
                string result = await hrm.Content.ReadAsStringAsync();
                return result;
            }


            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }


            //var stream = new System.IO.MemoryStream(image);
            //var content = new StreamContent(stream);

            //var Email = CrossSecureStorage.Current.GetValue("userEmail");
            //var Password = CrossSecureStorage.Current.GetValue("userPassword");

            //Hclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
            //    Settings.Current.AuthToken);

            //var method = new HttpMethod("POST");
            //var request = new HttpRequestMessage(method, baseurl + "/user/upload");
            //request.Content = content;
            ////request.Content = new StringContent("");
            ////request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            //try
            //{
            //    HttpResponseMessage hrm = await Hclient.SendAsync(request);
            //    if (hrm.StatusCode == HttpStatusCode.Unauthorized)
            //    {
            //        var AuthToken = JsonConvert.DeserializeObject<ServiceAuth>(await PostLogin(Email, Password));
            //        Settings.Current.AuthToken = AuthToken.AccessToken;
            //        Hclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
            //            Settings.Current.AuthToken);
            //        await Task.Delay(200);
            //        hrm = await Hclient.SendAsync(request);
            //    }
            //    string result = await hrm.Content.ReadAsStringAsync();
            //    return result;
            //}
            //catch (Exception e)
            //{              
            //   Debug.WriteLine(e);
            //    return e.ToString();
            //}
        }


        public async Task<string> PatchUserPassword(string user_id, string Password)
        {
            var request = await MakeRequest("PATCH", "/users/" + user_id + "?password=" + Password);
            return request;
        }

        public async Task<string> PatchUserPhoneNumber(string user_id, string country_id, string phonenumber)
        {
            var request = await MakeRequest("PATCH",
                "/users/" + user_id + "?contry_id=" + country_id + "&mobile" + phonenumber);
            return request;
        }

        #endregion

        #region Coordinates

        /// <summary>
        ///  gets the current coordinates for alle vessels
        /// </summary>
        /// <returns>a list of vessels and thier coordinates </returns>
        public async Task<string> GetAllCoordinates()
        {
            string result = await MakeRequest("GET", "/coordinates/get-all");
            return result;
        }

        #endregion

        #region Countries

        /// <summary>
        /// gets all countries in the database
        /// </summary>
        /// <returns>a list of all the countries in the database </returns>
        public async Task<string> GetAllCountries()
        {
            var request = new HttpRequestMessage(new HttpMethod("GET"), baseurl + "/countries");
            var postclient = new HttpClient();
            var HRM = await postclient.SendAsync(request);
            var result = await HRM.Content.ReadAsStringAsync();
            return result;
        }

        #endregion

         #region Boats
        /// <summary>
        /// Creates a Boat
        /// </summary>
        /// <param name="boatname"></param>
        /// <param name="model"></param>
        /// <param name="country_id"></param>
        /// <param name="category_id"></param>
        /// <param name="year"></param>
        /// <param name="color"></param>
        /// <param name="harbor"></param>
        /// <param name="registry_no"></param>
        /// <returns> a response confirming creation or giving a error </returns>
        public async Task<string> CreateBoat(string boatname, string model, int country_id, int category_id, int year, string color, string harbor, string registry_no, string boatlength)
        {
            var result =await MakeRequest("POST",
                "/boats?name=" + boatname + "&model=" + model + "&country_id=" + country_id + "&category_id=" +
                category_id + "&year=" + year + "&color=" + color + "&harbor=" + harbor + "&registry_no=" + registry_no+ "&length="+boatlength);
            return result;

        }

        /// <summary>
        /// get all the users boats
        /// </summary>
        /// <returns></returns>    
        public async Task<string> GetUsersBoats()
        {
            var result = await MakeRequest("GET", "/boats");
            return result;
        }

        public async Task<string> GetBoat(string boat_id)
        {
            var result = await MakeRequest("GET", "/boats/" + boat_id);
            return result;
        }

        public async Task<string> UploadBoatImage(string boatId, string base64Image)
        {
            try
            {
                Hclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Settings.Current.AuthToken);
                var body = new List<KeyValuePair<string, string>> {new KeyValuePair<string, string>("image", base64Image)};
                HttpResponseMessage hrm = await Hclient.SendAsync(new HttpRequestMessage(new HttpMethod("POST"), baseurl + "/boats/" + boatId + "/upload"){Content = new MyFormUrlEncodedContent(body) });
                var result = await hrm.Content.ReadAsStringAsync();
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }


        public async Task<string> UpdateBoat(Boat boat)
        {
            var result = await MakeRequest("PATCH", "/boats/" + boat.id+ "?name=" + boat.name + "&model=" + boat.model + "&country_id=" + boat.country_id + "&category_id=" +
            boat.category_id + "&year=" + boat.year + "&color=" + boat.color + "&harbor=" + boat.harbor + "&registry_no=" + boat.registry_no + "&length=" + boat.length);
            return result;
        }

        #endregion

        #region Armbands

        /// <summary>
        ///  gets all Armbands in the database
        /// </summary>
        /// <returns>a list of all armbands in the database</returns>
        public async Task<string> GetWristbands()
        {
            string result = await MakeRequest("GET", "/armbands/all");
            return result;
        }

        /// <summary>
        /// gets the Armbands belonging to the user who is making the request
        /// </summary>
        /// <returns>a list of the users armbands </returns>
        public async Task<string> GetUserWristbands()
        {
            string result = await MakeRequest("GET", "/armbands");
            return result;
        }


        /// <summary>
        /// updates the Armband with the giving ID
        /// </summary>
        /// <param name="ArmbandID"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="gender"></param>
        /// <param name="birthday"></param>
        /// <param name="weight"></param>
        /// <param name="height"></param>
        /// <param name="country_id"></param>
        /// <param name="haircolor"></param>
        /// <param name="swim_exp"></param>
        /// <param name="sea_exp"></param>
        /// <param name="user_id"></param>
        /// <returns>HTTP result code</returns>
        public async Task<string> UpdateArmband(int ArmbandID, string firstname, string lastname, string gender,
            string birthday, int weight, int height, int country_id, string haircolor, int swim_exp, int sea_exp)
        {
            string result = await MakeRequest("PATCH",
                "/armbands/" + ArmbandID + "?firstname=" + firstname + "&lastname=" + lastname + "&gender=" + gender +
                "&birthday=" + birthday + "&weight=" + weight + "&height=" + height +
                "&country_id=" + country_id + "&haircolor=" + haircolor + "&swim_exp=" + swim_exp + "&sea_exp=" +
                sea_exp);
            return result;
        }

        /// <summary>
        /// Creates a new Armband
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="serial"></param>
        /// <param name="gender"></param>
        /// <param name="birthday"></param>
        /// <param name="weight"></param>
        /// <param name="height"></param>
        /// <param name="country_id"></param>
        /// <param name="haircolor"></param>
        /// <param name="swim_exp"></param>
        /// <param name="sea_exp"></param>
        /// <param name="user_id"></param>
        /// <returns>HTTP result code</returns>
        public async Task<string> CreateArmband(string firstname, string lastname, string serial, string gender,
            string birthday, int weight, int height, int country_id, string haircolor, int swim_exp, int sea_exp,
            int user_id)
        {
            string result = await MakeRequest("POST",
                "/armbands?firstname=" + firstname + "&lastname=" + lastname + "&gender=" + gender + "&birthday=" +
                birthday + "&weight=" + weight + "&height=" + height +
                "&country_id=" + country_id + "&haircolor=" + haircolor + "&swim_exp=" + swim_exp + "&sea_exp=" +
                sea_exp + "&user_id=" + user_id + "&serial=" + serial);
            return result;
        }

        #endregion

        #region Friends

        /// <summary>
        /// Gets the users friends 
        /// </summary>
        /// <returns>users friends</returns>
        public async Task<string> GetUserFriends()
        {
            string result = await MakeRequest("GET", "/friends");
            return result;
        }

        /// <summary>
        /// Gets pending Friend Requests
        /// </summary>
        /// <returns>pending friend requests</returns>
        public async Task<string> GetPendingFriendRequest()
        {
            string result = await MakeRequest("GET", "/friends/pending");
            return result;
        }

        /// <summary>
        /// Gets send friend requests that are not yet accepted by the recipient
        /// </summary>
        /// <returns>a list of active friend requests</returns>
        public async Task<string> GetSentFriendRequests()
        {
            string result = await MakeRequest("GET", "/friends/sent");
            return result;
        }


        /// <summary>
        /// sends a friend request to the given user id
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns>HTTP result code</returns>
        public async Task<string> SendFriendRequest(string user_id)
        {
            string result = await MakeRequest("POST", "/friends/" + user_id);
            return result;
        }

        /// <summary>
        /// Accept give friend request
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns>HTTP result code</returns>
        public async Task<string> AcceptFriendRequest(string user_id)
        {
            string result = await MakeRequest("POST", "/friends/" + user_id + "/accept");
            return result;
        }


        /// <summary>
        /// Decline given friend Request
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns>HTTP result code</returns>
        public async Task<string> DeclineFriendRequest(string user_id)
        {
            string result = await MakeRequest("POST", "/friends/" + user_id + "/decline");
            return result;
        }

        /// <summary>
        ///  Deletes given user from friends
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns></returns>
        public async Task<string> DeleteFriend(string user_id)
        {
            string result = await MakeRequest("DELETE", "/friends/" + user_id);
            return result;
        }

        #endregion


        #region Trips

        public async Task<string> GetTrip(string trip_id)
        {
            string result = await MakeRequest("GET", "/trips/" + trip_id);
            return result;
        }

        #endregion
    }
}