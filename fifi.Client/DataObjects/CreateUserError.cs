﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fifi.Client.DataObjects
{
 public class CreateUserError
    {
        public string message { get; set; }
        public UserErrors errors { get; set; }
        public int status_code { get; set; }
    }

    public class UserErrors
    {
        public List<string> email { get; set; }
        public List<string> mobile { get; set; }
    }
}
