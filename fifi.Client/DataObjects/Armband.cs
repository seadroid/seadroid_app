﻿namespace fifi.Client.DataObjects
{
    public class Armband
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public string serial { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public int? height { get; set; }
        public int? weight { get; set; }
        public string gender { get; set; }
        public string birthday { get; set; }
        public int? country_id { get; set; }
        public string haircolor { get; set; }
        public int? swim_exp { get; set; }
        public int? sea_exp { get; set; }
        public int active { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public User user { get; set; }
        public Country country { get; set; }
        public string Battery { get; set; }
        public bool Signal { get; set; }


        public Armband(string firstname, string lastname, string birthday, int? height, int? weight, string serial, string gender, int? countryId, string haircolor, int? swimExp, int? seaExp)
        {

            this.serial = serial;
            this.firstname = firstname;
            this.lastname = lastname;
            this.height = height;
            this.weight = weight;
            this.gender = gender;
            this.birthday = birthday;
            country_id = countryId;
            this.haircolor = haircolor;
            swim_exp = swimExp;
            sea_exp = seaExp;
            this.country = country;
            Signal = false;
        }

        public Armband()
        {
            Signal = false;
        }
    }
}