﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fifi.Client.DataObjects
{
  
    public class Pivot
    {
        public int master_id { get; set; }
        public int trip_id { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }

    public class Position
    {
        public int id { get; set; }
        public int trip_id { get; set; }
        public int master_id { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }
        public int? altitude { get; set; }
        public object heading { get; set; }
        public object speed { get; set; }
        public string received_at { get; set; }
        public string created_at { get; set; }
    }


  

    public class CurrentTrip
    {
        public int id { get; set; }
        public int? weather_id { get; set; }
        public string started_at { get; set; }
        public string ended_at { get; set; }
        public Pivot pivot { get; set; }
        public Position position { get; set; }
        public Weather weather { get; set; }
    }

  

    public class TripMaster
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int? boat_id { get; set; }
        public object firmware_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string serial { get; set; }
        public bool sending { get; set; }
        public bool active { get; set; }
        public string created_at { get; set; }
        public List<CurrentTrip> trip { get; set; }
        public Boat boat { get; set; }
    }


    public class ActiveVessels
    {
        public List<TripMaster> data { get; set; }
    }
}
