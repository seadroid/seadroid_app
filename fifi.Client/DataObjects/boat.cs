﻿using Newtonsoft.Json;

namespace fifi.Client.DataObjects
{
    
    public class Boat
    {

        public int id { get; set; }
        public int user_id { get; set; }
        public int country_id { get; set; }
        public int category_id { get; set; }
        public string name { get; set; }
        public string model { get; set; }
        public int year { get; set; }
        public string color { get; set; }
        public string harbor { get; set; }
        public string registry_no { get; set; }
        public int length { get; set; }
        public string picture { get; set; }
        public Country country { get; set; }
        public Category category { get; set; }

    }
}