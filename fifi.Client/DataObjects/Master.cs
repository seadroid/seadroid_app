﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace fifi.Client.DataObjects
{
    public class Master
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("user_id")]
        public int UserId { get; set; }

        [JsonProperty("boat_id")]
        public int? BoatId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("serial")]
        public string Serial { get; set; }

        [JsonProperty("created_at ")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        //[JsonProperty("Boat")]
        //public string Boat { get; set; }

        [JsonProperty("trip")]
        public IList<Trip> Trips { get; set; }
    }

    public class Trip
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("master_id")]
        public int? MasterId { get; set; }

        [JsonProperty("crew_id")]
        public int? CrewId { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("started_at")]
        public string StartedAt { get; set; }

        [JsonProperty("ended_at")]
        public string EndedAt { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("crew")]
        public object Crew { get; set; }

        [JsonProperty("coordinate")]
        public IList<Coordinate> Coordinates { get; set; }

        public Weather weather { get; set; }
        public List<object> armband { get; set; }
        public object speed { get; set; }
        public int? heading { get; set; }
        public int? altitude { get; set; }
        

    }

    public class Coordinate
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("trip_id")]
        public int TripId { get; set; }
        [JsonProperty("longitude")]
        public string Longitude { get; set; }
        [JsonProperty("latitude")]
        public string Latitude { get; set; }
        [JsonProperty("altitude")]
        public int? Altitude { get; set; }
        public object heading { get; set; }
        public double? speed { get; set; }
        public object wind_heading { get; set; }
        public object wind_speed { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public int? master_id { get; set; }
        public string recorded_at { get; set; }

        [JsonIgnore]
        public double Distance { get; set; }
    }
}