﻿using System.Collections.Generic;
using fifi.Client.DataObjects;
using Newtonsoft.Json;

namespace fifi.Client.DataObjects



{

    public  class User
    {
        public int id { get; set; }
        public string firstname { get; set; }
        public object lastname { get; set; }
        public string email { get; set; }
        public string created_at { get; set; }
    }

    public class Country
    {
        public int id { get; set; }
        public string name { get; set; }
        public string shortcode { get; set; }
        public string currency_shortcode { get; set; }
        public string language { get; set; }
        public string created_at { get; set; }
        public object updated_at { get; set; }
    }

 


    public class ArmbandsList
    {
        public List<Armband> data { get; set; }
    }
}



