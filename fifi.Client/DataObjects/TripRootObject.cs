﻿namespace fifi.Client.DataObjects
{
    public class TripRootObject
    {
        public bool success { get; set; }
        public string errors { get; set; }
        public Trip data { get; set; }
    }
}