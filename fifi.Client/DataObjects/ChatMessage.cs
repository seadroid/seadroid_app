﻿using System;

namespace fifi.Client.DataObjects
{
    public class ChatMessage
    {
         public string Username { get; set; }
            public string Text { get; set; }
            public string Channel { get; set; }
            public string Sender_id { get; set; }
            public long TimeToken { get; set; }
            public DateTime Date { get; set; }
            public PnApns pn_apns { get; set; }


            public ChatMessage(string sender_id, string username, string text, string channel, DateTime date, long timeToken)
            {
                Sender_id = sender_id;
                Username = username;
                Text = text;
                Channel = channel;
                Date = date;
                TimeToken = timeToken;
                pn_apns = new PnApns(new Aps("test", 2));
              

            }

        
    }

    public class Aps
    {
        public string alert { get; set; }
        public int badge { get; set; }

        public Aps(string Alert, int Badge)
        {
            alert = Alert;
            badge = Badge;
        }
    }

    public class PnApns
    {
        public Aps aps { get; set; }


        public PnApns(Aps Aps)
        {
            aps = Aps;
        }
    }

   
}