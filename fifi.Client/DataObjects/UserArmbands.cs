﻿using System.Collections.Generic;

namespace fifi.Client.DataObjects
{
    public class UserArmbands
    {
        public class Data
        {
            public int id { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string email { get; set; }
            public object facebook_id { get; set; }
            public string created_at { get; set; }
            public List<Armband> armband { get; set; }
        }
      
            public Data data { get; set; }
        
    }
}