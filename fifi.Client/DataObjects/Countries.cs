﻿using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace fifi.Client.DataObjects
{
    public class Countries
    {
        public List<Country> data { get; set; }
        public int status_code { get; set; }

        public class Country
        {
            public int id { get; set; }
            public string name { get; set; }
            public string shortcode { get; set; }
            public string currency_shortcode { get; set; }
            public string phone_format { get; set; }
            public string language { get; set; }
        }

    }


}