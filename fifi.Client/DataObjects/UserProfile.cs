﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace fifi.Client.DataObjects
{
    public class UserProfile
    {
        public string Firstname { get; set; }

		public string Lastname { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public int Id { get; set; }
        
        public string image { get; set; }

        public int country_id { get; set; }

        public bool verified { get; set; }

        public List<Armband> armband { get; set; }

        public List<Master> master { get; set; }

        public List<Boat> boat { get; set; }
    }
}