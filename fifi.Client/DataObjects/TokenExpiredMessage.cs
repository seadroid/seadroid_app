﻿namespace fifi.Client.DataObjects
{
    public class TokenExpiredMessage
    {
        public string message { get; set; }
        public int status_code { get; set; }
    }
}