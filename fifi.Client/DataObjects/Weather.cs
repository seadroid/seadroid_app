﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fifi.Client.DataObjects
{
    public class Weather
    {
        public int id { get; set; }
        public string wind_heading { get; set; }
        public string wind_speed { get; set; }
        public string weather_name { get; set; }
        public int weather_temp { get; set; }
        public string weather_icon { get; set; }
        public string weather_description { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }
}
