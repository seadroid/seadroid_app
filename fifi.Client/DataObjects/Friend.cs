﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace fifi.Client.DataObjects
{
    public class Friend
    {       
            public int id { get; set; }
            public string firstname { get; set; }
            public object lastname { get; set; }
            public string email { get; set; }
            public string image { get; set; }
            bool  verified { get; set; }
            public string created_at { get; set; }
            public DateTime latest_message_recived_at { get; set; }
            public string latest_message;
        [JsonProperty("pivot")]
        public FriendRequest FriendRequest { get; set; }


        public Friend()
        {
            
        }

        public Friend(int id, string firstname, object lastname, string email, string image, bool verified)
        {
            this.id = id;
            this.firstname = firstname;
            this.lastname = lastname;
            this.email = email;
            this.image = image;
            this.verified = verified;
        }
    }
    public class FriendRequest
    {
        public int recipient_id { get; set; }
        public int sender_id { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public int status { get; set; }
    }
    public class FriendList
    {
        public List<Friend> data { get; set; }
    }

    
}