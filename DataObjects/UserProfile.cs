﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataObjects
{
    public class UserProfile
    {
        public string name { get; set; }

        public string email { get; set; }

        public int id { get; set; }
    }
}
