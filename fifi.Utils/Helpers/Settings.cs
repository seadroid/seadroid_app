﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Refit;

namespace fifi.Utils.Helpers
{
    public class Settings : INotifyPropertyChanged
    {
        private const string LoginAttemptsKey = "login_attemps";
        private const int LoginAttempsDefault = 0;

        private static Settings _settings;

        private bool _isConnected;

        static ISettings AppSettings => CrossSettings.Current;

        /// <summary>
        ///     Gets or sets the current settings. This should always be used
        /// </summary>
        /// <value>The current.</value>
        public static Settings Current => _settings ?? (_settings = new Settings());

        public bool IsLoggedIn => !string.IsNullOrWhiteSpace(AuthToken) && !string.IsNullOrWhiteSpace(UserId);


        public int LoginAttemps
        {
            get { return AppSettings.GetValueOrDefault(LoginAttemptsKey, LoginAttempsDefault); }
            set { AppSettings.AddOrUpdateValue(LoginAttemptsKey, value); }
        }

        public bool IsConnected
        {
            get { return _isConnected; }
            set
            {
                if (_isConnected == value)
                {
                    return;
                }
                _isConnected = value;
                OnPropertyChanged();
            }
        }



        #region User Profile


        private const string IsVerifiedkey = "IsVerified";
        private static readonly bool IsVerifiedDefault = false;

        public bool IsVerified
        {
            get { return AppSettings.GetValueOrDefault(IsVerifiedkey, IsVerifiedDefault); }
            set { AppSettings.AddOrUpdateValue(IsVerifiedkey, value); }          
        }

        private const string UserIdKey = "user_id";
        private static readonly string UserIdDefault = string.Empty;

        public string UserId
        {
            get { return AppSettings.GetValueOrDefault(UserIdKey, UserIdDefault); }
            set { AppSettings.AddOrUpdateValue(UserIdKey, value); }
        }

        private const string AuthTokenKey = "authtoken";
        private static readonly string AuthTokenDefault = string.Empty;

        public string AuthToken
        {
            get { return AppSettings.GetValueOrDefault(AuthTokenKey, AuthTokenDefault); }
            set { AppSettings.AddOrUpdateValue(AuthTokenKey, value); }
        }

        private const string RefreshAuthTokenKey = "refreshauthtoken";
        private static readonly string RefreshAuthTokenDefault = string.Empty;

        public string RefreshAuthToken
        {
            get { return AppSettings.GetValueOrDefault(RefreshAuthTokenKey, RefreshAuthTokenDefault); }
            set { AppSettings.AddOrUpdateValue(RefreshAuthTokenKey, value); }
        }

        private const string AuthTokenExpireTimeKey = "expire_time";
        private static readonly string AuthTokenExpireTimeDefault = DateTime.Now.ToString();

        public string AuthTokenExpreTime
        {
            get { return AppSettings.GetValueOrDefault(AuthTokenExpireTimeKey, AuthTokenDefault); }
            set { AppSettings.AddOrUpdateValue(AuthTokenExpireTimeKey, value); }
        }

        private const string FirstnameKey = "user_firstname";
        private static readonly string FirstnameDefault = string.Empty;

        public string UserFirstname
        {
            get { return AppSettings.GetValueOrDefault(FirstnameKey, FirstnameDefault); }
            set { AppSettings.AddOrUpdateValue(FirstnameKey, value); }
        }

		private const string LastnameKey = "user_lastname";
		private static readonly string LastnameDefault = string.Empty;

		public string UserLastname 
		{
			get { return AppSettings.GetValueOrDefault(LastnameKey, LastnameDefault); }
			set { AppSettings.AddOrUpdateValue(LastnameKey, value); }
		}

        private const string UserImageBase64Key = "user_image_bas64";
        private static readonly string UserImageBase64Default = string.Empty;

        public string UserImageBase64
        {
            get { return AppSettings.GetValueOrDefault(UserImageBase64Key, UserImageBase64Default); }
            set { AppSettings.AddOrUpdateValue(UserImageBase64Key, value); }
        }

        private const string UserImageUrlKey = "user_image_url";
        private static readonly string UserImageUrlDefault = string.Empty;

        public string UserImageUrl
        {
            get { return AppSettings.GetValueOrDefault(UserImageUrlKey, UserImageUrlDefault); }
            set { AppSettings.AddOrUpdateValue(UserImageUrlKey, value); }
        }

        private const string ActiveChatIdKey = "ative_chat_id";
        private static readonly string ActiveChatIdDefault = string.Empty;

        public string ActiveChatId
        {
            get { return AppSettings.GetValueOrDefault(ActiveChatIdKey, ActiveChatIdDefault); }
            set { AppSettings.AddOrUpdateValue(ActiveChatIdKey, value); }          
        }

        private const string PubnubUiidKey = "pubnub_uuid";
        private static readonly string PubnubUiidDefault = string.Empty;

        public string PubnubUIID
        {
            get { return AppSettings.GetValueOrDefault(PubnubUiidKey, PubnubUiidDefault); }
            set { AppSettings.AddOrUpdateValue(PubnubUiidKey, value); }
        }

        private const string RandomNumberKey = "random_number";
        private static readonly string RandomNumberDefault = string.Empty;

        public string RandomNumber
        {
            get { return AppSettings.GetValueOrDefault(RandomNumberKey, RandomNumberDefault); }
            set { AppSettings.AddOrUpdateValue(RandomNumberKey, value); }
        }

      

        public bool Logout()
        {
            AuthToken = string.Empty;
            UserFirstname = string.Empty;
            UserId = string.Empty;
            IsVerified = false;
            ActiveChatId = string.Empty;
            UserImageUrl = string.Empty;
            UserLastname = string.Empty;
            Mapmode = string.Empty;

            return true;
        }

        #endregion

        #region UserSettings
        private const string Mapmodekey = "user_MapMode";
        private static readonly string MapmodeDefault = string.Empty;

        public string Mapmode
        {
            get { return AppSettings.GetValueOrDefault(Mapmodekey, MapmodeDefault); }
            set { AppSettings.AddOrUpdateValue(Mapmodekey, value); }
        }


        #endregion

        #region Device

        private const string DeviceIdKey = "DeviceKey";
        private static readonly string DeviceIdDefault = string.Empty;

        public string DeviceId
        {
            get { return AppSettings.GetValueOrDefault(DeviceIdKey, DeviceIdDefault); }
            set { AppSettings.AddOrUpdateValue(DeviceIdKey, value); }
        }

        private const string Platformkey = "PlatformKey";
        private static readonly string PlatformDefault = string.Empty;

        public string Platform
        {
            get { return AppSettings.GetValueOrDefault(Platformkey, PlatformDefault); }
            set { AppSettings.AddOrUpdateValue(Platformkey, value); }
        }

        #endregion

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string name = "")
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        #endregion
    }
}
