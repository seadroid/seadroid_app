﻿namespace fifi.Utils.Extension
{
    public static class StringExtensions
    {
        public static bool IsAllZeros(this string input)
        {
            return input == new string('0', input.Length);
        }
    }
}