﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace fifi.Utils.Extension
{
    public static class JsonExtensions
    {
        public static IList<T> JTokenToObject<T>(this JToken token, string key)
        {
            var f = token[key];
            return token[key].ToObject<IList<T>>();
        }
    }
}