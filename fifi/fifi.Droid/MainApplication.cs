using System;
using Acr.UserDialogs;
using Android.App;
using Android.OS;
using Android.Content;
using Android.Runtime;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using Plugin.CurrentActivity;
using Plugin.SecureStorage;
using fifi.Droid.Activities;
using fifi.Droid.Helpers;
using fifi.Helpers;
using fifi.Utils.Helpers;
using Newtonsoft.Json;
using Android.Graphics;
using Android.Graphics.Drawables;
using PubnubApi;
using System.Diagnostics;

namespace fifi.Droid
{
    //You can specify additional application information in this attribute
    [Application]
    public class MainApplication : Application, Application.IActivityLifecycleCallbacks
    {
        public MainApplication(IntPtr handle, JniHandleOwnership transer)
          : base(handle, transer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            Settings.Current.Platform = "ANDROID";
            RegisterActivityLifecycleCallbacks(this);
            ViewModel.ViewModelBase.Init();
            SecureStorageImplementation.StoragePassword = "Instant2150Rescue";

            UserDialogs.Init(() => CrossCurrentActivity.Current.Activity);
          //  A great place to initialize Xamarin.Insights and Dependency Services!



            // 

            



        }

        public override void OnTerminate()
        {
            base.OnTerminate();
            UnregisterActivityLifecycleCallbacks(this);
        }

        public void OnActivityCreated(Activity activity, Bundle savedInstanceState)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivityDestroyed(Activity activity)
        {
        }

        public void OnActivityPaused(Activity activity)
        {
        }

        public void OnActivityResumed(Activity activity)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivitySaveInstanceState(Activity activity, Bundle outState)
        {
        }

        public void OnActivityStarted(Activity activity)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivityStopped(Activity activity)
        {
        }

      
        //private async void NotificationHandling()
        //{


        //    if (PubNubSingleton.Instance.ChatMessage.Sender_id != Settings.Current.UserId && PubNubSingleton.Instance.ChatMessage.Channel != Settings.Current.ActiveChatId)
        //    {
        //        var _api = new SeaDroidApi();
        //        var result = await _api.GetSpecificUser(PubNubSingleton.Instance.ChatMessage.Sender_id);
        //        var user = JsonConvert.DeserializeObject<SpecificUser>(result);

        //        Intent intent = new Intent(this, typeof(ChatActivity));

        //        intent.PutExtra("Friend_id", PubNubSingleton.Instance.ChatMessage.Sender_id);
        //        intent.PutExtra("Friend_name", user.data.Firstname + " " + user.data.Lastname);
        //        intent.PutExtra("Friend_imageUrl", user.data.image);
        //        intent.PutExtra("OpenWithNotifcation", "true");






        //        const int pedingIntentId = 0;
        //        PendingIntent pendingIntent = PendingIntent.GetActivity(this, pedingIntentId, intent, PendingIntentFlags.OneShot);


        //        Notification.Builder builder = new Notification.Builder(this);
        //        builder.SetContentIntent(pendingIntent).SetContentText(PubNubSingleton.Instance.ChatMessage.Text);
        //        builder.SetContentTitle(user.data.Firstname + " " + user.data.Lastname);
        //        builder.SetSmallIcon(Resource.Drawable.logo);
        //        builder.SetDefaults(NotificationDefaults.Vibrate);
        //        builder.SetPriority(2);
        //        if (!string.IsNullOrEmpty(user.data.image))
        //        {
        //            var imagebytes = Convert.FromBase64String(new ImageUrlDownloader().GetImageBitmapFromUrl(user.data.image));
        //            var bitmap = BitmapFactory.DecodeByteArray(imagebytes, 0, imagebytes.Length);
        //            var Image = new CircleDrawable(bitmap);
        //            builder.SetLargeIcon(Image.bmp);
        //        }


        //        Notification notification = builder.Build();
        //        notification.Flags = NotificationFlags.AutoCancel;

        //        NotificationManager notificationManager = this.GetSystemService(Context.NotificationService) as NotificationManager;
        //        const int notificationId = 0;
        //        notificationManager.Notify(notificationId, notification);
        //    }



        //}
    }
}