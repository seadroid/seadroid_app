using System;
using System.Diagnostics;
using Android.Animation;
using Android.Content;
using Android.Graphics;
using Android.Renderscripts;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Views.Animations;

namespace fifi.Droid.Controls
{
    public class SpeedCircle : View
    {
        private Bitmap _bitmap;
        private Canvas _canvas;

        private RectF _circleOuterBounds = new RectF();
        private RectF _circleCenterBounds;
        private RectF _circleInnerBounds;

        private Paint _eraserPaint;
        private Path _path = new Path();
        private Context _context;

        private float _currentRating;
        private float _rating;
        private float _nextRatingCurrent;
        private float _nextRating;

        private ValueAnimator _ratingAnimator;
        private ValueAnimator _nextRatingAnimator;

        public SpeedCircle(IntPtr handle, JniHandleOwnership transfer) : base(handle, transfer) { }

        public SpeedCircle(Context context) : this(context, null) { }

        public SpeedCircle(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init(context, attrs);
        }

        public SpeedCircle(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            Init(context, attrs);
        }

        public float Rating
        {
            get { return _rating; }
            set
            {
                _rating = value;
                _currentRating = 0;
                if (PlayAnimation)
                {
                    Start(1);
                }
                else
                {
                    Invalidate();
                }
            }
        }

        public float NextRating
        {
            get { return _nextRating; }
            set
            {
                _nextRating = value;
                _nextRatingCurrent = 0;
                if (PlayAnimation)
                {
                    Start(1);
                }
                else
                {
                    Invalidate();
                }
            }
        }

        private Color _ratingColor;
        public Color RatingColor
        {
            get { return _ratingColor; }
            set
            {
                _ratingColor = value;
                RatingPaint = new Paint
                {
                    Color = _ratingColor,
                    AntiAlias = true
                };
            }
        }

        private Color _nextRatingColor;
        public Color NextRatingColor
        {
            get { return _nextRatingColor; }
            set
            {
                _nextRatingColor = value;
                NextRatingPaint = new Paint
                {
                    Color = _nextRatingColor,
                    AntiAlias = true
                };
            }
        }

        public bool PlayAnimation { get; set; } = true;
        public Paint RatingPaint { get; set; }
        public Paint NextRatingPaint { get; set; }

        private void Init(Context context, IAttributeSet attributeSet)
        {
            SetBackgroundColor(Color.Transparent);
            _context = context;
            _eraserPaint = new Paint
            {
                Color = Color.Transparent,
                AntiAlias = true
            };
            _eraserPaint.SetXfermode(new PorterDuffXfermode(PorterDuff.Mode.Clear));
        }

        protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
        {
            if (w != oldw || h != oldh)
            {
                _bitmap = Bitmap.CreateBitmap(w, h, Bitmap.Config.Argb8888);
                _bitmap.EraseColor(Color.Transparent);
                _canvas = new Canvas(_bitmap);
            }
            base.OnSizeChanged(w, h, oldw, oldh);
            UpdateBounds();
        }

        private void UpdateBounds()
        {
            var logicalDensity = _context.Resources.DisplayMetrics.Density;
            Debug.WriteLine(logicalDensity);
            var thickness = (int)Math.Ceiling(16 * logicalDensity + .5f);
            _circleOuterBounds = new RectF(0, 0, Width, Height);

            _circleInnerBounds = new RectF(
                _circleOuterBounds.Left + 3f,
                _circleOuterBounds.Top + 3f,
                _circleOuterBounds.Right - 3f,
                _circleOuterBounds.Bottom - 3f);

            _circleCenterBounds = new RectF(
                _circleOuterBounds.Left + thickness + 5f,
                _circleOuterBounds.Top + thickness + 5f,
                _circleOuterBounds.Right - thickness - 5f,
                _circleOuterBounds.Bottom - thickness - 5f);

            Invalidate();
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            var spec = widthMeasureSpec;
            base.OnMeasure(spec, spec);
        }

        protected override void OnDraw(Canvas canvas)
        {
            _canvas.DrawColor(Color.Transparent, PorterDuff.Mode.Clear);
            var x = _circleOuterBounds.Width() / 2;
            var y = _circleOuterBounds.Height() / 2;
            var radius = _circleOuterBounds.Width() / 2;
            const float a = 270 * (float)Math.PI / 180;
            var l = 1.2f;

            //_canvas.DrawA

            _canvas.DrawOval(_circleInnerBounds, new Paint { Color = Color.Black, AntiAlias = true });
            _canvas.DrawOval(_circleInnerBounds.Left + 5f, _circleInnerBounds.Top + 5f, _circleInnerBounds.Right - 5f,
                _circleInnerBounds.Bottom - 5f, new Paint { Color = Color.White, AntiAlias = true });

            var sweepAngle = PlayAnimation ? _currentRating * 180 / 100 : Rating * 180 / 100;
            var sweepAngleNextRating = PlayAnimation ? _nextRatingCurrent * 180 / 100 : NextRating * 180 / 100;

            if (sweepAngle > 0f)
            {
                _canvas.DrawArc(_circleInnerBounds.Left + 5f, _circleInnerBounds.Top + 5f, _circleInnerBounds.Right - 5f,
                    _circleInnerBounds.Bottom - 5f, 0, sweepAngle, true,
                    new Paint { Color = RatingColor, AntiAlias = true });
            }

            if (sweepAngleNextRating > 0f)
            {
                _canvas.DrawArc(_circleInnerBounds.Left + 5f, _circleInnerBounds.Top + 5f, _circleInnerBounds.Right - 5f,
                    _circleInnerBounds.Bottom - 5f, 180, sweepAngleNextRating, true,
                    new Paint { Color = NextRatingColor, AntiAlias = true });
            }

            _canvas.DrawOval(_circleCenterBounds, new Paint { Color = Color.Black, AntiAlias = true });
            _canvas.DrawOval(_circleCenterBounds.Left + 5f, _circleCenterBounds.Top + 5f, _circleCenterBounds.Right - 5f,
                _circleCenterBounds.Bottom - 5f, new Paint { Color = Color.White, AntiAlias = true });

            // Draw arrow
            // Define starting point based on angle

            //_path.MoveTo(x + (float)Math.Cos(a) * radius, y + (float)Math.Sin(a) * radius);

            //_path.LineTo(x + (float)Math.Cos(a + 0.1) * radius * l, y + (float)Math.Sin(a + 0.1) * radius * l);

            //_path.LineTo(x + (float)Math.Cos(a - 0.1) * radius * l, y + (float)Math.Sin(a - 0.1) * radius * l);

            //_path.LineTo(x + (float)Math.Cos(a) * radius, y + (float)Math.Sin(a) * radius);

            //// Draw arrow on canvas
            //_canvas.DrawPath(_path, new Paint { Color = Color.Black, AntiAlias = true });

            canvas.DrawBitmap(_bitmap, 0, 0, null);
        }

        private void Start(long secs)
        {
            _ratingAnimator = ValueAnimator.OfFloat(_currentRating, Rating);
            _ratingAnimator.SetDuration(Java.Util.Concurrent.TimeUnit.Seconds.ToMillis(secs));
            _ratingAnimator.SetInterpolator(new AccelerateInterpolator());
            _ratingAnimator.Update += RatingAnimatorOnUpdate;
            _ratingAnimator.Start();

            _nextRatingAnimator = ValueAnimator.OfFloat(_nextRatingCurrent, NextRating);
            _nextRatingAnimator.SetDuration(Java.Util.Concurrent.TimeUnit.Seconds.ToMillis(secs));
            _nextRatingAnimator.SetInterpolator(new AccelerateInterpolator());
            _nextRatingAnimator.Update += NextRatingAnimatorOnUpdate;
            _nextRatingAnimator.Start();
        }

        private void RatingAnimatorOnUpdate(object sender, ValueAnimator.AnimatorUpdateEventArgs animatorUpdateEventArgs)
        {
            _currentRating = (float)animatorUpdateEventArgs.Animation.AnimatedValue;
            Invalidate();
        }

        private void NextRatingAnimatorOnUpdate(object sender, ValueAnimator.AnimatorUpdateEventArgs animatorUpdateEventArgs)
        {
            _nextRatingCurrent = (float)animatorUpdateEventArgs.Animation.AnimatedValue;
            Invalidate();
        }
    }
}