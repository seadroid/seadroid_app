﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;

namespace fifi.Droid.Controls
{
    /// <summary>
    /// Custom CardView class that intercepts touch events that would otherwise be passed to its
    /// children. Useful when one of the children is a MapView, which independently receives touch events
    /// to markers and info windows
    /// </summary>
    public class MapCardView : Android.Support.V7.Widget.CardView
    {
        public MapCardView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public MapCardView(Context context) : base(context)
        {
        }

        public MapCardView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public MapCardView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public override bool OnInterceptTouchEvent(MotionEvent ev)
        {
            return true;
        }
    }
}