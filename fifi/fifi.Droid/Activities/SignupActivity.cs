using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using fifi.Utils.Helpers;
using fifi.ViewModel;
using Java.IO;
using fifi.Droid;
using fifi.Client.Interfaces;
using fifi.Client.DataObjects;
using fifi.Droid.Adapters;
using fifi.Model;
using Console = System.Console;

namespace fifi.Droid.Activities
{
    [Activity(Label = "Sign up",
        Theme = "@style/MyThemeDark",
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class SignupActivity : BaseActivity, View.IOnTouchListener
    {
        private TextInputEditText _firstname;
        private TextInputEditText _lastname;
        private TextInputEditText _PhoneNumber;
        private TextInputEditText _email;
        private TextInputEditText _confirm_email;
        private TextInputEditText _password;
        private TextInputEditText _confirm_password;
        private Spinner _Phone_country;
        public static CreateUserViewModel _ViewModel;
        private JavaList<Flag> Flags;
        private numverifyApi _numverifyApi;
        private bool PhoneValidated;

        protected override int LayoutResource => Resource.Layout.activity_signup;

     
        

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            PhoneValidated = false;
            if ((int) Build.VERSION.SdkInt >= 21)
            {
                Window.SetStatusBarColor(new Color(ContextCompat.GetColor(this, Resource.Color.primary_dark)));
                Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
            }
           
            _ViewModel = new CreateUserViewModel();



            _email = FindViewById<TextInputEditText>(Resource.Id.signup_input_email);
            _password = FindViewById<TextInputEditText>(Resource.Id.signup_input_password);
            _firstname = FindViewById<TextInputEditText>(Resource.Id.signup_input_firstname);
            _lastname = FindViewById<TextInputEditText>(Resource.Id.signup_input_lastname);
            _confirm_email = FindViewById<TextInputEditText>(Resource.Id.signup_input_confirm_email);
            _confirm_password = FindViewById<TextInputEditText>(Resource.Id.signup_input_confirm_password);
            _PhoneNumber = FindViewById<TextInputEditText>(Resource.Id.signup_input_phone);
       
            _Phone_country = FindViewById<Spinner>(Resource.Id.signup_input_Country);


           
           

            _numverifyApi = new numverifyApi();
         

            _PhoneNumber.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _email.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _confirm_email.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _firstname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _lastname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);

           
            // adding Countries to the phone spi
             Flags = new JavaList<Flag>();
            Flags.Add(new Flag("DK", (Resource.Drawable.DK), "+45"));
            Flags.Add(new Flag("SE", (Resource.Drawable.SE), "+46"));
            Flags.Add(new Flag("NO", (Resource.Drawable.NO), "+47"));
            Flags.Add(new Flag("FI", (Resource.Drawable.FI), "+358"));
            Flags.Add(new Flag("DE", (Resource.Drawable.DE), "+49"));
            Flags.Add(new Flag("UK", (Resource.Drawable.UK), "+44"));
         




            var coountryAdapeter = new SpinnerPictureAdapter(this, Flags);           
            _Phone_country.Adapter = coountryAdapeter;
            
            
         

            _firstname.TextChanged += (sender, args) =>
            {
                ConfirmPassword();
                if (!ValidateFirstname()) return;
                if (!validateLastname()) return;
                if (PhoneValidated) return; ;
                if (!ValidateEmail()) return;
                if (!ConfirmEmail()) return;
                if (!ValidatePassword()) return;
                if (!ConfirmPassword()) return;
                _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
            };

         

            _lastname.TextChanged +=  (sender, args) =>
            {
                ConfirmPassword();
                if (!validateLastname()) return;
                if (!ValidateFirstname()) return;
                if (PhoneValidated) return; ;
                if (!ValidateEmail()) return;
                if (!ConfirmEmail()) return;
                if (!ValidatePassword()) return;
                if (!ConfirmPassword()) return;
                _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
            };

            _PhoneNumber.TextChanged += async (sender, args) =>
            {
                ConfirmPassword();
                if(!await ValidatePhoneNumber()) return;
                if (!validateLastname()) return;
                if (!ValidateFirstname()) return;
                if (!ValidateEmail()) return;
                if (!ConfirmEmail()) return;
                if (!ValidatePassword()) return;
                if (!ConfirmPassword()) return;
                _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
            };
            _Phone_country.ItemSelected += async (sender, args) =>
            {
                ConfirmPassword();
                if (!await ValidatePhoneNumber()) return;
                if (!validateLastname()) return;
                if (!ValidateFirstname()) return;
                if (!ValidateEmail()) return;
                if (!ConfirmEmail()) return;
                if (!ValidatePassword()) return;
                if (!ConfirmPassword()) return;
                _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);

            };



            _email.TextChanged +=  (sender, args) =>
            {
                ConfirmPassword();
                if (!ValidateEmail()) return;
                if (!ValidateFirstname()) return;
                if (!validateLastname()) return;
                if (PhoneValidated) return; ;
                if (!ConfirmEmail()) return;
                if (!ValidatePassword()) return;
                if (!ConfirmPassword()) return;
                _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
            };

            _confirm_email.TextChanged +=  (sender, args) =>
            {
                ConfirmPassword();
                if (!ConfirmEmail()) return;
                if (!ValidateFirstname()) return;
                if (!validateLastname()) return;
                if (!PhoneValidated) return;
                if (!ValidateEmail()) return;
                if (!ValidatePassword()) return;
                if (!ConfirmPassword()) return;
                _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
            };


            _password.TextChanged +=  (sender, args) =>
            {            
                ConfirmPassword();
                if (!ValidatePassword()) return;
                if (!ValidateFirstname()) return;
                if (!validateLastname()) return;
                if (!PhoneValidated) return; ;
                if (!ValidateEmail()) return;
                if (!ConfirmEmail()) return;
                if (!ConfirmPassword()) return;
                _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
            };

            _confirm_password.TextChanged +=  (sender, args) =>
            {
                try
                {
                    if (!ConfirmPassword()) return;
                    if (!ValidateFirstname()) return;
                    if (!validateLastname()) return;
                    if (!PhoneValidated) return;
                    if (!ValidateEmail()) return;
                    if (!ConfirmEmail()) return;
                    if (!ValidatePassword()) return;
                    _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
                    _confirm_password.SetOnTouchListener(this);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Confirmpassword textchanged "+e);
                }

              
            };
        }

        public override void OnWindowFocusChanged(bool hasFocus)
        {
            base.OnWindowFocusChanged(hasFocus);
            _Phone_country.DropDownWidth = _Phone_country.Width;          
        }

        private bool ValidateFirstname()

        {
            try
            {
                if (!string.IsNullOrWhiteSpace(_firstname.Text) && _firstname.Text.Length > 1 && _firstname.Text.Length < 60)
                {
                    _firstname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    return true;
                }
                _firstname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("validateFirstname" + e);
                throw;
            }
           

        }

        private bool validateLastname()

        {
            try
            {
                if (!string.IsNullOrWhiteSpace(_lastname.Text) && _lastname.Text.Length > 0 && _lastname.Text.Length < 60)
                {
                    _lastname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    return true;
                }
                _lastname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("validateLastname "+e);
                throw;
            }
           
        }


        private bool ValidateEmail()
        {
            try
            {
                if (Android.Util.Patterns.EmailAddress.Matcher(_email.Text).Matches())
                {
                    _email.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    return true;
                }
                _email.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
                _confirm_email.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("validateEmail " + e);
                throw;
            }
           
        }

        private bool ConfirmEmail()
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(_email.Text) && _email.Text == _confirm_email.Text)
                {
                    _confirm_email.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    return true;
                }
                _confirm_email.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("confirmEmail" + e);
                throw;
            }
           
        }

        private async Task<bool> ValidatePhoneNumber()
        {
        
           
            try
            {
                if (_PhoneNumber.Text.Length > 4)
                {
                    var SelectedPhoneFormat = Flags[_Phone_country.SelectedItemPosition].Countrycode;
                    VerifiedPhoneNumber verifiedPhoneNumber = await _numverifyApi.VerifyNumber(SelectedPhoneFormat + _PhoneNumber.Text);
                 
                    if (verifiedPhoneNumber.valid)
                    {
                        _PhoneNumber.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);                 
                        PhoneValidated = true;
                        return true;
                    }
                }

              
                _PhoneNumber.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
                PhoneValidated = false;
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("validate Phonenumber " + e);
                throw;
            }
           
           
        }


        private bool ValidatePassword()
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(_password.Text) && _password.Text.Length >= 6)
                {
                    _password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    return true;
                }

                _password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("validatePassword "+ e);
                throw;
            }
           

        }

        private bool ConfirmPassword()
        {
            try
            {
                if (_password.Text == _confirm_password.Text && _password.Text.Length >= 6)
                {
                    _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    return true;
                }
                _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("confirmationPassword " +e);
                throw; 
            }
           
        }


        public async Task<bool> CreateUser()
        {
            _ViewModel.Firstname = _firstname.Text;
            _ViewModel.Lastname = _lastname.Text;
            _ViewModel.Password = _password.Text;
            _ViewModel.Email = _email.Text;
            _ViewModel.PhoneNumber = _PhoneNumber.Text;
            _ViewModel.Country_id = _Phone_country.SelectedItemPosition + 1;
            
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            var result = await _ViewModel.ExecuteCreateUserAsync();
            if (result)
            {            
                alert.SetTitle("Account was created");
                alert.SetMessage( "To Activate your account please use one of the following option on the follwing screen");
                alert.SetPositiveButton("OK", (senderAlert, a) =>
                {
                    var intent = new Intent(this, typeof(AccountPairingActivity));
                    StartActivity(intent);
                });
                alert.Show();
            }

            return result;
           
        }

        public override void OnBackPressed()
        {
            //  base.OnBackPressed();
            var _intent = new Intent(this, typeof(LoginActivity));
            _intent.AddFlags(ActivityFlags.ClearTop);
            StartActivity(_intent);
            Finish();
        }

        public  bool OnTouch(View v, MotionEvent e)
        {
            const int drawableRight = 2;

            if (!ConfirmPassword()) return false;
            if (!ValidateFirstname()) return false;
            if (!validateLastname()) return false;
            if (!ValidateEmail()) return false;
            if (!ConfirmEmail()) return false;
            if (!ValidatePassword()) return false;
            if (e.Action != MotionEventActions.Up)
            {
                _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
                return false;
            }
            if (!(e.RawX >= _password.Right - _password.GetCompoundDrawables()[drawableRight].Bounds.Width()))
            {
                _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
                return false;
            }
           
            SmsVerificationActivity.phonenumber = Flags[_Phone_country.SelectedItemPosition].Countrycode + _PhoneNumber.Text;
            var intent = new Intent(this, typeof(SmsVerificationActivity));          
            base.StartActivityForResult(intent, 1);


            _confirm_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
            return true;
        }

        protected override async void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {

            base.OnActivityResult(requestCode, resultCode, data);

            try
            {
                if (resultCode == Result.Ok)
                {
                    CreateUser();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


        }
      

    }
}