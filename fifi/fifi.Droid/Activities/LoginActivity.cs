using System;
using System.ComponentModel;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Media.Audiofx;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.Client.Interfaces;
using fifi.Droid.Helpers;
using fifi.Utils.Helpers;
using fifi.ViewModel;

namespace fifi.Droid.Activities
{
    [Activity(Label = "Login",
        Theme = "@style/MyTheme",
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class LoginActivity : BaseActivity, View.IOnTouchListener
    {
        /*
                private static readonly string Tag = "LoginActivity";
        */
        private const int RequestSignup = 0;

        private ImageUrlDownloader ImageDownloader;
        private TextInputEditText _email;
        private TextInputEditText _password;
        private TextView _signup;
        private Intent _intent;
        private bool isverifying;
        /// <summary>
        /// Field used to hook up to the corresponding ViewModel
        /// </summary>
        private LoginViewModel _viewModel;

        protected override int LayoutResource => Resource.Layout.activity_login;

        /// <summary>
        /// Define whether the password has changed???
        /// </summary>
        public bool HasPasswordChanged { get; set; }


    
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            isverifying = false;
            if ((int)Build.VERSION.SdkInt >= 21)
            {
                Window.SetStatusBarColor(new Color(ContextCompat.GetColor(this, Resource.Color.primary_dark)));
                Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
            }
            _intent = null;
            _email = FindViewById<TextInputEditText>(Resource.Id.input_email);
            _password = FindViewById<TextInputEditText>(Resource.Id.input_password);
            _signup = FindViewById<TextView>(Resource.Id.link_signup);


            // Test Code
            _email.Text = "test@test.dk";
            _password.Text = "test123";
            //

            _email.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);

            _viewModel = new LoginViewModel();
            _viewModel.PropertyChanged += ViewModel_PropertyChanged;
            ImageDownloader = new ImageUrlDownloader();
            _password.SetOnTouchListener(this);
            _email.TextChanged += (sender, e) =>
            {
                _viewModel.Email = ((TextInputEditText)sender).Text;
                if (!ValidateEmail()) return;
                if (!HasPasswordChanged) return;
                if (ValidatePassword())
                {
                    // Enable continue compound drawable
                    _password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
                }
            };

            _password.TextChanged += (sender, e) =>
            {
                _viewModel.Password = ((TextInputEditText)sender).Text;
                HasPasswordChanged = true;
                if (!ValidatePassword()) return;
                if (ValidateEmail())
                {
                    _password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.continue_selector, 0);
                }
            };

            _signup.Click += (sender, e) =>
            {          
                var intent = new Intent(ApplicationContext, typeof(SignupActivity));              
                StartActivityForResult(intent, RequestSignup);
                Finish();
            };
        }

        protected override void OnStart()
        {
            base.OnStart();
              Intent intent = null;
        }

        private async  void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {

            

            if (!_viewModel.IsLoggedIn)
            {
                return;
            }

            if (isverifying == false)
            {
                isverifying = true;
                await _viewModel.ExecuteVerifUserAsync();
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                if (!_viewModel.IsVerified)
                {
                    if (_viewModel.MasterMissingBoatProfile)
                    {
                        alert.SetTitle("Master is missing vessel info");
                        alert.SetMessage("Please finish activating your master unit by filling out the information about the vessel on the next page");
                         
                        alert.SetPositiveButton("OK", (senderAlert, a) =>
                        {
                            var AccountParingIntent = new Intent(this, typeof(CreateBoatActivity));
                            AccountParingIntent.PutExtra("master_id", _viewModel.UserProfile.master[0].Id);
                            StartActivity(AccountParingIntent);
                            isverifying = false;
                            Finish();
                        });

                        alert.SetNegativeButton("Cancel", (senderAlert, a) =>
                        {
                            return;
                        });

                        alert.Show();
                    }
                    else
                    {
                        alert.SetTitle("User is not Activtated");
                        alert.SetMessage("Please Activate the account with one of the following options on the next screen");

                        alert.SetPositiveButton("OK", (senderAlert, a) =>
                        {
                            var AccountParingIntent = new Intent(this, typeof(AccountPairingActivity));
                            StartActivity(AccountParingIntent);
                            isverifying = false;
                            Finish();
                        });

                        alert.SetNegativeButton("Cancel", (senderAlert, a) =>
                        {
                            return;
                        });

                        alert.Show();
                    }
                    
                  
                }
                else
                {

                    if (_intent == null)
                    {
                        if (!string.IsNullOrEmpty(Settings.Current.UserImageUrl))
                        {
                            Settings.Current.UserImageBase64 = ImageDownloader.GetImageBitmapFromUrl(Settings.Current.UserImageUrl);
                        }
                        else
                        {
                            Settings.Current.UserImageBase64 = string.Empty;
                        }
                        FinishActivity(1);
                        _intent = new Intent(this, typeof(DashboardActivity));
                        _intent.AddFlags(ActivityFlags.ClearTop);
                        StartActivity(_intent);
                        Finish();
                        isverifying = false;
                    }
                }
            }
            

            
               
           
          
        }

        private void Login()
        {
            // Perform login from viewModel
            _viewModel.LoginCommand.Execute(null);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (requestCode != RequestSignup) return;
            if (resultCode == Result.Ok)
            {
                Toast.MakeText(ApplicationContext, "Bruger oprettet", ToastLength.Short).Show();
                //Finish();
            }
        }

        private bool ValidateEmail()
        {
            if (Android.Util.Patterns.EmailAddress.Matcher(_email.Text).Matches())
            {
                _email.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                return true;
            }
            _email.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            return false;
        }

        private bool ValidatePassword()
        {
            if (!string.IsNullOrWhiteSpace(_password.Text))
            {
                _password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                return true;
            }
            //_password.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            var d = ContextCompat.GetDrawable(this, Resource.Drawable.do_not_match);
            d.SetBounds(0, 0, d.IntrinsicWidth, d.IntrinsicHeight);
            _password.SetError("enter a password", d);
            return false;
        }

        public bool OnTouch(View v, MotionEvent e)
        {
            const int drawableRight = 2;

            if (e.Action != MotionEventActions.Up) return false;
            if (_password.GetCompoundDrawables()[drawableRight] == null) return false;
            if (!(e.RawX >= _password.Right - _password.GetCompoundDrawables()[drawableRight].Bounds.Width()))
                return false;
            Login();
            return true;
        }
    }
}