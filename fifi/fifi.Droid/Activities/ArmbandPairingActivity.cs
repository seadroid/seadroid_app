﻿using System.IO;
using System.Xml.Serialization;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using fifi.Client.DataObjects;
using fifi.Droid.Adapters;
using fifi.Model;
using fifi.ViewModel;

namespace fifi.Droid.Activities
{
    [Activity(Label = "Armband Pairing", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class ArmbandPairingActivity : BaseActivity
    {
        private LinearLayoutManager _layoutManager;
        private PairingViewModel _viewmodel;
        private RecyclerView _recyclerView;
        private PairingAdapter _adapter;
        private SwipeRefreshLayout _refresher;
        private BluetoothDevice bluetoothDevice;

        protected override int LayoutResource => Resource.Layout.activity_armband_pairing;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            if ((int) Build.VERSION.SdkInt >= 21)
            {
                Window.SetStatusBarColor(new Color(ContextCompat.GetColor(this, Resource.Color.primary_dark)));
                Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
            }

            _viewmodel = new PairingViewModel();
            _adapter = new PairingAdapter(this, _viewmodel);
            _recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            _refresher = FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
           // _refresher.Refresh += (sender, args) => _viewmodel.RefreshBluetoothcommand.Execute(null);
            _adapter.ItemClick += OnItemClick;
            _recyclerView.SetAdapter(_adapter);
            _layoutManager = new LinearLayoutManager(this) {Orientation = LinearLayoutManager.Vertical};
            _recyclerView.SetLayoutManager(_layoutManager);

        }

        protected override async void OnStart()
        {
            base.OnStart();
            _viewmodel.PropertyChanged += ViewModel_PropertyChanged;
            if (_viewmodel.FoundDevices.Count == 0)
            {
                Snackbar snackbar = Snackbar.Make(_recyclerView, "Searching for Devices.....", Snackbar.LengthIndefinite);
                snackbar.Show();
                await _viewmodel.ExecuteLoadBluetoothCommandAsync();              
                snackbar.Dismiss();
                snackbar.Dispose();
            }
        }


        protected override void OnStop()
        {
            base.OnStop();
            _viewmodel.StopBluetoothCommand.Execute(null);
        }

        protected override void OnPause()
        {
            base.OnPause();
            _viewmodel.StopBluetoothCommand.Execute(null);
        }



        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(_viewmodel.IsBusy):
                    _refresher.Refreshing = _viewmodel.IsBusy;
                    break;

            }
        }


        private async void OnItemClick(object sender, PairingAdapter.PairingDeviceClickEventArgs args)
        {
            if (_viewmodel.IsBusy == true)
            {
                return;
            }
            _viewmodel.IsBusy = true;
            bluetoothDevice = _viewmodel.Bluetoothdevices[args.Position];
            var user_id = 0;
            var result = await _viewmodel.ExecutePingBluetoothdeviceCommandAsync(bluetoothDevice.Device);
            Context contex = this;
            AlertDialog.Builder alert = new AlertDialog.Builder(contex);
            var wristbandfound = false;

            if (result == true)
            {
                var DetailsViewModel = new PairingDetailsViewModel();
                DetailsViewModel.BluetoothDeviceId = bluetoothDevice.Device.Name;

                foreach (var wristband in _viewmodel.Armbands.data)
                {
                    if (bluetoothDevice.Device.Name == wristband.serial)
                    {
                        DetailsViewModel.Wristband = wristband;
                        wristbandfound = true;
                        user_id = wristband.user_id;
                    }
                }

                if (wristbandfound == false)
                {
                    DetailsViewModel.Wristband = new Armband();
                }




                PairingDetailsActivity._viewModel = DetailsViewModel;
                if (user_id == 0)
                {
                    alert.SetTitle("create profile");
                    alert.SetMessage(
                        "Do you whant to create a profile for this Armband? Creating a profile will bind this Armband to you account");

                    alert.SetPositiveButton("Yes", (senderAlert, a) =>
                    {

                        var intent = new Intent(this, typeof(PairingDetailsActivity));
                        intent.PutExtra("Id", bluetoothDevice.Device.Name);
                        _viewmodel.StopBluetoothCommand.Execute(null);
                        base.StartActivityForResult(intent, 1);
                    });

                    alert.SetNegativeButton("Cancel", (senderAlert, a) =>
                    {

                    });
                }

                if (user_id == _viewmodel.UserProfile.Id)
                {
                    alert.SetTitle("Edit profile");
                    alert.SetMessage("Do you whant to Edit the profile for this Armband?");
                    alert.SetPositiveButton("Yes", (senderAlert, a) =>
                    {

                        var intent = new Intent(this, typeof(PairingDetailsActivity));
                        intent.PutExtra("Id", bluetoothDevice.Device.Name);
                        _viewmodel.StopBluetoothCommand.Execute(null);

                        base.StartActivityForResult(intent, 1);
                    });

                    alert.SetNegativeButton("Cancel", (senderAlert, a) =>
                    {

                    });


                    if (user_id != 0 && user_id != _viewmodel.UserProfile.Id)
                    {
                        alert.SetTitle("insufficient permission");
                        alert.SetMessage("Cant edit selected Armband, the Armband belongs to someone else");
                        alert.SetPositiveButton("ok", (senderAlert, a) =>
                        {

                        });
                    }


                   
                }

                Dialog dialog = alert.Create();
                dialog.Show();
                _viewmodel.IsBusy = false;



            }
            else
            {
                alert.SetTitle("Connection failed");
                alert.SetMessage("Could not connect to Armband, please try again");
                alert.SetPositiveButton("OK", (senderAlert, a) =>
                {

                });
                Dialog dialog = alert.Create();
                dialog.Show();
                _viewmodel.IsBusy = false;
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                _viewmodel.StopBluetoothCommand.Execute(null);
                Finish();
            }

            return base.OnOptionsItemSelected(item);
        }

        protected override async void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {

            if (resultCode == Result.Ok)
            {
                Armband wristband = new Armband();
                XmlSerializer deserializer = new XmlSerializer(typeof(Armband));
                using (TextReader tr = new StringReader(data.GetStringExtra("Armband")))
                {
                    wristband = (Armband) deserializer.Deserialize(tr);
                }
                _viewmodel.wristband = wristband;
                await _viewmodel.ExecuteUpdateWristbandCommandAsync();
                SetResult(Result.Ok, data);
                Finish();
            }
        }
    }
}