using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Transitions;
using Mapbox;
using Mapbox.Telemetry;
using Plugin.Permissions;

namespace fifi.Droid.Activities
{
    public abstract class BaseActivity : AppCompatActivity
    {
        public Toolbar Toolbar { get; set; }
        protected abstract int LayoutResource { get; }

        protected int ActionBarIcon
        {
            set { Toolbar.SetNavigationIcon(value); }
        }

        protected override void OnCreate(Bundle bundle)
        {
            InitActivityTransiotions();
            base.OnCreate(bundle);
            SetContentView(LayoutResource);
            Toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (Toolbar == null) return;
            SetSupportActionBar(Toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void InitActivityTransiotions()
        {
            if ((int)Build.VERSION.SdkInt < 21) return;
            var transition = new Slide();
            transition.ExcludeTarget(Android.Resource.Id.StatusBarBackground, true);
            Window.EnterTransition = transition;
            Window.ReturnTransition = transition;
            Window.RequestFeature(Android.Views.WindowFeatures.ContentTransitions);
            Window.RequestFeature(Android.Views.WindowFeatures.ActivityTransitions);
            Window.SharedElementEnterTransition = new ChangeBounds();
            Window.SharedElementReturnTransition = new ChangeBounds();
            Window.AllowEnterTransitionOverlap = true;
            Window.AllowReturnTransitionOverlap = true;
        }

        public void CropImage(Android.Net.Uri Uri)
        {
            var CropIntent = new Intent("com.android.camera.action.CROP");
            CropIntent.SetDataAndType(Uri, "image/*");
            CropIntent.PutExtra("crop", "true");
            CropIntent.PutExtra("outputX", 500);
            CropIntent.PutExtra("outputY", 500);
            CropIntent.PutExtra("aspectY", 1);
            CropIntent.PutExtra("aspectX", 1);
            CropIntent.PutExtra("ScaleUpIfNeeded", true);
            CropIntent.PutExtra("return-data", true);
            StartActivityForResult(CropIntent, 1);
        }

        public bool CheckIfNumber(string text)
        {
            foreach (char c in text)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }
    }
}