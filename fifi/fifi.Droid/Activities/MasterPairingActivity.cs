﻿using System;
using System.ComponentModel;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Views;
using fifi.ViewModel;
using Java.IO;

namespace fifi.Droid.Activities
{

    [Activity(Label = "Master pairing",  Theme = "@style/MyTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,  ScreenOrientation = ScreenOrientation.Portrait)]
    public class MasterPairingActivity : BaseActivity, View.IOnTouchListener
    {
        private MasterPairingViewModel _viewModel;
        private TextInputEditText _MasterSerialTextInput;
        private bool busy;
        private bool StartedByMastersFragment;
        protected override int LayoutResource => Resource.Layout.activity_master_pairing;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            busy = false;
            _viewModel = new MasterPairingViewModel();

            StartedByMastersFragment = Intent.GetBooleanExtra("StartedByMastersFragment", false);

           

            if ((int)Build.VERSION.SdkInt >= 21)
            {
                Window.SetStatusBarColor(new Color(ContextCompat.GetColor(this, Resource.Color.primary_dark)));
                Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
            }

            _MasterSerialTextInput = FindViewById<TextInputEditText>(Resource.Id.master_pairing_serial_textinput);
            _MasterSerialTextInput.SetCompoundDrawablesWithIntrinsicBounds(0,0, Resource.Drawable.do_not_match,0);
            _MasterSerialTextInput.SetOnTouchListener(this);
  
            _MasterSerialTextInput.TextChanged += (sender, args) =>
            {
                if(VerifyMasterSerial()) return;;
               
            };

            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
        }

        private void ViewModelOnPropertyChanged(object o, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (StartedByMastersFragment == true)
            {
                var intent1 = new Intent(this, typeof(CreateBoatActivity));
                intent1.PutExtra("MasterSerial", _MasterSerialTextInput.Text);
                intent1.PutExtra("StartedByMastersFragment", true);
                StartActivityForResult(intent1, 1);
                busy = false;
                return;
            }
            var intent = new Intent(this, typeof(CreateBoatActivity));
            intent.PutExtra("MasterSerial", _MasterSerialTextInput.Text);
            StartActivity(intent);
            busy = false;
        }

        private bool VerifyMasterSerial()
        {
            if (_MasterSerialTextInput.Text.Length > 0)
            {
                _MasterSerialTextInput.SetCompoundDrawablesWithIntrinsicBounds(0,0, Resource.Drawable.continue_selector,0);
               
                return true;
            }
            _MasterSerialTextInput.SetCompoundDrawablesWithIntrinsicBounds(0,0, Resource.Drawable.do_not_match, 0);
            return false;
        }

        public bool OnTouch(View v, MotionEvent e)
        {
            const int drawableRight = 2;
            if (!VerifyMasterSerial()) return false;
            if (e.Action != MotionEventActions.Up) return false;
            if (_MasterSerialTextInput.GetCompoundDrawables()[drawableRight] == null) return false;
            if (!(e.RawX >= _MasterSerialTextInput.Right - _MasterSerialTextInput.GetCompoundDrawables()[drawableRight].Bounds.Width()))
                return false;
            _viewModel.Serial = _MasterSerialTextInput.Text;
            _viewModel.PairMasterCommand.Execute(null);
            return true;
           

        }

        protected override async void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {

            base.OnActivityResult(requestCode, resultCode, data);
             Finish();
            


        }


        public override void OnBackPressed() 
        {
            //  base.OnBackPressed();
            if (StartedByMastersFragment)
            {
                base.OnBackPressed();
                return;
            }

            var _intent = new Intent(this, typeof(AccountPairingActivity));
            _intent.AddFlags(ActivityFlags.ClearTop);
            StartActivity(_intent);
            Finish();
        }
    }



}