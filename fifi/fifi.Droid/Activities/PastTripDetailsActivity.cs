﻿using System;
using System.Globalization;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using fifi.Client.DataObjects;
using fifi.Helpers;
using fifi.ViewModel;
using Mapbox.Annotations;
using Mapbox.Camera;
using Mapbox.Geometry;
using Mapbox.Maps;

namespace fifi.Droid.Activities
{
    [Activity(Label = "_trip Details", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class PastTripDetailsActivity : BaseActivity, IOnMapReadyCallback
    {
        private Marker _boatMarker;
        private TextView _altitude, _speed, _speedUnits, _windSpeed, _windSpeedUnits, _windHeading, _windHeadingUnits;
        private string _id;

        private MapboxMap _map;
        private SupportMapFragment _mapFrag;
        private SeekBar _seekBar;
        private TextView _startTime, _endTime;

        private PastTripDetailsViewModel _viewModel;

        protected override int LayoutResource => Resource.Layout.activity_past_trip_details;

        public void OnMapReady(MapboxMap map)
        {
            _map = map;
            _map.StyleUrl = "mapbox://styles/otbardram/cirol0jn3001lgxm79yh61kzu";
            _startTime.Text = _viewModel.Trip.StartedAt;
            _endTime.Text = !string.IsNullOrWhiteSpace(_viewModel.Trip.EndedAt) ? _viewModel.Trip.EndedAt : "Not Ended";
            SupportActionBar.Title = _viewModel.Title;
            SetupMap();
            UpdateStats();
        }

        public static Trip _trip { get; set; }

      

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            if ((int)Build.VERSION.SdkInt >= 21)
            {
                Window.SetStatusBarColor(new Color(ContextCompat.GetColor(this, Resource.Color.primary_dark)));
                Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
            }

            _viewModel = new PastTripDetailsViewModel
            {
                Title = _id = Intent.GetStringExtra("Id"),
                Trip = _trip
            };

            _seekBar = FindViewById<SeekBar>(Resource.Id.trip_progress);
            _seekBar.Enabled = false;

            _startTime = FindViewById<TextView>(Resource.Id.text_start_time);
            _endTime = FindViewById<TextView>(Resource.Id.text_end_time);
            _startTime.Text = _endTime.Text = string.Empty;

            _altitude = FindViewById<TextView>(Resource.Id.text_altitude);
            _speed = FindViewById<TextView>(Resource.Id.text_speed);
            _speedUnits = FindViewById<TextView>(Resource.Id.text_speed_units);
            _windSpeed = FindViewById<TextView>(Resource.Id.text_wind_speed);
            _windSpeedUnits = FindViewById<TextView>(Resource.Id.text_wind_speed_units);
            _windHeading = FindViewById<TextView>(Resource.Id.text_wind_direction);
            _windHeadingUnits = FindViewById<TextView>(Resource.Id.text_wind_direction_units);

            _mapFrag = (SupportMapFragment)SupportFragmentManager.FindFragmentById(Resource.Id.map);
            _mapFrag.GetMapAsync(this);
        }

        private void UpdateStats()
        {
            _altitude.Text = _viewModel.Altitude;
            _speed.Text = _viewModel.Speed;
            _speedUnits.Text = _viewModel.SpeedUnits;
            _windSpeed.Text = _viewModel.WindSpeed;
            _windSpeedUnits.Text = _viewModel.WindSpeedUnits;
            _windHeading.Text = _viewModel.WindHeading;
            _windHeadingUnits.Text = _viewModel.WindHeadingUnits;
        }

        private void SetupMap()
        {
            if (_mapFrag.View.Width == 0)
            {
                _mapFrag.View.PostDelayed(SetupMap, 500);
                return;
            }

            if (_viewModel.Trip?.Coordinates == null || _viewModel.Trip.Coordinates.Count <= 0)
            {
                return;
            }

           
            _trip.Coordinates = _trip.Coordinates.OrderBy(x => Convert.ToDateTime(x.recorded_at)).ThenBy(x => Convert.ToDateTime(x.recorded_at)).ToList();


            var start =
                _trip.Coordinates.FirstOrDefault(
                    c =>
                        c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
                        c.Longitude.Where(char.IsDigit).Any(x => x != '0'));

            var end =
                _trip.Coordinates.LastOrDefault(
                    c =>
                        c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
                        c.Longitude.Where(char.IsDigit).Any(x => x != '0'));

            if (start == null || end == null)
            {
                Finish();
                return;
            }

            _seekBar.Max =
                _trip.Coordinates.Count(
                    c =>
                        c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
                        c.Longitude.Where(char.IsDigit).Any(x => x != '0')) - 1;
            _seekBar.ProgressChanged += SeekBar_ProgressChanged;

            var logicalDensity = Resources.DisplayMetrics.Density;
            var thicknessBoat = (int)Math.Ceiling(26 * logicalDensity + 0.5f);
            var thicknessPoints = (int)Math.Ceiling(20 * logicalDensity + 0.5f);

            var iconFactory = IconFactory.GetInstance(this);
            var b = ContextCompat.GetDrawable(this, Resource.Drawable.logo) as BitmapDrawable;
            var finalIcon =
                iconFactory.FromBitmap(Bitmap.CreateScaledBitmap(b?.Bitmap, thicknessBoat, thicknessBoat, false));

            var boat = new MarkerOptions();
            boat.SetPosition(new LatLng(double.Parse(start.Latitude.Replace(".", ",")),
                double.Parse(start.Longitude.Replace(".", ","))));
            boat.SetIcon(finalIcon);

            b = ContextCompat.GetDrawable(this, Resource.Drawable.ic_start_point) as BitmapDrawable;
            finalIcon =
                iconFactory.FromBitmap(Bitmap.CreateScaledBitmap(b?.Bitmap, thicknessPoints, thicknessPoints, false));

            var startMarker = new MarkerOptions();
            startMarker.SetPosition(new LatLng(double.Parse(start.Latitude.Replace(".", ",")),
                double.Parse(start.Longitude.Replace(".", ","))));
            startMarker.SetIcon(finalIcon);

            b = ContextCompat.GetDrawable(this, Resource.Drawable.ic_end_point) as BitmapDrawable;
            finalIcon =
                iconFactory.FromBitmap(Bitmap.CreateScaledBitmap(b?.Bitmap, thicknessPoints, thicknessPoints, false));

            var endMarker = new MarkerOptions();
            endMarker.SetPosition(new LatLng(double.Parse(end.Latitude.Replace(".", ",")),
                double.Parse(end.Longitude.Replace(".", ","))));
            endMarker.SetIcon(finalIcon);

            var points =
                _trip.Coordinates.Where(
                        c =>
                            c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
                            c.Longitude.Where(char.IsDigit).Any(x => x != '0'))
                    .Select(
                        s =>
                            new LatLng(double.Parse(s.Latitude.Replace(".", ",")),
                                double.Parse(s.Longitude.Replace(".", ","))))
                    .ToArray();
            var rectOptions = new PolylineOptions();
            rectOptions.Add(points);
            rectOptions.SetColor(ContextCompat.GetColor(this, Resource.Color.primary_dark));
            _map.AddPolyline(rectOptions);

            _boatMarker = _map.AddMarker(boat);

            _map.AddMarker(startMarker);
            _map.AddMarker(endMarker);

            if (points.Length <= 0)
            {
                return;
            }

            var boundsPoints = new LatLngBounds.Builder();
            foreach (var point in points)
            {
                boundsPoints.Include(point);
            }

            var bounds = boundsPoints.Build();
            _map.MoveCamera(CameraUpdateFactory.NewLatLngBounds(bounds, 64));

            _map.MoveCamera(CameraUpdateFactory.NewLatLng(_boatMarker.Position));

            _seekBar.Enabled = true;
        }

        private void SeekBar_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            if (_boatMarker == null)
            {
                return;
            }

            _viewModel.CurrentPosition =
                _trip.Coordinates.Where(
                    c =>
                        c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
                        c.Longitude.Where(char.IsDigit).Any(x => x != '0')).ToList()[e.Progress];

            RunOnUiThread(() =>
            {
                UpdateStats();
                _boatMarker.Position = new LatLng(double.Parse(_viewModel.CurrentPosition.Latitude.Replace(".", ",")),
                    double.Parse(_viewModel.CurrentPosition.Longitude.Replace(".", ",")));
                _map.MoveCamera(CameraUpdateFactory.NewLatLng(_boatMarker.Position));
            });
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}