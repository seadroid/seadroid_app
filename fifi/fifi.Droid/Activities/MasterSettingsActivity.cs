﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net.Mime;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Provider;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using fifi.Droid.Helpers;
using fifi.ViewModel;

namespace fifi.Droid.Activities
{
    [Activity(Label = "Master Settings", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MasterSettingsActivity : BaseActivity
    {

        #region Parameters
        protected override int LayoutResource => Resource.Layout.activity_master_settings;

        public static MasterSettingsViewModel _viewModel;
        private Button _saveButton;
        private TextInputEditText _boatname;
        private TextInputEditText _registryNumber;
        private TextInputEditText _harbor;
        private TextInputEditText _color;
        private Spinner _country;
        private ImageView _countryVerifyImage;
        private ImageView _boatImage;

        #endregion

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            if ((int)Build.VERSION.SdkInt >= 21)
            {
                Window.SetStatusBarColor(new Color(ContextCompat.GetColor(this, Resource.Color.primary_dark)));
                Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
            }

            #region InfoFieldsInit
            _boatname = FindViewById<TextInputEditText>(Resource.Id.master_settings_boat_boatname);
            _registryNumber = FindViewById<TextInputEditText>(Resource.Id.master_settings_boat_registry_number);
            _harbor = FindViewById<TextInputEditText>(Resource.Id.master_settings_boat_harbor);
            _color = FindViewById<TextInputEditText>(Resource.Id.master_settings_boat_color);

            _boatname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _registryNumber.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _harbor.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _color.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);

            _country = FindViewById<Spinner>(Resource.Id.master_settings_boat_country);

            _countryVerifyImage = FindViewById<ImageView>(Resource.Id.master_settings_country_image);
            _countryVerifyImage.SetImageDrawable(GetDrawable(Resource.Drawable.success));

            var CountryAdapter = ArrayAdapter.CreateFromResource(this, Resource.Array.Country_array,
                Android.Resource.Layout.SimpleSpinnerItem);
            CountryAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            _country.Adapter = CountryAdapter;

            _boatname.TextChanged += (sender, args) =>
            {
                ValidateInputsTextField(_boatname);
            };
            _registryNumber.TextChanged += (sender, args) =>
            {
                ValidateInputsTextField(_registryNumber);
            };
            _harbor.TextChanged += (sender, args) =>
            {
                ValidateInputsTextField(_harbor);
            };
            _color.TextChanged += (sender, args) =>
            {
                ValidateInputsTextField(_color);
            };
            _country.ItemSelected += (sender, args) =>
            {
                ValidateSpinner(_country, _countryVerifyImage);
            };

            #endregion

            #region boatImageInit
            _boatImage = FindViewById<ImageView>(Resource.Id.master_settings_boat_image);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            _boatImage.Click += (sender, args) =>
            {
                string[] items = new string[] { "Camera", "Gallery", "cancel" };
                builder.SetItems(items, (o, eventArgs) =>
                {
                    switch (eventArgs.Which)
                    {
                        case 0:
                            Intent intent = new Intent(MediaStore.ActionImageCapture);
                            StartActivityForResult(intent, 2);
                            break;
                        case 1:
                            var imageIntent = new Intent();
                            imageIntent.SetType("image/*");
                            imageIntent.SetAction(Intent.ActionGetContent);
                            StartActivityForResult(Intent.CreateChooser(imageIntent, "Select photo"), 0);
                            break;
                        case 2:
                            break;

                    }
                });
                builder.Show();
            };
            builder.Show();

            #endregion


            #region SaveButtonInit
            _saveButton = FindViewById<Button>(Resource.Id.update_boat_edit_button);
            _saveButton.Click += SaveButtonOnClick;
            _saveButton.Text = "Missing Information";
            #endregion

            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;

            _viewModel.GetBoatCommand.Execute(null);
            
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }

        #region Functions



        private void ViewModelOnPropertyChanged(object sender1, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (_viewModel.Updated)
            {
                _viewModel.Updated = false;
                SetResult(Result.Ok);
                Finish();
                return;
            }

            if (_viewModel._boat.picture != null)
            {                                 
                _boatImage.SetImageDrawable(new CircleDrawable(BitmapFactory.DecodeByteArray(Convert.FromBase64String(new ImageUrlDownloader().GetImageBitmapFromUrl(_viewModel._boat.picture)), 0, Convert.FromBase64String(new ImageUrlDownloader().GetImageBitmapFromUrl(_viewModel._boat.picture)).Length)));
                _boatname.Text = _viewModel._boat.name;
                _registryNumber.Text = _viewModel._boat.registry_no;
                _harbor.Text = _viewModel._boat.harbor;
                _color.Text = _viewModel._boat.color;
                _country.SetSelection(_viewModel._boat.country_id);
            }
        }


        private void SaveButtonOnClick(object sender1, EventArgs eventArgs1)
        {
            _viewModel._boat.name = _boatname.Text;
            _viewModel._boat.registry_no = _registryNumber.Text;
            _viewModel._boat.harbor = _harbor.Text;
            _viewModel._boat.country_id = _country.SelectedItemPosition;
            _viewModel._boat.color = _color.Text;
            _viewModel.UpdateBoatImageCommand.Execute(null);
            _viewModel.UpdateBoatCommand.Execute(null);
            
        }

        private void CropImage(Android.Net.Uri Uri)
        {
            var CropIntent = new Intent("com.android.camera.action.CROP");
            CropIntent.SetDataAndType(Uri, "image/*");
            CropIntent.PutExtra("crop", "true");
            CropIntent.PutExtra("outputX", 500);
            CropIntent.PutExtra("outputY", 500);
            CropIntent.PutExtra("aspectY", 1);
            CropIntent.PutExtra("aspectX", 1);
            CropIntent.PutExtra("ScaleUpIfNeeded", true);
            CropIntent.PutExtra("return-data", true);
            StartActivityForResult(CropIntent, 1);
        }

        private void ValidateInputsTextField(TextInputEditText textfield)
        {
            if (!verifyText(textfield)) return;
            if (!verifyText(_boatname)) return;
            if (!verifyText(_color)) return;
            if (!verifyText(_registryNumber)) return;
            if (!verifyText(_harbor)) return;
            if (!verifyspinner(_country, _countryVerifyImage)) return;
            _saveButton.Text = "Save";
            _saveButton.Clickable = true;
            _saveButton.SetBackgroundResource(Resource.Drawable.green_button_selector);
        }    

        private void ValidateSpinner(Spinner Spinner, ImageView image)
        {
            if (!verifyspinner(Spinner, image)) return;
            if (!verifyText(_boatname)) return;
            if (!verifyspinner(_country, _countryVerifyImage)) return;
            if (!verifyText(_color)) return;
            if (!verifyText(_registryNumber)) return;
            if (!verifyText(_harbor)) return;
            _saveButton.Clickable = true;
            _saveButton.Text = "Save";
            _saveButton.SetBackgroundResource(Resource.Drawable.green_button_selector);
        }

        private bool verifyText(TextInputEditText textfield)
        {
            if (!string.IsNullOrEmpty(textfield.Text) && textfield.Text.Length < 50)
            {
                textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                return true;
            }
            _saveButton.Clickable = false;
            _saveButton.Text = "Missing Information";
            _saveButton.SetBackgroundResource(Resource.Drawable.red_button_selector);
            textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            return false;
        }
        

        private bool verifyspinner(Spinner spinner, ImageView imageView)
        {
                if (spinner.SelectedItemPosition > 0)
                {
                    imageView.SetImageDrawable(GetDrawable(Resource.Drawable.success));
                    return true;
                }
                _saveButton.Clickable = false;
                _saveButton.Text = "Missing Information";
                _saveButton.SetBackgroundResource(Resource.Drawable.red_button_selector);
                imageView.SetImageDrawable(GetDrawable(Resource.Drawable.do_not_match));
                return false;
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == 2)
            {
                try
                {

                    Bitmap bitmap = (Bitmap)data.Extras.Get("data");
                    bitmap = ThumbnailUtils.ExtractThumbnail(bitmap, 500, 500);
                    var circleimage = new CircleDrawable(bitmap);
                    _boatImage.SetImageDrawable(circleimage);

                    using (var stream = new MemoryStream())
                    {
                        bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
                        var bytes = stream.ToArray();
                        var base64 = Convert.ToBase64String(bytes);
                        var imagesize = System.Text.ASCIIEncoding.ASCII.GetByteCount(base64) / 1024;

                        if (imagesize < 1024)
                        {

                            _viewModel._base64Image = Convert.ToBase64String(bytes);
                           
                        }
                        else
                        {
                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                            alertBuilder.SetTitle("Image is to large");
                            alertBuilder.SetPositiveButton("ok", (sender, args) => { });
                            alertBuilder.Show();
                        }

                    }
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e);
                    return;
                }




            }

            if (requestCode == 0)
            {
                if (data != null)
                {
                    CropImage(data.Data);
                }

            }

            if (resultCode == Result.Ok && requestCode == 1)
            {
                Bundle bundle = data.Extras;
                Bitmap bitmap = (Bitmap)bundle.GetParcelable("data");
                var circleimage = new CircleDrawable(bitmap);
                _boatImage.SetImageDrawable(circleimage);
                using (var stream = new MemoryStream())
                {
                    bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
                    var bytes = stream.ToArray();
                    var base64 = Convert.ToBase64String(bytes);
                    var imagesize = System.Text.ASCIIEncoding.Unicode.GetByteCount(base64);
                    imagesize = (imagesize / 1024);
                    if (imagesize < 1048)
                    {
                        _viewModel._base64Image = base64;                     
                    }
                    else
                    {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                        alertBuilder.SetTitle("Image is to large");
                        alertBuilder.SetPositiveButton("ok", (sender, args) => { });
                        alertBuilder.Show();
                    }
                }


            }

        }

        #endregion
        }

}
     
   
