using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.App;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Droid.Helpers;
using fifi.Helpers;
using fifi.Utils.Helpers;
using Newtonsoft.Json;

namespace fifi.Droid.Activities
{
    [Activity(Label = "FIFI",
        Theme = "@style/SplashTheme",
        MainLauncher = true)]
    public class SplashActivity : AppCompatActivity
    {
        SeaDroidApi _api = new SeaDroidApi();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Intent newIntent;
            if (Settings.Current.IsLoggedIn && Settings.Current.IsVerified)
            {
              

                var channels = PubNubSingleton.Instance.GetSubcribedChannels();


                // subcribtion to channel one and two is there because if the suscribed channels are the same as the last that where unsubscribe from the messages get doubled.
                if (Settings.Current.RandomNumber != string.Empty && Convert.ToInt32(Settings.Current.RandomNumber) == 1)
                {  PubNubSingleton.Instance.Subcribe("2");  Settings.Current.RandomNumber = "2"; }
                else
                { PubNubSingleton.Instance.Subcribe("1"); Settings.Current.RandomNumber = "1";}


                PubNubSingleton.Instance.Subcribe(Settings.Current.UserId);
                PubNubSingleton.Instance.ClearLatestMessages();

                var a = new Task(async () =>
                {
                    var friends = JsonConvert.DeserializeObject<FriendList>(await _api.GetUserFriends());
                    
                    foreach (var friend in friends.data)
                    {
                        PubNubSingleton.Instance.SubcribeToFriendChat(Convert.ToInt32(Settings.Current.UserId), friend.id);
                    }

                    var SentFriendRequests = JsonConvert.DeserializeObject<FriendList>(await _api.GetSentFriendRequests());

                    foreach (var friendrequest in SentFriendRequests.data)
                    {
                        PubNubSingleton.Instance.SubcribeToFriendChat(Convert.ToInt32(Settings.Current.UserId), friendrequest.id);
                    }

                    channels = PubNubSingleton.Instance.GetSubcribedChannels();

                    await Task.Delay(100);
                    PubNubSingleton.Instance.GetLastestMessageFromSubcribedChannels();
                });
                a.Start();
                newIntent = new Intent(this, typeof(DashboardActivity));




                PubNubSingleton.Instance.PropertyChanged += (sender, args) =>
                {
                    NotificationHandling();
                };



            }
            else
            {
                newIntent = new Intent(this, typeof(LoginActivity));
            }

            newIntent.AddFlags(ActivityFlags.ClearTop);
            newIntent.AddFlags(ActivityFlags.SingleTop);
            StartActivity(newIntent);
            Finish();
        }




        private async void NotificationHandling()
        {


            if (PubNubSingleton.Instance.ChatMessage.Sender_id != Settings.Current.UserId && PubNubSingleton.Instance.ChatMessage.Channel != Settings.Current.ActiveChatId)
            {
                var _api = new SeaDroidApi();
                var result = await _api.GetSpecificUser(PubNubSingleton.Instance.ChatMessage.Sender_id);
                var user = JsonConvert.DeserializeObject<SpecificUser>(result);

                Intent intent = new Intent(this, typeof(ChatActivity));

                intent.PutExtra("Friend_id", PubNubSingleton.Instance.ChatMessage.Sender_id);
                intent.PutExtra("Friend_name", user.data.Firstname + " " + user.data.Lastname);
                intent.PutExtra("Friend_imageUrl", user.data.image);
                intent.PutExtra("OpenWithNotifcation", "true");






                int pedingIntentId = Convert.ToInt32(Settings.Current.UserId);Convert.ToInt32(PubNubSingleton.Instance.ChatMessage.Sender_id);
                PendingIntent pendingIntent = PendingIntent.GetActivity(this, pedingIntentId, intent, PendingIntentFlags.OneShot);


                Notification.Builder builder = new Notification.Builder(this);
                builder.SetContentIntent(pendingIntent).SetContentText(PubNubSingleton.Instance.ChatMessage.Text);
                builder.SetContentTitle(user.data.Firstname + " " + user.data.Lastname);
                builder.SetSmallIcon(Resource.Drawable.logo);
                builder.SetDefaults(NotificationDefaults.Vibrate);
                builder.SetPriority(2);
                if (!string.IsNullOrEmpty(user.data.image))
                {
                    var imagebytes = Convert.FromBase64String(new ImageUrlDownloader().GetImageBitmapFromUrl(user.data.image));
                    var bitmap = BitmapFactory.DecodeByteArray(imagebytes, 0, imagebytes.Length);
                    var Image = new CircleDrawable(bitmap);
                    builder.SetLargeIcon(Image.bmp);
                }


                Notification notification = builder.Build();
                notification.Flags = NotificationFlags.AutoCancel;

                NotificationManager notificationManager = this.GetSystemService(Context.NotificationService) as NotificationManager;
                const int notificationId = 0;
                notificationManager.Notify(notificationId, notification);
            }



        }
    }
}