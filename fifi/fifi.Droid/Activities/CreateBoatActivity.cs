﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Provider;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using fifi.Annotations;
using fifi.Droid.Helpers;
using fifi.ViewModel;


namespace fifi.Droid.Activities
{
    [Activity(Label = "Create Boat", Theme = "@style/MyTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class CreateBoatActivity : BaseActivity
    {
        protected override int LayoutResource => Resource.Layout.activity_create_boat;
        private CreateBoatViewModel _viewModel;

        private ImageView _image;
        private TextInputEditText _name;
        private Spinner _typeSpinner;
        private TextInputEditText _model;
        private TextInputEditText _color;
        private TextInputEditText _productionYear;
        private TextInputEditText _length;
        private TextInputEditText _registryNumber;
        private TextInputEditText _harbor;
        private Spinner _countrySpinner;
        private Button _doneButton;
        private bool PickingDate;
        private bool ImagePicked;
        private bool StartedByMastersFragment;


        private ImageView _typeimage;
        private ImageView _countryimage;
   
        private bool _boatCreated;

        protected override void OnCreate(Bundle bundle)
        {

            

            if ((int)Build.VERSION.SdkInt >= 21)
            {
                Window.SetStatusBarColor(new Color(ContextCompat.GetColor(this, Resource.Color.primary_dark)));
                Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
            }

            base.OnCreate(bundle);

            StartedByMastersFragment = Intent.GetBooleanExtra("StartedByMastersFragment", false);

           
           
            ImagePicked = false; 

            _viewModel = new CreateBoatViewModel(){ MasterSerial = Intent.GetStringExtra("MasterSerial") };

            if (StartedByMastersFragment)
            {
                _viewModel.VerifyingAccount = false;
            }

            #region ImageInit

            _image = FindViewById<ImageView>(Resource.Id.create_boat_boat_image);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            _image.Click += (sender, args) =>
            {
                string[] items = new string[] { "Camera", "Gallery", "cancel" };
                builder.SetItems(items, (o, eventArgs) =>
                {
                    switch (eventArgs.Which)
                    {
                        case 0:
                            Intent intent = new Intent(MediaStore.ActionImageCapture);
                            StartActivityForResult(intent, 2);
                            break;
                        case 1:
                            var imageIntent = new Intent();
                            imageIntent.SetType("image/*");
                            imageIntent.SetAction(Intent.ActionGetContent);
                            StartActivityForResult(Intent.CreateChooser(imageIntent, "Select photo"), 0);
                            break;
                        case 2:
                            break;

                    }
                });
                builder.Show();
            };

            
            #endregion

            _doneButton = FindViewById<Button>(Resource.Id.create_boat_edit_button);
          


            PickingDate = false; 


            #region TextField Init


            _name = FindViewById<TextInputEditText>(Resource.Id.create_boat_boatname);         
            _model = FindViewById<TextInputEditText>(Resource.Id.create_boat_model);
            _color = FindViewById<TextInputEditText>(Resource.Id.create_boat_color);
            _productionYear = FindViewById<TextInputEditText>(Resource.Id.create_boat_production_year);
            _length = FindViewById<TextInputEditText>(Resource.Id.create_boat_length);
            _registryNumber = FindViewById<TextInputEditText>(Resource.Id.create_boat_registry_number);
            _harbor = FindViewById<TextInputEditText>(Resource.Id.create_boat_harbor);
                                     
            _name.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);            
            _model.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _productionYear.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0); 
            _color.SetCompoundDrawablesWithIntrinsicBounds(0,0, Resource.Drawable.do_not_match, 0);
            _length.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _registryNumber.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _harbor.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);         

            _name.TextChanged += (sender, args) => { ValidateInputsTextField(_name); };        
            _model.TextChanged += (sender, args) => { ValidateInputsTextField(_model); };
            _color.TextChanged += (sender, args) => { ValidateInputsTextField(_color); };
            _productionYear.TextChanged += (sender, args) => { ValidateYear(_productionYear); };
            _length.TextChanged += (sender, args) => { ValidateInputsNumberTextField(_length); };
            _registryNumber.TextChanged += (sender, args) => { ValidateInputsTextField(_registryNumber); };
            _harbor.TextChanged += (sender, args) => { ValidateInputsTextField(_harbor); };
           

           
            #endregion

            #region spinnerInit
            _typeSpinner = FindViewById<Spinner>(Resource.Id.create_boat_boat_type);
            _countrySpinner = FindViewById<Spinner>(Resource.Id.create_boat_country);


            _countryimage = FindViewById<ImageView>(Resource.Id.create_boat_country_image);
            _typeimage = FindViewById<ImageView>(Resource.Id.create_boat_boat_type_image);

            _typeimage.SetImageDrawable(GetDrawable(Resource.Drawable.do_not_match));
            _countryimage.SetImageDrawable(GetDrawable(Resource.Drawable.do_not_match));
        

            _typeSpinner.ItemSelected += (sender, args) => { ValidateSpinner(_typeSpinner, _typeimage); };
            _countrySpinner.ItemSelected += (sender, args) => { ValidateSpinner(_countrySpinner, _countryimage); };

            var TypeAdapter = ArrayAdapter.CreateFromResource(this, Resource.Array.BoatType_array,
                Android.Resource.Layout.SimpleSpinnerItem);
            TypeAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            _typeSpinner.Adapter = TypeAdapter;

            var CountryAdapter = ArrayAdapter.CreateFromResource(this, Resource.Array.Country_array, Android.Resource.Layout.SimpleSpinnerItem);
            CountryAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerItem);
            _countrySpinner.Adapter = CountryAdapter;

            #endregion

            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
        }

        private void ViewModelOnPropertyChanged(object sender1, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (StartedByMastersFragment && _viewModel.NavigateToNext)
            {
               SetResult(Result.Ok);
                Finish();
                return;
            }

            if (StartedByMastersFragment && _viewModel.MasterUnpaired)
            {
               SetResult(Result.Canceled);
                Finish();
                return;
            }

            if (_viewModel.NavigateToNext)
            {
                var _intent = new Intent(this, typeof(DashboardActivity));
                _intent.AddFlags(ActivityFlags.ClearTop);
                StartActivity(_intent);
                Finish();
            }
        }

        private void DoneButtonOnClick(object sender1, EventArgs eventArgs1)
        {
            _viewModel.Boatname = _name.Text;
            _viewModel.Model = _model.Text;
            _viewModel.BoatLength = _length.Text;
            _viewModel.Color = _color.Text;
            _viewModel.Category_id = _typeSpinner.SelectedItemPosition;
            _viewModel.Country_id = _countrySpinner.SelectedItemPosition;
            _viewModel.Harbor = _harbor.Text;
            _viewModel.ProductionYear = Convert.ToInt32(_productionYear.Text);
            _viewModel.Registry_no = _registryNumber.Text;
           _viewModel.CreateBoatCommand.Execute(null);

        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == 2)
            {
                try
                {

                    Bitmap bitmap = (Bitmap)data.Extras.Get("data");
                    bitmap = ThumbnailUtils.ExtractThumbnail(bitmap, 500, 500);
                    var circleimage = new CircleDrawable(bitmap);
                    _image.SetImageDrawable(circleimage);

                    using (var stream = new MemoryStream())
                    {
                        bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
                        var bytes = stream.ToArray();
                        var base64 = Convert.ToBase64String(bytes);
                        var imagesize = System.Text.ASCIIEncoding.ASCII.GetByteCount(base64) / 1024;

                        if (imagesize < 1048)
                        {
                              
                            _viewModel.Base64EncodedImage = base64;
                            ImagePicked = true;
                            if (!verifyText(_name)) return;
                            if (!verifyspinner(_typeSpinner, _typeimage)) return;
                            if (!verifyText(_model)) return;
                            if (!verifyText(_color)) return;
                            if (!verifyText(_productionYear)) return;
                            if (!verifynumber(_length)) return;
                            if (!verifyText(_registryNumber)) return;
                            if (!verifyText(_harbor)) return;
                            if (!verifyspinner(_countrySpinner, _countryimage)) return;
                            _doneButton.Clickable = true;
                            _doneButton.SetBackgroundResource(Resource.Drawable.green_button_selector);
                        }
                        else
                        {
                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                            alertBuilder.SetTitle("Image is to large");
                            alertBuilder.SetPositiveButton("ok", (sender, args) => { });
                            alertBuilder.Show();
                        }


                    }
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e);
                    return;
                }


            }



            if (requestCode == 0)
            {
                if (data != null)
                {
                    CropImage(data.Data);
                }

            }

            if (resultCode == Result.Ok && requestCode == 1)
            {
                Bundle bundle = data.Extras;
                Bitmap bitmap = (Bitmap)bundle.GetParcelable("data");
                var circleimage = new CircleDrawable(bitmap);
                _image.SetImageDrawable(circleimage);
                using (var stream = new MemoryStream())
                {
                    bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
                    var bytes = stream.ToArray();
                    var base64 = Convert.ToBase64String(bytes);
                    var imagesize = System.Text.ASCIIEncoding.Unicode.GetByteCount(base64);
                    imagesize = (imagesize / 1024);
                    if (imagesize < 1048)
                    {
                        _viewModel.Base64EncodedImage = base64;
                        ImagePicked = true;
                        if (!verifyText(_name)) return;
                        if (!verifyspinner(_typeSpinner, _typeimage)) return;
                        if (!verifyText(_model)) return;
                        if (!verifyText(_color)) return;
                        if (!verifyText(_productionYear)) return;
                        if (!verifynumber(_length)) return;
                        if (!verifyText(_registryNumber)) return;
                        if (!verifyText(_harbor)) return;
                        if (!verifyspinner(_countrySpinner, _countryimage)) return;
                        _doneButton.Clickable = true;
                        _doneButton.SetBackgroundResource(Resource.Drawable.green_button_selector);

                    }
                    else
                    {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                        alertBuilder.SetTitle("Image is to large");
                        alertBuilder.SetPositiveButton("ok", (sender, args) => { });
                        alertBuilder.Show();
                    }
                }


            }
        }



        private void ValidateInputsTextField(TextInputEditText textfield)
        {
            if (!verifyText(textfield)) return;
            if (!verifyText(_name)) return;
            if (!verifyspinner(_typeSpinner, _typeimage)) return;
            if (!verifyText(_model)) return;
            if (!verifyText(_color)) return;
            if (!verifyYear(_productionYear)) return;
            if (!verifynumber(_length)) return;
            if (!verifyText(_registryNumber)) return;
            if (!verifyText(_harbor)) return;
            if (!verifyspinner(_countrySpinner, _countryimage)) return;
            if (_image.Drawable == GetDrawable(Resource.Drawable.choose_Image)) return;
            _doneButton.Text = "Save";
            _doneButton.Clickable = true;
            _doneButton.SetBackgroundResource(Resource.Drawable.green_button_selector);
        }

        private void ValidateInputsNumberTextField(TextInputEditText textfield)
        {
            if (!verifynumber(textfield)) return;
            if (!verifyText(_name)) return;
            if (!verifyspinner(_typeSpinner, _typeimage)) return;
            if (!verifyText(_model)) return;
            if (!verifyText(_color)) return;
            if (!verifyYear(_productionYear)) return;
            if (!verifynumber(_length)) return;
            if (!verifyText(_registryNumber)) return;
            if (!verifyText(_harbor)) return;
            if (!verifyspinner(_countrySpinner, _countryimage)) return;
            if (!ImagePicked) return;
            _doneButton.Text = "Save";
            _doneButton.Clickable = true;
            _doneButton.SetBackgroundResource(Resource.Drawable.green_button_selector);
        }

        private void ValidateSpinner(Spinner Spinner, ImageView image)
        {
            if (!verifyspinner(Spinner, image)) return;
            if (!verifyText(_name)) return;
            if (!verifyspinner(_typeSpinner, _typeimage)) return;
            if (!verifyText(_model)) return;
            if (!verifyText(_color)) return;
            if (!verifyYear(_productionYear)) return;
            if (!verifynumber(_length)) return;
            if (!verifyText(_registryNumber)) return;
            if (!verifyText(_harbor)) return;
            if (!verifyspinner(_countrySpinner, _countryimage)) return;
            if (!ImagePicked) return;
          

            _doneButton.Clickable = true;
            _doneButton.Text = "Save";
            _doneButton.SetBackgroundResource(Resource.Drawable.green_button_selector);
        }

        private void ValidateYear(TextInputEditText textfield)
        {
            if(!verifyYear(textfield)) return;
            if (!verifyText(_name)) return;
            if (!verifyspinner(_typeSpinner, _typeimage)) return;
            if (!verifyText(_model)) return;
            if (!verifyText(_color)) return;
            if (!verifyYear(_productionYear)) return;
            if (!verifynumber(_length)) return;
            if (!verifyText(_registryNumber)) return;
            if (!verifyText(_harbor)) return;
            if (!verifyspinner(_countrySpinner, _countryimage)) return;
            if (!ImagePicked) return;
            _doneButton.Clickable = true;
            _doneButton.Text = "Save";
            _doneButton.SetBackgroundResource(Resource.Drawable.green_button_selector);
        }


        private bool verifyText(TextInputEditText textfield)
        {
            if (!string.IsNullOrEmpty(textfield.Text) && textfield.Text.Length < 50)
            {
                textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                return true;
            }
            _doneButton.Clickable = false;
            _doneButton.Text = "Missing Information";
            _doneButton.SetBackgroundResource(Resource.Drawable.red_button_selector);
            textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            return false;
        }

        private bool verifynumber(TextInputEditText textfield)
        {
            if (CheckIfNumber(textfield.Text) && !string.IsNullOrEmpty(textfield.Text) && CheckIfSmallInt(Convert.ToInt32(textfield.Text)))
            {
                textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                return true;
            }

            _doneButton.Clickable = false;
            _doneButton.Text = "Missing Information";
            _doneButton.SetBackgroundResource(Resource.Drawable.red_button_selector);
            textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            return false;
        }

        private bool verifyspinner(Spinner spinner, ImageView imageView)
        {
            if (spinner.SelectedItemPosition > 0)
            {
                imageView.SetImageDrawable(GetDrawable(Resource.Drawable.success));
                return true;
            }
            _doneButton.Clickable = false;
            _doneButton.Text = "Missing Information";
            _doneButton.SetBackgroundResource(Resource.Drawable.red_button_selector);
            imageView.SetImageDrawable(GetDrawable(Resource.Drawable.do_not_match));
            return false;
        }

      

       

        private bool CheckIfSmallInt(int number)
        {
            if (number < 32767)
            {
                return true;
            }
            return false;
        }

        private bool verifyYear(TextInputEditText textfield)
        {
            if (!string.IsNullOrEmpty(textfield.Text) && Convert.ToInt32(textfield.Text) > 1899 && Convert.ToInt32(textfield.Text) < DateTime.Today.Year + 1)
            {
                textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                return true;
            }
            _doneButton.Clickable = false;
            _doneButton.Text = "Missing Information";
            _doneButton.SetBackgroundResource(Resource.Drawable.red_button_selector);
            textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            return false;
        }

        public override void OnBackPressed()
        {
            if (StartedByMastersFragment != null && StartedByMastersFragment == true)
            {     
                _viewModel.CancelMasterPairing.Execute(null);
            }
            else
            {
                var _intent = new Intent(this, typeof(LoginActivity));
                _intent.AddFlags(ActivityFlags.ClearTop);
                StartActivity(_intent);
                Finish();
            }
           
        }

    }
}