﻿using System;
using System.ComponentModel;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.Content;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using fifi.Client.DataObjects;
using fifi.Droid.Adapters;
using fifi.ViewModel;
using Mapbox;
using Debug = System.Diagnostics.Debug;

namespace fifi.Droid.Activities
{
    [Activity(Label = "Device Details", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class DeviceDetailsActivity : BaseActivity
    {
        private DeviceDetailsAdapter _adapter;
        private RecyclerView _recyclerView;
        private SwipeRefreshLayout _refresher;
        private DeviceDetailsViewModel _viewModel;
        private string SelectedTripPosition;
        protected override int LayoutResource => Resource.Layout.activity_device_details;

        public static Master Master { get; set; }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            if ((int)Build.VERSION.SdkInt >= 21)
            {
                Window.SetStatusBarColor(new Color(ContextCompat.GetColor(this, Resource.Color.primary_dark)));
                Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
            }
     
          
            MapboxAccountManager.Start(this, GetString(Resource.String.access_token));
            _viewModel = new DeviceDetailsViewModel { Title = Intent.GetStringExtra("Id") };

            _viewModel.Master = Master;
            _viewModel.Trips.ReplaceRange(_viewModel.Master.Trips);
            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
            _recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            _refresher = FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);

            _adapter = new DeviceDetailsAdapter(this, _viewModel);
            _adapter.ItemClick += OnItemClick;
            // Determine the number of columns to display, based on screen width.
            const int rows = 1;
            var layoutManager = new GridLayoutManager(this, rows, LinearLayoutManager.Vertical, false);
            _recyclerView.SetLayoutManager(layoutManager);
            _recyclerView.SetAdapter(_adapter);

            //_refresher.Refresh += (sender, e) => 


        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            var trip = _viewModel.SelectedTrip;
            var intent = new Intent(this, typeof(PastTripDetailsActivity));
            intent.PutExtra("Id", _viewModel.Trip_id);
            PastTripDetailsActivity._trip = trip;
            StartActivity(intent);
            Debug.WriteLine(trip.Id + " " + trip.Coordinates.Count);
        }
  

        private void OnItemClick(object sender, DeviceDetailsClickArgs e)
        {
             SelectedTripPosition = e.Position.ToString();
            _viewModel.Trip_id = _viewModel.Trips[e.Position].Id.ToString();
            _viewModel.LoadTripCommand.Execute(null);                     
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }

            var intent = new Intent(this, typeof(MasterSettingsActivity));
            MasterSettingsActivity._viewModel = new MasterSettingsViewModel(){ _master = Master};
            StartActivityForResult(intent, 1);

            return base.OnOptionsItemSelected(item);
        }


        public override bool OnPrepareOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.device_details_menu, menu);          
            return base.OnCreateOptionsMenu(menu);
        }


       
        //public override void OnLowMemory()
        //{
        //    base.OnLowMemory();

        //    if (_adapter == null) return;
        //    foreach (var mapView in _adapter.GetMapViews())
        //    {
        //        try
        //        {
        //            mapView.OnLowMemory();
        //        }
        //        catch (Exception)
        //        {
        //            // ignore
        //        }
        //    }
        //}

        //protected override void OnPause()
        //{
        //    base.OnPause();
        //    if (_adapter == null) return;
        //    foreach (var mapView in _adapter.GetMapViews())
        //    {
        //        try
        //        {
        //            mapView.OnPause();
        //        }
        //        catch (Exception)
        //        {
        //            // ignore
        //        }
        //    }
        //}

        //protected override void OnResume()
        //{
        //    base.OnResume();

        //    if (_adapter == null) return;
        //    foreach (var mapView in _adapter.GetMapViews())
        //    {
        //        try
        //        {
        //            mapView.OnResume();
        //        }
        //        catch (Exception)
        //        {
        //            // ignore
        //        }
        //    }
        //}

        //protected override void OnDestroy()
        //{
        //    if (_adapter == null) return;
        //    foreach (var mapView in _adapter.GetMapViews())
        //    {
        //        try
        //        {
        //            mapView.OnDestroy();
        //        }
        //        catch (Exception)
        //        {
        //            // ignore
        //        }
        //    }

        //    base.OnDestroy();
        //}

        //protected override void OnSaveInstanceState(Bundle outState)
        //{
        //    base.OnSaveInstanceState(outState);
        //    if (_adapter == null) return;
        //    foreach (var mapView in _adapter.GetMapViews())
        //    {
        //        try
        //        {
        //            mapView.OnSaveInstanceState(outState);
        //        }
        //        catch (Exception)
        //        {
        //            // ignore
        //        }
        //    }
        //}
    }
}