using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Policy;
using System.Text;
using System.Xml.Serialization;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Icu.Util;
using Android.Media.Midi;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.Client.DataObjects;
using fifi.Droid.Adapters;
using fifi.Droid.Fragments;
using fifi.Model;
using fifi.ViewModel;
using Java.IO;
using Java.Nio.Charset;
using Javax.Security.Auth;
using Plugin.BLE.Abstractions.Contracts;
using Console = System.Console;

namespace fifi.Droid.Activities
{
    [Activity(Label = "Armband Details", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class PairingDetailsActivity : BaseActivity
    {
        protected override int LayoutResource => Resource.Layout.activity_pairing_details;
        public static PairingDetailsViewModel _viewModel;
        private TextView _Title;
        private TextInputEditText _firstName;
        private TextInputEditText _lastName;
        private TextInputEditText _birthday;
        private TextInputEditText _height;
        private TextInputEditText _weight;

        private ImageView _languageValidater;
        private ImageView _genderValidater;
        private ImageView _seaExpValidater;
        private ImageView _swimExpValidater;


        private TextInputEditText _Haircolor;
        private Button _edit_button;
        private bool PickingDate;
        private Spinner _swimExperience;
        private Spinner _seaExperience;
        private Spinner _gender;
        private Spinner _Language;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            if ((int) Build.VERSION.SdkInt >= 21)
            {
                Window.SetStatusBarColor(new Color(ContextCompat.GetColor(this, Resource.Color.primary_dark)));
                Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
            }
            // _viewModel = new PairingDetailsViewModel {BluetoothDevice = BluetoothDevice};
            PickingDate = false;
            _edit_button = FindViewById<Button>(Resource.Id.Edit_button);

            _languageValidater = FindViewById<ImageView>(Resource.Id.pairing_detial_language_image);
            _genderValidater = FindViewById<ImageView>(Resource.Id.pairing_detial_gender_image);
            _seaExpValidater = FindViewById<ImageView>(Resource.Id.pairing_detial_sea_exp_image);
            _swimExpValidater = FindViewById<ImageView>(Resource.Id.pairing_detial_swim_exp_image);

            _firstName = FindViewById<TextInputEditText>(Resource.Id.input_FirstName);

            _firstName.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);

            _lastName = FindViewById<TextInputEditText>(Resource.Id.input_LastName);

            _lastName.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);

            _birthday = FindViewById<TextInputEditText>(Resource.Id.input_birthday);
            _birthday.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);

            _birthday.Touch += (sender, EventArgs) =>
            {
                DatePickerDialog Datepicker;

                if (PickingDate == false)
                {
                    PickingDate = true;

                    if (_viewModel.Wristband.firstname != null)
                    {
                        try
                        {
                            var date = _birthday.Text.Split('-');

                            Datepicker = new DatePickerDialog(this, OnDateSet, Convert.ToInt16(date[0]),
                                Convert.ToInt16(date[1]), Convert.ToInt16(date[2]));
                            Datepicker.CancelEvent += (sender2, args) => { PickingDate = false; };
                            Datepicker.DismissEvent += (sender2, args) => { PickingDate = false; };
                            Datepicker.Show();
                        }
                        catch (Exception e)
                        {
                            var date = _birthday.Text.Split('/');
                            Datepicker = new DatePickerDialog(this, OnDateSet, Convert.ToInt16(date[0]),
                                Convert.ToInt16(date[1]), Convert.ToInt16(date[2]));
                            Datepicker.CancelEvent += (sender2, args) => { PickingDate = false; };
                            Datepicker.DismissEvent += (sender2, args) => { PickingDate = false; };
                            Datepicker.Show();
                        }
                    }

                    else
                    {
                        if (_birthday.Text != string.Empty)
                        {
                            var date = _birthday.Text.Split('/');
                            Datepicker = new DatePickerDialog(this, OnDateSet, Convert.ToInt16(date[0]),
                                Convert.ToInt16(date[1]), Convert.ToInt16(date[2]));
                            Datepicker.CancelEvent += (sender2, args) => { PickingDate = false; };
                            Datepicker.DismissEvent += (sender2, args) => { PickingDate = false; };
                            Datepicker.Show();
                        }
                        else
                        {
                            Datepicker = new DatePickerDialog(this);
                            Datepicker.CancelEvent += (sender2, args) => { PickingDate = false; };
                            Datepicker.DismissEvent += (sender2, args) => { PickingDate = false; };
                            Datepicker.Show();
                        }
                    }
                }
            };


            _height = FindViewById<TextInputEditText>(Resource.Id.input_height);
            _height.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);


            _weight = FindViewById<TextInputEditText>(Resource.Id.input_Weight);
            _weight.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);



            _Haircolor = FindViewById<TextInputEditText>(Resource.Id.input_Haircolor);
            _Haircolor.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);


            _swimExperience = FindViewById<Spinner>(Resource.Id.input_SwimExperience);


            #region spinnerInit

            var Swimadapter = ArrayAdapter.CreateFromResource(this, Resource.Array.SwimExperience_array,
                Android.Resource.Layout.SimpleSpinnerItem);
            Swimadapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            _swimExperience.Adapter = Swimadapter;


            _seaExperience = FindViewById<Spinner>(Resource.Id.input_SeaExperience);
            var seadapter = ArrayAdapter.CreateFromResource(this, Resource.Array.SeaExperience_array,
                Android.Resource.Layout.SimpleSpinnerItem);
            seadapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            _seaExperience.Adapter = seadapter;


            _gender = FindViewById<Spinner>(Resource.Id.input_Gender);
            var genderAdapeter = ArrayAdapter.CreateFromResource(this, Resource.Array.gender_array,
                Android.Resource.Layout.SimpleSpinnerItem);
            genderAdapeter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            _gender.Adapter = genderAdapeter;


            _Language = FindViewById<Spinner>(Resource.Id.input_language);
            var langaugeAdapter = ArrayAdapter.CreateFromResource(this, Resource.Array.Language_array,
                Android.Resource.Layout.SimpleSpinnerItem);
            langaugeAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            _Language.Adapter = langaugeAdapter;

            #endregion


            _firstName.TextChanged += (Sender, args) =>
            {
                if (!verifyText(_firstName)) return;
                if (!verifyText(_lastName)) return;
                if (!verifyspinner(_gender, _genderValidater)) return;
                if (!verifyText(_birthday)) return;
                if (!verifyText(_height)) return;
                if (!verifyText(_weight)) return;
                if (!verifyspinner(_Language, _languageValidater)) return;
                if (!verifyText(_Haircolor)) return;
                if (!verifyspinner(_swimExperience, _swimExpValidater)) return;
                if (!verifyspinner(_seaExperience, _seaExpValidater)) return;
                _edit_button.Clickable = true;
                _edit_button.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

            _lastName.TextChanged += (sender, EventArgs) =>
            {
                if (!verifyText(_lastName)) return;
                if (!verifyText(_firstName)) return;
                if (!verifyspinner(_gender, _genderValidater)) return;
                if (!verifyText(_birthday)) return;
                if (!verifyText(_height)) return;
                if (!verifyText(_weight)) return;
                if (!verifyspinner(_Language, _languageValidater)) return;
                if (!verifyText(_Haircolor)) return;
                if (!verifyspinner(_swimExperience, _swimExpValidater)) return;
                if (!verifyspinner(_seaExperience, _seaExpValidater)) return;
                _edit_button.Clickable = true;
                _edit_button.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

            _gender.ItemSelected += (sender, args) =>
            {
                if (!verifyspinner(_gender, _genderValidater)) return;
                if (!verifyText(_lastName)) return;
                if (!verifyText(_firstName)) return;
                if (!verifyText(_birthday)) return;
                if (!verifyText(_height)) return;
                if (!verifyText(_weight)) return;
                if (!verifyspinner(_Language, _languageValidater)) return;
                if (!verifyText(_Haircolor)) return;
                if (!verifyspinner(_swimExperience, _swimExpValidater)) return;
                if (!verifyspinner(_seaExperience, _seaExpValidater)) return;
                _edit_button.Clickable = true;
                _edit_button.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

            _birthday.TextChanged += (sender, args) =>
            {
                if (!verifyText(_birthday)) return;
                if (!verifyspinner(_gender, _genderValidater)) return;
                if (!verifyText(_lastName)) return;
                if (!verifyText(_firstName)) return;
                if (!verifyText(_height)) return;
                if (!verifyText(_weight)) return;
                if (!verifyspinner(_Language, _languageValidater)) return;
                if (!verifyText(_Haircolor)) return;
                if (!verifyspinner(_swimExperience, _swimExpValidater)) return;
                if (!verifyspinner(_seaExperience, _seaExpValidater)) return;
                _edit_button.Clickable = true;
                _edit_button.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

            _height.TextChanged += (sender, EventArgs) =>
            {
                if (!verifyText(_height)) return;
                if (!verifyText(_birthday)) return;
                if (!verifyspinner(_gender, _genderValidater)) return;
                if (!verifyText(_lastName)) return;
                if (!verifyText(_firstName)) return;
                if (!verifyText(_weight)) return;
                if (!verifyspinner(_Language, _languageValidater)) return;
                if (!verifyText(_Haircolor)) return;
                if (!verifyspinner(_swimExperience, _swimExpValidater)) return;
                if (!verifyspinner(_seaExperience, _seaExpValidater)) return;
                _edit_button.Clickable = true;
                _edit_button.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

            _weight.TextChanged += (sender, EventArgs) =>
            {
                if (!verifyText(_weight)) return;
                if (!verifyText(_height)) return;
                if (!verifyText(_birthday)) return;
                if (!verifyspinner(_gender, _genderValidater)) return;
                if (!verifyText(_lastName)) return;
                if (!verifyText(_firstName)) return;
                if (!verifyspinner(_Language, _languageValidater)) return;
                if (!verifyText(_Haircolor)) return;
                if (!verifyspinner(_swimExperience, _swimExpValidater)) return;
                if (!verifyspinner(_seaExperience, _seaExpValidater)) return;
                _edit_button.Clickable = true;
                _edit_button.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

            _Language.ItemSelected += (sender, args) =>
            {
                if (!verifyspinner(_Language, _languageValidater)) return;
                if (!verifyText(_weight)) return;
                if (!verifyText(_height)) return;
                if (!verifyText(_birthday)) return;
                if (!verifyspinner(_gender, _genderValidater)) return;
                if (!verifyText(_lastName)) return;
                if (!verifyText(_firstName)) return;
                if (!verifyText(_Haircolor)) return;
                if (!verifyspinner(_swimExperience, _swimExpValidater)) return;
                if (!verifyspinner(_seaExperience, _seaExpValidater)) return;
                _edit_button.Clickable = true;
                _edit_button.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

            _Haircolor.TextChanged += (sender, args) =>
            {
                if (!verifyText(_Haircolor)) return;
                if (!verifyspinner(_Language, _languageValidater)) return;
                if (!verifyText(_weight)) return;
                if (!verifyText(_height)) return;
                if (!verifyText(_birthday)) return;
                if (!verifyspinner(_gender, _genderValidater)) return;
                if (!verifyText(_lastName)) return;
                if (!verifyText(_firstName)) return;
                if (!verifyspinner(_swimExperience, _swimExpValidater)) return;
                if (!verifyspinner(_seaExperience, _seaExpValidater)) return;
                _edit_button.Clickable = true;
                _edit_button.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

            _swimExperience.ItemSelected += (sender, args) =>
            {
                if (!verifyspinner(_swimExperience, _swimExpValidater)) return;
                if (!verifyText(_Haircolor)) return;
                if (!verifyspinner(_Language, _languageValidater)) return;
                if (!verifyText(_weight)) return;
                if (!verifyText(_height)) return;
                if (!verifyText(_birthday)) return;
                if (!verifyspinner(_gender, _genderValidater)) return;
                if (!verifyText(_lastName)) return;
                if (!verifyText(_firstName)) return;
                if (!verifyspinner(_seaExperience, _seaExpValidater)) return;
                _edit_button.Clickable = true;
                _edit_button.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

            _seaExperience.ItemSelected += (sender, args) =>
            {
                if (!verifyspinner(_seaExperience, _seaExpValidater)) return;
                if (!verifyspinner(_swimExperience, _swimExpValidater)) return;
                if (!verifyText(_Haircolor)) return;
                if (!verifyspinner(_Language, _languageValidater)) return;
                if (!verifyText(_weight)) return;
                if (!verifyText(_height)) return;
                if (!verifyText(_birthday)) return;
                if (!verifyspinner(_gender, _genderValidater)) return;
                if (!verifyText(_lastName)) return;
                if (!verifyText(_firstName)) return;
                _edit_button.Clickable = true;
                _edit_button.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

           
                try
                {
                    _firstName.Text = _viewModel.Wristband.firstname;
                    _lastName.Text = _viewModel.Wristband.lastname;
                    _birthday.Text = _viewModel.Wristband.birthday;
                    _height.Text = Convert.ToString(_viewModel.Wristband.height);
                    _weight.Text = Convert.ToString(_viewModel.Wristband.weight);
                    _gender.SetSelection(_viewModel.Wristband.gender == "Female" ? 2 : 1);
                    _Language.SetSelection((int)_viewModel.Wristband.country_id);
                    _Haircolor.Text = _viewModel.Wristband.haircolor;
                    _swimExperience.SetSelection((int)_viewModel.Wristband.swim_exp + 1);
                    _seaExperience.SetSelection((int)_viewModel.Wristband.sea_exp + 1);



                    _firstName.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    _lastName.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    _genderValidater.SetImageDrawable(GetDrawable(Resource.Drawable.success));
                    _birthday.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    _height.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    _weight.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    _languageValidater.SetImageDrawable(GetDrawable(Resource.Drawable.success));
                    _Haircolor.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    _swimExpValidater.SetImageDrawable(GetDrawable(Resource.Drawable.success));
                    _seaExpValidater.SetImageDrawable(GetDrawable(Resource.Drawable.success));
                }
                catch (Exception e)
                {
                    _firstName.Text = "";
                    _lastName.Text = "";
                    _birthday.Text = "2000/01/01";
                    _height.Text = "";
                    _weight.Text = "";
                    _Haircolor.Text = "";
                    _swimExperience.SetSelection(0);
                    _seaExperience.SetSelection(0);
                    _gender.SetSelection(0);
                    _Language.SetSelection(0);
                }
               

           


            Intent myIntent = new Intent(this, typeof(FragmentArmbands));

            //myIntent.PutExtra("Id", _viewModel.Wristband.id);
            //myIntent.PutExtra("firstname", _firstName.Text);
            //myIntent.PutExtra("lastname", _lastName.Text);
            //myIntent.PutExtra("age", _birthday.Text);
            //myIntent.PutExtra("height", _height.Text);
            //myIntent.PutExtra("weight", _weight.Text);
            //myIntent.PutExtra("bluetoothid", Intent.GetStringExtra("Id"));
            //myIntent.PutExtra("Language", _Language.SelectedItemPosition);
            //myIntent.PutExtra("Haircolor", _Haircolor.Text);
            //myIntent.PutExtra("Gender", _gender.SelectedItemPosition == 1 ? "Male" : "Female");
            //myIntent.PutExtra("SwinExperience", Convert.ToString(_swimExperience.SelectedItemPosition));
            //myIntent.PutExtra("SeaExperience", Convert.ToString(_seaExperience.SelectedItemPosition));
            
            SetResult(Result.Canceled, myIntent);

            _edit_button.Click += (sender, e) =>
            {
                _edit_button.Clickable = false;
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                try
                {
                    if (_firstName.Text == string.Empty || _lastName.Text == string.Empty ||
                        _birthday.Text == string.Empty || _height.Text == string.Empty ||
                        _weight.Text == string.Empty || _Haircolor.Text == string.Empty ||
                        _swimExperience.SelectedItemPosition == 0 || _seaExperience.SelectedItemPosition == 0 ||
                        _Language.SelectedItemPosition == 0)
                    {
                        alert.SetTitle("Edit failed");
                        alert.SetMessage("One or more textfields have not been filled out");
                        alert.SetPositiveButton("OK", (SenderAlert, a) => { return; });
                        alert.Show();
                    }
                    else
                    {
                        alert.SetTitle("Edit WristBand");
                        alert.SetMessage("Comfirm Eddit");

                        alert.SetPositiveButton("OK", (senderAlert, a) =>
                        {
                        //myIntent.PutExtra("Id", _viewModel.Wristband.id);
                        //myIntent.PutExtra("firstname", _firstName.Text);
                        //myIntent.PutExtra("lastname", _lastName.Text);
                        //myIntent.PutExtra("birthday", _birthday.Text);
                        //myIntent.PutExtra("height", _height.Text);
                        //myIntent.PutExtra("weight", _weight.Text);
                        //myIntent.PutExtra("bluetoothid", Intent.GetStringExtra("Id"));
                        //myIntent.PutExtra("language", Convert.ToString(_Language.SelectedItemPosition));
                        //myIntent.PutExtra("haircolor", _Haircolor.Text);
                        //myIntent.PutExtra("gender", _gender.SelectedItemPosition == 1 ? "Male" : "Female");
                        //myIntent.PutExtra("swinExperience",
                        //    Convert.ToString(_swimExperience.SelectedItemPosition - 1));
                        //myIntent.PutExtra("seaExperience",
                        //    Convert.ToString(_seaExperience.SelectedItemPosition- 1));

                        _viewModel.Wristband.firstname = _firstName.Text;
                        _viewModel.Wristband.lastname = _lastName.Text;
                        _viewModel.Wristband.birthday = _birthday.Text;
                        _viewModel.Wristband.height = Convert.ToInt32(_height.Text);
                        _viewModel.Wristband.weight = Convert.ToInt32(_weight.Text);
                        _viewModel.Wristband.country_id = _Language.SelectedItemPosition;
                        _viewModel.Wristband.haircolor = _Haircolor.Text;
                        _viewModel.Wristband.gender = _gender.SelectedItemPosition == 1 ? "Male" : "Female";
                        _viewModel.Wristband.swim_exp = _swimExperience.SelectedItemPosition - 1;
                        _viewModel.Wristband.sea_exp = _seaExperience.SelectedItemPosition - 1;
                        _viewModel.Wristband.serial = _viewModel.BluetoothDeviceId;

                            string SerializedArmband = string.Empty;

                            XmlSerializer serializer = new XmlSerializer(_viewModel.Wristband.GetType());
                            using (System.IO.StringWriter sw = new System.IO.StringWriter())
                            {
                                serializer.Serialize(sw, _viewModel.Wristband);
                                SerializedArmband = sw.ToString();
                            }

                             
                           
                            myIntent.PutExtra("Armband", SerializedArmband); 



                            SetResult(Result.Ok, myIntent);


                            Finish();
                        });
                        alert.SetNegativeButton("Cancel", (senderAlert, a) => { });
                        alert.Create();
                        alert.Show();
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    throw;
                }
                finally

                {
                    _edit_button.Clickable = true;
                }
            };

          
        }


        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }

            return base.OnOptionsItemSelected(item);
        }

        void OnDateSet(Object sender, DatePickerDialog.DateSetEventArgs e)
        {
            _birthday.Text = e.Year + "/" + (e.Month + 1) + "/" + e.DayOfMonth;
            PickingDate = false;
        }

        #region Verification

        private bool verifyText(TextInputEditText textfield )
        {
            if (!string.IsNullOrEmpty(textfield.Text) && textfield.Text.Length < 50)
            {
                textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                return true;
            }
            _edit_button.Clickable = false;
            _edit_button.SetBackgroundResource(Resource.Drawable.red_button_selector);
            textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            return false;
          
        }


        private bool verifynumber(TextInputEditText textfield)
        {
            if (CheckIfNumber(textfield.Text) && !string.IsNullOrEmpty(textfield.Text))
            {
                textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                return true;
            }

            _edit_button.Clickable = false;
            _edit_button.SetBackgroundResource(Resource.Drawable.red_button_selector);
            textfield.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            return false;

        }


        private bool verifyspinner( Spinner spinner, ImageView imageView)
        {
            if (spinner.SelectedItemPosition > 0)
            {
                      imageView.SetImageDrawable(GetDrawable(Resource.Drawable.success));
                return true;
            }
            _edit_button.Clickable = false;
            _edit_button.SetBackgroundResource(Resource.Drawable.red_button_selector);
            imageView.SetImageDrawable(GetDrawable(Resource.Drawable.do_not_match));
                return false;
            
        }

       


        private bool CheckIfNumber(string text)
        {
            foreach (char c in text)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }
        #endregion
    }
}