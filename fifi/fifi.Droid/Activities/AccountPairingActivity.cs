using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using fifi.Droid.Fragments;
using Plugin.SecureStorage;

namespace fifi.Droid.Activities
{
    [Activity(Label = "Pairing Account", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class AccountPairingActivity : BaseActivity
    {       
        protected override int LayoutResource => Resource.Layout.activity_account_pairing;

        private Button _invite_button;
        private Button _armband_button;
        private Button _master_button;
        private Button _shop_button;
      
        protected override void OnCreate(Bundle bundle)
        {

            base.OnCreate(bundle);

            if ((int)Build.VERSION.SdkInt >= 21)
            {
                Window.SetStatusBarColor(new Color(ContextCompat.GetColor(this, Resource.Color.primary_dark)));
                Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
            }

            _invite_button = FindViewById<Button>(Resource.Id.account_pairing_Invitation_button);
            _armband_button = FindViewById<Button>(Resource.Id.account_pairing_armband_button);
            _master_button = FindViewById<Button>(Resource.Id.account_pairing_master_button);
            _shop_button = FindViewById<Button>(Resource.Id.account_pairing_shop_button);
            _armband_button.Click += (sender, args) =>
            {
                var intent = new Intent(this, typeof(AccountPairingArmbandActivity));
                StartActivity(intent);
            };
            

            _invite_button.Click += (sender, args) =>
            {
                Snackbar snackbar = Snackbar.Make(this.CurrentFocus, "Option comming soon....", Snackbar.LengthShort);
                snackbar.Show();

            };

            _master_button.Click += (sender, args) =>
            {
                var intent = new Intent(this, typeof(MasterPairingActivity));
                StartActivity(intent);
            };

            _shop_button.Click += (sender, args) =>
            {
                Snackbar snackbar = Snackbar.Make(this.CurrentFocus, "Option comming soon....", Snackbar.LengthShort);
                snackbar.Show();
            };

            _master_button = FindViewById<Button>(Resource.Id.account_pairing_master_button);
            _shop_button = FindViewById<Button>(Resource.Id.account_pairing_shop_button);
        }


        public override void OnBackPressed()
        {
            //  base.OnBackPressed();
            var _intent = new Intent(this, typeof(LoginActivity));
            _intent.AddFlags(ActivityFlags.ClearTop);
            StartActivity(_intent);
            Finish();
        }


    }
}