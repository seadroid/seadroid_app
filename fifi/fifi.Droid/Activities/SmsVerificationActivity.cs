using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using fifi.ViewModel;


namespace fifi.Droid.Activities
{
    [Activity(Label = "Sms Verification",
          Theme = "@style/MyThemeDark",
          ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
          ScreenOrientation = ScreenOrientation.Portrait)]
    public class SmsVerificationActivity : BaseActivity, View.IOnTouchListener
    {
        protected override int LayoutResource => Resource.Layout.activity_sms_verification;
        public static  string phonenumber;
        private TextInputEditText _verificationPassword;
        private CreateUserViewModel _viewModel;



       

        protected  override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _viewModel = new CreateUserViewModel();
            _viewModel.PhoneNumber = phonenumber;
            _viewModel.SendVerificationCodeCommand.Execute(null);
            _verificationPassword = FindViewById<TextInputEditText>(Resource.Id.sms_verification_input_Password);
            _verificationPassword.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _verificationPassword.TextChanged += (sender, args) =>
            {
                if(!Validatepassword())return;
                _verificationPassword.SetOnTouchListener(this);

            };
        }

        private bool Validatepassword()

        {
            try
            {
                if (!string.IsNullOrWhiteSpace(_verificationPassword.Text) && _verificationPassword.Text.Length >= 4)
                {
                    _verificationPassword.SetCompoundDrawablesWithIntrinsicBounds(0, 0,
                        Resource.Drawable.continue_selector, 0);
                    return true;
                }
                _verificationPassword.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("validateFirstname" + e);
                throw;
            }
        }

        public bool OnTouch(View v, MotionEvent e)
        {
            if (_viewModel.VerificationPassword == _verificationPassword.Text)
            {
                Intent myIntent = new Intent(this, typeof(SignupActivity));
                SetResult(Result.Ok, myIntent);
                Finish();
                return true;
            }
            else
            {
                Toast.MakeText(Application.Context, "Wrong Password", ToastLength.Short).Show();
                return false;
            }
        }
    }
}