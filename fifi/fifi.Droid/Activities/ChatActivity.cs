﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.Droid.Adapters;
using fifi.Droid.Helpers;
using fifi.Helpers;
using fifi.Model;
using fifi.Utils.Helpers;
using fifi.ViewModel;
using Java.Util.Concurrent.Atomic;
using MvvmHelpers;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Environment = System.Environment;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace fifi.Droid.Activities
{

    [Activity( Label = "Chat", Theme = "@style/MyThemeDark", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class ChatActivity : BaseActivity
    {
        private SwipeRefreshLayout _refresher;
        private LinearLayoutManager _layoutManager;
        private RecyclerView _recyclerView;
        private ChatViewModel _viewModel;
        private Button _sendButton;
        private EditText _messageText; 
        private ChatAdapter _adapter;
        private int _OldMessageCount;
        private string ImageUrl;
        private string Channel;
        private string Friend_id;
        private bool _LoadingMoreChatMessages;
        private int lasttVisiblePosition;
        protected override int LayoutResource => Resource.Layout.activity_chat;
        private Android.Widget.Toolbar Appbar;

      

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            if ((int)Build.VERSION.SdkInt >= 21)
            {
                Window.SetStatusBarColor(new Color(ContextCompat.GetColor(this, Resource.Color.primary_dark)));
                Window.DecorView.SystemUiVisibility = StatusBarVisibility.Visible;
            }
            Title = "Chat: " + Intent.GetStringExtra("Friend_name") ;
            Friend_id = Intent.GetStringExtra("Friend_id");          
            ImageUrl = Intent.GetStringExtra("Friend_imageUrl");         
             
            _viewModel = new ChatViewModel(Friend_id);
            
            _LoadingMoreChatMessages = false;
            _refresher = FindViewById<SwipeRefreshLayout>(Resource.Id.chat_refresher);
            _refresher.Refresh += RefresherOnRefresh;
            _adapter = new ChatAdapter(this, _viewModel.Messages, ImageUrl, Intent.GetStringExtra("Friend_name"));
            _layoutManager =new LinearLayoutManager(this) {Orientation = LinearLayoutManager.Vertical, StackFromEnd = true};
            _recyclerView = FindViewById<RecyclerView>(Resource.Id.chat_listView);
            _recyclerView.SetLayoutManager(_layoutManager);
            _recyclerView.SetAdapter(_adapter);
 
            _messageText = FindViewById<EditText>(Resource.Id.chat_editText);
            _messageText.SetTextColor(Color.Black);
            _sendButton = FindViewById<Button>(Resource.Id.chat_sendButton);
            _sendButton.Click += (sender, args) =>
            {
                var foundtext = false;
                var textlines = _messageText.Text.Split(Environment.NewLine.ToCharArray());

                var TextlistRemoveExtraLines = new List<string>();
                var cleanedTextlines = new List<string>();
                foreach (var textline in textlines)
                {
                    if (!foundtext && !string.IsNullOrEmpty(textline))
                    {
                        TextlistRemoveExtraLines.Add(textline);
                        foundtext = true;
                    }
                    else
                    {
                        if (foundtext)
                        {
                            TextlistRemoveExtraLines.Add(textline);                       
                        }
                       
                    }
                }

                var cleanedForEmptyStartLines = string.Join("\n", TextlistRemoveExtraLines);
                var listexstraspaces = cleanedForEmptyStartLines.Split(' ');
                listexstraspaces = listexstraspaces.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                var newtext = string.Join(" ", listexstraspaces);



              

                
            //    var text = string.Join(" ", newtext);
              
                if (newtext.Length >0)
                {
                    SendMessage(newtext);
                    _messageText.Text = string.Empty;
                }
                
            };

            _recyclerView.GetLayoutManager().ScrollToPosition(_viewModel.Messages.Count);

         


        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }

            return base.OnOptionsItemSelected(item);
        }

        private void RefresherOnRefresh(object o, EventArgs eventArgs)
        {
            _OldMessageCount = _viewModel.Messages.Count;
            _viewModel.LoadMoreMessagesCommand.Execute(null);
            lasttVisiblePosition = _layoutManager.FindLastCompletelyVisibleItemPosition();
            _LoadingMoreChatMessages = true;
            _refresher.Refreshing = false;

        }

        protected override void OnStart()
        {
            base.OnStart();
            _viewModel.Messages.CollectionChanged += MessagesOnCollectionChanged;
            Settings.Current.ActiveChatId = _viewModel.Channel;
            _viewModel.StartListeningForMessagesCommand.Execute(null);
        }

        protected override void OnPause()
        {

            base.OnPause();

            if (IsTaskRoot)
            {
                Intent intent = new Intent(this, typeof(DashboardActivity)).SetFlags(ActivityFlags.NewTask | ActivityFlags.MultipleTask);
                StartActivity(intent);
            }
        }


        protected override void OnStop()
        {         
           
            _viewModel.Messages.CollectionChanged -= MessagesOnCollectionChanged;
            Settings.Current.ActiveChatId = "";
            _viewModel.StopListeningForMessagesCommand.Execute(null);

          
            base.OnStop();
        }


        private void MessagesOnCollectionChanged(object o, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            try
            {
                this.RunOnUiThread(() =>
                    {
                        _recyclerView.GetAdapter().NotifyDataSetChanged();
                        if (!_LoadingMoreChatMessages)
                        {
                            _layoutManager.ScrollToPosition(_viewModel.Messages.Count - 1);
                            
                        }
                        else
                        {
                            _LoadingMoreChatMessages = false;
                            _layoutManager.ScrollToPosition(lasttVisiblePosition + (_viewModel.Messages.Count - _OldMessageCount));
 
                            
                        }
                        
                    }
                );
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        private void SendMessage(string messageText)
        {

            _viewModel.Time = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);
            _viewModel.MessageText = messageText;
            _viewModel.SendMessageCommand.Execute(null);
        }

       
    }
}