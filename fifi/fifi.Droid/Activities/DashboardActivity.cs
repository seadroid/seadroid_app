using System;
using System.ComponentModel;
using System.Net.Mime;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.Percent;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using fifi.Droid.Fragments;
using fifi.Droid.Helpers;
using fifi.Utils.Helpers;
using fifi.ViewModel;
using Fragment = Android.Support.V4.App.Fragment;

namespace fifi.Droid.Activities
{
    [Activity(Label = "Dashboard",
        Theme = "@style/MyTheme",
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class DashboardActivity : BaseActivity
    {
        private DashboardViewModel _viewModel;
        private View _navigationheader;
        public DrawerLayout _drawerLayout;
        private NavigationView _navigationView;
        public ImageView _imageview;
        public TextView _userName;
        public TextView _groupName;
        private int _oldPosition = -1;
        

        protected override int LayoutResource => Resource.Layout.activity_dashboard;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            _viewModel = new DashboardViewModel();
            _drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            // Set hamburger items menu
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);

            _navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            _navigationheader = _navigationView.GetHeaderView(0);
            _imageview = _navigationheader.FindViewById<ImageView>(Resource.Id.navigation_drawer_user_account_picture_profile);
            _imageview.Click += (sender, args) =>
            {
                ListItemClicked(Resource.Id.menu_profile);
                SupportActionBar.Title = "Profile";
                _drawerLayout.CloseDrawers();
            };

            if (!string.IsNullOrEmpty(Settings.Current.UserImageBase64))
            {
                Task task = new Task(() =>
                {
                    var bytes = Convert.FromBase64String(Settings.Current.UserImageBase64);
                    var bitmap = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                    var circleImage = new CircleDrawable(bitmap);
                    using (var h = new Handler(Looper.MainLooper))
                    {
                        h.Post((() =>
                        {
                            _imageview.SetImageDrawable(circleImage);

                        }));
                    }
                });
                task.Start();
               
        
            }
            
            _userName = _navigationheader.FindViewById<TextView>(Resource.Id.navigation_drawer_account_information_name);
            _userName.Text = Settings.Current.UserFirstname + " " + Settings.Current.UserLastname;


           

            _navigationView.NavigationItemSelected += (sender, e) =>
            {
                e.MenuItem.SetChecked(true);

                ListItemClicked(e.MenuItem.ItemId);


                SupportActionBar.Title = e.MenuItem.ItemId == Resource.Id.menu_dashboard
                    ? string.Empty
                    : e.MenuItem.TitleFormatted.ToString();

                _drawerLayout.CloseDrawers();
            };

            ListItemClicked(Resource.Id.menu_dashboard);

            _viewModel.PropertyChanged += ViewModel_PropertyChanged;

          
          

        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (!_viewModel.IsLoggedOut)
            {
                return;
            }

            var intent = new Intent(this, typeof(LoginActivity));
            intent.AddFlags(ActivityFlags.ClearTop);
            StartActivity(intent);
            Finish();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {

            
            switch (item.ItemId)
            {                
                case Android.Resource.Id.Home:
                    _drawerLayout.OpenDrawer(GravityCompat.Start);
                    return true;
            }
            
            return base.OnOptionsItemSelected(item);
        }

        public void ListItemClicked(int itemId)
        {
            if (itemId == _oldPosition)
            {
                return;
            }
            _oldPosition = itemId;

            Fragment fragment = null;
            switch (itemId)
            {
                case Resource.Id.menu_dashboard:
                    fragment = FragmentDashboard.NewInstance();
                    break;
                case Resource.Id.menu_maps:
                    fragment = FragmentMaps.NewInstance();
                    break;
                case Resource.Id.menu_signout:
                    SignOut();
                    break;                
                case Resource.Id.menu_friends:
                    fragment = FragmentFriends.NewInstance();
                    break;
                case Resource.Id.menu_devices:
                    fragment = FragmentDevices.NewInstance();
                    break;
                case Resource.Id.menu_pairing:
                    fragment = FragmentArmbands.NewInstance();
                    break;
                case Resource.Id.menu_invite:
                    fragment = FragmentInvite.NewInstance();
                    break;
                case Resource.Id.menu_profile:
                    fragment = FragmentUserSettings.NewInstance();
                    break;
                case Resource.Id.menu_messages:
                    fragment = FragmentChatHub.newInstance();
                    break;
                case Resource.Id.menu_help:
                     //IntercomSingletonAndroid.Instance.Displaymessage();
                    break;
                default:               
                    Snackbar.Make(_drawerLayout, "Yderligere views kommer senere...", Snackbar.LengthShort).Show();
                    break;
            }

            if (fragment != null)
            {
                SupportFragmentManager.BeginTransaction()
                    .Replace(Resource.Id.content_frame, fragment, "frag")
                    .Commit();
            }

            _navigationView.SetCheckedItem(itemId);
        }

     

        private void SignOut()
        {
            _viewModel.LogoutCommand.Execute(null);
        }

        public override void OnBackPressed()
        {
            if (!_drawerLayout.IsDrawerOpen((int) GravityFlags.Start))
                _drawerLayout.OpenDrawer(GravityCompat.Start);
            else
                MoveTaskToBack(true);       
                //base.OnBackPressed();
        }
    }
}