﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Android.Content;
using Android.Content.Res;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.Client.DataObjects;
using fifi.Droid.Adapters;
using fifi.Helpers;
using Mapbox.Annotations;
using Mapbox.Camera;
using Mapbox.Maps;
using Mapbox.Geometry;

namespace fifi.Droid.ViewHolders
{
    public class DeviceDetailsViewHolder : RecyclerView.ViewHolder
    {
        public TextView Title { get; set; }
        public TextView StartedAt { get; set; }
        public TextView EndedAt { get; set; }
       
        protected Trip Trip;

        private readonly Context _context;

        public DeviceDetailsViewHolder(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public DeviceDetailsViewHolder(View itemView) : base(itemView)
        {
        }

        public DeviceDetailsViewHolder(Context context, View itemView, Action<DeviceDetailsClickArgs> listener) : this(itemView)
        {
            _context = context;

            Title = itemView.FindViewById<TextView>(Resource.Id.text_title);
            StartedAt = itemView.FindViewById<TextView>(Resource.Id.text_started_at);
            EndedAt = itemView.FindViewById<TextView>(Resource.Id.text_ended_at);
            //MapView = itemView.FindViewById<MapView>(Resource.Id.mapView);
            itemView.Click +=
                (sender, e) => listener(new DeviceDetailsClickArgs { View = sender as View, Position = AdapterPosition });

            //MapView.OnCreate(null);
            //MapView.GetMap(this);
        }

        //public void SetMapLocation(_trip trip)
        //{
        //    _trip = trip;

        //    // If the map is ready, update its content.
        //    if (Map != null)
        //    {
        //        UpdateMapContents();
        //    }
        //}


      



        //public void OnMapReady(MapboxMap map)
        //{
        //    Map = map;
           
        //    // If we have map data, update the map content
        //    if (_trip != null)
        //    {
        //        UpdateMapContents();
        //    }
        //}

        //protected void UpdateMapContents()
        //{
        //    // Since the MapView is reused, we need to remove pre-existing MapView features.
        //    Map.Clear();
        //    Map.MoveCamera(CameraUpdateFactory.NewLatLngZoom(new LatLng(0, 0), 0.0f));

        //    // Update the MapView feature data and camera position
        //    if (_trip.Coordinates.Count == 0 || _trip.Coordinates.Count < 0)
        //    {
        //        return;
        //    }
            
        //    if (_trip.Coordinates.Count <= 0)
        //    {
        //        return;
        //    }

        //  //  _trip.Coordinates.HaversineDistance();
              

        //    var iconFactory = IconFactory.GetInstance(_context);
        //    var logicalDensity = Resources.System.DisplayMetrics.Density;
        //    var thicknessPoints = (int)Math.Ceiling(20 * logicalDensity + 0.5f);

        //    var start =
        //        _trip.Coordinates.LastOrDefault(
        //            c =>
        //                c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
        //                c.Longitude.Where(char.IsDigit).Any(x => x != '0'));
        //    var end =
        //        _trip.Coordinates.FirstOrDefault(
        //            c =>
        //                c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
        //                c.Longitude.Where(char.IsDigit).Any(x => x != '0'));

        //    if (start == null || end == null)
        //    {
        //        return;
        //    }


            


        //    //var drawable = ContextCompat.GetDrawable(_context, Resource.Drawable.ic_start_point) as BitmapDrawable;
        //    //var finalIcon =
        //    //    iconFactory.FromBitmap(Bitmap.CreateScaledBitmap(drawable?.Bitmap, thicknessPoints, thicknessPoints,
        //    //        false));
        //    var drawable = ContextCompat.GetDrawable(_context, Resource.Drawable.ic_start_point);
        //    var finalIcon = iconFactory.FromDrawable(drawable, thicknessPoints, thicknessPoints);

        //    var startMarker = new MarkerViewOptions();
        //    startMarker.SetPosition(new LatLng(double.Parse(start.Latitude.Replace(".", ",")), double.Parse(start.Longitude.Replace(".", ","))));
        //    startMarker.SetIcon(finalIcon);
        //    startMarker.SetAnchor(0.5f, 0.5f);

        //    //drawable = ContextCompat.GetDrawable(_context, Resource.Drawable.ic_end_point) as BitmapDrawable;
        //    //finalIcon =
        //    //    iconFactory.FromBitmap(Bitmap.CreateScaledBitmap(drawable?.Bitmap, thicknessPoints, thicknessPoints,
        //    //        false));
        //    drawable = ContextCompat.GetDrawable(_context, Resource.Drawable.ic_end_point);
        //    finalIcon = iconFactory.FromDrawable(drawable, thicknessPoints, thicknessPoints);

        //    var endMarker = new MarkerViewOptions();
        //    endMarker.SetPosition(new LatLng(double.Parse(end.Latitude.Replace(".", ",")), double.Parse(end.Longitude.Replace(".", ","))));
        //    endMarker.SetIcon(finalIcon);
        //    endMarker.SetAnchor(0.5f, 0.5f);

        //    if (!_trip.Coordinates.Any())
        //    {
        //        return;
        //    }

        //    var points =
        //        _trip.Coordinates.Where(
        //                c =>
        //                    c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
        //                    c.Longitude.Where(char.IsDigit).Any(x => x != '0'))
        //            .Select(
        //                s =>
        //                    new LatLng(double.Parse(s.Latitude.Replace(".", ",")),
        //                        double.Parse(s.Longitude.Replace(".", ","))))
        //            .ToArray();

        //    var rectOptions = new PolylineOptions();
        //    rectOptions.Add(points);
        //    rectOptions.SetColor(ContextCompat.GetColor(_context, Resource.Color.primary_dark));
        //    rectOptions.SetWidth(6f);
        //    Map.AddPolyline(rectOptions);
            
        //    Map.AddMarker(startMarker);
        //    Map.AddMarker(endMarker);

        //    if (points.Length <= 0)
        //    {
        //        return;
        //    }

        //    var boundsPoints = new LatLngBounds.Builder();

        //    foreach (var point in points)
        //    {
        //        boundsPoints.Include(point);
        //    }

        //    var bounds = boundsPoints.Build();
        //    Map.MoveCamera(CameraUpdateFactory.NewLatLngBounds(bounds, 64));
        //}
    }


}