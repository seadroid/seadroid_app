﻿using System;
using System.Net;
using System.Security.Cryptography;
using Android.Graphics;
using Java.Lang;

namespace fifi.Droid.Helpers
{
    
    public class ImageUrlDownloader
    {
        private string base64imageBytes;
        public string GetImageBitmapFromUrl(string url)
        {
            using (var webClient = new WebClient())
            {
                if (url != null)
                {
                    var imageBytes = webClient.DownloadData(url);
                    base64imageBytes = Convert.ToBase64String(imageBytes);
                }               
            }
            return base64imageBytes;
        }
    }
}