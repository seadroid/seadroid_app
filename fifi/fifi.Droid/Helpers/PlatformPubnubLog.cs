﻿using System.Diagnostics;
using PubnubApi;

namespace fifi.Droid.Helpers
{
    public class AndroidPubnubLog : IPubnubLog
    {
        private string logFilePath = "";

        public AndroidPubnubLog()
        {
            // Get folder path may vary based on environment
            string folder = System.IO.Directory.GetCurrentDirectory(); //For console
            //string folder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // For iOS
            System.Diagnostics.Debug.WriteLine(folder);
            logFilePath = System.IO.Path.Combine(folder, "pubnubmessaging.log");
            Trace.Listeners.Add(new TextWriterTraceListener(logFilePath));
        }

        public void WriteToLog(string log)
        {
            
            Trace.WriteLine(log);
            Trace.Flush();
        }
    }
}