﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using fifi.Droid.Activities;
using fifi.Droid.ViewHolders;
using fifi.ViewModel;
using Mapbox.Maps;

namespace fifi.Droid.Adapters
{
    public class DeviceDetailsAdapter : RecyclerView.Adapter
    {
        private readonly DeviceDetailsActivity _activity;
        private readonly DeviceDetailsViewModel _viewModel;

        //protected HashSet<MapView> MapViews = new HashSet<MapView>();

        public EventHandler<DeviceDetailsClickArgs> ItemClick;

        public DeviceDetailsAdapter(DeviceDetailsActivity activity, DeviceDetailsViewModel viewModel)
        {
            _activity = activity;
            _viewModel = viewModel;

            _viewModel.Trips.CollectionChanged += (sender, e) => { _activity.RunOnUiThread(NotifyDataSetChanged); };
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var vh = holder as DeviceDetailsViewHolder;
            if (vh == null)
            {
                return;
            }

            var trip = _viewModel.Trips[position];
           // vh.Title.Text = trip.Id.ToString();
            vh.Title.Text = position.ToString();
            vh.StartedAt.Text = trip.StartedAt;
            var tripTime = (Convert.ToDateTime(trip.EndedAt) - Convert.ToDateTime(trip.StartedAt));


            vh.EndedAt.Text = string.IsNullOrWhiteSpace(trip.EndedAt) ? "Not Ended" : Convert.ToString(tripTime);

           // vh.SetMapLocation(trip);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_device_detail, parent, false);
            var viewHolder = new DeviceDetailsViewHolder(parent.Context, view, OnClick);

           // MapViews.Add(viewHolder.MapView);

            return viewHolder;
        }

        public override int ItemCount => _viewModel.Trips.Count;

        //public HashSet<MapView> GetMapViews()
        //{
        //    return MapViews;
        //}

        private void OnClick(DeviceDetailsClickArgs args)
        {
            ItemClick?.Invoke(this, args);
        }
    }

    public class DeviceDetailsClickArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}