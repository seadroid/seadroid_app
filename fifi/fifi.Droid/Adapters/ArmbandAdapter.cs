﻿using System;
using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.ViewModel;

namespace fifi.Droid.Adapters
{
    public class ArmbandAdapter: RecyclerView.Adapter
    {
        private readonly Android.App.Activity _activity;
        private readonly ArmbandViewModel _viewModel;

        public ArmbandAdapter(Android.App.Activity activity, ArmbandViewModel viewmodel)
        {
            _viewModel = viewmodel;
            _activity = activity;

            _viewModel.Armbands.CollectionChanged += (sender, args) => {_activity.RunOnUiThread(NotifyDataSetChanged); };
        }

        public override int ItemCount => _viewModel.Armbands.Count;

        public event EventHandler<ArmbandClickEventArgs> ItemClick;
        public event EventHandler<ArmbandClickEventArgs> ItemLongClick;

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            => new ArmbandViewHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_pairing, parent, false), Onclick);

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var vh = holder as ArmbandViewHolder;
            if (vh == null)
            {
                return;
            }

            var armband = _viewModel.Armbands[position];
            vh.Name.Text = armband.firstname + " "+ armband.lastname;
            vh.BatteryImage.SetColorFilter(Color.ParseColor("#FFFFFF"));
            vh.SignalImage.SetColorFilter(Color.ParseColor("#FFFFFF"));
            vh.BatteryText.Text = string.Empty;


            if (armband.Signal && !string.IsNullOrEmpty(armband.Battery))
            {
                vh.BatteryText.Text = armband.Battery + "%";


                if (Convert.ToInt32(armband.Battery) <= 20)
                {
                    vh.BatteryImage.SetColorFilter(Color.ParseColor("#cf3619"));
                }
                if (Convert.ToInt32(armband.Battery) > 20 && Convert.ToInt32(armband.Battery) <= 30)
                {
                    vh.BatteryImage.SetColorFilter(Color.ParseColor("#ffe70f"));

                }
                if (Convert.ToInt32(armband.Battery) > 30)
                { 
                    vh.BatteryImage.SetColorFilter(Color.ParseColor("#00b157"));
                }

                vh.SignalImage.SetColorFilter(Color.ParseColor("#00b157"));

            }

        }
   

        private void Onclick(ArmbandClickEventArgs args)
        {
            ItemClick?.Invoke(this, args);

        }

        public class ArmbandClickEventArgs : EventArgs
        {
            public View View { get; set; }
            public int Position { get; set; }
        }

        internal class ArmbandViewHolder : RecyclerView.ViewHolder
        {
            public ArmbandViewHolder(View itemView, Action<ArmbandClickEventArgs> listener) : base(itemView)
            {
                Name = ItemView.FindViewById<TextView>(Resource.Id.Text_Bluetoothid);
                BatteryText = itemView.FindViewById<TextView>(Resource.Id.Text_power);
                BatteryImage = ItemView.FindViewById<ImageView>(Resource.Id.battery_Image);
                SignalImage = itemView.FindViewById<ImageView>(Resource.Id.Signal);
                itemView.Click +=(sender, e) => listener(new ArmbandClickEventArgs { View = sender as View, Position = AdapterPosition });
            }


            public TextView Name { get; set; }
            public TextView BatteryText { get; set; }
            public ImageView BatteryImage { get; set; }
            public ImageView SignalImage { get; set; }
        }

    }
}