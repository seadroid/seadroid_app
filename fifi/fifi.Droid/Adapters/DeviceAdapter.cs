﻿using System;
using System.Linq;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.ViewModel;

namespace fifi.Droid.Adapters
{
    public class DeviceAdapter : RecyclerView.Adapter
    {
        private readonly Android.App.Activity _activity;
        private readonly DevicesViewModel _viewModel;

        public DeviceAdapter(Android.App.Activity activity, DevicesViewModel viewModel)
        {
            _activity = activity;
            _viewModel = viewModel;

            _viewModel.Masters.CollectionChanged += (sender, e) => { _activity.RunOnUiThread(NotifyDataSetChanged); };
        }

        public override int ItemCount => _viewModel.Masters.Count;

        public event EventHandler<MasterClickEventArgs> ItemClick;
        public event EventHandler<MasterClickEventArgs> ItemLongClick;

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            =>
            new DeviceViewHolder(
                LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_device, parent, false), OnClick,
                OnClickLong);

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {

            var vh = holder as DeviceViewHolder;
            if (vh == null)
            {
                return;
            }

            var master = _viewModel.Masters[position];
            vh.Title.Text = master.Name;
            vh.Trips.Text = master.Trips.Count.ToString();
            vh.Date.Text = master.Trips.LastOrDefault()?.UpdatedAt;
        }

        private void OnClick(MasterClickEventArgs args)
        {
            ItemClick?.Invoke(this, args);
        }

        private void OnClickLong(MasterClickEventArgs args)
        {
            ItemLongClick?.Invoke(this, args);
        }
    }

    public class MasterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }

    public class DeviceViewHolder : RecyclerView.ViewHolder
    {
        public DeviceViewHolder(View itemView, Action<MasterClickEventArgs> listener, Action<MasterClickEventArgs> listenerLong) : base(itemView)
        {
            itemView.LongClickable = true;
            Title = itemView.FindViewById<TextView>(Resource.Id.text_title);
            Trips = itemView.FindViewById<TextView>(Resource.Id.text_trips);
            Date = itemView.FindViewById<TextView>(Resource.Id.text_date);
            itemView.Click +=
                (sender, e) => listener(new MasterClickEventArgs { View = sender as View, Position = AdapterPosition });
            itemView.LongClick +=
                (sender, e) =>
                        listenerLong(new MasterClickEventArgs { View = sender as View, Position = AdapterPosition });
        }

        public TextView Title { get; set; }
        public TextView Trips { get; set; }
        public TextView Date { get; set; }
    }
}