﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Views;
using Android.Widget;
using fifi.Client.DataObjects;
using fifi.Droid.Helpers;
using fifi.Model;
using fifi.Utils.Helpers;
using MvvmHelpers;

namespace fifi.Droid.Adapters
{
    public class ChatAdapter : RecyclerView.Adapter
    {

        private Activity _context;
        private ObservableRangeCollection<ChatMessage> _MessageList;
        public  const  int UserMessage = 0;
        private CircleDrawable Image;
        private string FriendName;
        public const  int FriendMessage = 1;
     

        public ChatAdapter(Android.App.Activity activity, ObservableRangeCollection<ChatMessage> messages, string imageurl, string friendName)
        {
            _context = activity;
            _MessageList = messages;

            if (!string.IsNullOrEmpty(imageurl))
            {
                var imagebytes = Convert.FromBase64String(new ImageUrlDownloader().GetImageBitmapFromUrl(imageurl));
                var bitmap = BitmapFactory.DecodeByteArray(imagebytes, 0, imagebytes.Length);
                Image = new CircleDrawable(bitmap);
                FriendName = friendName;
            }
                 
        }
        

    

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
           
            var message = _MessageList[position];
            if (message.Sender_id == Settings.Current.UserId)
            {
                var vh = holder as UserChatViewHolder;                
                vh.Message.Text = message.Text;
                vh.Username.Text = Settings.Current.UserFirstname + " " + Settings.Current.UserLastname;
                try
                {
                    vh.Date.Text = Convert.ToString(TimeZoneInfo.ConvertTimeFromUtc(message.Date, TimeZoneInfo.Local));
                }
                catch (Exception e)
                {
                    vh.Date.Text = Convert.ToString(message.Date);
                }
                        

            }
            else
            {
                var vh = holder as FriendChatViewHolder;
                vh.Message.Text = message.Text;
                vh.Username.Text = FriendName;
                vh.Date.Text = Convert.ToString(TimeZoneInfo.ConvertTimeFromUtc(message.Date, TimeZoneInfo.Local));             

                if (Image != null)
                {                   
                    vh.UserImage.SetImageDrawable(Image);
                }
                
            }
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            switch (viewType)
            {
                case UserMessage:
                  return  new UserChatViewHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_right_chatbox, parent, false));
                  
                case FriendMessage:
                   return new FriendChatViewHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_left_chatbox, parent, false));
                 
            }
            return null;
        }

        public override int ItemCount => _MessageList.Count;

        public override int GetItemViewType(int position)
        {
            var message = _MessageList[position];
            if (message.Sender_id == Settings.Current.UserId)
            {
                return UserMessage;
            }
            else
            {
               return FriendMessage;
            }
        }
    }

    public class UserChatViewHolder : RecyclerView.ViewHolder
    {
        public UserChatViewHolder(View itemView) : base(itemView)
        {
            itemView.LongClickable = false;               
            itemView.Clickable = false;           
            Username = itemView.FindViewById<TextView>(Resource.Id.right_text_box_userName);
            Username.SetTextColor(Color.Black);
            Message = itemView.FindViewById<TextView>(Resource.Id.right_text_box_message);
            Message.SetBackgroundColor(Color.ParseColor("#4091df"));
            Message.SetTextColor(Color.White);
            Date = itemView.FindViewById<TextView>(Resource.Id.right_text_box_date);
            Date.SetTextColor(Color.Black);

        }

        public TextView Username { get; set; }
        public TextView Message { get; set; }
        public TextView Date { get; set; }

    }
    public class FriendChatViewHolder : RecyclerView.ViewHolder
    {
        public FriendChatViewHolder(View itemView) : base(itemView)
        {
            itemView.LongClickable = false;
            itemView.Clickable = false;
            Username = itemView.FindViewById<TextView>(Resource.Id.left_text_box_userName);
            Username.SetTextColor(Color.Black);
            Message = itemView.FindViewById<TextView>(Resource.Id.left_text_box_message);
            Message.SetBackgroundColor(Color.ParseColor("#f2f2f2"));       
            Message.SetTextColor(Color.Black);
            UserImage = itemView.FindViewById<ImageView>(Resource.Id.left_chat_box_image);
            Date = itemView.FindViewById<TextView>(Resource.Id.left_text_box_date);
            Date.SetTextColor(Color.Black);

        }

    
        public ImageView UserImage { get; set; }
        public TextView Username { get; set; }
        public TextView Message { get; set; }
        public TextView Date { get; set; }  
    }
}
