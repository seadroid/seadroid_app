using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.ViewModel;
using Mapbox.Annotations;
using Object = Java.Lang.Object;

namespace fifi.Droid.Adapters
{
    class PairingAdapter: RecyclerView.Adapter
    {
        private readonly Android.App.Activity _activity;
        private readonly PairingViewModel _viewModel;

        public PairingAdapter(Android.App.Activity activity , PairingViewModel viewModel)
        {
            _activity = activity;
            _viewModel = viewModel;

            _viewModel.Bluetoothdevices.CollectionChanged += (sender, e) => { _activity.RunOnUiThread(NotifyDataSetChanged);};
        }

        public override int ItemCount => _viewModel.FoundDevices.Count;

        public event EventHandler<PairingDeviceClickEventArgs> ItemClick;
        public event EventHandler<PairingDeviceClickEventArgs> ItemLongClick;


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)    
        => new PairingViewHolder( LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_pairing, parent, false), Onclick);
    

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var vh = holder as PairingViewHolder;
            if (vh == null)
            {
                return;
            }
          
            var armband = _viewModel.Bluetoothdevices[position];
            vh.Bluetoothid.Text = armband.DeviceName;
            vh.BatteryText.Text = armband.BatteryLevel + "%";
            {
                if (Convert.ToInt32(armband.BatteryLevel) <= 20)
                {
                    vh.BatteryImage.SetColorFilter(Color.ParseColor("#cf3619"));
                }
                if (Convert.ToInt32(armband.BatteryLevel) > 20 && Convert.ToInt32(armband.BatteryLevel) <= 30)
                {
                    vh.BatteryImage.SetColorFilter(Color.ParseColor("#ffe70f"));

                }
                if (Convert.ToInt32(armband.BatteryLevel) > 30)
                {
                    vh.BatteryImage.SetColorFilter(Color.ParseColor("#00b157"));
                }

                vh.SignalImage.SetColorFilter(Color.ParseColor("#00b157"));
            }
        }

       

        private void Onclick(PairingDeviceClickEventArgs args)
        {
           ItemClick?.Invoke(this, args);
           
        }

        
        public class PairingDeviceClickEventArgs : EventArgs
        {
            public View View { get; set; }
            public int Position { get; set; }
        }

      

        public class PairingViewHolder : RecyclerView.ViewHolder
        {
            public PairingViewHolder(View itemView, Action<PairingDeviceClickEventArgs> listener): base(itemView)
            {
                Bluetoothid = ItemView.FindViewById<TextView>(Resource.Id.Text_Bluetoothid);
                itemView.Click += (sender, e) => listener(new PairingDeviceClickEventArgs { View = sender as View, Position = AdapterPosition });
                BatteryText = ItemView.FindViewById<TextView>(Resource.Id.Text_power);
                BatteryImage = itemView.FindViewById<ImageView>(Resource.Id.battery_Image);
                SignalImage = itemView.FindViewById<ImageView>(Resource.Id.Signal);
            }

        
            public TextView Bluetoothid { get; set; }
            public TextView BatteryText { get; set; }
            public ImageView SignalImage { get; set; }
            public ImageView BatteryImage { get; set; } 
        }

    }
}