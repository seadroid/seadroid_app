﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.Droid.Helpers;
using fifi.ViewModel;

namespace fifi.Droid.Adapters
{
    public class FriendRequestsRecivedAdapter : RecyclerView.Adapter
    {
        private readonly Android.App.Activity _activity;
        private readonly FriendViewModel _viewModel;


        public FriendRequestsRecivedAdapter(Android.App.Activity activity, FriendViewModel viewModel)
        {
            _activity = activity;
            _viewModel = viewModel;
            _viewModel.RecivedFriendRequests.CollectionChanged += (sender, e) => { _activity.RunOnUiThread(NotifyDataSetChanged); };
        }


        public event EventHandler<FriendRequestRecivedClickEventArgs> ItemClick;
        public event EventHandler<FriendRequestRecivedClickEventArgs> ItemLongClick;




        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var vh = holder as FriendRequestsRecivedViewHolder;
            if (vh == null)
            {
                return;
            }
            if (_viewModel.RecivedFriendRequests.Count > 0)
            {
                var friend = _viewModel.RecivedFriendRequests[position];
                // vh.boatname =
                vh.Image.SetImageResource(Resource.Drawable.ic_friends_ghost_image_blue3x);

                ImageUrlDownloader downloader = new ImageUrlDownloader();
                if (!string.IsNullOrEmpty(friend.image))
                {
                    var a = new Task((() =>
                    {
                        vh.Image.SetAdjustViewBounds(true);
                        var base64image = downloader.GetImageBitmapFromUrl(friend.image);
                        var imagebytes = Convert.FromBase64String(base64image);
                        var bitmap = BitmapFactory.DecodeByteArray(imagebytes, 0, imagebytes.Length);
                        var roundimage = new CircleDrawable(bitmap);
                        using (var h = new Handler(Looper.MainLooper))
                        {
                            h.Post((() =>
                            {
                                vh.Image.SetImageDrawable(roundimage);
                                ;
                            }));
                        }

                    }), CancellationToken.None);
                    a.Start();


                }

                vh.Username.Text = friend.firstname + " " + friend.lastname;
            }

            
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        => new FriendRequestsRecivedViewHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_friend_request_recieved, parent, false), Onclick);



        private void Onclick(FriendRequestRecivedClickEventArgs args)
        {
            ItemClick?.Invoke(this, args);
        }

        public class FriendRequestRecivedClickEventArgs : EventArgs
        {
            public View View { get; set; }
            public int Position { get; set; }
            public bool Result { get; set; }

        }

        public override int ItemCount => _viewModel.RecivedFriendRequests.Count;
    }

    public class FriendRequestsRecivedViewHolder : RecyclerView.ViewHolder
    {
        public FriendRequestsRecivedViewHolder(View itemView, Action<FriendRequestsRecivedAdapter.FriendRequestRecivedClickEventArgs> listener) : base(itemView)
        {
            Username = ItemView.FindViewById<TextView>(Resource.Id.friendRequest_recived_friend_text_username);
            Boatname = ItemView.FindViewById<TextView>(Resource.Id.friendRequest_recived_friend_text_boat_name);
            Image = ItemView.FindViewById<ImageView>(Resource.Id.friend_request_recieved_image);
            DeclineFriendRequestButton = itemView.FindViewById<Button>(Resource.Id.Decline_friend_request_button);
            ConfirmFriendRequestButton = itemView.FindViewById<Button>(Resource.Id.Confirm_friend_request_button);

            DeclineFriendRequestButton.Click +=
            (sender, e) => listener(new FriendRequestsRecivedAdapter.FriendRequestRecivedClickEventArgs { View = sender as View, Position = AdapterPosition, Result = false});
            ConfirmFriendRequestButton.Click += (sender, e) => listener(new FriendRequestsRecivedAdapter.FriendRequestRecivedClickEventArgs { View = sender as View, Position = AdapterPosition, Result = true });

        }

        public TextView Boatname { get; set; }
        public TextView Username { get; set; }
        public ImageView Image { get; set; }
        public Button DeclineFriendRequestButton { get; set; }
        public Button ConfirmFriendRequestButton { get; set; }
    }
}