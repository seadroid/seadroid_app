﻿using System;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.Droid.Helpers;
using fifi.ViewModel;

namespace fifi.Droid.Adapters
{
    public class AddFriendsAdapter : RecyclerView.Adapter
    {
        private readonly Android.App.Activity _activity;
        private readonly FriendViewModel _viewModel;


        public AddFriendsAdapter(Android.App.Activity activity, FriendViewModel viewModel)
        {
            _activity = activity;
            _viewModel = viewModel;
            _viewModel.NotFriends.CollectionChanged += (sender, e) =>
            {
                _activity.RunOnUiThread(NotifyDataSetChanged);
            };
        }


        public event EventHandler<AddFriendClickEventArgs> ItemClick;
        public event EventHandler<AddFriendClickEventArgs> ItemLongClick;


        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var vh = holder as AddFriendsViewHolder;
            if (vh == null)
            {
                return;
            }

            var user = _viewModel.NotFriends[position];
            // vh.boatname =
           
            if (!string.IsNullOrEmpty(user.image))
            {

                using (var h = new Handler(Looper.MainLooper))
                {

                    h.Post((() =>
                    {
                        vh.Image.SetAdjustViewBounds(true);
                        var base64image = new ImageUrlDownloader().GetImageBitmapFromUrl(user.image);
                        var imagebytes = Convert.FromBase64String(base64image);
                        var bitmap = BitmapFactory.DecodeByteArray(imagebytes, 0, imagebytes.Length);
                        var roundimage = new CircleDrawable(bitmap);
                        vh.Image.SetImageDrawable(roundimage);

                    }));
                }

               

            }
            else
            {     
                vh.Image.SetImageResource(Resource.Drawable.ic_friends_ghost_image_blue3x);              
            }
            vh.Username.Text = user.firstname + " " + user.lastname;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            => new AddFriendsViewHolder(
                LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_add_friend, parent, false), Onclick);


        private void Onclick(AddFriendClickEventArgs args)
        {
            ItemClick?.Invoke(this, args);
        }

        public class AddFriendClickEventArgs : EventArgs
        {
            public View View { get; set; }
            public int Position { get; set; }
        }

        public override int ItemCount => _viewModel.NotFriends.Count;
    }

  



    public class AddFriendsViewHolder : RecyclerView.ViewHolder
    {
        public AddFriendsViewHolder(View itemView,
            Action<AddFriendsAdapter.AddFriendClickEventArgs> listener) : base(itemView)
        {
            Username = ItemView.FindViewById<TextView>(Resource.Id.add_friend_text_username);
            Boatname = ItemView.FindViewById<TextView>(Resource.Id.add_friend_text_boat_name);
            Image = ItemView.FindViewById<ImageView>(Resource.Id.add_friend_image);
            Add_friend_button = ItemView.FindViewById<Button>(Resource.Id.add_friend_button);
            Add_friend_button.Click +=
                (sender, e) => listener(
                    new AddFriendsAdapter.AddFriendClickEventArgs()
                    {
                        View = sender as View,
                        Position = AdapterPosition
                    });
        }

        public TextView Username { get; set; }
        public TextView Boatname { get; set; }
        public ImageView Image { get; set; }
        public Button Add_friend_button { get; set; }
    }
}