﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.Droid.Helpers;
using fifi.ViewModel;

namespace fifi.Droid.Adapters
{
    public class ChatHubAdapter: RecyclerView.Adapter
    {

        private readonly Android.App.Activity _activity;
        private readonly ChatHubViewModel _viewModel;


        public ChatHubAdapter(Android.App.Activity activity, ChatHubViewModel viewModel)
        {
            _activity = activity;
            _viewModel = viewModel;
            _viewModel.Friends.CollectionChanged += (sender, e) =>
            {
                _activity.RunOnUiThread(NotifyDataSetChanged);
            };
        }


        public event EventHandler<FriendClickEventArgs> ItemClick;
        public event EventHandler<FriendClickEventArgs> ItemLongClick;





        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var vh = holder as ChatHubFriendsViewHolder;
            if (vh == null)
            {
                return;
            }

            var friend = _viewModel.Friends[position];
            vh.Image.SetImageResource(Resource.Drawable.ic_friends_ghost_image_blue3x);

            ImageUrlDownloader downloader = new ImageUrlDownloader();
            if (!string.IsNullOrEmpty(friend.image))
            {
                var a = new Task((() =>
                {
                    vh.Image.SetAdjustViewBounds(true);
                    var base64image = downloader.GetImageBitmapFromUrl(friend.image);
                    var imagebytes = Convert.FromBase64String(base64image);
                    var bitmap = BitmapFactory.DecodeByteArray(imagebytes, 0, imagebytes.Length);
                    var roundimage = new CircleDrawable(bitmap);
                    using (var h = new Handler(Looper.MainLooper))
                    {
                        h.Post((() =>
                        {
                            vh.Image.SetImageDrawable(roundimage);
                            ;
                        }));
                    }

                }), CancellationToken.None);
                a.Start();
              

            }          
            vh.Username.Text = friend.firstname + " " + friend.lastname;
            vh.LastMessage.Text = _viewModel.Friends[position].latest_message;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            => new ChatHubFriendsViewHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_chat_hub, parent, false), Onclick, OnClickLong);



        private void Onclick(FriendClickEventArgs args)
        {
            ItemClick?.Invoke(this, args);
        }

        private void OnClickLong(FriendClickEventArgs args)
        {
            ItemLongClick?.Invoke(this, args);
        }

        public class FriendClickEventArgs : EventArgs
        {
            public View View { get; set; }
            public int Position { get; set; }
        }

        public override int ItemCount => _viewModel.Friends.Count;
    }

    public class ChatHubFriendsViewHolder : RecyclerView.ViewHolder
    {
        public ChatHubFriendsViewHolder(View itemView, Action<ChatHubAdapter.FriendClickEventArgs> listener, Action<ChatHubAdapter.FriendClickEventArgs> listenerLong) : base(itemView)
        {
            Username = ItemView.FindViewById<TextView>(Resource.Id.chat_hub_friend_text_username);
            Boatname = ItemView.FindViewById<TextView>(Resource.Id.chat_hub_friend_text_boat_name);
            Image = ItemView.FindViewById<ImageView>(Resource.Id.chat_hub_friend_image);
            LastMessage = itemView.FindViewById<TextView>(Resource.Id.chat_hub_friend_sms_text);
            itemView.Click += (sender, e) => listener(new ChatHubAdapter.FriendClickEventArgs { View = sender as View, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => listenerLong(new ChatHubAdapter.FriendClickEventArgs { View = sender as View, Position = AdapterPosition });
        }

        public TextView Boatname { get; set; }
        public TextView Username { get; set; }
        public ImageView Image { get; set; }
        public TextView LastMessage { get; set; }
    }
}