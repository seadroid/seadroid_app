﻿using System;
using System.Collections;
using System.Net.Mime;
using Android.Content;
using Android.Media;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using fifi.Model;
using Java.Interop;
using Object = Java.Lang.Object;

namespace fifi.Droid.Adapters
{
    public class SpinnerPictureAdapter : BaseAdapter
    {
        private Context C;
        private JavaList<Flag> flags;
        private LayoutInflater inflater;

        public SpinnerPictureAdapter(Context context,JavaList<Flag> images)
        {
            flags = images;
            C = context;
        }


        public override Object GetItem(int position)
        {
            return flags.Get(position);
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (inflater == null )
            {
                inflater = (LayoutInflater) C.GetSystemService(Context.LayoutInflaterService);
            }
            if (convertView == null)
            {
                convertView = inflater.Inflate(Resource.Layout.item_spinner_picture, parent, false);
            }
            ImageView img = convertView.FindViewById<ImageView>(Resource.Id.spinner_flag_picture);
            img.SetImageResource(flags[position].Image);
            return convertView;
        }

        public override int Count { get { return flags.Size(); } }
    }
}