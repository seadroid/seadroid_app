﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.Droid.Helpers;
using fifi.Utils.Helpers;
using fifi.ViewModel;

namespace fifi.Droid.Adapters
{
    public class FriendRequestSentAdapter : RecyclerView.Adapter
    {
        private readonly Android.App.Activity _activity;
        private readonly FriendViewModel _viewModel;


        public FriendRequestSentAdapter(Android.App.Activity activity, FriendViewModel viewModel)
        {
            _activity = activity;
            _viewModel = viewModel;
            _viewModel.SentFriendRequests.CollectionChanged += (sender, e) => { _activity.RunOnUiThread(NotifyDataSetChanged); };
        }


        public event EventHandler<FriendRequestSentClickEventArgs> ItemClick;
        public event EventHandler<FriendRequestSentClickEventArgs> ItemLongClick;




        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var vh = holder as FriendRequestSentViewHolder;
            if (vh == null)
            {
                return;
            }
            if (_viewModel.SentFriendRequests.Count > 0)
            {
                var friend = _viewModel.SentFriendRequests[position];
                vh.Image.SetImageResource(Resource.Drawable.ic_friends_ghost_image_blue3x);

                ImageUrlDownloader downloader = new ImageUrlDownloader();
                if (!string.IsNullOrEmpty(friend.image))
                {
                    var a = new Task((() =>
                    {
                        vh.Image.SetAdjustViewBounds(true);
                        var base64image = downloader.GetImageBitmapFromUrl(friend.image);
                        var imagebytes = Convert.FromBase64String(base64image);
                        var bitmap = BitmapFactory.DecodeByteArray(imagebytes, 0, imagebytes.Length);
                        var roundimage = new CircleDrawable(bitmap);
                        using (var h = new Handler(Looper.MainLooper))
                        {
                            h.Post((() =>
                            {
                                vh.Image.SetImageDrawable(roundimage);
                                ;
                            }));
                        }

                    }), CancellationToken.None);
                    a.Start();


                }
               


                vh.Username.Text = friend.firstname +" " +friend.lastname;
            }

         
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        => new FriendRequestSentViewHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_friend_request_sent, parent, false), Onclick);



        private void Onclick(FriendRequestSentClickEventArgs args)
        {
            ItemClick?.Invoke(this, args);
        }

        public class FriendRequestSentClickEventArgs : EventArgs
        {
            public View View { get; set; }
            public int Position { get; set; }
        }

        public override int ItemCount => _viewModel.SentFriendRequests.Count;
    }

    public class FriendRequestSentViewHolder : RecyclerView.ViewHolder
    {
        public FriendRequestSentViewHolder(View itemView, Action<FriendRequestSentAdapter.FriendRequestSentClickEventArgs> listener) : base(itemView)
        {
            Username = ItemView.FindViewById<TextView>(Resource.Id.friendRequest_sent_friend_text_username);
            Boatname = ItemView.FindViewById<TextView>(Resource.Id.friendRequest_sent_friend_text_boat_name);
            Image = ItemView.FindViewById<ImageView>(Resource.Id.friendRquest_sent_friend_image);
            CancelFriendRequestButton = itemView.FindViewById<Button>(Resource.Id.cancel_friend_request_button);

            CancelFriendRequestButton.Click +=
            (sender, e) => listener(new FriendRequestSentAdapter.FriendRequestSentClickEventArgs { View = sender as View, Position = AdapterPosition });
        }

        public TextView Boatname { get; set; }
        public TextView Username { get; set; }
        public ImageView Image { get; set; }
        public Button CancelFriendRequestButton { get; set; }
    }
}