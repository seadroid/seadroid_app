using Android.Graphics;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using fifi.Droid.Activities;
using fifi.Droid.Controls;
using fifi.ViewModel;

namespace fifi.Droid.Fragments
{
    public class FragmentDashboard : Fragment
    {
        private ImageView _boatStatus, _windSpeed, _weatherStatus;
        private TextView _bearingInfo, _nearInfo, _weatherDegree, _weatherRain, _weatherWind, _crewMembers;
        private SpeedCircle _speedCircle;

        private DashboardViewModel _viewModel;

        public static FragmentDashboard NewInstance() => new FragmentDashboard { Arguments = new Bundle() };

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            HasOptionsMenu = true;
            var view = inflater.Inflate(Resource.Layout.fragment_dashboard, null);

            _viewModel = new DashboardViewModel();

            _speedCircle = view.FindViewById<SpeedCircle>(Resource.Id.speed_circle);

            _windSpeed = view.FindViewById<ImageView>(Resource.Id.boat_wind_direction);
            _weatherStatus = view.FindViewById<ImageView>(Resource.Id.weather_status);

            _bearingInfo = view.FindViewById<TextView>(Resource.Id.text_bearing);
            _nearInfo = view.FindViewById<TextView>(Resource.Id.text_harbor);
            _weatherDegree = view.FindViewById<TextView>(Resource.Id.text_weather_degree);
            _weatherRain = view.FindViewById<TextView>(Resource.Id.text_weather_rain);
            _weatherWind = view.FindViewById<TextView>(Resource.Id.text_weather_wind);
            _crewMembers = view.FindViewById<TextView>(Resource.Id.text_crew_members);

            UpdateUI();

            var activity = (BaseActivity)Activity;
            activity.SupportActionBar.Title = "Cacao";

            return view;
        }

        private void UpdateUI()
        {
            Activity.RunOnUiThread(() =>
            {
                _speedCircle.NextRating = 43f;
                _speedCircle.NextRatingColor = Color.ParseColor("#929497");
                _speedCircle.Rating = 97f;
                _speedCircle.RatingColor = Color.ParseColor("#E7592F");
                _crewMembers.Text = "6 on board";
                var drawable = ContextCompat.GetDrawable(Context, Resource.Drawable.ic_crew);
                var blackColor = Color.ParseColor("#000000");
                var mode = PorterDuff.Mode.SrcAtop;
                drawable.SetColorFilter(blackColor, mode);
                _crewMembers.SetCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                _windSpeed.Rotation = 315f;
            });
        }
    }
}