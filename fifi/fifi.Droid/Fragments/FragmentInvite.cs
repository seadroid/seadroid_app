﻿using System;
using System.ComponentModel;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using fifi.Droid.Activities;
using fifi.ViewModel;
using Fragment = Android.Support.V4.App.Fragment;

namespace fifi.Droid.Fragments
{
    public class FragmentInvite: Fragment, View.IOnTouchListener
    {
        private TextInputEditText InviteEmail;
        private InviteViewModel _viewModel;
        private InputMethodManager _inputManager;
        public static FragmentInvite NewInstance() => new FragmentInvite { Arguments = new Bundle()};
    

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.fragment_invite, null);
            InviteEmail = view.FindViewById<TextInputEditText>(Resource.Id.Email_Invite);
            InviteEmail.SetCompoundDrawablesWithIntrinsicBounds(0,0, Resource.Drawable.do_not_match, 0);
            _inputManager = (InputMethodManager)Activity.GetSystemService(Context.InputMethodService);
            InviteEmail.TextChanged += (sender, args) =>
            {
                if (!ValidatePassword())return;;
                InviteEmail.SetOnTouchListener(this);
            };
            view.Click += (sender, args) =>
            {
                _inputManager.HideSoftInputFromWindow(view.WindowToken, 0);
            };
            return view;

        }

        public async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _viewModel = new InviteViewModel();
            _viewModel.GetUserUnitsCommand.Execute(null);
            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
          
        }

        public override void OnStop()
        {
            try
            {
                _inputManager.HideSoftInputFromWindow(InviteEmail.WindowToken, 0);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            base.OnStop();           
        }

        private void ViewModelOnPropertyChanged(object o, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (!_viewModel.InviteAllowed)
            {
              AlertDialog.Builder alert = new AlertDialog.Builder(this.Context);
                alert.SetTitle("Cant Invite");
                alert.SetMessage("you need a Master paired to your accout, to invite new users");
                alert.SetPositiveButton("Ok", (sender, args) =>
                {
                    ((DashboardActivity)this.Activity).ListItemClicked(Resource.Id.menu_dashboard);
                });
                alert.Show();
            }
        }


        private bool ValidatePassword()
        {
            if (Android.Util.Patterns.EmailAddress.Matcher(InviteEmail.Text).Matches())
            {
               InviteEmail.SetCompoundDrawablesWithIntrinsicBounds(0, 0,
                   Resource.Drawable.continue_selector, 0);
                return true;
            }
            InviteEmail.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            return false;
        }

        public bool OnTouch(View v, MotionEvent e)
        {
            const int drawableRight = 2;         
            if (!(e.RawX >= InviteEmail.Right - InviteEmail.GetCompoundDrawables()[drawableRight].Bounds.Width()))
                return false;
            Toast.MakeText(Application.Context, "Email Invited", ToastLength.Short).Show();
            return true;
        }
    }
}