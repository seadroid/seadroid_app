using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using fifi.Droid.Activities;
using fifi.ViewModel;
using Mapbox;
using Mapbox.Maps;
using Fragment = Android.Support.V4.App.Fragment;
using fifi.Client.DataObjects;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Android.Content.Res;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Support.V7.Widget;
using fifi.Droid.Adapters;
using fifi.Helpers;
using fifi.Utils.Helpers;
using Mapbox.Annotations;
using Mapbox.Camera;
using Mapbox.Geometry;
using Debug = System.Diagnostics.Debug;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Android.Icu.Text;
using fifi.Client.Interfaces;
using Java.Lang;
using MvvmHelpers;
using PCLStorage;
using Exception = System.Exception;
using Math = System.Math;

namespace fifi.Droid.Fragments
{
    public class FragmentMaps : Fragment, IOnMapReadyCallback
    {
        private MapboxMap _map;
        private MapView _mapView;
        private MapsViewModel _viewModel;
        private static Toast CurrentToast;
/*     
        private bool _setZoom = true;
*/
        private bool _showFeedback;
        private double MyLatitude;
        private double MyLongitude;
        private bool busy;
        private IMenu _menu;
        private bool _haszoomed;
        private LinearLayout _markermenu;
        private LinearLayout _feedback;
        private LinearLayout _minimized_markermenu;
        private ImageView _warning;

        #region Help controls

        private TextView _helpTextView;
        private EditText _helpTitle, _helpDescription;
        private Button _submit;

        #endregion

        #region MarkerMenu controls
        private SeekBar _speedbar;
        private TextView _boatname;
        private Button _markerMenuCloseButton;
        private RelativeLayout _markerMenuOpenBar;
        private TextView _boattype;
        private TextView _boatDirection;
        private TextView _windspeed;
        private TextView _winddirection;
        private TextView _minSpeed;
        private TextView _maxSpeed;
        private TextView _currentSpeed;
        private Button _friendInvite;
        private ImageView _windDirectionImage;
        private ImageView _boatDirectionImage;
        private Button _sendFriendRequestButton;

        #endregion

        #region Minimized markermenu controls

        private TextView _minimized_boatname;
        private Button _open_markermenu_button;
        private RelativeLayout _markerMenuClosedBar;
        

        #endregion

        private BaseActivity _activity;


        public void OnMapReady(MapboxMap mapboxMap)
        {
            _map = mapboxMap;           
            _map.AnimateCamera(CameraUpdateFactory.NewLatLngZoom(new LatLng(0, 0), 200));
       
           
            SetupMap();

            _map.MapClick += (sender, args) =>
            {
                _minimized_markermenu.Visibility = ViewStates.Invisible;
            };
              
            
        // event called when a marker is clicked on the map      
        _map.MarkerClick += (sender, args) =>
            {
                _minSpeed.Visibility = ViewStates.Visible;
                _maxSpeed.Visibility = ViewStates.Visible;
                Debug.WriteLine(args.Marker.Title);
                _viewModel.user_id = args.Marker.Title;
                foreach (var boat in _viewModel.ActiveBoats)
                {
                    if (args.Marker.Title == Convert.ToString(boat))
                    {
                        var currentboat = boat;
                        //   _speedbar.Progress = Convert.ToInt16(Boat.trip[0].position.speed);

                       Random rnd = new Random();
                        var number = rnd.Next(15);
                       // var number = 0;
                        
                        //test data
                        _speedbar.Max = 15;
                        _maxSpeed.Text = Convert.ToString(15);
                        _speedbar.Progress = number;
                        _currentSpeed.Text = number +"Kn";

            
                        int thumbPos = ((((_speedbar.Right - _speedbar.Left) * _speedbar.Progress) / _speedbar.Max) + _speedbar.Left) - _speedbar.ThumbOffset;
                        var currentpositon = _currentSpeed.GetX();
                        _currentSpeed.SetX(thumbPos - (_currentSpeed.Width/2));
                        int[] currentspeed_positon = new int[2];
                        int[] maxspeed_positon = new int[2];
                        int[] minspeed_position = new int[2];
                        _currentSpeed.GetLocationOnScreen(currentspeed_positon);
                        _minSpeed.GetLocationOnScreen(minspeed_position);
                        _maxSpeed.GetLocationOnScreen(maxspeed_positon);

                        if (currentspeed_positon[0] + _currentSpeed.Width > _maxSpeed.Left)
                        {
                            _maxSpeed.Visibility = ViewStates.Invisible;
                        }

                        if (_speedbar.Progress == 0)
                        {
                            _currentSpeed.Visibility = ViewStates.Invisible;
                            _minSpeed.Text = "0 Kn";
                        }
                        else
                        {
                            _currentSpeed.Visibility = ViewStates.Visible;
                            _minSpeed.Text = "0";
;                       }

                    }
                }

                if (_markermenu.Visibility != ViewStates.Visible)
                {
                    _minimized_markermenu.Visibility = ViewStates.Visible;
                }

                var IsNotFriend = true;

                foreach (var friend in _viewModel.friends.data)
                {
                    if (args.Marker.Title == friend.id.ToString())
                    {
                        _sendFriendRequestButton.Background = ContextCompat.GetDrawable(Context, Resource.Drawable.neg_bluebox_req_sent);
                        IsNotFriend = false;
                    }
                    
                }
                foreach (var friendRequest in _viewModel.sentFriendRequests.data)
                {
                    if (args.Marker.Title == friendRequest.id.ToString())
                    {
                       _sendFriendRequestButton.Background = ContextCompat.GetDrawable(Context, Resource.Drawable.neg_bluebox_req_sent);
                        IsNotFriend = false;
                    }
                }

                if (IsNotFriend)
                {
                    _sendFriendRequestButton.Background = ContextCompat.GetDrawable(Context, Resource.Drawable.friendrequest_selector);                
                }

                if (args.Marker.Title == Settings.Current.UserId)
                {
                    // button background that is shown when a user clicks on themself on the map
                    _sendFriendRequestButton.Background = ContextCompat.GetDrawable(Context, Resource.Drawable.neg_bluebox_req_sent);
                }
            };
             
            var soloItem = _menu.FindItem(Resource.Id.menu_solo);
            var friendsItem = _menu.FindItem(Resource.Id.menu_friends);
            var globalItem = _menu.FindItem(Resource.Id.menu_global);

            switch (Settings.Current.Mapmode)
            {
                case "Solo":
                    soloItem.Icon.SetColorFilter(Color.ParseColor("#9EFFFF"), PorterDuff.Mode.SrcAtop);
                    globalItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    friendsItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    break;
                case "Friends":
                    soloItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    globalItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    friendsItem.Icon.SetColorFilter(Color.ParseColor("#9EFFFF"), PorterDuff.Mode.SrcAtop);
                    break;
                case "Global":
                    soloItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    globalItem.Icon.SetColorFilter(Color.ParseColor("#9EFFFF"), PorterDuff.Mode.SrcAtop);
                    friendsItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    break;
                default:
                    Settings.Current.Mapmode = "Solo";
                    soloItem.Icon.SetColorFilter(Color.ParseColor("#9EFFFF"), PorterDuff.Mode.SrcAtop);
                    globalItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    friendsItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    break;

            }
        }

        public static FragmentMaps NewInstance() => new FragmentMaps {Arguments = new Bundle()};

       

        public override  View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            busy = true;
           
            base.OnCreateView(inflater, container, savedInstanceState);
            MapboxAccountManager.Start(Context, GetString(Resource.String.access_token));
            var view = inflater.Inflate(Resource.Layout.fragment_maps, null);
           
            _mapView = view.FindViewById<MapView>(Resource.Id.map);
            _mapView.OnCreate(savedInstanceState);
            _mapView.GetMap(this);
            SetupMap();


            _feedback = view.FindViewById<LinearLayout>(Resource.Id.feedback);
            _feedback.Visibility = ViewStates.Invisible;
            _warning = view.FindViewById<ImageView>(Resource.Id.image_warning);

            #region Help FindViews

            _helpTextView = view.FindViewById<TextView>(Resource.Id.text_help);
            _helpTitle = view.FindViewById<EditText>(Resource.Id.text_help_title);
            _helpDescription = view.FindViewById<EditText>(Resource.Id.text_help_description);
            _submit = view.FindViewById<Button>(Resource.Id.btn_submit);

            #endregion

            var closeDrawable = ContextCompat.GetDrawable(Context, Resource.Drawable.ic_clear);
            closeDrawable.SetColorFilter(Color.Red, PorterDuff.Mode.SrcAtop);
            _helpTextView.SetCompoundDrawablesWithIntrinsicBounds(
                ContextCompat.GetDrawable(Context, Resource.Drawable.ic_warning_triangle), null, closeDrawable, null);
            _helpTextView.Touch += HelpTextViewOnTouch;



            _minimized_markermenu = view.FindViewById<LinearLayout>(Resource.Id.map_minimized_marker_menu);
            _minimized_markermenu.Visibility = ViewStates.Invisible;
            _markerMenuClosedBar = view.FindViewById<RelativeLayout>(Resource.Id.Closed_marker_menu_bar);

            #region Minimized markermenu findview


            _open_markermenu_button = view.FindViewById<Button>(Resource.Id.map_btn_open_mapmarker_menu);
            _minimized_boatname = view.FindViewById<TextView>(Resource.Id.map_text_boatname_minimized);

            _markerMenuClosedBar.Click += (sender, args) =>
            {
                _markermenu.Visibility = ViewStates.Visible;
                _minimized_markermenu.Visibility = ViewStates.Invisible;
            };

            _open_markermenu_button.Click += (sender, args) =>
            {
                _markermenu.Visibility = ViewStates.Visible;
                _minimized_markermenu.Visibility = ViewStates.Invisible;
            };

            #endregion

            _markermenu = view.FindViewById<LinearLayout>(Resource.Id.map_marker_menu);
            _markermenu.Visibility = ViewStates.Invisible;


            #region MarkerMenu FindViews
                                 
            _speedbar = view.FindViewById<SeekBar>(Resource.Id.map_speedbar);
            Bitmap img = BitmapFactory.DecodeResource(Resources, Resource.Drawable.speed_line);
            Drawable CustomThumb = new BitmapDrawable(Resources, img);
            _speedbar.SetThumb(CustomThumb);
            _speedbar.SetPadding(2, 0, 2, 0);
            _speedbar.Enabled = false;
            
            _boatname = view.FindViewById<TextView>(Resource.Id.map_text_boatname);
            _markerMenuCloseButton = view.FindViewById<Button>(Resource.Id.map_btn_close_mapmarker_menu);
            _markerMenuOpenBar = view.FindViewById<RelativeLayout>(Resource.Id.open_marker_menu_bar);

            _markerMenuOpenBar.Click += (sender, args) =>
            {
                _markermenu.Visibility = ViewStates.Invisible;
                _minimized_markermenu.Visibility = ViewStates.Visible;
            };

            _markerMenuCloseButton.Click += (sender, args) =>
            {
                _markermenu.Visibility = ViewStates.Invisible;  
                _minimized_markermenu.Visibility = ViewStates.Visible;                         
            };

            
            

            _boattype = view.FindViewById<TextView>(Resource.Id.map_text_boatType);
            _boatDirection = view.FindViewById<TextView>(Resource.Id.map_text_boat_direction);
            _winddirection = view.FindViewById<TextView>(Resource.Id.map_text_wind_direction);
            _windspeed = view.FindViewById<TextView>(Resource.Id.map_text_windspeed);
            _minSpeed = view.FindViewById<TextView>(Resource.Id.map_start_value_speedbar);
            _maxSpeed = view.FindViewById<TextView>(Resource.Id.map_end_value_speedbar);
            _currentSpeed = view.FindViewById<TextView>(Resource.Id.map_current_value_speedbar);
            _friendInvite = view.FindViewById<Button>(Resource.Id.map_btn_friendrequest);
            _boatDirectionImage = view.FindViewById<ImageView>(Resource.Id.map_image_small_ship_direction);
            _windDirectionImage = view.FindViewById<ImageView>(Resource.Id.map_image_wind_direction);
            _sendFriendRequestButton = view.FindViewById<Button>(Resource.Id.map_btn_friendrequest);

            _sendFriendRequestButton.Click += _sendFriendRequestButton_Click;
            #endregion

           
            ShowFeedback();

            busy = false;
            return view;
        
        }

        private void _sendFriendRequestButton_Click(object sender, EventArgs e)
        {
            if (_sendFriendRequestButton.Background == ContextCompat.GetDrawable(Context, Resource.Drawable.neg_bluebox_req_sent))
            {
                return;
            }
           _viewModel.SendFriendRequestCommand.Execute(null);
            Friend friend = new Friend();
            friend.id = Convert.ToInt32(_viewModel.user_id);          
           _viewModel.sentFriendRequests.data.Add(friend);
            _sendFriendRequestButton.Background = ContextCompat.GetDrawable(Context, Resource.Drawable.neg_bluebox_req_sent);

        }

        private void HelpTextViewOnTouch(object sender, View.TouchEventArgs touchEventArgs)
        {
            if (touchEventArgs.Event.Action != MotionEventActions.Up) return;
            if (
                !(touchEventArgs.Event.RawX >=
                  _helpTextView.Right - _helpTextView.GetCompoundDrawables()[2].Bounds.Width())) return;
            _showFeedback = !_showFeedback;
            ShowFeedback();
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            _viewModel = new MapsViewModel();
            base.OnCreate(savedInstanceState);
            HasOptionsMenu = true;
            _haszoomed = false;
            _viewModel.PropertyChanged -= ViewModel_PropertyChanged;  
            

            

        }

        private  void ActiveVessels_Collectionchanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateMap();
        }



        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
           
           
        }


        private void UpdateMap()
        {
           
            
            

            try
            {
                
                Debug.WriteLine("Changing map markers");



                if (busy)
                {
                    Debug.WriteLine("View is busy, map update canceled");
                    return;
                }

                busy = true;
                Debug.WriteLine("ViewModel_PropertyChanged was called");

         

                


                var iconFactory = IconFactory.GetInstance(this.Context);
                var logicalDensity = Resources.System.DisplayMetrics.Density;
                var thicknessPoints = (int)Math.Ceiling(50 * logicalDensity + 0.5f);

            


                foreach (var boat in _viewModel.ActiveBoats)
                {
                    if (boat.User_id == Convert.ToInt16(Settings.Current.UserId))
                    {
                       
                         MyLatitude = double.Parse(boat.Latitude.Replace(".", ","));
                         MyLongitude = double.Parse(boat.Longitude.Replace(".", ","));

                      
                        using (var h = new Handler(Looper.MainLooper))
                        {

                            h.Post((() =>
                            {                                
                                _map.Clear();
                                var drawable = ContextCompat.GetDrawable(this.Context, Resource.Drawable.ic_position_my_ship3x);
                                var finalIcon = iconFactory.FromDrawable(drawable, thicknessPoints, thicknessPoints);
                                var MyMarker = new MarkerViewOptions();
                                MyMarker.SetPosition(new LatLng(Convert.ToDouble(MyLatitude), Convert.ToDouble(MyLongitude)));
                                MyMarker.SetIcon(finalIcon);                           
                                MyMarker.SetAnchor(0.5f, 0.5f);
                                MyMarker.SetTitle(Convert.ToString(boat.User_id));                                                   
                                _map.AddMarker(MyMarker);
                                
                               
                                
                                
                            }));
                        }

                      
                    }
                }


                switch (Settings.Current.Mapmode)
                {
                    case "Solo":
                        using (var h = new Handler(Looper.MainLooper))
                        {
                             h.Post((() =>
                            {
                                if (!_haszoomed)
                                {
                                    _map.EaseCamera(CameraUpdateFactory.NewLatLngZoom((new LatLng(Convert.ToDouble(MyLatitude), Convert.ToDouble(MyLongitude))), (float)200));
                                    _haszoomed = true;
                                }
                               
                            }));
                        }
                        busy = false;
                        break;

                    case "Friends":
                        foreach (var boat in _viewModel.ActiveBoats)
                        {
                            foreach (var friend in _viewModel.friends.data)
                            {
                                if (boat.User_id == friend.id)
                                {
                                    var friendLatitude = double.Parse(boat.Latitude.Replace(".", ","));
                                    var friendLongitude = double.Parse(boat.Longitude.Replace(".", ","));
                                    using (var h = new Handler(Looper.MainLooper))
                                    {

                                        h.Post((() =>
                                        {
                                            var drawable = ContextCompat.GetDrawable(this.Context,
                                                Resource.Drawable.ic_position_friend_ship3x);
                                            var finalIcon = iconFactory.FromDrawable(drawable, thicknessPoints,
                                                thicknessPoints);
                                            var FriendMarker = new MarkerViewOptions();
                                            FriendMarker.SetPosition(new LatLng(Convert.ToDouble(friendLatitude),
                                                Convert.ToDouble(friendLongitude)));
                                            FriendMarker.SetIcon(finalIcon);
                                            FriendMarker.SetAnchor(0.5f, 0.5f);
                                            FriendMarker.SetTitle(Convert.ToString(boat.User_id));
                                            _map.AddMarker(FriendMarker);
                                        }));
                                    }
                                }
                            }
                        }
                        using (var h = new Handler(Looper.MainLooper))
                        {
                            h.Post((() =>
                            {
                                if (!_haszoomed)
                                {
                                    _map.EaseCamera(CameraUpdateFactory.NewLatLngZoom((new LatLng(Convert.ToDouble(MyLatitude), Convert.ToDouble(MyLongitude))), (float)200));
                                    _haszoomed = true;
                                }
                            }));
                        }
                        busy = false;
                        break;
                    case "Global":
                       
                        foreach (var boat in _viewModel.ActiveBoats)
                        {
                            bool IsFriend = false;
                            foreach (var friend in _viewModel.friends.data)
                            {
                                if (boat.User_id == friend.id)
                                {
                                    var friendLatitude = double.Parse(boat.Latitude.Replace(".", ","));
                                    var friendLongitude = double.Parse(boat.Longitude.Replace(".", ","));
                                    using (var h = new Handler(Looper.MainLooper))
                                    {
                                        h.Post((() =>
                                        {
                                            var drawable = ContextCompat.GetDrawable(this.Context,
                                                Resource.Drawable.ic_position_friend_ship3x);
                                            var finalIcon = iconFactory.FromDrawable(drawable, thicknessPoints,
                                                thicknessPoints);
                                            var GlobalMarker = new MarkerViewOptions();
                                            GlobalMarker.SetPosition(new LatLng(Convert.ToDouble(friendLatitude),
                                                Convert.ToDouble(friendLongitude)));
                                            GlobalMarker.SetIcon(finalIcon);
                                            GlobalMarker.SetAnchor(0.5f, 0.5f);
                                            GlobalMarker.SetTitle(Convert.ToString(boat.User_id));
                                            _map.AddMarker(GlobalMarker);
                                            IsFriend = true;
                                        }));
                                    }
                                }
                              
                            }
                            if (IsFriend == false)
                            {
                                if (boat.User_id != Convert.ToInt16(Settings.Current.UserId))
                                {
                                    var globaluserlatitude = double.Parse(boat.Latitude.Replace(".", ","));
                                    var globaluserlongitude = double.Parse(boat.Longitude.Replace(".", ","));
                                    using (var h = new Handler(Looper.MainLooper))
                                    {
                                        h.Post((() =>
                                        {
                                            var drawable = ContextCompat.GetDrawable(this.Context,
                                                Resource.Drawable.ic_position_global_ship3x);
                                            var finalIcon = iconFactory.FromDrawable(drawable, thicknessPoints,
                                                thicknessPoints);
                                            var GlobalMarker = new MarkerViewOptions();
                                            GlobalMarker.SetPosition(new LatLng(Convert.ToDouble(globaluserlatitude),
                                                Convert.ToDouble(globaluserlongitude)));
                                            GlobalMarker.SetIcon(finalIcon);
                                            GlobalMarker.SetAnchor(0.5f, 0.5f);
                                            GlobalMarker.SetTitle(Convert.ToString(boat.User_id));
                                            _map.AddMarker(GlobalMarker);
                                        }));
                                    }
                                }
                            }

                        }
                        using (var h = new Handler(Looper.MainLooper))
                        {
                            h.Post((() =>
                            {
                                if (!_haszoomed)
                                {
                                    _map.EaseCamera(CameraUpdateFactory.NewLatLngZoom((new LatLng(Convert.ToDouble(MyLatitude), Convert.ToDouble(MyLongitude))), (float)200));
                                    _haszoomed = true;
                                }
                            }));
                        }
                        busy = false;
                        break;

                }

            }


            catch (Exception exception)
            {
                Console.WriteLine(exception);

            }
        }

        



        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            _mapView.GetMapAsync();
            _activity = (BaseActivity) Activity;
            _activity.SupportActionBar.Title = string.Empty;
            base.OnActivityCreated(savedInstanceState);
        }

        private void ShowFeedback()
        {
            // Handle first run
            if (!_showFeedback && _feedback.Visibility == ViewStates.Invisible)
                return;

            if (_showFeedback)
            {
                _warning.Visibility = ViewStates.Invisible;
                _feedback.Visibility = ViewStates.Visible;
            }
            else
            {
                _warning.Visibility = ViewStates.Visible;
                _feedback.Visibility = ViewStates.Invisible;
                ResetHelp();
                HideKeyboard();
            }
        }

        private void SetupMap()
        {
            if (_map == null)
            {
                return;
            }

            if (_mapView.Width == 0)
            {
                _mapView.PostDelayed(SetupMap, 500);
            }
        }

        public override void OnStop()
        {
            base.OnStop();
            busy = true;
            _viewModel.StopLoadVesselsCommand.Execute(null);
            if (_warning != null)
            {
                _warning.Click -= OnWarningClick;
            }
            _viewModel.PropertyChanged -= ViewModel_PropertyChanged;
        }

        public override void OnStart()
        {
            base.OnStart();
            CurrentToast = Toast.MakeText(Application.Context, "test", ToastLength.Short);
            _viewModel = new MapsViewModel();
            _viewModel.StartLoadvesselsCommand.Execute(null);
            _viewModel.PropertyChanged += ViewModel_PropertyChanged;
            _viewModel.ActiveBoats.CollectionChanged += ActiveVessels_Collectionchanged;
            _viewModel.LoadSentFriendRequestsCommand.Execute(null);

            if (_warning != null)
            {
                _warning.Click += OnWarningClick;
            }

            if (_submit != null)
            {
                _submit.Click += OnSubmitClick;
            }
        }

        private void OnSubmitClick(object sender, EventArgs eventArgs)
        {
            if (string.IsNullOrWhiteSpace(_helpTitle.Text) || string.IsNullOrWhiteSpace(_helpDescription.Text))
            {
                Snackbar.Make(View, "Enter some text...", Snackbar.LengthShort).Show();
                return;
            }
            _submit.Text = "Message sent";
            _submit.SetBackgroundResource(Resource.Drawable.help_submitted_selector);
            HideKeyboard();
        }

        private void OnWarningClick(object sender, EventArgs e)
        {
            _showFeedback = !_showFeedback;
            ShowFeedback();
        }

        private void ResetHelp()
        {
            _submit.Text = "Submit";
            _submit.SetBackgroundResource(Resource.Drawable.help_submit_selector);
            _helpTitle.Text = string.Empty;
            _helpDescription.Text = string.Empty;
            Activity.SupportInvalidateOptionsMenu();
        }

        private void HideKeyboard()
        {
            var imm = (InputMethodManager) Activity.GetSystemService(Context.InputMethodService);
            imm.HideSoftInputFromWindow(Activity.CurrentFocus.WindowToken, HideSoftInputFlags.None);
        }

      

        #region Options Menu & User Actions

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.menu_maps, menu);
            _menu = menu;
            base.OnCreateOptionsMenu(menu, inflater);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            
          
            var soloItem = _menu.FindItem(Resource.Id.menu_solo);
            var friendsItem = _menu.FindItem(Resource.Id.menu_friends);
            var globalItem = _menu.FindItem(Resource.Id.menu_global);
           

            switch (item.ItemId)
            {
                case Resource.Id.menu_solo:
                    soloItem.Icon.SetColorFilter(Color.ParseColor("#9EFFFF"), PorterDuff.Mode.SrcAtop);
                    globalItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    friendsItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    Settings.Current.Mapmode = "Solo";
                    UpdateMap();   
                            
                    CurrentToast.Cancel();                
                    CurrentToast = Toast.MakeText(Application.Context, "Solo", ToastLength.Short);
                    CurrentToast.Show();                                  
                    break;

                case Resource.Id.menu_friends:
                    friendsItem.Icon.SetColorFilter(Color.ParseColor("#9EFFFF"), PorterDuff.Mode.SrcAtop);
                    soloItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    globalItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    Settings.Current.Mapmode = "Friends";
                    UpdateMap();

                    CurrentToast.Cancel();
                    CurrentToast = Toast.MakeText(Application.Context, "Friends", ToastLength.Short);
                    CurrentToast.Show();
                    break;

                case Resource.Id.menu_global:
                    globalItem.Icon.SetColorFilter(Color.ParseColor("#9EFFFF"), PorterDuff.Mode.SrcAtop);
                    soloItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    friendsItem.Icon.SetColorFilter(Color.White, PorterDuff.Mode.SrcAtop);
                    Settings.Current.Mapmode = "Global";
                    UpdateMap();   
                     
                    CurrentToast.Cancel();             
                    CurrentToast = Toast.MakeText(Application.Context, "Global", ToastLength.Short);
                    CurrentToast.Show();
                    break;
                
            }
            return base.OnOptionsItemSelected(item);
        }

        #endregion

        #region MapView Lifecycle Events

    

        public override void OnResume()
        {
            base.OnResume();
            _mapView?.OnResume();
        }

        public override void OnPause()
        {
            base.OnPause();
            _mapView?.OnPause();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            _mapView?.OnDestroy();
        }

        public override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            _mapView?.OnSaveInstanceState(outState);
        }

        public override void OnLowMemory()
        {
            base.OnLowMemory();
            _mapView?.OnLowMemory();
        }

        //public BitmapDrawable CreateSeekBarPinWithText(int drawableId, string text)
        //{
        //    Bitmap bm = BitmapFactory.DecodeResource(Resources, drawableId).Copy(Bitmap.Config.Argb8888, true);
        //    bm.Height = bm.Height * 2;
        //    bm.Width = bm.Width * 2;
        //    Paint paint = new Paint();
        //    paint.SetStyle(Paint.Style.Fill);
        //    paint.Color = Color.Black;
        //    paint.TextSize = 40;
            
        //    Canvas canvas = new Canvas(bm);
        //    canvas.DrawText(text, 0, bm.Height * (float)1.5, paint);
           
            
        //    return new BitmapDrawable(bm);
        //}

        #endregion
    }
}