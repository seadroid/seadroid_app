﻿using System;
using System.ComponentModel;
using System.Runtime.Hosting;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.Droid.Activities;
using fifi.Droid.Adapters;
using fifi.Droid.Helpers;
using fifi.ViewModel;
namespace fifi.Droid.Fragments
{
    public class FragmentChatHub : Fragment
    {
        private LinearLayoutManager _layoutManager;
        private Button ChatButton;
        private ChatHubViewModel _viewModel;
        private RecyclerView _RecyclerView;
        private ChatHubAdapter _chatHubAdapter;
        private SwipeRefreshLayout _refresher;
        


        public static FragmentChatHub newInstance ()=> new FragmentChatHub{Arguments =  new Bundle()};

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.fragment_chat_hub, null);
         
            _viewModel = new ChatHubViewModel();
            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;


            _refresher = view.FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
            _refresher.Refresh += (sender, args) => _viewModel.LoadFriendsCommand.Execute(null);


            _layoutManager = new LinearLayoutManager(Context){Orientation = LinearLayoutManager.Vertical};
            _RecyclerView = view.FindViewById<RecyclerView>(Resource.Id.ChatHub_RecyclerView);
            _chatHubAdapter  = new ChatHubAdapter(Activity, _viewModel);
            _chatHubAdapter.ItemClick += ChatHubAdapterOnItemClick;
            _RecyclerView.SetAdapter(_chatHubAdapter);
            _RecyclerView.SetLayoutManager(_layoutManager);
            
           _viewModel.LoadFriendsCommand.Execute(null);
            return view;

        }

        private void ChatHubAdapterOnItemClick(object sender, ChatHubAdapter.FriendClickEventArgs Args)
        {
            var intent = new Intent(Activity, typeof(ChatActivity));           
            intent.PutExtra("Friend_id", _viewModel.Friends[Args.Position].id.ToString());
            intent.PutExtra("Friend_name",  _viewModel.Friends[Args.Position].firstname + " " + _viewModel.Friends[Args.Position].lastname);
            intent.PutExtra("Friend_imageUrl", _viewModel.Friends[Args.Position].image);
         
            base.StartActivityForResult(intent, 1);
        }


        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            _viewModel.LoadFriendsCommand.Execute(null);
            base.OnActivityResult(requestCode, resultCode, data);
        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(_viewModel.IsBusy):
                    _refresher.Refreshing = _viewModel.IsBusy;
                    break;
            }
        }
    }
}