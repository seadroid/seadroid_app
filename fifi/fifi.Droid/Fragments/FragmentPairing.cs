using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Icu.Text;
using Android.Media;
using Android.Media.Audiofx;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.Droid.Adapters;
using fifi.ViewModel;
using Android.OS;
using Android.Preferences;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Text.Style;
using Android.Views;
using Android.Widget;
using fifi.Client.DataObjects;
using fifi.Droid.Activities;
using fifi.Model;

using Java.Security;
using Fragment = Android.Support.V4.App.Fragment;

namespace fifi.Droid.Fragments
{
    class FragmentPairing : Fragment
    {

        private LinearLayoutManager _layoutManager;
        private PairingAdapter _adapter;
        private RecyclerView _recyclerView;
        private SwipeRefreshLayout _refresher;
        private PairingViewModel _viewModel;
        private BluetoothDevice bluetoothDevice;
        private Snackbar snackbar;
        public static FragmentPairing NewInstance() => new FragmentPairing() {Arguments = new Bundle()};


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            try
            {
              
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.fragment_pairing, null);
            _viewModel = new PairingViewModel();
            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            _refresher = view.FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
            _refresher.Refresh += (sender, e) =>  _viewModel.RefreshBluetoothcommand.Execute(null);                    
            _adapter = new PairingAdapter(Activity, _viewModel);
            _adapter.ItemClick += OnItemClick;
            _recyclerView.SetAdapter(_adapter);
            _layoutManager = new LinearLayoutManager(Activity) {Orientation = LinearLayoutManager.Vertical};
            _recyclerView.SetLayoutManager(_layoutManager);
         



            

            //_recyclerView.ClearOnScrollListeners();
            //_recyclerView.AddOnScrollListener(new DevicesOnScrollListenerListener(_viewModel, _layoutManager));
            return view;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }
            return View;
        }

     

        internal class PairingOnScrollListener : RecyclerView.OnScrollListener
        {
            private readonly LinearLayoutManager _layoutManager;
            private readonly PairingViewModel _viewModel;

            public PairingOnScrollListener(PairingViewModel viewModel, LinearLayoutManager layoutManager)
            {
                _layoutManager = layoutManager;
                _viewModel = viewModel;
            }
        }

        public override async void OnStart()
        {
            try
            {
                base.OnStart();
                _viewModel.PropertyChanged += ViewModel_PropertyChanged;

                if (_viewModel.FoundDevices.Count == 0)
                {
                    snackbar = Snackbar.Make(_recyclerView, "Searching for Devices.....", Snackbar.LengthIndefinite);
                    snackbar.Show();

                    await _viewModel.ExecuteLoadBluetoothCommandAsync();
                    snackbar.Dismiss();

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
           
        }

        public override void OnStop()
        {
            base.OnStop();

            try
            {
                if (snackbar.IsShown)
                {
                    try
                    {
                        snackbar.Dismiss();
                        snackbar.Dispose();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                
            }
          
            _viewModel.PropertyChanged -= ViewModel_PropertyChanged;
        }


        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(_viewModel.IsBusy):
                    _refresher.Refreshing = _viewModel.IsBusy;                                
                    break;           

            }
        }
        /// <summary>
        /// Navigates the user to the PairingDetailsActivity so that they can eddit the chosen BluetoothDevice
        /// </summary>
        private async void OnItemClick(object sender, PairingAdapter.PairingDeviceClickEventArgs args)
        {
            try
            {
                if (_viewModel.IsBusy == true)
                {
                    return;
                }
                _viewModel.IsBusy = true;
                bluetoothDevice = _viewModel.Bluetoothdevices[args.Position];
                var user_id = 0;
                var result = await _viewModel.ExecutePingBluetoothdeviceCommandAsync(bluetoothDevice.Device);
                Context contex = Context;
                AlertDialog.Builder alert = new AlertDialog.Builder(contex);
                var wristbandfoud = false;

                if (result == true)
                {
                    PairingDetailsActivity.BluetoothDevice = bluetoothDevice.Device;
                    var DetailsViewModel = new PairingDetailsViewModel();

                    foreach (var wristband in _viewModel.Armbands.data)
                    {
                        if (bluetoothDevice.Device.Name == wristband.serial)
                        {
                            DetailsViewModel.Wristband = new Armband(wristband.firstname, wristband.lastname, wristband.birthday, wristband.height.Value, wristband.weight.Value, wristband.serial, wristband.gender, wristband.country_id.Value, wristband.haircolor, wristband.swim_exp.Value, wristband.sea_exp.Value);
                            wristbandfoud = true;
                            user_id = wristband.user_id;
                        }

                    }

                    if (wristbandfoud == false)
                    {
                        DetailsViewModel.Wristband = new Armband();
                    }




                    PairingDetailsActivity._viewModel = DetailsViewModel;
                    if (user_id == 0)
                    {
                        alert.SetTitle("create profile");
                        alert.SetMessage("Do you whant to create a profile for this Armband? Creating a profile will bind this Armband to you account");

                        alert.SetPositiveButton("Yes", (senderAlert, a) =>
                        {

                            var intent = new Intent(Activity, typeof(PairingDetailsActivity));
                            intent.PutExtra("Id", bluetoothDevice.Device.Name);

                            base.StartActivityForResult(intent, 1);
                        });

                        alert.SetNegativeButton("Cancel", (senderAlert, a) =>
                        {

                        });
                    }
                    if (user_id == _viewModel.UserProfile.Id)
                    {
                        alert.SetTitle("Edit profile");
                        alert.SetMessage("Do you whant to Edit the profile for this Armband?");
                        alert.SetPositiveButton("Yes", (senderAlert, a) =>
                        {

                            var intent = new Intent(Activity, typeof(PairingDetailsActivity));
                            intent.PutExtra("Id", bluetoothDevice.Device.Name);

                            base.StartActivityForResult(intent, 1);
                        });

                        alert.SetNegativeButton("Cancel", (senderAlert, a) =>
                        {

                        });
                    }
                    if (user_id != 0 && user_id != _viewModel.UserProfile.Id)
                    {
                        alert.SetTitle("insufficient permission");
                        alert.SetMessage("Cant edit selected Armband, the Armband belongs to someone else");
                        alert.SetPositiveButton("ok", (senderAlert, a) =>
                        {

                        });
                    }


                    Dialog dialog = alert.Create();
                    dialog.Show();
                    _viewModel.IsBusy = false;
                }

                else
                {
                    alert.SetTitle("Connection failed");
                    alert.SetMessage("Could not connect to Armband, please try again");
                    alert.SetPositiveButton("OK", (senderAlert, a) =>
                    {

                    });
                    Dialog dialog = alert.Create();
                    dialog.Show();
                    _viewModel.IsBusy = false;
                }

                _viewModel.IsBusy = false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
           

        }
     

        public override async void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            
            base.OnActivityResult(requestCode, resultCode, data);

            try
            {
                if (resultCode == -1)
                {
                    Armband wristband = new Armband(data.GetStringExtra("firstname"), data.GetStringExtra("lastname"), data.GetStringExtra("birthday"), Convert.ToInt16(data.GetStringExtra("height")), Convert.ToInt16(data.GetStringExtra("weight")), data.GetStringExtra("bluetoothid"), data.GetStringExtra("gender"), Convert.ToInt16(data.GetStringExtra("language")), data.GetStringExtra("haircolor"), Convert.ToInt16(data.GetStringExtra("swinExperience")), Convert.ToInt16(data.GetStringExtra("seaExperience")));
                    _viewModel.wristband = wristband;
                    await _viewModel.ExecuteUpdateWristbandCommandAsync();
                    _recyclerView.GetAdapter().NotifyDataSetChanged();
                }
               
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        

        }

       
    }
}