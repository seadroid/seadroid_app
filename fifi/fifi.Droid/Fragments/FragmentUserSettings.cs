﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Media;
using Android.OS;
using Android.Provider;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.View.Menu;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using fifi.Droid.Activities;
using fifi.Droid.Helpers;
using fifi.ViewModel;
using fifi.Helpers;
using Java.IO;
using Java.Lang;
using Java.Net;
using Java.Security;
using Console = System.Console;
using Fragment = Android.Support.V4.App.Fragment;
using Settings = fifi.Utils.Helpers.Settings;

namespace fifi.Droid.Fragments
{
    public class FragmentUserSettings: Fragment
    {
        private static Toast CurrentToast;
        private bool MadeChanges;
        private IMenu _menu;
        private UserSettingsViewModel _viewModel;
        private TextInputEditText _textFirstname;
        private TextInputEditText _textLastname;
        private ScrollView _Scrollview; 
        private bool FirstnameValidated;
        private bool LastnameValidated;
        private InputMethodManager _inputManager;
        private Button _saveButton;
        private ImageView _UserImage;
        private ImageView ImageForUri;
        private Android.Net.Uri _ImageUri;

        public static FragmentUserSettings NewInstance ()=> new FragmentUserSettings{Arguments =  new Bundle()};

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            
            MadeChanges = false;
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.fragment_user_settings, null);
            _viewModel = new UserSettingsViewModel();
            _inputManager = (InputMethodManager)Activity.GetSystemService(Context.InputMethodService);
            _Scrollview = view.FindViewById<ScrollView>(Resource.Id.UserSettings_ScrollView);
            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
            _Scrollview.Click += (sender, args) =>
            {
                _inputManager.HideSoftInputFromWindow(view.WindowToken, 0);
            };
            view.Click += (sender, args) =>
            {
                _inputManager.HideSoftInputFromWindow(view.WindowToken, 0);
            };

            view.Touch += (sender, args) =>
            {
                _inputManager.HideSoftInputFromWindow(view.WindowToken, 0);
            };

           
            
            _UserImage = view.FindViewById<ImageView>(Resource.Id.UserSettings_UserImange);
            if (_viewModel.imagedata != null)
            {
                var bitmap = BitmapFactory.DecodeByteArray(_viewModel.imagedata, 0, _viewModel.imagedata.Length);
                var circleimage = new CircleDrawable(bitmap);
                _UserImage.SetImageDrawable(circleimage);
               
            }
       
            LastnameValidated = true;
            FirstnameValidated = true;
            
            _textFirstname = view.FindViewById<TextInputEditText>(Resource.Id.UserSettings_firstname);
            _textFirstname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
            _textFirstname.Text = _viewModel.UserProfile.Firstname;
            _textFirstname.TextChanged += (sender, args) =>
            {
                MadeChanges = true;
                _viewModel.UserProfile.Firstname = _textFirstname.Text;
                if (!ValidateFirstname()) return;
                if (!ValidateLastname()) return;         
                _saveButton.Clickable = true;
                _saveButton.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

            _textFirstname.FocusChange += (sender, args) =>
            {
                _inputManager.HideSoftInputFromWindow(_textFirstname.WindowToken, 0);
            };

            _textLastname = view.FindViewById<TextInputEditText>(Resource.Id.UserSettings_lastname);
            _textLastname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
            _textLastname.Text = _viewModel.UserProfile.Lastname;
            _textLastname.TextChanged += (sender, args) =>
            {
                MadeChanges = true;
                _viewModel.UserProfile.Lastname = _textLastname.Text;
                if (!ValidateLastname()) return;
                if (!ValidateFirstname()) return;
                _saveButton.Clickable = true;
                _saveButton.SetBackgroundResource(Resource.Drawable.green_button_selector);
            };

            _textLastname.FocusChange += (sender, args) =>
            {
                _inputManager.HideSoftInputFromWindow(_textLastname.WindowToken, 0);
            };

            _saveButton = view.FindViewById<Button>(Resource.Id.UserSettings_save_button);
            _saveButton.Click += (sender, args) =>
            {

                _inputManager.HideSoftInputFromWindow(this.View.WindowToken, 0);
                AlertDialog.Builder alert = new AlertDialog.Builder(Context);
                alert.SetTitle("Error");
                if (FirstnameValidated && LastnameValidated)
                {
                    UpdateUser();
                    MadeChanges = false;
                }
                else
                {
                    alert.SetMessage("one or more fields not filled out correctly");
                    alert.SetPositiveButton("OK", (o, eventArgs) => { });
                    alert.Show();
                }                
              
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this.Context);
            _UserImage.Click += (sender, args) =>
            {
                string[] items = new string[] {"Camera", "Gallery", "cancel"};
                builder.SetItems(items, (o, eventArgs) =>
                {
                    switch (eventArgs.Which)
                    {
                        case 0:                      
                            Intent intent = new Intent(MediaStore.ActionImageCapture);   
                            StartActivityForResult(intent, 2);
                            break;
                        case 1:
                            var imageIntent = new Intent();
                            imageIntent.SetType("image/*");
                            imageIntent.SetAction(Intent.ActionGetContent);
                            StartActivityForResult(Intent.CreateChooser(imageIntent, "Select photo"), 0);
                            break;
                        case 2:
                            break;

                    }
                });
                builder.Show();
            };
            

            return view;
        }

        


        private void ViewModelOnPropertyChanged(object sender1, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            CurrentToast = Toast.MakeText(Application.Context, "User  updated", ToastLength.Short);
            CurrentToast.Show();
        }

       

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == 2)
            {
                try
                {              
                   
                    Bitmap bitmap = (Bitmap)data.Extras.Get("data");
                    bitmap = ThumbnailUtils.ExtractThumbnail(bitmap, 500, 500);
                    var circleimage = new CircleDrawable(bitmap);
                    _UserImage.SetImageDrawable(circleimage);

                    using (var stream = new MemoryStream())
                    {
                        bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
                        var bytes = stream.ToArray();
                        var base64 = Convert.ToBase64String(bytes);
                        var imagesize = System.Text.ASCIIEncoding.ASCII.GetByteCount(base64) /1024;
                        
                        if (imagesize < 1024)
                        {

                            _viewModel.imagedata = bytes;
                            MadeChanges = true;
                        }
                        else
                        {
                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this.Context);
                            alertBuilder.SetTitle("Image is to large");
                            alertBuilder.SetPositiveButton("ok", (sender, args) => { });
                            alertBuilder.Show();
                        }

                    }
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e);
                    return;                   
                }
               
               


            }
           


            if (requestCode == 0)
            {
                if (data !=null)
                {
                    CropImage(data.Data);
                }
                
            }

            if (resultCode == -1 && requestCode ==1)
            {
                Bundle bundle = data.Extras;
                Bitmap bitmap = (Bitmap) bundle.GetParcelable("data");    
                var circleimage = new CircleDrawable(bitmap);
                _UserImage.SetImageDrawable(circleimage);
                using (var stream = new MemoryStream())
                {
                    bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
                    var bytes = stream.ToArray();
                    var base64 = Convert.ToBase64String(bytes);
                    var imagesize = System.Text.ASCIIEncoding.Unicode.GetByteCount(base64);
                    imagesize = (imagesize / 1024);
                    if (imagesize < 1048)
                    {
                        _viewModel.imagedata = bytes;
                        MadeChanges = true;
                    }
                    else
                    {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this.Context);
                        alertBuilder.SetTitle("Image is to large");
                        alertBuilder.SetPositiveButton("ok", (sender, args) => { });
                        alertBuilder.Show();
                    }
                }
             

            }

        }



        public override void OnCreate(Bundle savedInstanceState)
        {
            _viewModel = new UserSettingsViewModel();
            base.OnCreate(savedInstanceState);
        }

        public override void OnStart()
        {
            ((DashboardActivity)this.Activity)._drawerLayout.DrawerOpened += DrawerLayoutOnDrawerOpened;
            base.OnStart();
        }

        public override void OnPause()
        {
           
            ((DashboardActivity)this.Activity)._drawerLayout.DrawerOpened -= DrawerLayoutOnDrawerOpened;
            base.OnPause();
        }

        private void DrawerLayoutOnDrawerOpened(object sender1, DrawerLayout.DrawerOpenedEventArgs drawerOpenedEventArgs)
        {
            if (MadeChanges)
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(this.Context);
                alert.SetTitle("Save Changes to profile?");
                alert.SetNegativeButton("No", (sender, args) => { });
                alert.SetPositiveButton("YES", (sender, args) =>
                {
                    UpdateUser();
                });
                alert.Show();
            }
        }
     

        private bool ValidateFirstname()
        {
         
                if (!string.IsNullOrWhiteSpace(_textFirstname.Text) && _textFirstname.Text.Length > 1 && _textFirstname.Text.Length < 60)
                {
                    _textFirstname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                    return true;
                }
                _textFirstname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _saveButton.Clickable = false;
            _saveButton.SetBackgroundResource(Resource.Drawable.red_button_selector);
            return false;
            
        }

        private bool ValidateLastname()
        {
            if (!string.IsNullOrWhiteSpace(_textLastname.Text) && _textLastname.Text.Length > 0 && _textLastname.Text.Length < 60)
            {
                _textLastname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.success, 0);
                return true;
            }
            _textLastname.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.do_not_match, 0);
            _saveButton.Clickable = false;
            _saveButton.SetBackgroundResource(Resource.Drawable.red_button_selector);
            return false;

        }


        private void UpdateUser()
        {

            try
            {
                _viewModel.UploadImageCommand.Execute(null);
                _viewModel.PatchUserProfileCommand.Execute(null);

                var bitmap = BitmapFactory.DecodeByteArray(_viewModel.imagedata, 0, _viewModel.imagedata.Length);
                var circleImage = new CircleDrawable(bitmap);
                ((DashboardActivity)this.Activity)._imageview.SetImageDrawable(circleImage);            
             ((DashboardActivity)this.Activity)._userName.Text = _viewModel.UserProfile.Firstname + " " + _viewModel.UserProfile.Lastname;
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e);
            }
          
        }

        private void CropImage(Android.Net.Uri Uri)
        {
           var  CropIntent = new Intent("com.android.camera.action.CROP");
            CropIntent.SetDataAndType(Uri, "image/*");
            CropIntent.PutExtra("crop", "true");
            CropIntent.PutExtra("outputX", 500);
            CropIntent.PutExtra("outputY", 500);
            CropIntent.PutExtra("aspectY", 1);
            CropIntent.PutExtra("aspectX", 1);
            CropIntent.PutExtra("ScaleUpIfNeeded", true);
            CropIntent.PutExtra("return-data", true);
            StartActivityForResult(CropIntent, 1);
        }

       
    }

    
}