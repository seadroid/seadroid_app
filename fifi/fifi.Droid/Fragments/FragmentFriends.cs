﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using fifi.Droid.Adapters;
using fifi.ViewModel;
using Fragment = Android.Support.V4.App.Fragment;
using Android.Views.InputMethods;
using fifi.Droid.Activities;
using Java.Util;

namespace fifi.Droid.Fragments
{
    public class FragmentFriends : Fragment, View.IOnClickListener
    {
        #region Help controls

        private FrameLayout _friendsList;
        private FrameLayout _friendsRequests;
        private FrameLayout _friendsAdd;
       
        private FrameLayout _activeFrame;
        

      
        #region SubViews

        #region FriendRequests
        private RelativeLayout _FriendRequestsView;
        private RelativeLayout _RecievedFriendRequests;
        private RelativeLayout _SentFriendRequests;
        private RelativeLayout _RecivedFriendsRequests_button;
        private RelativeLayout _SentFriendRequests_button;
        private ImageView _RecievedFriendRequests_button_image;
        private ImageView _SentFriendRequests_button_image;
        private RecyclerView _RecivedFriendRequest_recyclerview;
        private RecyclerView _SentFriendRequest_recyclerview;
        private SwipeRefreshLayout _SentFriendRequest_refresher;
        private SwipeRefreshLayout _RecivedFriendRequest_refresher;

        private FriendRequestsRecivedAdapter _friendRequestRecived_adapter;
        private FriendRequestSentAdapter _friendRequestSent_adapter;

        private LinearLayoutManager _friendRequestRecived_layoutManager;
        private LinearLayoutManager _friendRequestSent_LayoutManager;

        private bool _Recived_Open;
        private bool _Sent_Open;
        #endregion

        #region AddFriends

        private RecyclerView _addfriends_Recyclerview;
        private SwipeRefreshLayout _addfriends_refresher;
        private AddFriendsAdapter _add_friend_adapter;
        private LinearLayoutManager _add_friendLayoutManager;
        private RelativeLayout _AddFriendView;

        private EditText _add_friends_search_text;
        private Button _add_friends_search_button;
        #endregion

        #region Friends

        private RecyclerView _FriendsListrecyclerView;
        private SwipeRefreshLayout _FriendsListrefresher;
        private LinearLayoutManager _friendsLayoutManger;
        private FriendViewModel _viewModel;
        private FriendsAdapter _friends_adapter;
        private InputMethodManager _inputManager;
        private RelativeLayout _FriendsListView;

        private EditText _Friends_searc_text;
        private Button _Friends_search_button;

        private IMenu _menu;
        #endregion



        #endregion


        #endregion

        private static Toast currentToast;
        public static FragmentFriends NewInstance() => new FragmentFriends {Arguments = new Bundle()};

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            try
            {
                _viewModel = new FriendViewModel();
                 _inputManager = (InputMethodManager)Activity.GetSystemService(Context.InputMethodService);

                base.OnCreateView(inflater, container, savedInstanceState);
                var view = inflater.Inflate(Resource.Layout.fragment_friends, null);

                view.Click += (sender, args) =>
                {
                    _inputManager.HideSoftInputFromWindow(view.WindowToken, 0);
                };
               

                _friendsList = view.FindViewById<FrameLayout>(Resource.Id.friends_list);
                _friendsRequests = view.FindViewById<FrameLayout>(Resource.Id.friends_requests);
                _friendsAdd = view.FindViewById<FrameLayout>(Resource.Id.friends_add);
                _friendsList.SetOnClickListener(this);
                _friendsRequests.SetOnClickListener(this);
                _friendsAdd.SetOnClickListener(this);


           
                #region AddFriendsView
                _AddFriendView = view.FindViewById<RelativeLayout>(Resource.Id.AddFriends_menu);
                _addfriends_Recyclerview = view.FindViewById<RecyclerView>(Resource.Id.Add_friend_recyclerView);
                _addfriends_refresher = view.FindViewById<SwipeRefreshLayout>(Resource.Id.add_friend_refresher);
                _addfriends_refresher.Refresh += RefreshAddFriends;
                _add_friend_adapter = new AddFriendsAdapter(Activity, _viewModel);
                _add_friendLayoutManager = new LinearLayoutManager(Activity) { Orientation = LinearLayoutManager.Vertical };
                

                _addfriends_Recyclerview.SetLayoutManager(_add_friendLayoutManager);
                _addfriends_Recyclerview.SetAdapter(_add_friend_adapter);
                _add_friend_adapter.ItemClick += OnAddFriendClicked;

                _add_friends_search_button = view.FindViewById<Button>(Resource.Id._Add_friends_search_button);
                _add_friends_search_text = view.FindViewById<EditText>(Resource.Id.Add_friends_search_text);

                _add_friends_search_button.Click += OnAddFriendsSearchClicked;
                #endregion

                #region FriendsListview

                _FriendsListView = view.FindViewById<RelativeLayout>(Resource.Id.Friends_menu);

                _FriendsListrecyclerView = view.FindViewById<RecyclerView>(Resource.Id.friend_recyclerView);
                _FriendsListrefresher = view.FindViewById<SwipeRefreshLayout>(Resource.Id.friend_refresher);
                _FriendsListrefresher.Refresh += RefreshFriends;

                _friends_adapter = new FriendsAdapter(Activity, _viewModel);
                _friendsLayoutManger = new LinearLayoutManager(Activity) { Orientation = LinearLayoutManager.Vertical };

                _FriendsListrecyclerView.SetLayoutManager(_friendsLayoutManger);
                _FriendsListrecyclerView.SetAdapter(_friends_adapter);

                _Friends_searc_text = view.FindViewById<EditText>(Resource.Id.Friends_search_text);
                _Friends_search_button = view.FindViewById<Button>(Resource.Id.Friends_search_button);

                _Friends_search_button.Click += OnFriendsSearchClicked;
                _friends_adapter.ItemLongClick += OnFriendLongClick;
                #endregion

                #region FriendsRequestView

                _Recived_Open = false;
                _Sent_Open = false;

                _FriendRequestsView = view.FindViewById<RelativeLayout>(Resource.Id.FriendRequests_menu);

                _RecievedFriendRequests = view.FindViewById<RelativeLayout>(Resource.Id.Recieved_FriendRequests_menu);
                _RecivedFriendRequest_recyclerview = view.FindViewById<RecyclerView>(Resource.Id.friendRequest_Recived_recyclerView);
                _RecivedFriendRequest_refresher = view.FindViewById<SwipeRefreshLayout>(Resource.Id.friendRequest_Recived_refresher);
                _RecivedFriendsRequests_button = view.FindViewById<RelativeLayout>(Resource.Id.RecievedButton);
                _RecievedFriendRequests_button_image = view.FindViewById<ImageView>(Resource.Id.Recieved_OpenClosed_Image);
                _RecivedFriendRequest_refresher.Refresh += (sender, args) => _viewModel.LoadRecivedFriendRequestsCommand.Execute(null);
                _friendRequestRecived_adapter = new FriendRequestsRecivedAdapter(Activity, _viewModel);
                _friendRequestRecived_layoutManager = new LinearLayoutManager(Activity) { Orientation = LinearLayoutManager.Vertical };

                _RecivedFriendRequest_recyclerview.SetLayoutManager(_friendRequestRecived_layoutManager);
                _RecivedFriendRequest_recyclerview.SetAdapter(_friendRequestRecived_adapter);
                _friendRequestRecived_adapter.ItemClick  += OnAcceptDeclineFriendClicked;
                _RecivedFriendsRequests_button.Click += (sender, args) =>
                {
                    if (_Recived_Open)
                    {
                        _Recived_Open = false;
                        _RecievedFriendRequests_button_image.SetImageResource(Resource.Drawable.ic_swipe_up3x);      
                        _RecivedFriendRequest_refresher.Visibility = ViewStates.Gone;                 
                    }
                    else
                    {
                        _Recived_Open = true;
                        _RecievedFriendRequests_button_image.SetImageResource(Resource.Drawable.ic_swipe_down3x);
                    
                        _RecivedFriendRequest_refresher.Visibility = ViewStates.Visible;
                        _viewModel.LoadRecivedFriendRequestsCommand.Execute(null);
                        
                    }
                };         

                _SentFriendRequests = view.FindViewById<RelativeLayout>(Resource.Id.Sent_FriendRequests_menu);
                _SentFriendRequests_button = view.FindViewById<RelativeLayout>(Resource.Id.SentButton);
                _SentFriendRequests_button_image = view.FindViewById<ImageView>(Resource.Id.Sent_OpenClosed_Image);
                _SentFriendRequest_recyclerview = view.FindViewById<RecyclerView>(Resource.Id.friendRequest_Sent_recyclerView);
                _SentFriendRequest_refresher = view.FindViewById<SwipeRefreshLayout>(Resource.Id.friendRequest_Sent_refresher);
                _SentFriendRequest_refresher.Refresh += (sender, args) => _viewModel.LoadSentFriendRequestsCommand.Execute(null);
                _friendRequestSent_adapter = new FriendRequestSentAdapter(Activity, _viewModel);
                _friendRequestSent_LayoutManager = new LinearLayoutManager(Activity) { Orientation = LinearLayoutManager.Vertical };
                _friendRequestSent_adapter.ItemClick += OnCancelFriendRequestClicked;

                _SentFriendRequest_recyclerview.SetAdapter(_friendRequestSent_adapter);
                _SentFriendRequest_recyclerview.SetLayoutManager(_friendRequestSent_LayoutManager);


                _SentFriendRequests_button.Click += (sender, args) =>
                {
                    if (_Sent_Open)
                    {
                        _Sent_Open = false;
                        _SentFriendRequests_button_image.SetImageResource(Resource.Drawable.ic_swipe_up3x);               
                        _SentFriendRequest_refresher.Visibility = ViewStates.Gone;
                    }
                    else
                    {
                        _Sent_Open = true;
                        _SentFriendRequests_button_image.SetImageResource(Resource.Drawable.ic_swipe_down3x);
                      
                        _SentFriendRequest_refresher.Visibility = ViewStates.Visible;
                        _viewModel.LoadSentFriendRequestsCommand.Execute(null);
                    }
                };






                #endregion


                _FriendRequestsView.Visibility = ViewStates.Invisible;
                _AddFriendView.Visibility = ViewStates.Invisible;
                _FriendsListView.Visibility = ViewStates.Visible;

                SelectList(_friendsList);


             
             



                return view;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return View;
            }
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            HasOptionsMenu = true;
            base.OnCreate(savedInstanceState);
        }


        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.friend_menu, menu);
            _menu = menu;
            base.OnCreateOptionsMenu(menu, inflater);
        }

        private void RefreshFriends(object sender, EventArgs args)
        {
            _viewModel.LoadFriendsCommand.Execute(null);
            _Friends_searc_text.Text = string.Empty;
        }

        private void RefreshAddFriends(object sender, EventArgs args)
        {
            _viewModel.LoadAllUsersCommand.Execute(null);
            _add_friends_search_text.Text = string.Empty;
        }

        private void OnAddFriendClicked(Object sender, AddFriendsAdapter.AddFriendClickEventArgs args)
        {
            try
            {
               
                    _SentFriendRequest_recyclerview.Clickable = false;
                    _viewModel.user = _viewModel.NotFriends[args.Position];
                    _viewModel.AddFriendCommand.Execute(null);
                    currentToast = Toast.MakeText(Application.Context, "Friend Request sent to " + _viewModel.NotFriends[args.Position].firstname + " " + _viewModel.NotFriends[args.Position].lastname, ToastLength.Short);
                    _viewModel.NotFriends.Remove(_viewModel.NotFriends[args.Position]);
                    _addfriends_Recyclerview.GetAdapter().NotifyItemRemoved(args.Position);
                    _SentFriendRequest_recyclerview.Clickable = true;
            }

            catch (Exception e)
            {
                    Console.WriteLine(e);                  
            }
               
            
          
        }


        private void OnFriendLongClick(Object sender, FriendsAdapter.FriendClickEventArgs args)
        {
            _viewModel.user_id = _viewModel.Friends[args.Position].id.ToString();
            AlertDialog.Builder alert = new AlertDialog.Builder(Context);
            alert.SetTitle("Delete Friend");
            alert.SetMessage("Do you whant do remove " + _viewModel.Friends[args.Position].firstname + " " +
                             _viewModel.Friends[args.Position].lastname + " from your friends list?");
            alert.SetPositiveButton("Yes", (o, eventArgs) =>
            {
                _viewModel.DeleteFriendCommand.Execute(null);
                _viewModel.Friends.Remove(_viewModel.Friends[args.Position]);
                _FriendsListrecyclerView.GetAdapter().NotifyItemRemoved(args.Position);              
            });
            alert.SetNegativeButton("No", (o, eventArgs) =>
            {

            });
            alert.Show();
        }

        private void OnAcceptDeclineFriendClicked(Object sender, FriendRequestsRecivedAdapter.FriendRequestRecivedClickEventArgs args)
        {
            _viewModel.user_id = _viewModel.RecivedFriendRequests[args.Position].id.ToString();
            if (args.Result)
            {
                _viewModel.AcceptFriendCommand.Execute(null);
                _viewModel.RecivedFriendRequests.Remove(_viewModel.RecivedFriendRequests[args.Position]);
            }
            else
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(Context);
                alert.SetTitle("Decline Friend Request");
                alert.SetMessage("Do you whant to decline the friend request from " + _viewModel.RecivedFriendRequests[args.Position].firstname + " " +_viewModel.RecivedFriendRequests[args.Position].lastname +"?");
                alert.SetPositiveButton("Yes", (o, eventArgs) =>
                {
                    _viewModel.DeclineFriendRequestCommand.Execute(null);
                    _viewModel.RecivedFriendRequests.Remove(_viewModel.RecivedFriendRequests[args.Position]);
                    _RecivedFriendRequest_recyclerview.GetAdapter().NotifyItemRemoved(args.Position);
                });

                alert.SetNegativeButton("No", (o, eventArgs) =>
                {
                    return;
                });
                alert.Show();
            }
           
        }

        private  void OnCancelFriendRequestClicked(object sender, FriendRequestSentAdapter.FriendRequestSentClickEventArgs args)
        {
            _SentFriendRequest_recyclerview.FilterTouchesWhenObscured = true;
            _SentFriendRequest_recyclerview.Clickable = false;
            AlertDialog.Builder alert = new AlertDialog.Builder(Context);
            alert.SetTitle("Decline Friend Request");
            alert.SetMessage("Do you whant to cancel the friend request to " + _viewModel.SentFriendRequests[args.Position].firstname + " " + _viewModel.SentFriendRequests[args.Position].lastname + "?");
            alert.SetPositiveButton("Yes",  (o, eventArgs) =>
            {
                _viewModel.user = _viewModel.SentFriendRequests[args.Position];
                _viewModel.DeleteFriendCommand.Execute(null);
                _viewModel.SentFriendRequests.Remove(_viewModel.SentFriendRequests[args.Position]);
                _SentFriendRequest_recyclerview.GetAdapter().NotifyItemRemoved(args.Position);
                _SentFriendRequest_recyclerview.Clickable = true;

            });

            alert.SetNegativeButton("No", (o, eventArgs) =>
            {
                _SentFriendRequest_recyclerview.Clickable = true;
                return;
            });
            alert.Show();
        }

        private void OnFriendsSearchClicked(object sender, EventArgs args)
        {
            _viewModel.FriendsListSortText = _Friends_searc_text.Text;
            _viewModel.SortFriendsListCommand.Execute(null);
        }

        private void OnAddFriendsSearchClicked(object sender, EventArgs args)
        {
            _viewModel.AddFriendsListSortText = _add_friends_search_text.Text;
            _viewModel.SortAddFriendsListCommnad.Execute(null);
        }



        public override void OnStart()
        {
            base.OnStart();
            _viewModel.LoadFriendsCommand.Execute(null);
            _viewModel.PropertyChanged += ViewModel_PropertyChanged;
         

        }


        public override void OnStop()
        {
            base.OnStop();
            _viewModel.PropertyChanged -= ViewModel_PropertyChanged;
            _inputManager.HideSoftInputFromWindow(this.View.WindowToken, 0);
        }

       

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            _FriendsListrecyclerView.GetAdapter().NotifyDataSetChanged();
            _addfriends_Recyclerview.GetAdapter().NotifyDataSetChanged();
            _RecivedFriendRequest_recyclerview.GetAdapter().NotifyDataSetChanged();
            _SentFriendRequest_recyclerview.GetAdapter().NotifyDataSetChanged();

            switch (e.PropertyName)
            {
                case nameof(_viewModel.IsBusy):
                    _FriendsListrefresher.Refreshing = _viewModel.IsBusy;
                    _addfriends_refresher.Refreshing = _viewModel.IsBusy;
                    _RecivedFriendRequest_refresher.Refreshing = _viewModel.IsBusy;
                    _SentFriendRequest_refresher.Refreshing = _viewModel.IsBusy;
                    break;
            }
        }

        public void OnClick(View v)
        {
            SelectList((FrameLayout) v);
        }

        

        private void SelectList(FrameLayout frameLayout)
        {
            if (_activeFrame != null)
            {
                _activeFrame.Selected = false;
                _activeFrame = null;
            }

            _activeFrame = frameLayout;
            frameLayout.Selected = true;

            if (frameLayout == _friendsList)
            {
                _FriendRequestsView.Visibility = ViewStates.Invisible;
                _AddFriendView.Visibility = ViewStates.Invisible;
                _FriendsListView.Visibility = ViewStates.Visible;
                _inputManager.HideSoftInputFromWindow(_friendsList.WindowToken, 0);
                _Recived_Open = false;
                _RecievedFriendRequests_button_image.SetImageResource(Resource.Drawable.ic_swipe_up3x);
                _RecivedFriendRequest_refresher.Visibility = ViewStates.Gone;
                _Sent_Open = false;
                _SentFriendRequests_button_image.SetImageResource(Resource.Drawable.ic_swipe_up3x);
                _SentFriendRequest_refresher.Visibility = ViewStates.Gone;
                _viewModel.LoadFriendsCommand.Execute(null);

            }

            if (frameLayout == _friendsRequests)
            {
                _FriendRequestsView.Visibility = ViewStates.Visible;
                _AddFriendView.Visibility = ViewStates.Invisible;
                _FriendsListView.Visibility = ViewStates.Invisible;
                _inputManager.HideSoftInputFromWindow(_friendsRequests.WindowToken, 0);

            }

            if (frameLayout == _friendsAdd)
            {
                _FriendRequestsView.Visibility = ViewStates.Invisible;
                _AddFriendView.Visibility = ViewStates.Visible;
                _FriendsListView.Visibility = ViewStates.Invisible;
                _viewModel.LoadAllUsersCommand.Execute(null);
                _inputManager.HideSoftInputFromWindow(_friendsAdd.WindowToken, 0);
                _Recived_Open = false;
                _RecievedFriendRequests_button_image.SetImageResource(Resource.Drawable.ic_swipe_up3x);
                _RecivedFriendRequest_refresher.Visibility = ViewStates.Gone;
                _Sent_Open = false;
                _SentFriendRequests_button_image.SetImageResource(Resource.Drawable.ic_swipe_up3x);
                _SentFriendRequest_refresher.Visibility = ViewStates.Gone;
            }
        }
    }
}