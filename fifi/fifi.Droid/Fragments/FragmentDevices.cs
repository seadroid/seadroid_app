﻿using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using fifi.Droid.Activities;
using fifi.Droid.Adapters;
using fifi.ViewModel;
using Debug = System.Diagnostics.Debug;

namespace fifi.Droid.Fragments
{
    public class FragmentDevices : Fragment
    {
        private DeviceAdapter _adapter;
        private LinearLayoutManager _layoutManager;
        private BaseActivity _activity;
        private RecyclerView _recyclerView;
        private SwipeRefreshLayout _refresher;
        private DevicesViewModel _viewModel;
        private IMenu _menu;

        public static FragmentDevices NewInstance() => new FragmentDevices { Arguments = new Bundle() };


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.fragment_devices, null);

            _viewModel = new DevicesViewModel();

            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            _refresher = view.FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);

            _refresher.Refresh += (sender, e) => _viewModel.LoadDevicesCommand.Execute(null);

            _adapter = new DeviceAdapter(Activity, _viewModel);
            _adapter.ItemClick += OnItemClick;
            _adapter.ItemLongClick += OnItemLongClick;
            _layoutManager = new LinearLayoutManager(Activity) { Orientation = LinearLayoutManager.Vertical };
            _recyclerView.SetLayoutManager(_layoutManager);
            _recyclerView.SetAdapter(_adapter);
            _recyclerView.ClearOnScrollListeners();
            _recyclerView.AddOnScrollListener(new DevicesOnScrollListenerListener(_viewModel, _layoutManager));

  

            return view;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            HasOptionsMenu = true;
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.device_menu, menu);
            _menu = menu;
            base.OnCreateOptionsMenu(menu, inflater);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
          
            _activity = (BaseActivity)Activity;
            _activity.SupportActionBar.Title = string.Empty;
            base.OnActivityCreated(savedInstanceState);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            var intent = new Intent(this.Context, typeof(MasterPairingActivity));
            intent.PutExtra("StartedByMastersFragment", true);
            StartActivityForResult(intent, 1);
            return base.OnOptionsItemSelected(item);
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            _viewModel.LoadDevicesCommand.Execute(null);
        }

        public override async void OnStart()
        {
            base.OnStart();
            _viewModel.PropertyChanged += ViewModel_PropertyChanged;
            if (_viewModel.Masters.Count == 0)
            {
                await _viewModel.ExecuteLoadDevicesCommandAsync();
            }
        }

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(_viewModel.IsBusy):
                    _refresher.Refreshing = _viewModel.IsBusy;
                    break;
            }
        }

        public override void OnStop()
        {
            base.OnStop();
            _viewModel.PropertyChanged -= ViewModel_PropertyChanged;
        }

        private void OnItemClick(object sender, MasterClickEventArgs args)
        {
            var master = _viewModel.Masters[args.Position];
            // Master clicked - navigate to show trips
            var intent = new Intent(Activity, typeof(DeviceDetailsActivity));
            intent.PutExtra("Id", master.Name);
            DeviceDetailsActivity.Master = master;
            Activity.StartActivity(intent);
        }

        private void OnItemLongClick(object sender, MasterClickEventArgs args)
        {
            var master = _viewModel.Masters[args.Position];
            // Master long clicked - delete master
            Debug.WriteLine("Delete master: " + master.Name);
            //await _viewModel.ExecuteDeleteMasterCommand(master);
        }
    }

    internal class DevicesOnScrollListenerListener : RecyclerView.OnScrollListener
    {
        private readonly LinearLayoutManager _layoutManager;
        private readonly DevicesViewModel _viewModel;

        public DevicesOnScrollListenerListener(DevicesViewModel viewModel, LinearLayoutManager layoutManager)
        {
            _layoutManager = layoutManager;
            _viewModel = viewModel;
        }

        public override void OnScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            base.OnScrolled(recyclerView, dx, dy);
            if (_viewModel.IsBusy || _viewModel.Masters.Count == 0 || !_viewModel.CanLoadMore)
            {
                return;
            }

            var lastVisiblePosition = _layoutManager.FindLastCompletelyVisibleItemPosition();
            if (lastVisiblePosition == RecyclerView.NoPosition)
            {
                return;
            }

            // If we are at the bottom and can load more.
            if (lastVisiblePosition == _viewModel.Masters.Count - 1)
            {
                //_viewModel.LoadMoreDevicesCommand.Execute(null);
            }


        }
    }
}