﻿
using System;
using System.ComponentModel;
using System.IO;
using Android.Content;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using fifi.Client.DataObjects;
using fifi.Droid.Activities;
using System.Xml.Serialization;
using fifi.Droid.Adapters;
using Fragment = Android.Support.V4.App.Fragment;
using fifi.ViewModel;

namespace fifi.Droid.Fragments
{
   

    public class FragmentArmbands :Fragment
    {
        private LinearLayoutManager _layoutManager;
        private ArmbandAdapter _adapter;
        private RecyclerView _recyclerView;
        private SwipeRefreshLayout _refresher;
        private IMenu _menu;


        public static FragmentArmbands NewInstance() => new FragmentArmbands() {Arguments = new Bundle()};
        private ArmbandViewModel _viewModel;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_pairing, null);
            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            _refresher = view.FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
            _refresher.Enabled = false;
            _viewModel = new ArmbandViewModel();
            _adapter = new ArmbandAdapter(Activity, _viewModel);
            _adapter.ItemClick += AdapterOnItemClick;
            _recyclerView.SetAdapter(_adapter);
            _layoutManager = new LinearLayoutManager(Activity){Orientation = LinearLayoutManager.Vertical};
            _recyclerView.SetLayoutManager(_layoutManager);
            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
            return view;
            

        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
           _adapter.NotifyDataSetChanged();
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            HasOptionsMenu = true;
            base.OnCreate(savedInstanceState);
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.armbands_menu, menu);
            _menu = menu;
            base.OnCreateOptionsMenu(menu, inflater);
            

        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            var intent = new Intent(Activity, typeof(ArmbandPairingActivity));
            StartActivityForResult(intent, 1);
            return base.OnOptionsItemSelected(item);
        }

        public override void OnStart()
        {
            base.OnStart();
            _viewModel.GetArmbandsCommand.Execute(null); 
            _viewModel.SearchForDevicesCommand.Execute(null);
        }

        public override void OnStop()
        {
            base.OnStop();
            _viewModel.SoftStopSearchingForDevicesCommand.Execute(null);
        }

        public override void OnPause()
        {
            base.OnPause();
            _viewModel.HardStopSearchingForDevicesCommand.Execute(null);

        }

        public override void OnResume()
        {
            base.OnResume();
          //  _viewModel.SearchForDevicesCommand.Execute(null);

        }

        private void AdapterOnItemClick(object sender, ArmbandAdapter.ArmbandClickEventArgs args)
        {
          _viewModel._SelectedArmband = _viewModel.Armbands[args.Position];
          var intent = new Intent(Activity, typeof(PairingDetailsActivity));
          PairingDetailsActivity._viewModel = new PairingDetailsViewModel() { Wristband = _viewModel._SelectedArmband };
           
          StartActivityForResult(intent, 1);
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            if (resultCode == -1 && !string.IsNullOrEmpty(data.GetStringExtra("Armband")))
            {
                Armband wristband = new Armband();
                XmlSerializer deserializer = new XmlSerializer(typeof(Armband));
                using (TextReader tr = new StringReader(data.GetStringExtra("Armband")))
                {
                    wristband = (Armband)deserializer.Deserialize(tr);
                }    
                _viewModel._SelectedArmband = wristband;
                _viewModel.UpdateArmband.Execute(null);
                
            }
            else
            {
                _viewModel.GetArmbandsCommand.Execute(null);
            }
        }
    }
}