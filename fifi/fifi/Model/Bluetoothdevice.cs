﻿using System;
using Plugin.BLE.Abstractions.Contracts;

namespace fifi.Model
{
    public class BluetoothDevice
    {
        public IDevice Device { get; set; }
        public string DeviceName { get; set; }
        public string BatteryLevel { get; set; }
        public DateTime LastContacted { get; set; }

        public BluetoothDevice(IDevice device, string deviceName, string batterylevel)
        {
            BatteryLevel = batterylevel;
            Device = device;
            DeviceName = deviceName;
            LastContacted = new DateTime(2000, 1, 1, 0, 0, 0);

        }
        public BluetoothDevice(IDevice device, string deviceName, string batterylevel, DateTime lastcontacted)
        {
            BatteryLevel = batterylevel;
            Device = device;
            DeviceName = deviceName;
            LastContacted = lastcontacted;
        }
    }
}