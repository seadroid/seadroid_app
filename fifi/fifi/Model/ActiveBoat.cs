﻿using fifi.Client.DataObjects;

namespace fifi.Model
{
    public class ActiveBoat
    {
        public int Master_id { get; set; }
       
        public int? User_id { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Recieved_at { get; set; }
        public string User_name { get; set; }      
        public Boat Boat { get; set; }
        public bool Active { get; set; }

        public ActiveBoat(int masterId , int? userId, string longitude, string latitude, string recieved_at, Boat boat, bool active)
        {
            Master_id = masterId;
            Boat = boat;
            User_id = userId;
            Longitude = longitude;
            Latitude = latitude;
            Recieved_at = recieved_at;
            Active = active;
            /*  User_name = user_name;
              Boat_name = boat_name; */

        }
    }
}