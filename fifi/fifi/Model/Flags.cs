﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fifi.Model
{
    public class Flag
    {
        private int _image;
        private string _country;
        private string _countrycode;

        public string Countrycode
        {
            get { return _countrycode; }
            set { _countrycode = value; }
        }

        public int Image
        {
            get { return _image; }
            set { _image = value; }
        }

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public Flag(string country, int image, string countrycode)
        {
            Image = image;
            Country = country;
            Countrycode = countrycode;
        }


    }
}
