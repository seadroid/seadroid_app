﻿using System.Collections.Generic;
using System.Threading.Tasks;

using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Utils.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace fifi.Helpers
{
    public class UserProfileHelper
    {
        // Returns info for the authenticated user
        public static async Task<UserProfile> GetUserProfileAsync(SeaDroidApi client)
        {
            var userProf = JsonConvert.DeserializeObject<UserProfileCollection>(await client.GetUserProfile());

            if (userProf == null) return null;
            var userProfile = userProf.data[0];
			if (userProfile.Lastname == null) 
			{
				userProfile.Lastname = "";
			}
			Settings.Current.UserFirstname = userProfile.Firstname;
			Settings.Current.UserLastname = userProfile.Lastname;
            Settings.Current.UserId = userProfile.Id.ToString();
            Settings.Current.UserImageUrl = userProfile.image;
            
            return userProfile;
        }
    }
}