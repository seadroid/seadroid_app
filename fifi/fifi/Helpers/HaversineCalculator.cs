﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using fifi.Client.DataObjects;

namespace fifi.Helpers
{
    public static class HaversineCalculator
    {
        public static void HaversineDistance(this IEnumerable<Coordinate> list)
        {
            var coordinates = list.ToList();
            double R = 6371;
            var coord = coordinates[coordinates.Count - 1];

            foreach (var coordinate in coordinates)
            {
                var pos1Latitude = double.Parse(coord.Latitude.Replace(".", ","));
                var pos1Longitude = double.Parse(coord.Longitude.Replace(".", ","));

                var pos2Latitude = double.Parse(coordinate.Latitude.Replace(".", ","));
                var pos2Longitude = double.Parse(coordinate.Longitude.Replace(".", ","));

                var lat = (pos2Latitude - pos1Latitude).ToRadians();
                var lng = (pos2Longitude - pos1Longitude).ToRadians();

                var h1 = Math.Sin(lat / 2) * Math.Sin(lat / 2) +
                         Math.Cos(pos1Latitude.ToRadians()) * Math.Cos(pos2Latitude.ToRadians()) * Math.Sin(lng / 2) *
                         Math.Sin(lng / 2);

                var h2 = 2 * Math.Asin(Math.Min(1, Math.Sqrt(h1)));
                coordinate.Distance = R * h2;
            }
        }

        public static double ToRadians(this double val)
        {
            return (Math.PI / 180) * val;
        }
    }
}