﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Utils;
using fifi.Utils.Extension;
using fifi.Utils.Helpers;
using MvvmHelpers;
using Newtonsoft.Json;

namespace fifi.ViewModel
{
    public class DevicesViewModel : ViewModelBase
    {
       // private readonly IClient _client;
        private readonly SeaDroidApi _api;

        private ICommand _loadDevicesCommand;
        public ObservableRangeCollection<Master> Masters { get; } = new ObservableRangeCollection<Master>();

        public ICommand LoadDevicesCommand
            =>
            _loadDevicesCommand ??
            (_loadDevicesCommand = new RelayCommand(async () => await ExecuteLoadDevicesCommandAsync()));

        public DevicesViewModel()
        {
            _api = new SeaDroidApi();
        }


        /// <summary>
        /// loads the users Masters
        /// </summary>
        /// <returns></returns>
        public async Task ExecuteLoadDevicesCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }

            ProgressDialogManager.LoadProgressDialog("Loading Devices...");

            try
            {
                IsBusy = true;
                CanLoadMore = true;        
                var items = JsonConvert.DeserializeObject<MasterCollection>(await _api.GetUserMasters());

                foreach (var item in items.data)
                {
                    if (item.UpdatedAt == null)
                    {
                        item.UpdatedAt = item.CreatedAt;
                    }

                    foreach (var trip in item.Trips)
                    {
                        if (trip.UpdatedAt == null)
                        {
                            trip.UpdatedAt = trip.StartedAt;
                        }
                    }
                }

                Masters.ReplaceRange(items.data);

                CanLoadMore = Masters.Count == 25;
                Debug.WriteLine("LoadDevices");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
                ProgressDialogManager.DisposeProgressDialog();
            }

            if (Masters.Count == 0)
            {
                UserDialogs.Instance.Alert("Looks like you don't have any FoundDevices yet, feel free to add one.",
                    "No Devices", "OK");
            }
        }
    }
}
