﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Utils.Helpers;
using Newtonsoft.Json;
using Org.BouncyCastle.Asn1.Crmf;
using Org.BouncyCastle.Bcpg.Attr;

namespace fifi.ViewModel
{
    public class MasterSettingsViewModel : ViewModelBase
    {

        #region Parameters
        public Master _master;
        public string _base64Image;
        public Boat _boat;
        private SeaDroidApi _api;
        public bool Updated;
        #endregion

        #region commands
        private ICommand _updateBoatCommand;
        private ICommand _updateBoatImageCommand;
        private ICommand _getBoatCommand;

        public ICommand GetBoatCommand => _getBoatCommand ??(_getBoatCommand = new RelayCommand((ExecuteGetBoatAsync)));
        public ICommand UpdateBoatImageCommand => _updateBoatImageCommand ??(_updateBoatImageCommand = new RelayCommand(()=> ExecuteUpdateBoatImageAsync(_base64Image)));
        public ICommand UpdateBoatCommand => _updateBoatCommand ??(_updateBoatCommand = new RelayCommand(( () =>  ExecuteUpdateBoatAsync(_boat))));
        #endregion

        public MasterSettingsViewModel()
        {
           _api = new SeaDroidApi();   
            _boat = new Boat();
        }

        #region Functions
        private async void ExecuteGetBoatAsync()
        {
            if (_master.BoatId != null)
            {
                ProgressDialogManager.LoadProgressDialog("Getting Master info...");
                _boat = JsonConvert.DeserializeObject<SpecificBoat>(await _api.GetBoat(_master.BoatId.ToString())).data;
                await Task.Delay(300);
                ProgressDialogManager.DisposeProgressDialog();

            }
            OnPropertyChanged();

        }

        private async void ExecuteUpdateBoatAsync(Boat boat)
        {
           ProgressDialogManager.LoadProgressDialog("Updating Master info...");
           var result = await _api.UpdateBoat(boat);
           Updated = true;
           ProgressDialogManager.DisposeProgressDialog();
           UserDialogs.Instance.Alert(new AlertConfig(){ OnAction = new Action(InvokePropertyChanged), Message = "Master Updated", OkText = "Ok", Title = "Succes"});        
        }

        private void InvokePropertyChanged()
        {
            OnPropertyChanged();
        }

        
        private async void ExecuteUpdateBoatImageAsync(string base64image)
        {
          var result = await _api.UploadBoatImage(Convert.ToString(_boat.id), base64image);
        }
        #endregion

    }
}