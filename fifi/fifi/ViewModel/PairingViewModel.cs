﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using fifi.Client.DataObjects;
using fifi.Helpers;
using fifi.Model;
using fifi.Utils;
using fifi.Utils.Extension;
using fifi.Utils.Helpers;
using MvvmHelpers;
using PCLStorage;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Extensions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Splat;
using fifi.Client.Interfaces;
using Newtonsoft.Json;
using Plugin.BLE.Abstractions;

namespace fifi.ViewModel
{
    public class PairingViewModel : ViewModelBase
    {

        public IDevice device;
        public Armband wristband;
        public UserProfile UserProfile;
        private SeaDroidApi _api;
        private readonly CancellationTokenSource _cancelSourece;
        IAdapter adapter = CrossBluetoothLE.Current.Adapter;
        IBluetoothLE ble = CrossBluetoothLE.Current;

        public ObservableRangeCollection<BluetoothDevice> FoundDevices { get; set; } 
        public ObservableRangeCollection<BluetoothDevice> Bluetoothdevices { get; set; } = new ObservableRangeCollection<BluetoothDevice>();
        public ArmbandsList Armbands { get; set; } = new ArmbandsList();

        private ICommand _LoadBluetoothCommand;
        private ICommand _PingBluetoothCommand;
        private ICommand _RefreshBluetoothCommand;
        private ICommand _updateWristbandCommand;
		private ICommand _checkbluetoothCommand;
        private ICommand _stopBluetoothCommand;

        public ICommand StopBluetoothCommand => _stopBluetoothCommand ??(_stopBluetoothCommand = new RelayCommand(ExecuteStopLoadBluetoothCommand));
       
        public ICommand LoadBluetoothCommand
            => _LoadBluetoothCommand ??
               (_LoadBluetoothCommand = new RelayCommand(async () => await ExecuteLoadBluetoothCommandAsync()));

        public ICommand PingBluetoothComandT
            =>
                _PingBluetoothCommand ??
                (_PingBluetoothCommand = new RelayCommand(async () => await ExecutePingBluetoothdeviceCommandAsync(device)));

        //public ICommand RefreshBluetoothcommand
        //    =>
        //        _RefreshBluetoothCommand ??
        //        (_RefreshBluetoothCommand = new RelayCommand(async () => await ExecuteRefreshBluetoothCommandAsync()));
        /// <summary>
        /// Takes wristband property from the Viewmodel and updates / creates a new wristband in the database;
        /// </summary>
        public  ICommand UpdateWristbandCommand
             => _updateWristbandCommand ?? (_updateWristbandCommand = new RelayCommand(async () => await ExecuteUpdateWristbandCommandAsync()));

		public ICommand CheckBluetoothCommand 
		=> _checkbluetoothCommand ?? (_checkbluetoothCommand = new RelayCommand(async () => await ExecuteCheckBluetoothCommandAsync()));


		public PairingViewModel()
		{
            _api = new SeaDroidApi();
			var a = ble.IsOn;
		    _cancelSourece = new CancellationTokenSource();
            FoundDevices = new ObservableRangeCollection<BluetoothDevice>();

        }


        public async Task ExecuteUpdateWristbandCommandAsync()
        {
            IsBusy = true;


         var bluetemp = new ObservableRangeCollection<BluetoothDevice>();

         Armbands = JsonConvert.DeserializeObject<ArmbandsList>(await _api.GetWristbands());


            var DidContainWristband = false;
            foreach (var band in Armbands.data)
            {
                if ( band.serial == wristband.serial)
                {
                var result = await _api.UpdateArmband(band.id, wristband.firstname, wristband.lastname, 
                    wristband.gender, wristband.birthday, (int)wristband.weight, (int)wristband.height, (int)wristband.country_id, wristband.haircolor,
                    (int)wristband.swim_exp, (int)wristband.sea_exp);
                    DidContainWristband = true;
                    Debug.WriteLine(result);
                }

            }
            if (DidContainWristband == false)
            {
              var UserProfile = await UserProfileHelper.GetUserProfileAsync(_api);
              var result =  await  _api.CreateArmband(wristband.firstname, wristband.lastname, wristband.serial, wristband.gender,
                  wristband.birthday, (int)wristband.weight, (int)wristband.height, (int)wristband.country_id, wristband.haircolor,
                  (int)wristband.swim_exp, (int)wristband.sea_exp, UserProfile.Id);
              Debug.WriteLine(result);
            }


            //Armbands = JsonConvert.DeserializeObject<ArmbandsList>(await _api.GetWristbands());

            //foreach (var Device in Bluetoothdevices)
            //{
            //    var wristbandfound = false;
            //    var SplitDeviceName = Device.Device.Name.Split(' ');
            //    foreach (var band in Armbands.data)
            //    {                  
            //        if (Device.Device.Name == band.serial)
            //        {
                       
            //          bluetemp.Add(new BluetoothDevice(Device.Device, band.firstname + " " + band.lastname + " - " + SplitDeviceName[1]));
            //          wristbandfound = true;

            //        }
                    
            //    }
            //    if (wristbandfound == false)
            //    {                    
            //        bluetemp.Add(new BluetoothDevice(Device.Device, "SEADROID " + SplitDeviceName[1]));                   
            //    }


           
            //}

            
   
            //Bluetoothdevices.ReplaceRange(bluetemp);
            //await Task.Delay(10);
            //IsBusy = false;


        }

        public async Task<bool> ExecutePingBluetoothdeviceCommandAsync(IDevice device)
        {
           
          
            try
            {
                UserProfile = await UserProfileHelper.GetUserProfileAsync(_api);
                IsBusy = true;
                ProgressDialogManager.LoadProgressDialog("Pinging Device...");
                CancellationTokenSource stopToken = new CancellationTokenSource(10000);
                await adapter.ConnectToDeviceAsync(device);
                var service = await device.GetServiceAsync(Guid.Parse("FFFFFFF0-00F7-4000-B000-000000000000"));       
                var characteristic = await service.GetCharacteristicAsync(Guid.Parse("FFFFFFF5-00F7-4000-B000-000000000000"));                           
                Debug.WriteLine("Characteristic Name: " + characteristic.Name + " " + "Characterisitc ID: " + characteristic.Id);
                Debug.WriteLine("ID: "+ device.Id + "" + "NativeDeivce: " + device.NativeDevice);             
                var data = new byte[5];
                data[0] = 0x80;
                data[1] = 0xbe;
                data[2] = 0xf5;
                data[3] = 0xac;
                data[4] = 0xff;
               await Task.Delay(10);                 
               var  answer = await characteristic.WriteAsync(data);                                    
               service = await device.GetServiceAsync(Guid.Parse("FFFFFFF0-00F7-4000-B000-000000000000"));
               characteristic = await service.GetCharacteristicAsync(Guid.Parse("FFFFFFF1-00F7-4000-B000-000000000000"));

             
                var stealthmodedata = new byte[1];
               stealthmodedata[0] = 0x00;
                await Task.Delay(10);       
               var stealthmode =  await characteristic.WriteAsync(stealthmodedata);                    
               stealthmodedata = new byte[1]; stealthmodedata[0] = 0x03;
                await Task.Delay(10);
               stealthmode =  await characteristic.WriteAsync(stealthmodedata);
               Debug.WriteLine("Verification Answer: " + answer);
                await Task.Delay(10);
               await adapter.DisconnectDeviceAsync(device);
               IsBusy = false;
               return true;
            }
            catch (Exception e)
            {            
                Debug.WriteLine(e);
                IsBusy = false;
                return false;
            }
            finally
            {
                ProgressDialogManager.DisposeProgressDialog();
                IsBusy = false;
            }

           
        }
     

        //public async Task ExecuteRefreshBluetoothCommandAsync()
        //{
        //    ObservableRangeCollection<IDevice> bluFound = new ObservableRangeCollection<IDevice>();
         
        //    if (IsBusy || adapter.IsScanning)
        //    {
        //        return;
        //    }

        //    try
        //    {
          

        //        if (ble.IsOn == true)
        //        {
        //            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
        //            if (status != PermissionStatus.Granted)
        //            {
        //                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] {Permission.Location});
        //                status = results[Permission.Location];
        //            }

        //            if (status == PermissionStatus.Granted)            
        //            {
        //                IsBusy = true;
        //                Armbands = JsonConvert.DeserializeObject<ArmbandsList>(await _api.GetWristbands());
        //                await adapter.StartScanningForDevicesAsync(dev => dev.Name != null);
        //                var devices = adapter.DiscoveredDevices;
                    
        //                foreach (var bluetoothdevice in devices)
        //                {
        //                    try
        //                    {
        //                        if (bluetoothdevice.Name.Contains("V.ALRT"))
        //                        {
        //                            bluFound.Add(bluetoothdevice);
        //                        }
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        Debug.WriteLine(e);
        //                    }
        //                }

        //                foreach (var device in bluFound)
        //                {
        //                    if (FoundDevices.Contains(device) == false)
        //                    {
        //                        FoundDevices.Add(device);
        //                    }
        //                }
        //            }
        //        }

        //        else
        //        {
        //            UserDialogs.Instance.Alert("To See pairable FoundDevices please turn on bluetooth", "ok");
        //        }
        //    }

        //    catch (Exception e)
        //    {
        //        Debug.WriteLine(e);
        //    }

        //    finally
        //    {
        //        var TempBluetoothDevices = new ObservableRangeCollection<BluetoothDevice>();
        //        foreach (var bluetoothDevice in FoundDevices)
        //        {

        //            var SplitDeviceName = bluetoothDevice.Name.Split(' ');

        //            var DeviceName = "SEADRIOD " + SplitDeviceName[1];

        //            await adapter.ConnectToDeviceAsync(bluetoothDevice);
        //            var service = await device.GetServiceAsync(Guid.Parse("FFFFFFF0-00F7-4000-B000-000000000000"));
        //            var characteristic = await service.GetCharacteristicAsync(Guid.Parse("FFFFFFF5-00F7-4000-B000-000000000000"));
        //            var data = new byte[5];
        //            data[0] = 0x80;
        //            data[1] = 0xbe;
        //            data[2] = 0xf5;
        //            data[3] = 0xac;
        //            data[4] = 0xff;
        //            await Task.Delay(10);
        //            var answer = await characteristic.WriteAsync(data);
        //            service = await device.GetServiceAsync(Guid.Parse("0000180f-0000-1000-8000-00805f9b34fb"));
        //            characteristic = await service.GetCharacteristicAsync(Guid.Parse("00002a19-0000-1000-8000-00805f9b34fb"));
        //            var batterylevelBytes = await characteristic.ReadAsync();
        //            var batterylevel = BitConverter.ToString(batterylevelBytes);
        //            await Task.Delay(10);




        //            service = await device.GetServiceAsync(Guid.Parse("FFFFFFF0-00F7-4000-B000-000000000000"));
        //            characteristic = await service.GetCharacteristicAsync(Guid.Parse("FFFFFFF1-00F7-4000-B000-000000000000"));
        //            var stealthmodedata = new byte[1]; stealthmodedata[0] = 0x03;
        //            var stealthmode = await characteristic.WriteAsync(stealthmodedata);
           
        //            await adapter.DisconnectDeviceAsync(bluetoothDevice);
        //            await adapter.DisconnectDeviceAsync(device);

        //            foreach (var wristband in Armbands.data)
        //            {
                       
        //                if (bluetoothDevice.Name == wristband.serial )
        //                {
        //                   DeviceName = wristband.firstname + " " + wristband.lastname + " - " + SplitDeviceName[1];
        //                }
                      
        //            }
        //        TempBluetoothDevices.Add(new BluetoothDevice(bluetoothDevice,  DeviceName, batterylevel));

        //        }

        //        Bluetoothdevices.ReplaceRange(TempBluetoothDevices);

        //        IsBusy = false;
        //        ProgressDialogManager.DisposeProgressDialog();
        //    }
        //}



        public  void ExecuteStopLoadBluetoothCommand()
        {
            _cancelSourece.Cancel();
            adapter.StopScanningForDevicesAsync();
        }

		public async Task ExecuteCheckBluetoothCommandAsync()

		{
			var a = ble.IsOn;
			await Task.Delay(100);

		}

        public async Task ExecuteLoadBluetoothCommandAsync()
        {
          loadbluetooth(_cancelSourece.Token);       
        }

        public async void loadbluetooth(CancellationToken token)
        {
            ObservableRangeCollection<IDevice> bluFound = new ObservableRangeCollection<IDevice>();
            FoundDevices = new ObservableRangeCollection<BluetoothDevice>();

            var blueAllowed = false;
            await adapter.StopScanningForDevicesAsync();
            if (IsBusy)
            {
                return;
            }

            ProgressDialogManager.LoadProgressDialog("Searching for bluetooth devices...");

            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                    {
                        UserDialogs.Instance.Alert("need location", "SEADROID needs acess to your location to find Armbands", "ok");
                    }
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Location });
                    status = results[Permission.Location];

                    if (status == PermissionStatus.Granted)
                    {
                        blueAllowed = true;
                    }
                }

                if (ble.State == BluetoothState.Unknown)
                {
                    return;
                }

                if (ble.IsOn == true)
                {

#if __IOS__

                    blueAllowed = true;

#endif

                    Armbands = JsonConvert.DeserializeObject<ArmbandsList>(await _api.GetWristbands());


                    if (blueAllowed)
                        IsBusy = true;
                    {
                        Stopwatch watch = new Stopwatch();
                        watch.Start();
                        while (true)
                        {

                            if (token.IsCancellationRequested)
                            {
                                ProgressDialogManager.DisposeProgressDialog();
                                return;
                            }                          

                            await adapter.StartScanningForDevicesAsync(dev => dev.Name != null && dev.Name.Contains("V.ALRT"));
                            var devices = adapter.DiscoveredDevices;
                         

                            
                               

                            foreach (var device in devices)
                            {
                                var NewDevice = true;
                                
                                    foreach (var bluetoothdevice in FoundDevices)
                                    {
                                        if (device.Name == bluetoothdevice.DeviceName)
                                        {
                                            NewDevice = false;
                                        }
                                    }
                                
                               
                                if (NewDevice)
                                {
                                    FoundDevices.Add(new BluetoothDevice(device, device.Name, string.Empty));
                                }
                            }

                            var TempBluetoothDevices = new ObservableRangeCollection<BluetoothDevice>();

                            foreach (var BluetoothDevice in FoundDevices)
                            {
                                if (DateTime.Now.Subtract(BluetoothDevice.LastContacted).TotalMinutes > 2)
                                { 
                                 var SplitDeviceName = BluetoothDevice.DeviceName.Split(' ');

                                 var DeviceName = "SEADRIOD " + SplitDeviceName[1];
                                 await adapter.ConnectToDeviceAsync(BluetoothDevice.Device);
                                 var service = await BluetoothDevice.Device.GetServiceAsync(Guid.Parse("FFFFFFF0-00F7-4000-B000-000000000000"));
                                 var characteristic = await service.GetCharacteristicAsync(Guid.Parse("FFFFFFF5-00F7-4000-B000-000000000000"));
                                 var data = new byte[5];
                                 data[0] = 0x80;
                                 data[1] = 0xbe;
                                 data[2] = 0xf5;
                                 data[3] = 0xac;
                                 data[4] = 0xff;
                                 await Task.Delay(10, token);
                                 var answer = await characteristic.WriteAsync(data);
                                 service = await BluetoothDevice.Device.GetServiceAsync(Guid.Parse("0000180f-0000-1000-8000-00805f9b34fb"));
                                 characteristic = await service.GetCharacteristicAsync(Guid.Parse("00002a19-0000-1000-8000-00805f9b34fb"));
                                 var batterylevelBytes = await characteristic.ReadAsync();

                         
                                 service = await BluetoothDevice.Device.GetServiceAsync(Guid.Parse("FFFFFFF0-00F7-4000-B000-000000000000"));
                                 characteristic = await service.GetCharacteristicAsync(Guid.Parse("FFFFFFF1-00F7-4000-B000-000000000000"));

                                 var stealthmodedata = new byte[1];
                                 stealthmodedata[0] = 0x03;
                                 await Task.Delay(10, token);
                                 var stealthmode = await characteristic.WriteAsync(stealthmodedata);                              
                                 var batterylevel = Convert.ToString((int)batterylevelBytes[0]);
                                 await adapter.DisconnectDeviceAsync(BluetoothDevice.Device);


                                  foreach (var wristband in Armbands.data)
                                  {

                                    if (BluetoothDevice.DeviceName == wristband.serial)
                                    {
                                        DeviceName = wristband.firstname + " " + wristband.lastname + " - " + SplitDeviceName[1];

                                    }

                                  }

                                 BluetoothDevice.BatteryLevel = batterylevel;
                                 BluetoothDevice.LastContacted = DateTime.Now;

                              

                                }

                             TempBluetoothDevices.Add(BluetoothDevice);
                            }


                            Bluetoothdevices.ReplaceRange(TempBluetoothDevices);
                            ProgressDialogManager.DisposeProgressDialog();
                            IsBusy = false;


                        }
                    }
                }

                else
                {
                    UserDialogs.Instance.Alert("To See pairable FoundDevices please turn on bluetooth", "ok");
                }

                IsBusy = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            finally
            {               
                ProgressDialogManager.DisposeProgressDialog();
            }

        }

    }


   
}