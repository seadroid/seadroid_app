﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Utils.Helpers;
using Newtonsoft.Json;
using Plugin.SecureStorage;

namespace fifi.ViewModel
{
    public class UserSettingsViewModel : ViewModelBase
    {
        private SeaDroidApi _api;     
        public UserProfile UserProfile { get; set; }
        private ICommand _getUserProfileCommand;
        private ICommand _PatchUserProfileCommand;
        private ICommand _UploadImageCommand;
        public byte[] imagedata;
       
        

        public ICommand UploadImageCommand => _UploadImageCommand ??
         (_UploadImageCommand =   new RelayCommand( async () => await ExecuteUploadImageAsync(Convert.ToBase64String(imagedata))));

        public ICommand PatchUserProfileCommand => _PatchUserProfileCommand ??
       (_PatchUserProfileCommand = new RelayCommand(async ()=> await ExecuteUpdateUserProfileAsync(UserProfile.Firstname,UserProfile.Lastname,UserProfile.Email)));

        public ICommand GetUserProfileCommand => _getUserProfileCommand ?? 
            (_getUserProfileCommand = new RelayCommand(ExecuteGetUserProfileAsync));


        public UserSettingsViewModel()
        {
            
            _api = new SeaDroidApi();
            UserProfile = new UserProfile();
            UserProfile.Firstname = Settings.UserFirstname;
            UserProfile.Lastname = Settings.UserLastname;
            UserProfile.Id = Convert.ToInt32(Settings.UserId);
            UserProfile.Email = CrossSecureStorage.Current.GetValue("userEmail");
            if (!string.IsNullOrEmpty(Settings.UserImageBase64))
            {
                imagedata = Convert.FromBase64String(Settings.UserImageBase64);
            }
           

        }


        private void ExecuteGetUserProfileAsync()
        {
            GetUserProfile();
        }

        private async void GetUserProfile()
        {

            UserProfile = await UserProfileHelper.GetUserProfileAsync(_api);

        }

        private async Task<bool> ExecuteUpdateUserProfileAsync(string firstname, string lastname, string email)
        {
           return await UpdateUserProfile(firstname, lastname, email);
           
        }

        private async Task<bool> ExecuteUploadImageAsync(string Base64image)
        {
           return await UploadImage(Base64image);
        }

        private async Task<bool> UploadImage(string Base64image)
        {
            
            await _api.UploadImange(Base64image, Settings.Current.UserId);
            Settings.Current.UserImageBase64 = Base64image;
            return true;
        }

        private async Task<bool> UpdateUserProfile(string firstname, string lastname, string email)
        {
            try
            {
                await _api.PatchUserInfo(Settings.Current.UserId,firstname, lastname);
                Settings.UserFirstname = firstname;
                Settings.UserLastname = lastname;
                CrossSecureStorage.Current.SetValue("userEmail", email);
                OnPropertyChanged();
                 
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
         
            
        }
        
    }
}