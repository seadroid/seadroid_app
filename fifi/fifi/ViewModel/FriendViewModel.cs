﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Utils.Helpers;
using MvvmHelpers;
using Newtonsoft.Json;

namespace fifi.ViewModel
{
    public class FriendViewModel: ViewModelBase
    {
        public ObservableRangeCollection<Friend> Friends { get; set; }
        public ObservableRangeCollection<Friend> NotFriends { get; set; }
        public string user_id { get; set; }
        public Friend user { get; set; }
        public string FriendsListSortText { get; set; }
        public string AddFriendsListSortText { get; set; }

        public ObservableRangeCollection<Friend> SentFriendRequests { get; set; }

        public ObservableRangeCollection<Friend> RecivedFriendRequests { get; set; }
        private readonly SeaDroidApi _api;
        private readonly CancellationTokenSource _cancelSourece;

        #region Icommands

        private ICommand _LoadFriendsCommand;
        private ICommand _LoadAllUsersCommand;
        private ICommand _AddFriendCommand;
        private ICommand _DeleteFriendCommand;
        private ICommand _AcceptFriendCommand;
        private ICommand _DeclineFriendCommand;
        private ICommand _LoadRecivedFriendRequestsCommand;
        private ICommand _LoadSentFriendRequestsCommand;
        private ICommand _SortFriendsListCommand;
        private ICommand _SortAddFriendsListCommand;

        /// <summary>
        /// Loads a list of the user friends into the friends list
        /// </summary>
        public ICommand LoadFriendsCommand
            => _LoadFriendsCommand ??
               (_LoadFriendsCommand = new RelayCommand(async () => await ExecuteLoadFriendsCommand()));

        /// <summary>
        ///  Loads all users into the AllUsers list
        /// </summary>
        public ICommand LoadAllUsersCommand
            => _LoadAllUsersCommand ??
               (_LoadAllUsersCommand = new RelayCommand(async () => await ExecuteLoadAllUsersCommand()));

        /// <summary>
        ///  Sends a friend request to the given user_id
        /// </summary>
        public ICommand AddFriendCommand
            => _AddFriendCommand ??
               (_AddFriendCommand = new RelayCommand(async () => await ExecuteAddFriendCommand(user)));

        /// <summary>
        ///  Deletes the giver user_id from the logged in users friendslist
        /// </summary>
        public ICommand DeleteFriendCommand
            => _DeleteFriendCommand ??
               (_DeleteFriendCommand = new RelayCommand(async () => await ExecuteDeleteFriendCommand(user)));

        /// <summary>
        /// Accepts a friend request from the given user_id
        /// </summary>
        public ICommand AcceptFriendCommand
            =>
                _AcceptFriendCommand ??
                (_AcceptFriendCommand = new RelayCommand(async () => await ExecuteAcceptFriendCommand(user_id)));

        /// <summary>
        ///  Declines a friend request from the given user_id
        /// </summary>
        public ICommand DeclineFriendRequestCommand
            =>
                _DeclineFriendCommand ??
                (_DeclineFriendCommand = new RelayCommand(async () => await ExecuteDeclineFriendCommand(user_id)));

        /// <summary>
        /// Loads all the users friendrequests into the PendingFriendRequests list
        /// </summary>
        public ICommand LoadRecivedFriendRequestsCommand
            =>
                _LoadRecivedFriendRequestsCommand ??
                (_LoadRecivedFriendRequestsCommand = new RelayCommand(async () => await ExecuteLoadRecievedFriendRequestsCommand()));

        /// <summary>
        /// Loads all friend requests sent by the user
        /// </summary>
        public ICommand LoadSentFriendRequestsCommand
            =>
                _LoadSentFriendRequestsCommand ??
                (_LoadSentFriendRequestsCommand =   new RelayCommand(async () => await ExecuteLoadSentFriendRequestsCommand()));

        /// <summary>
        /// Sorts the Friends list by the text given to the FriendsListSortText string
        /// </summary>
        public ICommand SortFriendsListCommand
            =>
                _SortFriendsListCommand ??
                (_SortFriendsListCommand = new RelayCommand(async () => await ExecuteSortFriendsCommand(FriendsListSortText)));


        /// <summary>
        /// Sorts the NotFriends list by the text given to the AddFriendsListSortText string
        /// </summary>
        public ICommand SortAddFriendsListCommnad
            =>
                _SortAddFriendsListCommand ??
                (_SortFriendsListCommand =
                    new RelayCommand(async () => await ExecuteSortAddFriendsCommand(AddFriendsListSortText)));

        #endregion

        public FriendViewModel()
        {
            _api = new SeaDroidApi();
            _cancelSourece = new CancellationTokenSource();
            Friends = new ObservableRangeCollection<Friend>();
           NotFriends = new ObservableRangeCollection<Friend>();
           RecivedFriendRequests = new ObservableRangeCollection<Friend>();
           SentFriendRequests = new ObservableRangeCollection<Friend>();
       
        }

        #region Functions

        private async Task ExecuteLoadFriendsCommand()
        {
             await LoadFriends(_cancelSourece.Token);
        }


        private async Task  LoadFriends(CancellationToken token)
        {
            if (IsBusy)
            {
                return;
            }
            try
            {
                IsBusy = true;
                ProgressDialogManager.LoadProgressDialog("Loading Friends...");
                Friends.ReplaceRange(JsonConvert.DeserializeObject<FriendList>(await _api.GetUserFriends()).data);

               
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                ProgressDialogManager.DisposeProgressDialog();
                IsBusy = false;
                UserDialogs.Instance.Alert("Could not contact the server, please check you have a net connection and try again");               
            }

            finally
            {
              
                IsBusy = false;
                ProgressDialogManager.DisposeProgressDialog();
                
            }
        }


        private async Task<bool> ExecuteLoadAllUsersCommand()
        {
            if (IsBusy)
            {
                return false;
            }
            ProgressDialogManager.LoadProgressDialog("Loading Users...");
            NotFriends = (await LoadAllUsers(_cancelSourece.Token));
            OnPropertyChanged();
            ProgressDialogManager.DisposeProgressDialog();
            return true;

        }


        /// <summary>
        /// loads all users and sorts the users who the loggin in users is friends with out.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private async Task<ObservableRangeCollection<Friend>> LoadAllUsers(CancellationToken token)
        {
            var sortedusers = new ObservableRangeCollection<Friend>();
            if (IsBusy)
            {
                return sortedusers;
            }
            try
            {
                IsBusy = true;
                

                var friends = (JsonConvert.DeserializeObject<FriendList>(await _api.GetUserFriends()).data);
                var Allusers = (JsonConvert.DeserializeObject<FriendList>(await _api.GetUsers()).data);
                var SendtFriendRequests = (JsonConvert.DeserializeObject<FriendList>(await _api.GetSentFriendRequests()).data);
                var users = new ObservableRangeCollection<Friend>();
                
                foreach (var user in Allusers)
                {
                    bool IsNotFriend = true;
                    foreach (var friend in friends)
                    {
                        if (friend.id == user.id)
                        {
                            IsNotFriend = false;
                        }
                    }
                    if (IsNotFriend && Convert.ToString(user.id) != Settings.Current.UserId)
                    {
                        users.Add(user);
                    }
                }

                foreach (var user in users)
                {
                    bool haveNotSendtFriendRequest = true;
                    foreach (var sendtFriendRequest in SendtFriendRequests)
                    {
                        if (sendtFriendRequest.FriendRequest.recipient_id == user.id)
                        {
                            haveNotSendtFriendRequest = false;
                        }                     
                    }
                    if (haveNotSendtFriendRequest)
                    {
                        sortedusers.Add(user);
                    }
                }

               // NotFriends.ReplaceRange(sortedusers);
                Debug.WriteLine("NotFriends list updated");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
             

                IsBusy = false;
                return sortedusers ;
            }
            IsBusy = false;
            return sortedusers;
        }


        private async Task<bool> ExecuteLoadRecievedFriendRequestsCommand()
        {
            return await LoadRecievedFriendRequests(_cancelSourece.Token);
        }

        private async Task<bool> LoadRecievedFriendRequests(CancellationToken token)
        {
          
            try
            {
                
                ProgressDialogManager.LoadProgressDialog("Loading Friend Requests...");
                RecivedFriendRequests.ReplaceRange(JsonConvert.DeserializeObject<FriendList>(await _api.GetPendingFriendRequest()).data);
                OnPropertyChanged();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                UserDialogs.Instance.Alert(
                    "Could not contact the server, please check you have a net connection and try again");
                ProgressDialogManager.DisposeProgressDialog();
                IsBusy = false;
                return false;
            }
            ProgressDialogManager.DisposeProgressDialog();
            
            return true;
        }

        private async Task<bool> ExecuteLoadSentFriendRequestsCommand()
        {
            return await LoadSentFriendRequests(_cancelSourece.Token);
        }

        private async Task<bool> LoadSentFriendRequests(CancellationToken token)
        {
                 
            try
            {
              
                ProgressDialogManager.LoadProgressDialog("Loading Friend Requests...");
                SentFriendRequests.ReplaceRange(JsonConvert.DeserializeObject<FriendList>(await _api.GetSentFriendRequests()).data);
                OnPropertyChanged();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                UserDialogs.Instance.Alert(
                    "Could not contact the server, please check you have a net connection and try again");
                ProgressDialogManager.DisposeProgressDialog();
                IsBusy = false;
                return false;
            }
            ProgressDialogManager.DisposeProgressDialog();
            return true;
        }

        private async Task<bool> ExecuteAddFriendCommand(Friend user)
        {
            return await Addfriend(user, _cancelSourece.Token);
        }

        private async Task<bool> Addfriend(Friend user, CancellationToken token)
        {       
            try
            {
         
               await _api.SendFriendRequest(user.id.ToString());
               PubNubSingleton.Instance.SubcribeToFriendChat(Convert.ToInt32(Settings.Current.UserId),user.id);
              
                                          
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                UserDialogs.Instance.Alert(
                    "Could not contact the server, please check you have a net connection and try again");
                return false;
            }

            return true;
        }


        private async Task<bool> ExecuteAcceptFriendCommand(string user_id)
        {
            return await AcceptFriend(user_id, _cancelSourece.Token);
           
        }

        /// <summary>
        /// accepts friends request and sets up pubnub chat with them. 
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private async Task<bool> AcceptFriend(string user_id, CancellationToken token)
        {
            try
            {
                await _api.AcceptFriendRequest(user_id);
                PubNubSingleton.Instance.SubcribeToFriendChat(Convert.ToInt32(Settings.Current.UserId), Convert.ToInt32(user_id));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                UserDialogs.Instance.Alert("Could not contact the server, please check you have a net connection and try again");
                return false;
            }
            return true;
        }


        private async Task<bool> ExecuteDeclineFriendCommand(string user_id)
        {
            return await DeclineFriend(user_id, _cancelSourece.Token);
        }

        private async Task<bool> DeclineFriend(string user_id, CancellationToken token)
        {
            try
            {
               await _api.DeclineFriendRequest(user_id);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                UserDialogs.Instance.Alert("Could not contact the server, please check you have a net connection and try again");
                return false;
            }
            return true;
        }


        private async Task<bool> ExecuteDeleteFriendCommand(Friend Friend)
        {
            return await DeleteFriend(Friend, _cancelSourece.Token);
        }

        private async Task<bool> DeleteFriend(Friend Friend, CancellationToken token)
        {
            try
            {
                await _api.DeleteFriend(Friend.id.ToString());
                PubNubSingleton.Instance.UnSubcribefromFriendChat(Convert.ToInt32(Settings.Current.UserId),Friend.id);
               
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                UserDialogs.Instance.Alert("Could not contact the server, please check you have a net connection and try again");            
                return false;
            }
            return true;
        }

        private async Task<bool> ExecuteSortFriendsCommand(string SortText)
        {
            return await SortFriends(SortText, _cancelSourece.Token);          
        }

        /// <summary>
        ///  filter friends so that only friends whoses name contains the SortText remain
        /// </summary>
        /// <param name="SortText">The text that is sorted by</param>
        /// <param name="token"></param>
        /// <returns></returns>
        private async Task<bool> SortFriends(string SortText, CancellationToken token)
        {
            ProgressDialogManager.LoadProgressDialog("Sorting friends...");
            var UserFriends = (JsonConvert.DeserializeObject<FriendList>(await _api.GetUserFriends()).data);         
            var SortedFriends = await SortList(SortText, UserFriends);
            Friends.ReplaceRange(SortedFriends);
            OnPropertyChanged();
            ProgressDialogManager.DisposeProgressDialog();
            return true;
        }

        private async Task<bool> ExecuteSortAddFriendsCommand(string SortText)
        {
            return await SortAddFriends(SortText, _cancelSourece.Token);
        }

        private async Task<bool> SortAddFriends(string SortText, CancellationToken Token)
        {
            ProgressDialogManager.LoadProgressDialog("Sorting Users...");
            List<Friend> users = new List<Friend>(await LoadAllUsers(_cancelSourece.Token));
            var SortedUsers = await SortList(SortText, users);
            NotFriends = SortedUsers;
            OnPropertyChanged();
            ProgressDialogManager.DisposeProgressDialog();
            return true;
        }



        private async Task<ObservableRangeCollection<Friend>> SortList(string SortText, List<Friend> friendlist)
        {
         
            var SortedFriends = new ObservableRangeCollection<Friend>();
            foreach (var friend in friendlist)
            {
                var friendname = friend.firstname + friend.lastname;
                if (friendname.Contains(SortText))
                {
                    SortedFriends.Add(friend);
                }
            }

            return SortedFriends;

        }

        #endregion
    }
}