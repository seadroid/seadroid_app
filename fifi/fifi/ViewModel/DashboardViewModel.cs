﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Utils;
using fifi.Utils.Helpers;
using Newtonsoft.Json;
using PubnubApi;

namespace fifi.ViewModel
{
    public class DashboardViewModel : ViewModelBase
    {     

        private ICommand _logoutCommand;

        public ICommand LogoutCommand
            => _logoutCommand ?? (_logoutCommand = new RelayCommand(ExecuteLogoutCommand));


        private bool _isLoggedOut;
        public bool IsLoggedOut
        {
            get { return _isLoggedOut; }
            set { SetProperty(ref _isLoggedOut, value); }
        }

        private  void ExecuteLogoutCommand()
        {
            IsLoggedOut = Settings.Logout();
                 
            PubNubSingleton.Instance.UnsubcribeAll();
            PubNubSingleton.Instance.ClearLatestMessages();

        }
    
    }
}