﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Utils.Helpers;
using Newtonsoft.Json;

namespace fifi.ViewModel
{
    public class MasterPairingViewModel : ViewModelBase
    {
        public string Serial { get; set; }
        private SeaDroidApi _api;

        private bool NavigateToNext
        {
            get { return _navigateToNext; }
            set
            {
                _navigateToNext = value; 
                OnPropertyChanged();
            }
        }


        private ICommand _PairMasterCommand;
        private bool _navigateToNext;

        public ICommand PairMasterCommand => _PairMasterCommand ??  (_PairMasterCommand = new RelayCommand((async () =>  await PairMaster(Serial))));


        public MasterPairingViewModel()
        {
            _api = new SeaDroidApi();
            NavigateToNext = false;
        }


        public async Task<bool> PairMaster(string serial)
        {
            ProgressDialogManager.LoadProgressDialog("Pairing master...");
            
            var result = JsonConvert.DeserializeObject<ApiError>(await _api.AssignMaster(Serial));
            ProgressDialogManager.DisposeProgressDialog();
            if (result.success)
            {              
                UserDialogs.Instance.Alert(new AlertConfig().SetAction(new Action(Action)).SetTitle("master found").SetMessage(" Please fill out the Boat information on the next page, to finish pairing your master to your account").SetOkText("OK"));
            }
            else
            {
                UserDialogs.Instance.Alert("Master not found or already paired to an account", "Master pairing failed", "OK");
            }
            
            return true;
        }

        private void Action()
        {
            NavigateToNext = true;
        }

       
    }
}