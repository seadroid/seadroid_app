﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Model;
using fifi.Utils.Helpers;
using MvvmHelpers;
using Newtonsoft.Json;


namespace fifi.ViewModel
{
    public class ChatViewModel : ViewModelBase 
    {
        public ObservableRangeCollection<ChatMessage> Messages;
        public ObservableRangeCollection<Friend> Friends { get; set; }
        public string Channel { get; set; }
        public string MessageText; 
        private static long _oldestMessageTimeStamp;
        private SeaDroidApi _api;
        public DateTime Time;

        private ICommand _sendMessageCommand;
        private ICommand _loadMoreMessagesCommand;
        private ICommand _loadFriendsCommand;
        private ICommand _stopListeningForMessagesCommand;
        private ICommand _startListeningForMessagesCommand;


        public ICommand StopListeningForMessagesCommand => _stopListeningForMessagesCommand ?? (_stopListeningForMessagesCommand = new RelayCommand((ExecuteStopListeningForMessages)));

        public ICommand LoadFriendsCommand => _loadFriendsCommand ?? (_loadFriendsCommand = new RelayCommand(async () => await ExecuteLoadFriends()));

        public ICommand SendMessageCommand => _sendMessageCommand ??
                                              (_sendMessageCommand = new RelayCommand(()=> sendmessage(MessageText)));
        public ICommand LoadMoreMessagesCommand => _loadMoreMessagesCommand ??(_loadMoreMessagesCommand = new RelayCommand(LoadMoreMessages));

        public ICommand StartListeningForMessagesCommand => _startListeningForMessagesCommand ?? (_startListeningForMessagesCommand = new RelayCommand((ExecuteStartListeningForMessages)));


        public ChatViewModel(string Friend_id)
        {
            _api = new SeaDroidApi();
            _oldestMessageTimeStamp = 0;
            Messages = new ObservableRangeCollection<ChatMessage>();
            if (Convert.ToInt16(Settings.Current.UserId) > Convert.ToInt16(Friend_id))
            {
                Channel = Settings.UserId + "-" + Friend_id;
            }  
            else
            {
                Channel = Friend_id + "-" + Settings.Current.UserId;
            }
            

            var timezone = System.TimeZoneInfo.Utc;
           

            PubNubSingleton.Instance.PullHistory(Channel, Messages);
            
            
          
        }

        private void InstanceOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            {
                if (PubNubSingleton.Instance.ChatMessage.Channel == Channel)
                {
                    Messages.Add(PubNubSingleton.Instance.ChatMessage);
                }
            };
        }

       

        private void LoadMoreMessages()
        {
            PubNubSingleton.Instance.PullHistory(Channel, Messages);
        }

        private void ExecuteStopListeningForMessages()
        {
            PubNubSingleton.Instance.PropertyChanged -= InstanceOnPropertyChanged;
        }

        private void ExecuteStartListeningForMessages()
        {
            PubNubSingleton.Instance.PropertyChanged += InstanceOnPropertyChanged;
        }


        /// <summary>
        // sends a message with the givin text and the current time via pubnub 
        /// </summary>
        /// <param name="message"></param>
        private void sendmessage(string message)
        {
            var hour = Convert.ToString(DateTime.Now.Hour);
            if (hour.Length < 2)
            {
                hour = 0 + hour;
            }

            var minutes = Convert.ToString(DateTime.Now.Minute);
            if (minutes.Length < 2)
            {
                minutes = 0 + minutes;
            }
            //var date = DateTime.Now.Day + "/" + DateTime.Now.Month + "/"+ DateTime.Now.Year + " " + hour + ":" + minutes;
            

            PubNubSingleton.Instance.SendMessage(new ChatMessage(Settings.Current.UserId, Settings.Current.UserFirstname + " " + Settings.Current.UserLastname, message, Channel, Time, 0));           
        }

        private async Task<bool> ExecuteLoadFriends()
        {
            Friends.ReplaceRange(JsonConvert.DeserializeObject<FriendList>(await _api.GetUserFriends()).data);
            return true;
        }
    }
}