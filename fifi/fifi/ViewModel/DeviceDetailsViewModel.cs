﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Utils.Helpers;
using MvvmHelpers;
using Newtonsoft.Json;

namespace fifi.ViewModel
{
    public class DeviceDetailsViewModel : ViewModelBase
    {

        public Master Master { get; set; }

        public Trip SelectedTrip
        {
            get { return _selectedTrip; }
            set
            {
                _selectedTrip = value;
                OnPropertyChanged();
            }
        }

        private readonly SeaDroidApi _api;
        public string Trip_id { get; set; }

        private ICommand _loadTripCommand;
        private Trip _selectedTrip;

        public ObservableRangeCollection<Trip> Trips { get; } = new ObservableRangeCollection<Trip>();

        public ICommand LoadTripCommand => _loadTripCommand ?? (_loadTripCommand = new RelayCommand( ()=>  ExecuteGetTripAsync(Trip_id)));


        public DeviceDetailsViewModel()
        {
            _api = new SeaDroidApi();

        }




        /// <summary>
        /// gets specific trip and sets selected trip to that.
        /// </summary>
        /// <param name="trip_id"> the id of the trip wanted </param>
        private async void ExecuteGetTripAsync(string trip_id)
        {
            ProgressDialogManager.LoadProgressDialog("Loading Trip...");
            try
            {     
                SelectedTrip = JsonConvert.DeserializeObject<TripRootObject>(await _api.GetTrip(trip_id)).data;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
          
            ProgressDialogManager.DisposeProgressDialog();

        }

    }



  
}
