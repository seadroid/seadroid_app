﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Security.Tokens;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Model;
using MvvmHelpers;
using Newtonsoft.Json;
using PCLStorage;

namespace fifi.ViewModel
{
    public class MapsViewModel : ViewModelBase
    {
        public ObservableRangeCollection<ActiveBoat> ActiveBoats { get; set; }
      
        public FriendList friends;
        public FriendList sentFriendRequests;
        public UserProfileCollection Users;
        private SeaDroidApi _api;
        private ICommand _StartLoadVesselsCommand;
        private CancellationTokenSource cancelSource;
        private ICommand _StopLoadVesselsCommand;
        private ICommand _sendFriendRequestCommand;
        private ICommand _loadFriendRequestsCommand;
        public string user_id;

        public ICommand StopLoadVesselsCommand
            => _StopLoadVesselsCommand ??
               (_StopLoadVesselsCommand = new RelayCommand(async () => await ExecuteStopLoadVesselsAsync()));

        public ICommand StartLoadvesselsCommand
            => _StartLoadVesselsCommand ??
               (_StartLoadVesselsCommand = new RelayCommand(async () => await ExecutStarteLoadVesselsAsync()));

        public ICommand SendFriendRequestCommand => _sendFriendRequestCommand ??
            (_sendFriendRequestCommand = new RelayCommand(async ()=> await  ExecuteSendFriendRequestAsync(user_id)));

        public ICommand LoadSentFriendRequestsCommand => _loadFriendRequestsCommand ??
            (_loadFriendRequestsCommand = new RelayCommand( async () => await ExecuteLoadSentFriendRequests()));

        public MapsViewModel()
        {
            
            _api = new SeaDroidApi();
            ActiveBoats = new ObservableRangeCollection<ActiveBoat>();
            cancelSource = new CancellationTokenSource();
            ActiveBoats.CollectionChanged += (sender, args) =>
            {
                OnPropertyChanged();
            };
        }

        public async Task ExecutStarteLoadVesselsAsync()
        {
            new Task((() =>
            {
                try
                {
                    LoadVessels(cancelSource.Token); 
                }
                catch (OperationCanceledException)
                {
                    Debug.WriteLine("Canceled!");
                }
            })).Start();
        }

        public async Task ExecuteStopLoadVesselsAsync()
        {
            cancelSource.Cancel();
            Debug.WriteLine("Map Update Stopped");
        }


        /// <summary>
        ///  Gets the newest coordinates from every master on the server in a continues loop
        /// </summary>
        /// <param name="cancellationToken"></param>
        private async void LoadVessels(CancellationToken cancellationToken)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var loopcount = 0;
            while (true)
            {
                loopcount = loopcount + 1;    
                if (cancellationToken.IsCancellationRequested)
                {
                    return;
                }

                Debug.WriteLine("Getting new coordinates, " +"cyclenumber: "+loopcount + " Time Passed: " + stopwatch.Elapsed);
  
                try
                {
                  var result = await _api.GetAllCoordinates();
                  var  vessels = JsonConvert.DeserializeObject<ActiveVessels>(result);
                  friends = JsonConvert.DeserializeObject<FriendList>(await _api.GetUserFriends());

               var Boats = new List<ActiveBoat>();

                    foreach (var vessel in vessels.data)
                    {
                       // if (vessel.active)
                       // {
                            if (vessel.trip.Last().position != null)
                            {
                                Boats.Add(new ActiveBoat(vessel.id, vessel.user_id, vessel.trip.Last().position.longitude, vessel.trip.Last().position.latitude, vessel.trip.Last().position.received_at, vessel.boat, vessel.active));
                            }
                      // }
                    }
                    ActiveBoats.ReplaceRange(Boats);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }

                await Task.Delay(6000);
            }
        }


        private async Task<bool> ExecuteSendFriendRequestAsync(string user_id)
        {
           await  SendFriendRequest(user_id, cancelSource.Token);
            return true;
        }

        private async Task<bool> SendFriendRequest(string user_id, CancellationToken token)
        {
            try
            {
                await _api.SendFriendRequest(user_id);
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
          
        }

        private async Task<bool> ExecuteLoadSentFriendRequests()
        {
            await LoadSentFriendRequests();
            return true;
        }

        private async Task<bool> LoadSentFriendRequests()
        {
           sentFriendRequests = JsonConvert.DeserializeObject<FriendList>(await _api.GetSentFriendRequests());
           return true;
        }
       
    }
}