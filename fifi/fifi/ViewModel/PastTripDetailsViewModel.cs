﻿using System;
using System.Linq;
using fifi.Client.DataObjects;

namespace fifi.ViewModel
{
    public class PastTripDetailsViewModel : ViewModelBase
    {
        private Trip _trip;
        public Trip Trip
        {
            get { return _trip; }
            set
            {
                SetProperty(ref _trip, value);
                Title = _trip.Id?.ToString();
            }
        }

        private Coordinate _currentPosition;
        public Coordinate CurrentPosition
        {
            get { return _currentPosition; }
            set
            {
                SetProperty(ref _currentPosition, value);
                UpdateTripInformationForPoint();
            }
        }

        private string _speed = "N/A";
        public string Speed
        {
            get { return _speed; }
            set { SetProperty(ref _speed, value); }
        }

        private string _speedUnits = "N/A";
        public string SpeedUnits
        {
            get { return _speedUnits; }
            set { SetProperty(ref _speedUnits, value); }
        }

        private string _altitude = "N/A";
        public string Altitude
        {
            get { return _altitude; }
            set { SetProperty(ref _altitude, value); }
        }

        private string _windSpeed;
        public string WindSpeed
        {
            get { return _windSpeed; }
            set { SetProperty(ref _windSpeed, value); }
        }

        private string _windSpeedUnits = "m/s";
        public string WindSpeedUnits
        {
            get { return _windSpeedUnits; }
            set { SetProperty(ref _windSpeedUnits, value); }
        }

        private string _windHeading;
        public string WindHeading
        {
            get { return _windHeading; }
            set { SetProperty(ref _windHeading, value); }
        }

        private string _windHeadingUnits = "N/A";
        public string WindHeadingUnits
        {
            get { return _windHeadingUnits; }
            set { SetProperty(ref _windHeadingUnits, value); }
        }

        public PastTripDetailsViewModel()
        {
        }

        public PastTripDetailsViewModel(Trip trip) : this()
        {
            Trip = trip;
        }

        public void UpdateTripInformationForPoint()
        {
            var currentSpeed = CurrentPosition;

            if (currentSpeed?.speed != null)
            {
                Speed = currentSpeed.speed.Value.ToString("f");
            }

            if (currentSpeed?.Altitude != null)
            {
                Altitude = currentSpeed.Altitude.Value.ToString("f");
            }

            if (currentSpeed?.wind_speed != null)
            {
                WindSpeed = currentSpeed.wind_speed.ToString();
            }

            if (currentSpeed?.wind_heading != null)
            {
                WindHeading = currentSpeed.wind_heading.ToString();
            }
        }
    }
}