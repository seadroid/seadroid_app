﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Model;
using fifi.Utils.Helpers;
using Newtonsoft.Json;

namespace fifi.ViewModel
{
    public class InviteViewModel : ViewModelBase
    {
        private SeaDroidApi _api;
        public UserArmbands _armbands;
        public MasterCollection _masters;
        private ICommand _getUserUnitesCommand;
        public bool InviteAllowed;

       public InviteViewModel()
        {
            _api = new SeaDroidApi();
            _armbands = new UserArmbands();
            _masters = new MasterCollection();
        }

        public ICommand GetUserUnitsCommand => _getUserUnitesCommand ??
                     (_getUserUnitesCommand = new RelayCommand(async () => await ExecuteGetUserUnitsAsync()));



        private async Task<bool> ExecuteGetUserUnitsAsync()
        {
            return await GetUserMasters();
        }

       

        private async Task<bool> GetUserMasters()
        {
            try
            {
            ProgressDialogManager.LoadProgressDialog("Checking Users Masters...");
            _masters = JsonConvert.DeserializeObject<MasterCollection>(await _api.GetUserMasters());
            ProgressDialogManager.DisposeProgressDialog();
           
                if (_masters.data.Count <= 0)
                {
                    InviteAllowed = false;
                    OnPropertyChanged();
                    return false;
                }
                InviteAllowed = true;
                OnPropertyChanged();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
            
        }
    }
}