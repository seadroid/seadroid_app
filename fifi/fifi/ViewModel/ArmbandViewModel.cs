﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Model;
using MvvmHelpers;
using Newtonsoft.Json;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Extensions;

namespace fifi.ViewModel
{
    public class ArmbandViewModel: ViewModelBase
    {
        public ObservableRangeCollection<Armband> Armbands { get; set; }
        private SeaDroidApi _api;
        public Armband _SelectedArmband;
        public CancellationTokenSource _cancelsSource;
        IAdapter adapter = CrossBluetoothLE.Current.Adapter;
        public List<BluetoothDevice> BluetoothDevices;

        public ArmbandViewModel()
        {
            _cancelsSource = new CancellationTokenSource();
            _api = new SeaDroidApi();
            Armbands = new ObservableRangeCollection<Armband>();
            BluetoothDevices = new List<BluetoothDevice>();
        }

        private IList<IDevice> Devices;

        private ICommand _GetArmbands;
        private ICommand _UpdateArmband;
        private ICommand _SearchForDevices;
        private ICommand _HardStopSearchingForDevices;
        private ICommand _SoftStopSearchingForDevices;

        public ICommand HardStopSearchingForDevicesCommand => _HardStopSearchingForDevices ?? (_HardStopSearchingForDevices = new RelayCommand(ExecuteHardStopSearchForDevices));

        public ICommand SoftStopSearchingForDevicesCommand => _SoftStopSearchingForDevices ?? (_SoftStopSearchingForDevices = new RelayCommand(ExecuteSoftStopSearchFOrDevices));

        public ICommand SearchForDevicesCommand => _SearchForDevices?? (_SearchForDevices = new RelayCommand(ExecuteSearchForDevicesAync));

        public ICommand GetArmbandsCommand => _GetArmbands ??
                                       (_GetArmbands = new RelayCommand(async () => await ExecuteGetArmbandsAsync()));

        public ICommand UpdateArmband => _UpdateArmband ??
                                         (_UpdateArmband =new RelayCommand(async ()=> await ExecuteUpdateArmbandAync(_SelectedArmband)));


        /// <summary>
        /// Gets the users armbands
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ExecuteGetArmbandsAsync()
        {
            var UserArmbands = JsonConvert.DeserializeObject<UserArmbands>(await _api.GetUserWristbands());

            if (Armbands.Count == 0)
            {
                Armbands.ReplaceRange(UserArmbands.data.armband);
            }
            else
            {
                foreach (var Userarmband in UserArmbands.data.armband)
                {
                    foreach (var armband in Armbands)
                    {
                        if (armband.serial == Userarmband.serial && !string.IsNullOrEmpty(armband.Battery))
                        {
                            Userarmband.Battery = armband.Battery;
                            Userarmband.Signal = true;

                        }
                    }
                }
                Armbands.ReplaceRange(UserArmbands.data.armband);
            }
            
           
            return true;
        }
     

        /// <summary>
        /// method that updates the users armband with the givin armband data
        /// </summary>
        /// <param name="armband"></param>
        /// <returns></returns>
        private async Task<bool> ExecuteUpdateArmbandAync(Armband armband)
        {
          var result = await _api.UpdateArmband(armband.id, armband.firstname, armband.lastname,
              armband.gender, armband.birthday, (int)armband.weight, (int)armband.height,
              (int)armband.country_id, armband.haircolor, (int)armband.swim_exp, (int)armband.sea_exp);

           await ExecuteGetArmbandsAsync();        
           return true;
        }


        /// <summary>
        /// cancels any already running searches for bluetooth devices and starts a new one.
        /// </summary>
        private void ExecuteSearchForDevicesAync()
        {
            _cancelsSource.Cancel();
            _cancelsSource = new CancellationTokenSource();
             SearchForDevices(_cancelsSource.Token);
        }


        private void ExecuteSoftStopSearchFOrDevices()
        {
            _cancelsSource.Cancel();
        }

        private void ExecuteHardStopSearchForDevices()
        {
           _cancelsSource.Cancel();          
            adapter.StopScanningForDevicesAsync();
        }

        /// <summary>
        ///  Searches for armbands in the area and gets their battery level, and puts them into a collection
        /// </summary>
        /// <param name="cancelToken"></param>
        /// <returns></returns>
        private async Task SearchForDevices(CancellationToken cancelToken)
        {
            while (true)
            {
                if (cancelToken.IsCancellationRequested)
                {
                    return;
                    
                }

                await adapter.StartScanningForDevicesAsync(dev => dev.Name !=null && dev.Name.Contains("V.ALRT"));
                await Task.Delay(100, cancelToken);
                var devices = adapter.DiscoveredDevices;

                foreach (var device in adapter.DiscoveredDevices)
                {
                    var NewBlueToothDevice = true;
                    foreach (var bluetoothdevice in BluetoothDevices)
                    {
                        if (bluetoothdevice.DeviceName == device.Name)
                        {
                            NewBlueToothDevice = false;
                        }
                       
                    }
                    if (NewBlueToothDevice)
                    {
                        BluetoothDevices.Add(new BluetoothDevice(device, device.Name, string.Empty));                       
                    }

                }

                var foundNewDevice = false;
                foreach (var Bluetoothdevice in BluetoothDevices)
                {
                   
                    foreach (var armband in Armbands)
                    {
                        
                        if (Bluetoothdevice.DeviceName == armband.serial && DateTime.Now.Subtract(Bluetoothdevice.LastContacted).TotalMinutes > 20000)
                        {
                            await adapter.ConnectToDeviceAsync(Bluetoothdevice.Device);
                            var service = await Bluetoothdevice.Device.GetServiceAsync(Guid.Parse("FFFFFFF0-00F7-4000-B000-000000000000"));
                            var characteristic = await service.GetCharacteristicAsync(Guid.Parse("FFFFFFF5-00F7-4000-B000-000000000000"));
                            var data = new byte[5];
                            data[0] = 0x80;
                            data[1] = 0xbe;
                            data[2] = 0xf5;
                            data[3] = 0xac;
                            data[4] = 0xff;
                            await Task.Delay(10, cancelToken);
                            var answer = await characteristic.WriteAsync(data);
                            service = await Bluetoothdevice.Device.GetServiceAsync(Guid.Parse("0000180f-0000-1000-8000-00805f9b34fb"));
                            characteristic = await service.GetCharacteristicAsync(Guid.Parse("00002a19-0000-1000-8000-00805f9b34fb"));
                            var batterylevelBytes = await characteristic.ReadAsync();


                            service = await Bluetoothdevice.Device.GetServiceAsync(Guid.Parse("FFFFFFF0-00F7-4000-B000-000000000000"));
                            characteristic = await service.GetCharacteristicAsync(Guid.Parse("FFFFFFF1-00F7-4000-B000-000000000000"));

                            var stealthmodedata = new byte[1];
                            stealthmodedata[0] = 0x03;
                            await Task.Delay(10, cancelToken);
                            var stealthmode = await characteristic.WriteAsync(stealthmodedata);
                            var batterylevel = Convert.ToString((int)batterylevelBytes[0]);
                            await adapter.DisconnectDeviceAsync(Bluetoothdevice.Device);

                            armband.Battery = batterylevel;
                            armband.Signal = true;
                            foundNewDevice = true;

                            Bluetoothdevice.LastContacted = DateTime.Now;


                        }

                    }
                    if (foundNewDevice)
                    {
                        OnPropertyChanged();
                    }
                   
                 
                }
            }
        }

        
    }
}