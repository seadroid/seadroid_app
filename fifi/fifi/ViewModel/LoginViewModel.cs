﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Utils;
using fifi.Utils.Extension;
using fifi.Utils.Helpers;
using Newtonsoft.Json;
using Plugin.SecureStorage;

namespace fifi.ViewModel
{
    public class LoginViewModel : ViewModelBase
    {
    //    private readonly IClient _client;
        private SeaDroidApi _api;
        public Countries countries;
        private bool _isLoggedIn;        
        private ICommand _loginCommand;
        private ICommand _verifyUserCommand;
        private ICommand _loadCountriesCommand;

        private readonly Regex _emailRegex =
            new Regex(
                @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$");

        public LoginViewModel()
        {
           // _client = ServiceLocator.Instance.Resolve<IClient>();
            _api = new SeaDroidApi();
            MasterMissingBoatProfile = false;

        }


        public UserProfile UserProfile { get; set; }

        public bool IsVerified
        {
            get { return _isVerified; }
            set { SetProperty(ref _isVerified, value); }
        }

        public bool IsLoggedIn
        {
            get { return _isLoggedIn; }
            set { SetProperty(ref _isLoggedIn, value); }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private string _password;
        private bool _isVerified;

        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        public bool MasterMissingBoatProfile;
        public ICommand LoadCountriesCommand
          => _loadCountriesCommand ?? (_loadCountriesCommand = new RelayCommand(async() => await ExecuteLoadCountries()));
              

        public ICommand LoginCommand
            => _loginCommand ?? (_loginCommand = new RelayCommand(async () => await ExecuteLoginCommandAsync()));

        public ICommand VerifyUserCOmmand
            => _verifyUserCommand ?? (_verifyUserCommand = new RelayCommand(async () => await ExecuteVerifUserAsync()));

        private async Task ExecuteLoginCommandAsync()
        {
            //if (_client == null || IsBusy)
            //{
            //    return;
            //}

            if (IsBusy)
            {
                return;
            }

            IsLoggedIn = await LoginAsync();
        }

        private async Task<bool> LoginAsync()
        {
            if (!Plugin.Connectivity.CrossConnectivity.Current.IsConnected)
            {
                UserDialogs.Instance.Alert("Ensure you have internet connection to login.", "No Connection", "OK");

                return false;
            }

            ServiceAuth login = null;

            try
            {
                IsBusy = true;
                ProgressDialogManager.DisposeProgressDialog();
                ProgressDialogManager.LoadProgressDialog("Logging in...");
               // login = await _client.Client.PostLoginServiceAuth(Email, Password, "application/json");
                login = JsonConvert.DeserializeObject<ServiceAuth>(await _api.PostLogin(Email, Password));

                if (!string.IsNullOrWhiteSpace(login?.AccessToken))
                {
                   // _client.AuthenticationToken = login.AccessToken;
                    Settings.AuthToken = login.AccessToken;
                    Settings.RefreshAuthToken = login.RefreshToken;
                  UserProfile = await UserProfileHelper.GetUserProfileAsync(_api);
                  IsVerified = UserProfile.verified;

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
                ProgressDialogManager.DisposeProgressDialog();
            }

            if (login == null || UserProfile == null)
            {
                Settings.AuthToken = string.Empty;
                Settings.UserLastname = string.Empty;
                Settings.UserFirstname = string.Empty;
                Settings.UserId = string.Empty;
                Settings.UserImageUrl = string.Empty;
               
                UserDialogs.Instance.Alert("Unable to login", "Login Error", "OK");
                return false;
            }
        
            PubNubSingleton.Instance.SetUUID(Settings.Current.PubnubUIID);
            PubNubSingleton.Instance.ClearLatestMessages();

            var a = new Task(async () =>
            {
                

                if (Settings.RandomNumber != string.Empty && Convert.ToInt32(Settings.RandomNumber) == 1)
                { PubNubSingleton.Instance.Subcribe("2"); Settings.RandomNumber = "2"; }
                else
                { PubNubSingleton.Instance.Subcribe("1"); Settings.RandomNumber = "1"; }
                              
                PubNubSingleton.Instance.Subcribe(Settings.Current.UserId);
                var friends = JsonConvert.DeserializeObject<FriendList>(await _api.GetUserFriends());
                var SentFriendRequests = JsonConvert.DeserializeObject<FriendList>(await _api.GetSentFriendRequests()).data;

                try
                {
                    foreach (var friend in friends.data)
                    {
                        PubNubSingleton.Instance.SubcribeToFriendChat(Convert.ToInt32(Settings.Current.UserId), friend.id);
                        await Task.Delay(100);
                    }

                }
               
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }               

                foreach (var friend in SentFriendRequests)
                {
                    PubNubSingleton.Instance.SubcribeToFriendChat(Convert.ToInt32(Settings.Current.UserId), friend.id);
                }


                await Task.Delay(100);
                PubNubSingleton.Instance.GetLastestMessageFromSubcribedChannels();

                if (Settings.Current.Platform == "IOS")
                {
                  PubNubSingleton.Instance.SubcribeForPushNotifications(Settings.Current.DeviceId);
                }

            });
            a.Start();

            SaveCredentials();
        
            return true;
        }

        /// <summary>
        /// checks if user have any friends or any paired devices
        /// </summary>
        public async Task<bool> ExecuteVerifUserAsync()
        {
            try
            {
                IsBusy = true;
                ProgressDialogManager.LoadProgressDialog("varifying Account...");

                if (IsVerified)
                {
                    Settings.IsVerified = true;
                    return true;
                }

                if (UserProfile.master.Count == 0 && UserProfile.armband.Count == 0)
                {
                    Settings.IsVerified = false;
                    IsVerified = false;
                    ProgressDialogManager.DisposeProgressDialog();
                    return false;
                }

                if (UserProfile.master.Count == 1)
                {
                  
                        if (UserProfile.master[0].BoatId != null)
                        {
                            Settings.IsVerified = true;
                            IsVerified = true;
                            IsBusy = false;
                            ProgressDialogManager.DisposeProgressDialog();
                            await _api.VerifyUser(UserProfile.Id.ToString());
                            return true;
                        }
                    MasterMissingBoatProfile = true;
                    return false;
                }
               

                Settings.IsVerified = true;
                IsVerified = true;
                IsBusy = false;
                ProgressDialogManager.DisposeProgressDialog();
                await _api.VerifyUser(UserProfile.Id.ToString());
                return true;
            }
            catch (Exception e)
            { 
                Debug.WriteLine(e);
                throw;
            }
          

        }

        private void SaveCredentials()
        {
            CrossSecureStorage.Current.SetValue("userEmail", Email);
            CrossSecureStorage.Current.SetValue("userPassword", Password);
        }

        public async Task<bool> ExecuteLoadCountries()
        {
            try
            {

                countries = JsonConvert.DeserializeObject<Countries>(await _api.GetAllCountries());
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

        }
    }
}