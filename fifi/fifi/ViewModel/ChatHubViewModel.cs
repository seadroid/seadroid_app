﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.Utils.Helpers;
using MvvmHelpers;
using Newtonsoft.Json;
using Org.BouncyCastle.X509;
using PubnubApi;

namespace fifi.ViewModel
{
    public class ChatHubViewModel : ViewModelBase
    {
        public ObservableRangeCollection<Friend> Friends { get; set; }
        private SeaDroidApi _api;

        private ICommand _loadFriendsCommand;
        public ICommand LoadFriendsCommand => _loadFriendsCommand ?? (_loadFriendsCommand = new RelayCommand(async ()=> await ExecuteLoadFriends()));


        public  ChatHubViewModel()
        {
            _api = new SeaDroidApi();
            Friends = new ObservableRangeCollection<Friend>();        
          

        }


    /// <summary>
    /// Gets the Users friends and when the latest masseges from pubnub are loaded it puts the data into a observable colletion to be shown in the ui
    /// </summary>
    /// <returns></returns>
        private async Task<bool> ExecuteLoadFriends()
        {    

           var  FriendsData = JsonConvert.DeserializeObject<FriendList>(await _api.GetUserFriends()).data;
            var run = true;

            ProgressDialogManager.LoadProgressDialog("Loading Messages...");
            IsBusy = true;
            

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            

            while (run)
            {
                if (PubNubSingleton.Instance.HasLoadedLastMessages)
                {
                    foreach (var Friend in FriendsData)
                    {
                        foreach (var message in PubNubSingleton.Instance.LatestMessages)
                        {
                            var UserIds = message.Channel.Split('-');

                            foreach (var Id in UserIds)
                            {
                                if (Convert.ToInt32(Id) == Friend.id)
                                {
                                    Friend.latest_message_recived_at = message.Date.ToLocalTime();
                                    Friend.latest_message = message.Text;
                                }
                            }


                        }
                    }

                    var sortedlist = FriendsData.OrderByDescending(x => x.latest_message_recived_at).ThenByDescending(x => x.latest_message_recived_at.TimeOfDay).ToList();

                    foreach (var friend in sortedlist)
                    {
                        Debug.WriteLine(friend.latest_message_recived_at.ToString());
                    }

                    Friends.ReplaceRange(sortedlist);

                    run = false;
                    ProgressDialogManager.DisposeProgressDialog();
                    IsBusy = false;
                    return true;
                }
                if (stopwatch.ElapsedMilliseconds > 5000)
                {
                    run = false;                   
                    ProgressDialogManager.DisposeProgressDialog();
                    UserDialogs.Instance.Alert("Coud Not Load Messages...", string.Empty, "OK");
                    IsBusy = false;
                    return false;
                }

            }

            return false;
        }
    }


    
}