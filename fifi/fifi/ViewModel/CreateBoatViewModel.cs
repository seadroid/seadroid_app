﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using Newtonsoft.Json;

namespace fifi.ViewModel
{
    public class CreateBoatViewModel : ViewModelBase
    {
        private SeaDroidApi _api;

        public string Boatname { get; set; }
        public int Country_id { get; set; }
        public int Category_id { get; set; }
        public string Model { get; set; }
        public int ProductionYear { get; set; }
        public string BoatLength { get; set; }
        public string Color { get; set; }
        public string Harbor { get; set; }
        public string Registry_no { get; set; }
        public string Base64EncodedImage { get; set; }
        public string MasterSerial { get; set; }
        public bool VerifyingAccount { get; set; } = true;

        public bool BoatCreated
        {
            get { return _boatCreated1; }
            set
            {
                _boatCreated1 = value; 
                OnPropertyChanged();
            }
        }

        public bool MasterUnpaired
        {
            get { return _masterUnpaired; }
            set
            {
                _masterUnpaired = value; 
                OnPropertyChanged();
            }
        }

        public bool NavigateToNext
        {
            get { return _navigateToNext; }
            set
            {
                _navigateToNext = value;
                OnPropertyChanged();
            }
        }

      

        private ICommand _addImageToBoatCommand;
        private ICommand _createBoatCommand;
        private ICommand _cancelmasterpairing;

        private bool _boatCreated;
        private bool _navigateToNext;
        private bool _masterUnpaired;
        private bool _boatCreated1;

        public ICommand CancelMasterPairing => _cancelmasterpairing ??
                                               (_cancelmasterpairing =
                                                   new RelayCommand( ExecuteCancelMasterPairing));

        public ICommand CreateBoatCommand => _createBoatCommand ?? (_createBoatCommand = new RelayCommand(async ()=> await ExecuteCreateBoat(Boatname, Country_id, Category_id, Model, ProductionYear, BoatLength, Color, Harbor, Registry_no)));

        public ICommand AddImageToBoatCommand => _addImageToBoatCommand ??
                                                 (_createBoatCommand =
                                                     new RelayCommand(
                                                         async () => await ExecuteAddImageToBoat(Base64EncodedImage)));


        public CreateBoatViewModel()
        {
            _api = new SeaDroidApi();
        }


        private async Task<bool> VerifyUser()
        {
            var result = _api.VerifyUser(Settings.UserId);
            return true;
        }

        private async Task<bool> ExecuteCreateBoat(string boatname, int country_id, int category_id, string model,
            int productionYear, string boatLength, string color, string harbor, string registry_no)
        {

            var result = await _api.CreateBoat(boatname, model, country_id, category_id, productionYear, color, harbor,
                registry_no, boatLength);

           await ExecuteAddImageToBoat(Base64EncodedImage);

           await ExecuteAssingBoat();

            if (!VerifyingAccount)
            {
                UserDialogs.Instance.Alert(new AlertConfig().SetTitle("Master Paired To Account").SetOkText("OK").SetAction(() =>
                    {
                        BoatCreated = true;
                        SetNavigateToNext();
                    }));
                return true;

            }
            else
            {
                await VerifyUser();
                UserDialogs.Instance.Alert(new AlertConfig().SetTitle("Account Verified").SetMessage("your account is now verified").SetOkText("OK").SetAction(SetNavigateToNext));
                return true;
            }
            
        }

        /// <summary>
        /// gets the users boats, and finds the boat that correspond with the one being currently edidted in the view, and uploads a image the is paired to this boat.
        /// </summary>
        /// <param name="Base64Image"></param>
        /// <returns></returns>
        private async Task<bool> ExecuteAddImageToBoat(string Base64Image)
        {      
            var userprofile = JsonConvert.DeserializeObject<UserBoats>(await _api.GetUsersBoats());
            foreach (var boat in userprofile.profile.boat)
            {
                if (boat.registry_no.ToString() == Registry_no)
                {
                  await  _api.UploadBoatImage(boat.id.ToString(), Base64Image);
                }
            }
          
            return false;
        }

        
        /// <summary>
        /// assings the boat to the master whose Id correspond to the MasterSerial value in the class
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ExecuteAssingBoat()
        {
            Boat seletedBoat;
            var profiledata = await _api.GetSpecificUser(Settings.UserId);
            var profile = JsonConvert.DeserializeObject<SpecificUser>(profiledata);

            var boatdata = await _api.GetUsersBoats();
            var boats = JsonConvert.DeserializeObject<UserBoats>(await _api.GetUsersBoats());

            foreach (var boat in boats.profile.boat)
            {
                if (boat.registry_no != null && boat.registry_no.ToString() == Registry_no)
                {
                    seletedBoat = boat;

                    foreach (var master in profile.data.master)
                    {
                        if (master.Serial == MasterSerial)
                        {
                            var result = await _api.AssingBoatToMaster(seletedBoat.id, master.Id);
                            return true;
                        }
                    }
                }
            }


           


            return false;

        }

        /// <summary>
        /// shows a Userdialog to the users where the can choose to cancel the pairing of the current master
        /// </summary>
        private async void ExecuteCancelMasterPairing()
        {
            UserDialogs.Instance.Confirm(new ConfirmConfig().UseYesNo().SetAction(UnpairMaster).SetTitle("Cancel master pairing").SetMessage("Do you what to cancel the pairing of the master?"));

        }

     
        /// <summary>
        /// Unpairs the currently selected master from the user.
        /// </summary>
        /// <param name="b"></param>
        private  async void UnpairMaster(bool b)
        {
            if (b)
            {
                var profiledata = await _api.GetSpecificUser(Settings.UserId);
                var profile = JsonConvert.DeserializeObject<SpecificUser>(profiledata);
                foreach (var master in profile.data.master)
                {
                    if (master.Serial == MasterSerial)
                    {
                        var result = await _api.RemoveMaster(master.Id.ToString());
                        MasterUnpaired = true;

                    }
                }
            }
           
            
        }
        

        private void SetNavigateToNext()
        {
            NavigateToNext = true;
        }
    }
}