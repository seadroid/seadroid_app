﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using fifi.ViewModel;
using fifi.Client.Interfaces;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Acr.UserDialogs;

using fifi.Client.DataObjects;
using fifi.Helpers;
using fifi.Utils;
using fifi.Utils.Helpers;
using Newtonsoft.Json;
using Plugin.SecureStorage;
namespace fifi.ViewModel
{
    public class CreateUserViewModel : ViewModelBase
    {
        public string VerificationPassword;
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string ReconfirmPassword { get; set; }
        public string ReconfirmEmail { get; set; }      
        public string Email { get; set; }
        public int Country_id { get; set; }
        public  Countries Countries { get; set; }
		private ICommand _createuserCommand;     
        private GatewayApi _gatewayApi;
		private SeaDroidApi _Api;
        public UserProfile UserProfile { get; set; }
        private numverifyApi _numverifyApi;
        private ICommand _SendVerificationCode;
       


        public CreateUserViewModel()
		{
			_Api = new SeaDroidApi();
            _gatewayApi = new GatewayApi();
            _numverifyApi = new numverifyApi();
		    VerificationPassword = string.Empty;            
		}

      

        public ICommand SendVerificationCodeCommand
            => _SendVerificationCode ?? (_SendVerificationCode = new RelayCommand(async () => await ExecuteSendVerificationCodeAsync()));

		public ICommand CreateUserCommand
		=> _createuserCommand ?? (_createuserCommand = new RelayCommand(async () => await ExecuteCreateUserAsync()));


        /// <summary>
        /// Creates a new user with the current info given by the user, if the user is not created a apropiat response i given to the user as to why it was not created.
        /// </summary>
        /// <returns></returns>

		public async Task<bool> ExecuteCreateUserAsync()
		{
		    ServiceAuth Login = new ServiceAuth();
		    await Task.Delay(300);
            ProgressDialogManager.LoadProgressDialog("Creating Account...");
		    var result = await _Api.CreateUser(Firstname, Lastname, Email, Password, PhoneNumber, Country_id);
            CreateUserError Error = JsonConvert.DeserializeObject<CreateUserError>(result);

            if (Error.errors == null)
		    {
		        Login = JsonConvert.DeserializeObject<ServiceAuth>(await _Api.PostLogin(Email, Password));
            }

            if (!string.IsNullOrWhiteSpace(Login?.AccessToken))
            {
                Settings.AuthToken = Login.AccessToken;
                UserProfile = await UserProfileHelper.GetUserProfileAsync(_Api);
            }

            if (Login == null || UserProfile == null)
            {
                Settings.AuthToken = string.Empty;
                Settings.UserLastname = string.Empty;
                Settings.UserFirstname = string.Empty;
                Settings.UserId = string.Empty;
                ProgressDialogManager.DisposeProgressDialog();

                if (Error.errors != null)
                {
                    if (Error.errors.email != null && Error.errors.mobile != null)
                    {
                        UserDialogs.Instance.Alert("Mail and Phonenumber is already used", "Unable to create account", "OK");
                        return false;
                    }

                    if (Error.errors.email != null)
                    {
                        UserDialogs.Instance.Alert("Mail is already used", "Unable to create account", "OK");
                        return false;
                    }

                    if (Error.errors.mobile != null)
                    {
                        UserDialogs.Instance.Alert("Phonenumber is already used", "Unable to create account", "OK");
                        return false;
                    }
                }            
                return false;
            }
            SaveCredentials();
            ProgressDialogManager.DisposeProgressDialog();
            return true;
        }

        /// <summary>
        /// saves the user credentials so that they can be used to auto login when user restarts the app, aswell as get new Auth tokens
        /// </summary>
        private void SaveCredentials()
        {
            CrossSecureStorage.Current.SetValue("userEmail", Email);
            CrossSecureStorage.Current.SetValue("userPassword", Password);
        }

        /// <summary>
        /// Sends an sms to the users with a verification password.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ExecuteSendVerificationCodeAsync()
        {
            VerificationPassword = CreatePassword(4);
            var result  = await _gatewayApi.sendSms("Din engangskode til SEADROID er: " + VerificationPassword, PhoneNumber);
            return result;
        }

        /// <summary>
        /// Creates a ramdon series of numbers with verying lenght.
        /// </summary>
        /// <param name="length"> decides the length of the nuber series </param>
        /// <returns></returns>
        private string CreatePassword(int length)
        {
            const string valid = "1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

       



    }
}
