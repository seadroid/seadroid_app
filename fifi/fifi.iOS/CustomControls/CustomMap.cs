﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using fifi.iOS.Annotations;
using Foundation;
using UIKit;

namespace fifi.iOS.CustomControls

{
    using MapKit;
	using CoreGraphics;
    public class CustomMap: MKMapView
    {
        public CustomMap(CGRect frame)
        {
			Frame = frame;
            Delegate = new CustomMapViewDelegate();          
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            NSNotificationCenter.DefaultCenter.PostNotificationName("MarkerDeselected", this);
        }
    } 
}