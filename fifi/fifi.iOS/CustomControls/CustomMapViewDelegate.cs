﻿using MapKit;
using UIKit;
using Foundation;
using fifi.ViewModel;
using fifi.iOS.CustomControls;
using CoreGraphics;
using CoreLocation;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using CoreImage;
using fifi.Client.DataObjects;
using fifi.Helpers;
using fifi.iOS;
using fifi.iOS.Helpers;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using fifi.iOS.Annotations;

namespace fifi.iOS.CustomControls
{
    public class CustomMapViewDelegate :MKMapViewDelegate
    {
        public override MKOverlayView GetViewForOverlay(MKMapView mapView, IMKOverlay overlay)
        {
            if (overlay.GetType() == typeof(MKPolyline))
            {
                MKPolylineView p = new MKPolylineView((MKPolyline)overlay);
                p.LineWidth = 6.0f;
				p.StrokeColor = UIColor.Blue;
                return p;
            }
            else
                return null;
        }

		static string StartAnnotationID = "StartAnnotation";
        static string BoatAnnotationID = "BoatAnnotation";
        static string GlobalAnnotationID = "GlobalAnnotation";
		static string EndAnnotationID = "EndAnnotation";
        static string FriendAnnotationID = "FriendAnnotation";
		public override MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
		{

			MKAnnotationView annotationview = null;

			if (annotation is StartAnnotation) 
			{
				annotationview = mapView.DequeueReusableAnnotation(StartAnnotationID);

				if (annotationview == null) 
				{
					annotationview = new MKAnnotationView(annotation, StartAnnotationID);
					var image = ResizeImage(UIImage.FromBundle("ic_start_point"), 20, 20);
					annotationview.Image = image;
					annotationview.CanShowCallout = false;						
				}
			}

			if (annotation is EndAnnotation)
			{
				annotationview = mapView.DequeueReusableAnnotation(EndAnnotationID);

				if (annotationview == null)
				{
					annotationview = new MKAnnotationView(annotation, EndAnnotationID);
					var image = ResizeImage(UIImage.FromBundle("ic_end_point"), 20, 20);
					annotationview.Image = image;
					annotationview.CanShowCallout = false;
				}
			}

			if (annotation is BoatAnnotation)
			{
				annotationview = mapView.DequeueReusableAnnotation(BoatAnnotationID);

				if (annotationview == null)
				{
					annotationview = new MKAnnotationView(annotation, BoatAnnotationID);					 
					var image = ResizeImage(UIImage.FromBundle("position_my_ship"),30, 30);
					annotationview.Image = image;
					annotationview.CanShowCallout = false;
				}
			}

		    if (annotation is FriendAnnotation)
		    {
		        annotationview = mapView.DequeueReusableAnnotation(FriendAnnotationID);

		        if (annotationview == null)
		        {
		            annotationview = new MKAnnotationView(annotation, FriendAnnotationID);
		            var image = ResizeImage(UIImage.FromBundle("position_friend_ship"), 30, 30);
		            annotationview.Image = image;
		            annotationview.CanShowCallout = false;
		        }
		    }

		    if (annotation is GlobalAnnotation)
		    {
		        annotationview = mapView.DequeueReusableAnnotation(GlobalAnnotationID);
		        if (annotationview == null)
		        {
		           annotationview = new MKAnnotationView(annotation, GlobalAnnotationID);
		            var image = ResizeImage(UIImage.FromBundle("position_global_ship"), 30, 30);
		            annotationview.Image = image;
		            annotationview.CanShowCallout = false;
		        }
		    }


			return annotationview;
		}

     

        public UIImage ResizeImage(UIImage sourceimage, float maxwidth, float maxheight) 
		{

			var sourcesize = sourceimage.Size;
			var MaxResizeFactur = Math.Max(maxwidth / sourcesize.Width, maxheight / sourcesize.Height);
			if (MaxResizeFactur > 1) return sourceimage;
			var width = MaxResizeFactur * sourcesize.Width;
			var height = MaxResizeFactur * sourcesize.Height;
            UIGraphics.BeginImageContextWithOptions(new SizeF((float)width, (float)height), false, 2.0f);
            sourceimage.Draw(new RectangleF(0, 0, (float)width, (float)height));
			var resultimage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return resultimage;
		}

        
        //public override void DidDeselectAnnotationView(MKMapView mapView, MKAnnotationView view)
        //{
        //    NSNotificationCenter.DefaultCenter.PostNotificationName("MarkerDeselected", this);
        //}

        


        public override void DidSelectAnnotationView(MKMapView mapView, MKAnnotationView view)
        {
            NSNotificationCenter.DefaultCenter.PostNotificationName("MarkerSelected", this);                           
        }


   
    }
   
}