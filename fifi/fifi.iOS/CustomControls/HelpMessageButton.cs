﻿using System;
using System.ComponentModel;
using Foundation;
using UIKit;

namespace fifi.iOS
{
	[Register("HelpMessageButton"), DesignTimeVisible(true)]
	public class HelpMessageButton : UIButton
	{
		public HelpMessageButton(IntPtr handle) : base(handle)
		{
			Initialize();
		}

		public HelpMessageButton()
		{
			Initialize();
		}

		private void Initialize()
		{
			HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
		}

		public override CoreGraphics.CGRect TitleRectForContentRect(CoreGraphics.CGRect rect)
		{
			var rectangle = base.TitleRectForContentRect(rect);
			rectangle.X = 16.0f;
			return rectangle;
		}

		public override CoreGraphics.CGRect ImageRectForContentRect(CoreGraphics.CGRect rect)
		{
			var rectangle = base.ImageRectForContentRect(rect);
			rectangle.X = rect.Right - rectangle.Width;
			return rectangle;
		}
	}
}
