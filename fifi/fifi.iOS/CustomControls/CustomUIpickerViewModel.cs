﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Mime;
using CoreGraphics;
using fifi.iOS.Helpers;
using Foundation;
using UIKit;

namespace fifi.iOS.CustomControls
{
    public class CustomUIpickerViewModel : UIPickerViewModel
    {
        public event EventHandler<EventArgs> ValueChanged;

        /// <summary>
        /// The color we wish to display
        /// </summary>
        public List<IosFlag> Items { get; private set; }

        /// <summary>
        /// The current selected item
        /// </summary>
        public IosFlag SelectedItem
        {
            get { return Items[selectedIndex]; }
        }

        public int SelectedRow
        {
            get
            {
                return selectedIndex;               
            }
        }

        int selectedIndex = 0;

        public CustomUIpickerViewModel(List<IosFlag> images )
        {
            Items = images;
        }

        
        /// <summary>
        /// Called by the picker to determine how many rows are in a given spinner item
        /// </summary>
        public override nint GetRowsInComponent(UIPickerView picker, nint component)
        {
            return Items.Count;
        }

        /// <summary>
        /// called by the picker to get the number of spinner items
        /// </summary>
        public override nint GetComponentCount(UIPickerView picker)
        {
            return 1;
        }

        /// <summary>
        /// called when a row is selected in the spinner
        /// </summary>
        public override void Selected(UIPickerView picker, nint row, nint component)
        {
            selectedIndex = (int) row;
            if (ValueChanged != null)
            {
                ValueChanged(this, new EventArgs());
            }
        }


        /// <summary>
        /// Custom row view.
        ///
        /// The <c>view</c> param is the reusable view for the row. It will be null initially.
        ///
        /// You can add subviews, etc., but prefer to do that in the lazy-initialization block rather
        /// than every time this method is called.
        ///
        /// Note that GetTitle() is no longer overridden since we aren't using the default row view
        /// </summary>
        public override UIView GetView(UIPickerView picker, nint row, nint component, UIView view)
        {
            //Lazy initialize
            if (view == null)
            {
                CGSize rowSize = picker.RowSizeForComponent(component);
                view = new UIImageView(Items[(int) row].flagImage) {Frame = new CGRect(new CGPoint(0, 0), new CGSize(((double)rowSize.Width/3),(rowSize.Height *0.9)))};

            }
            //Modify state to reflect data

            return view;
        }

        /// <summary>
        /// Make the rows in the second component half the size of those in the first
        /// </summary>
        public override nfloat GetRowHeight(UIPickerView picker, nint component)
        {
            return 44 / (component % 2 + 1);
        }
    }
}