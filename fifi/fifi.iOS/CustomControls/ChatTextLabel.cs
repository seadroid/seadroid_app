using Foundation;
using System;
using CoreGraphics;
using UIKit;

namespace fifi.iOS
{
    public partial class ChatTextLabel : UILabel
    {
        public ChatTextLabel (IntPtr handle) : base (handle)
        {
        }

       

      

        public override void DrawRect(CoreGraphics.CGRect area, UIViewPrintFormatter formatter)
        {
            area.Width = area.Width + (float) 10.0f;
            

            base.DrawRect(area, formatter);
		}

        public override void DrawText(CGRect rect)
        {
            var pad = 5.0f;
            var pad_vert = 6.0f;  

            UIEdgeInsets insets = new UIEdgeInsets(0, pad, 0, pad);         
            base.DrawText(insets.InsetRect(rect));
           // base.DrawText(rect);
        }

        public override CGRect TextRectForBounds(CGRect bounds, nint numberOfLines)
        {
            var pad = 5.0f;
            var pad_vert = 5.0f;

            UIEdgeInsets insets = new UIEdgeInsets(0, pad, 0, pad);
            var rect = base.TextRectForBounds(insets.InsetRect(bounds), numberOfLines);
            rect.X -= pad;
            rect.Y -= pad_vert;
            rect.Width += (pad + pad);
            rect.Height += (pad_vert + pad_vert);

            return rect;
        }
    }
}