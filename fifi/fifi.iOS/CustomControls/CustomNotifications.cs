﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.iOS.Annotations;
using Newtonsoft.Json;
using UserNotifications;
using System;
using System.ComponentModel;
using System.Linq;
using System.Security.Policy;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.iOS.CustomControls;
using fifi.Utils.Helpers;
using Newtonsoft.Json;
using SidebarNavigation;
using UIKit;
using UserNotifications;

namespace fifi.iOS.CustomControls
{
    public class CustomNotifications: UNUserNotificationCenterDelegate , INotifyPropertyChanged
    {
        #region Constructors

        public string UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                OnPropertyChanged();
            }
        }

        private SeaDroidApi _api;
        private string _userId;

        public CustomNotifications()
        {
            _api = new SeaDroidApi();
        }
        #endregion

        #region Override Methods
        public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            // Do something with the notification
            if (!notification.Request.Identifier.All(char.IsDigit))
            {
                return;
            }
            Console.WriteLine("Active Notification: {0}", notification);

            // Tell system to display the notification anyway or use
            // `None` to say we have handled the display locally.
            completionHandler(UNNotificationPresentationOptions.Alert);
        }


        public override async void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
        {

            UserId = response.Notification.Request.Identifier;


            //var data = JsonConvert.DeserializeObject<SpecificUser>(await _api.GetSpecificUser(response.Notification.Request.Identifier));
            //Friend friend = new Friend(data.data.Id, data.data.Firstname, data.data.Lastname, data.data.Email, data.data.image, data.data.verified);
            // base.DidReceiveNotificationResponse(center, response, completionHandler);
        }

        #endregion


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}