using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using CoreAnimation;
using CoreGraphics;
using fifi.iOS.Helpers;
using Foundation;
using UIKit;

namespace fifi.iOS.CustomControls
{
    [Register("SpeedCircle"), DesignTimeVisible(true)]
    public partial class SpeedCircle : UIView
    {
        private float _currentRating;
        private float _nextRating;
        private string _ratingColor;
        private string _nextRatingColor;

        public SpeedCircle(IntPtr p) : base(p)
        {
            Initialize();
        }

        public SpeedCircle()
        {
            Initialize();
        }

        [Export("CurrentRating"), Browsable(true)]
        public float CurrentRating
        {
            get { return _currentRating; }
            set
            {
                _currentRating = value;
                SetNeedsDisplay();
            }
        }

        [Export("NextRating"), Browsable(true)]
        public float NextRating
        {
            get { return _nextRating; }
            set
            {
                _nextRating = value;
                SetNeedsDisplay();
            }
        }

        [Export("RatingColor"), Browsable(true)]
        public string RatingColor
        {
            get { return _ratingColor; }
            set
            {
                _ratingColor = value;
                SetNeedsDisplay();
            }
        }

        [Export("NextRatingColor"), Browsable(true)]
        public string NextRatingColor
        {
            get { return _nextRatingColor; }
            set
            {
                _nextRatingColor = value;
                SetNeedsDisplay();
            }
        }

        private async void Initialize()
        {
            CurrentRating = 75;
            NextRating = 50;
            RatingColor = "#000000";
            NextRatingColor = "#D9D9D9";

            await Wait5sec();
            CurrentRating = 100;
            NextRating = 10;
            RatingColor = "#FF0000";
            NextRatingColor = "#00FF00";
        }

        private async Task Wait5sec()
        {
            await Task.Delay(5000);
        }

        public override void Draw(CGRect frame)
        {
            var context = UIGraphics.GetCurrentContext();

            #region drawing
            // drawing code
            var logicalDensity = UIScreen.MainScreen.Scale;
            var thickness = (int)NMath.Ceiling(16 * logicalDensity + .5f);

            var circleOuterBounds = new CGRect(0, 0, frame.Width, frame.Height);
            var circleInnerBounds = new CGRect(circleOuterBounds.X + 1.5f, circleOuterBounds.Y + 1.5f, circleOuterBounds.Width - 3f, circleOuterBounds.Height - 3f);
            var circleCenterBounds = new CGRect(circleOuterBounds.X + thickness / 2 + 2.5f, circleOuterBounds.Y + thickness / 2 + 2.5f, circleOuterBounds.Width - thickness - 5f, circleOuterBounds.Height - thickness - 5f);

            // backgroundView drawing
            var backgroundPath = UIBezierPath.FromOval(circleOuterBounds);
            UIColor.FromRGB(255, 255, 255).SetFill();
            backgroundPath.Fill();

            var blackOuterBorder = UIBezierPath.FromOval(circleInnerBounds);
            UIColor.FromRGB(0, 0, 0).SetFill();
            blackOuterBorder.Fill();

            var whiteOuterBorder = UIBezierPath.FromOval(new CGRect(circleInnerBounds.X + 2.5f, circleInnerBounds.Y + 2.5f, circleInnerBounds.Width - 5f, circleInnerBounds.Height - 5f));
            UIColor.FromRGB(255, 255, 255).SetFill();
            whiteOuterBorder.Fill();

            // draw arcs

            var ratingArc = new UIBezierPath();
            ratingArc.AddArc(new CGPoint(whiteOuterBorder.Bounds.GetMidX(), whiteOuterBorder.Bounds.GetMidY()),
                whiteOuterBorder.Bounds.Width / 2, 0.0f.ToRadians(), (CurrentRating / 100.0f * 180.0f).ToRadians(), true);
            ratingArc.AddLineTo(new CGPoint(whiteOuterBorder.Bounds.GetMidX(), whiteOuterBorder.Bounds.GetMidY()));
            ratingArc.ClosePath();
            RatingColor.ToUiColor().SetFill();
            ratingArc.Fill();

            var nextRatingArc = new UIBezierPath();
            nextRatingArc.AddArc(new CGPoint(whiteOuterBorder.Bounds.GetMidX(), whiteOuterBorder.Bounds.GetMidY()),
                whiteOuterBorder.Bounds.Width / 2, 180.0f.ToRadians(), (180.0f + NextRating / 100.0f * 180.0f).ToRadians(), true);
            nextRatingArc.AddLineTo(new CGPoint(whiteOuterBorder.Bounds.GetMidX(), whiteOuterBorder.Bounds.GetMidY()));
            nextRatingArc.ClosePath();
            NextRatingColor.ToUiColor().SetFill();
            nextRatingArc.Fill();

            // end draw arcs

            var centerCircle = UIBezierPath.FromOval(circleCenterBounds);
            UIColor.FromRGB(0, 0, 0).SetFill();
            centerCircle.Fill();

            var centerWhiteCircle =
                UIBezierPath.FromOval(new CGRect(circleCenterBounds.X + 1.5f, circleCenterBounds.Y + 1.5f,
                    circleCenterBounds.Width - 3f, circleCenterBounds.Height - 3f));
            UIColor.FromRGB(255, 255, 255).SetFill();
            centerWhiteCircle.Fill();
            #endregion

            #region MyDriving impl

            //// coverView Drawing
            //var coverViewPath =
            //    UIBezierPath.FromOval(new CGRect(frame.GetMinX() - 5.0f, frame.GetMinY() + 4.0f, frame.Width - 10.0f,
            //        frame.Height - 10.0f));
            //UIColor.FromRGB(21, 169, 254).SetFill();
            //coverViewPath.Fill();

            //// completedView Drawing
            //context.SaveState();
            //context.SaveState();
            //context.TranslateCTM(frame.GetMaxX() - 65.0f, frame.GetMinY() + 64.0f);
            //context.RotateCTM(-90.0f * NMath.PI / 180.0f);

            //var completedViewRect = new CGRect(-60.0f, -60.0f, 120.0f, 120.0f);
            //var completedViewPath = new UIBezierPath();
            //completedViewPath.AddArc(new CGPoint(completedViewRect.GetMidX(), completedViewRect.GetMidY()),
            //    completedViewRect.Width / 2.0f, -360.0f * NMath.PI / 180,
            //    -(expression - 17.0f) * NMath.PI / 180.0f, true);
            //completedViewPath.AddLineTo(new CGPoint(completedViewRect.GetMidX(), completedViewRect.GetMidY()));
            //completedViewPath.ClosePath();

            //UIColor.FromRGB(247, 247, 247).SetFill();
            //completedViewPath.Fill();
            //context.RestoreState();

            //// backgroundView Drawing
            //var backgroundViewPath =
            //    UIBezierPath.FromOval(new CGRect(frame.GetMinX() + 12.0f, frame.GetMinY() + 11.0f, frame.Width - 24.0f,
            //        frame.Height - 24.0f));
            //UIColor.FromRGB(0, 0, 0).SetFill();
            //backgroundViewPath.Fill();

            #endregion
        }
    }
}

