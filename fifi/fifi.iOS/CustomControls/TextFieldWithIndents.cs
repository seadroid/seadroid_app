using System;
using System.ComponentModel;
using Foundation;
using UIKit;

namespace fifi.iOS
{
	[Register("TextFieldWithIndents"), DesignTimeVisible(true)]
	public partial class TextFieldWithIndents : UITextField
	{
		public TextFieldWithIndents(IntPtr handle) : base(handle)
		{
			Initialize();
		}

		public TextFieldWithIndents()
		{
			Initialize();
		}

		[Export("InsetX"), Browsable(true)]
		public nfloat InsetX
		{
			get;
			set;
		}

		[Export("InsetY"), Browsable(true)]
		public nfloat InsetY
		{
			get;
			set;
		}

		public override CoreGraphics.CGRect TextRect(CoreGraphics.CGRect forBounds)
		{
			return CoreGraphics.RectangleFExtensions.Inset(forBounds, InsetX, InsetY);
		}

		public override CoreGraphics.CGRect EditingRect(CoreGraphics.CGRect forBounds)
		{	
			return CoreGraphics.RectangleFExtensions.Inset(forBounds, InsetX, InsetY);
		}

		private void Initialize()
		{
			UIColor color = UIColor.Black;
		    if (string.IsNullOrEmpty(Placeholder))
		    {
		        Placeholder = " ";
		    }
		    try
		    {
		        AttributedPlaceholder = new NSAttributedString(Placeholder, foregroundColor: color);
            }
		    catch (Exception e)
		    {
		        Console.WriteLine(e);
		        throw;
		    }

		
		}
	}
}
