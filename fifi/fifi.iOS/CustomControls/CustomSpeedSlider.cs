using Foundation;
using System;
using CoreGraphics;
using UIKit;

namespace fifi.iOS
{
    public partial class CustomSpeedSlider : UISlider
    {
        public CustomSpeedSlider (IntPtr handle) : base (handle)
        {
        }

        public override CGRect TrackRectForBounds(CGRect forBounds)
        {
			var currentbounds = base.TrackRectForBounds(forBounds);
			var newbounds = new CGRect(0, 0, currentbounds.Width, 10);
			return newbounds;
        }
    }
}