﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using UIKit;

namespace fifi.iOS
{
	[Register("PlaceholderTextView"), DesignTimeVisible(true)]
	public class PlaceholderTextView : UITextView
	{
		public string RealText
		{
			get;
			set;
		}

		public UIColor RealTextColor
		{
			get;
			set;
		}

		public string Placeholder
		{
			get;
			set;
		}

		public UIColor PlaceholderColor
		{
			get;
			set;
		}

		public PlaceholderTextView(IntPtr handle) : base(handle)
		{
		}

		public PlaceholderTextView(CGRect frame) : base(frame)
		{
		}

		public PlaceholderTextView()
		{
		}

		#region Setters/Getters

		void SetPlaceHolder(string placeholder) { 
			if (string.Equals(this.RealText, placeholder) && !this.IsFirstResponder)
			{
				this.Text = placeholder;
			}
			if (placeholder != Placeholder)
			{
				this.Placeholder = placeholder;
			}
		}

		void SetPlaceholderColor(UIColor placeholderColor) {
			PlaceholderColor = placeholderColor;

			if (string.Equals(base.Text, this.Placeholder))
			{
				this.TextColor = this.PlaceholderColor;
			}
		}

		string GetText() {
			string text = base.Text;
			if (string.Equals(text, this.Placeholder))
			{
				return string.Empty;
			}
			return text;
		}
		#endregion
	}
}
