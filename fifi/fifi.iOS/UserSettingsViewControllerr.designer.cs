// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("UserSettingsViewControllerr")]
    partial class UserSettingsViewControllerr
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Save_Button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextEmail { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextFirstname { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextLastname { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView User_Imange { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Save_Button != null) {
                Save_Button.Dispose ();
                Save_Button = null;
            }

            if (TextEmail != null) {
                TextEmail.Dispose ();
                TextEmail = null;
            }

            if (TextFirstname != null) {
                TextFirstname.Dispose ();
                TextFirstname = null;
            }

            if (TextLastname != null) {
                TextLastname.Dispose ();
                TextLastname = null;
            }

            if (User_Imange != null) {
                User_Imange.Dispose ();
                User_Imange = null;
            }
        }
    }
}