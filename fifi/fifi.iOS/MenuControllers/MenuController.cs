﻿//using Foundation;
using System;
using CoreBluetooth;
using CoreGraphics;
using fifi.Annotations;
using fifi.Helpers;
using fifi.iOS.MenuControllers;
using fifi.Model;
using fifi.Utils.Helpers;
using fifi.ViewModel;
using Foundation;
using PassKit;
using Plugin.BLE.iOS;
using SeaDroid.iOS;
using UIKit;

namespace fifi.iOS
{
    public partial class MenuController : BaseController
    {

        public DashboardViewModel _viewModel;
        public UIImageView _UserImage;
        public UILabel _Username;
        public UILabel _Boatname;

        

        public MenuController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            _viewModel = new DashboardViewModel();
            DasboardButton.TouchUpInside += (sender, e) =>
            {
                ChangeSelected(DasboardButton);
          
                UINavigationController DasboardController = 
                new UINavigationController(this.Storyboard.InstantiateViewController("dashboardViewController") as DashboardViewController);

                if (DasboardController != null)
                {
                                           
                  SidebarController.ChangeContentView(DasboardController);
                  this.SidebarController.CloseMenu(true);
                                     
                }
            };
            

            _UserImage = UserImage;
            _Username = UserName;
            _Boatname = BoatName;
            UserImage.UserInteractionEnabled = true;
            UserImage.Layer.CornerRadius = _UserImage.Frame.Size.Height / 2;
            UserImage.AddGestureRecognizer(new UITapGestureRecognizer(OnImageTap));
            UserImage.Layer.MasksToBounds = true;
            UserImage.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(Settings.Current.UserImageBase64)));
            _Username.Text = Settings.Current.UserFirstname + " " + Settings.Current.UserLastname;


         

            PairingButton.TouchUpInside += (sender, e) => 
            {
                ChangeSelected(PairingButton);
                
              
                UINavigationController ArmbandController = new UINavigationController(this.Storyboard.InstantiateViewController("ArmbandViewController") as ArmbandViewController);


                if (ArmbandController != null)
                {

                    SidebarController.ChangeContentView(ArmbandController);
                    this.SidebarController.CloseMenu(true);
                }
            };

            MapsButton.TouchUpInside += (sender, e) =>

            {
                ChangeSelected(MapsButton);  
                UINavigationController MapController = 
                new UINavigationController(this.Storyboard.InstantiateViewController("mapsViewController") as MapsViewController);


                if (MapController != null)
                {
                    SidebarController.ChangeContentView(MapController);
                    this.SidebarController.CloseMenu(true);
                }

            };

            DeviceButton.TouchUpInside += (sender, e) =>
            {
                ChangeSelected(DeviceButton);
                UINavigationController DeviceController =
                    new UINavigationController(this.Storyboard.InstantiateViewController("deviceViewController") as DeviceViewController);
                if (DeviceController != null)
                {
                    SidebarController.ChangeContentView(DeviceController);
                    this.SidebarController.CloseMenu(true);
                }
            };


            SignOutButton.TouchUpInside += (sender, args) =>
            {
                ChangeSelected(SignOutButton);
                _viewModel.LogoutCommand.Execute(null);
                PubNubSingleton.Instance.UnSubscribeFromPushNotifications(Settings.Current.DeviceId);
                var app = (AppDelegate) UIApplication.SharedApplication.Delegate;
                var loginViewController = UIStoryboard.FromName("Main", null)
                    .InstantiateViewController("loginViewController");
                app.Window.RootViewController = loginViewController;
            };

            FriendsButton.TouchUpInside += (sender, e) =>
            {
                ChangeSelected(FriendsButton);

                UINavigationController FriendsController =
                    new UINavigationController(this.Storyboard.InstantiateViewController("FriendsViewController") as FriendsViewController);
                if (FriendsController != null)
                {
                    SidebarController.ChangeContentView(FriendsController);
                    this.SidebarController.CloseMenu(true);
                }
            };

            InviteButton.TouchUpInside += (sender, args) =>
            {
                ChangeSelected(InviteButton);
                UINavigationController InviteController =
                    new UINavigationController(this.Storyboard.InstantiateViewController("InviteViewController") as InviteViewController);
                if (InviteController != null)
                {
                    SidebarController.ChangeContentView(InviteController);
                    this.SidebarController.CloseMenu(true);
                }
            };

            UserSettingsButton.TouchUpInside += (sender, args) =>
            {
                ChangeSelected(UserSettingsButton);
                UINavigationController UserSettingsController = new UINavigationController(this.Storyboard.InstantiateViewController("UserSettingsViewController") as UserSettingsViewController);
                    
                if (UserSettingsController != null)
                {
                    SidebarController.ChangeContentView(UserSettingsController);
                    this.SidebarController.CloseMenu(true);
                }
            };

            MessagesButton.TouchUpInside += (sender, args) =>
            {
                ChangeSelected(MessagesButton);
                UINavigationController ChatHubViewController =
                    new UINavigationController(this.Storyboard.InstantiateViewController("ChatHunViewController") as ChatHunViewController);
                if (ChatHubViewController != null)
                {
                    SidebarController.ChangeContentView(ChatHubViewController);
                    this.SidebarController.CloseMenu(true);
                }
            };

            RaceButton.TouchUpInside += (sender, args) =>
            {
                //var PairingDetials = new PairingDetailsViewModel();
                //PairingDetials.Wristband = new Wristband();
                //var pairingDetailsView = this.Storyboard.InstantiateViewController("ArmbandDetailsController") as ArmbandDetailsController;
                //pairingDetailsView._viewModel = PairingDetials;
                //NavigationController.PushViewController(pairingDetailsView, true);

                //var CreateBoatView = this.Storyboard.InstantiateViewController("CreateBoatViewController") as CreateBoatViewController;
                //if (CreateBoatView != null)
                //{
                //    this.NavigationController.PushViewController(CreateBoatView, true);
                //    this.SidebarController.CloseMenu(true);
                //}

           


            };
        }




        private void OnImageTap()
        {
            ChangeSelected(UserSettingsButton);
            UINavigationController UserSettingsController = new UINavigationController(this.Storyboard.InstantiateViewController("UserSettingsViewController") as UserSettingsViewController);

            if (UserSettingsController != null)
            {
                SidebarController.ChangeContentView(UserSettingsController);
                this.SidebarController.CloseMenu(true);
            }
        }

     

        private void ChangeSelected(UIButton Selected)
        {
            PairingButton.BackgroundColor = UIColor.Clear;
            DasboardButton.BackgroundColor = UIColor.Clear;
            MapsButton.BackgroundColor = UIColor.Clear;
            DeviceButton.BackgroundColor = UIColor.Clear;
            SignOutButton.BackgroundColor = UIColor.Clear;
            FriendsButton.BackgroundColor = UIColor.Clear;
            InviteButton.BackgroundColor = UIColor.Clear;
            UserSettingsButton.BackgroundColor = UIColor.Clear;
            MessagesButton.BackgroundColor = UIColor.Clear;

            Selected.BackgroundColor = UIColor.LightGray;
        }

        /*partial void NavigateToNext(UIButton sender)
        {
            var mapsController = (MapsViewController)Storyboard.InstantiateViewController("mapsViewController");
            var dashboardController =
                (DashboardViewController)Storyboard.InstantiateViewController("dashboardViewController");

            if (!(NavigationController.TopViewController is MapsViewController))
            {
                NavigationController.PushViewController(mapsController, true);
            }
            else
            {
                NavigationController.PushViewController(dashboardController, true);
            }
            SidebarController.CloseMenu(); 
        }*/
    }
}