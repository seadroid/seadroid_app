﻿using System;
using SidebarNavigation;
using UIKit;

namespace fifi.iOS.MenuControllers
{
    public class BaseController : UIViewController
    {
        protected SidebarController SidebarController
            => (UIApplication.SharedApplication.Delegate as AppDelegate)?.RootViewController.SidebarController;

        //protected new NavigationController NavigationController
        //    => (UIApplication.SharedApplication.Delegate as AppDelegate)?.RootViewController.NavigationController;

        public override UIStoryboard Storyboard
            => (UIApplication.SharedApplication.Delegate as AppDelegate)?.RootViewController.Storyboard;

       protected RootViewController Rootview = (UIApplication.SharedApplication.Delegate as AppDelegate)?.RootViewController;

        public BaseController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NavigationItem.SetLeftBarButtonItem(
                new UIBarButtonItem(UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain,
                    (sender, args) =>
                    {
                        SidebarController.ToggleMenu();
                    }), true);
           
        }
    }
}
