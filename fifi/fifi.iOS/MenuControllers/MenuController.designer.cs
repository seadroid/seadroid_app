// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("MenuController")]
    partial class MenuController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AcessoriesButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel BoatName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ControlsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton DasboardButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton DeviceButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FeedbackButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FriendsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton HelpButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton InviteButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton MapsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton MessagesButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton NotificationButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton PairingButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton RaceButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SignOutButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView UserImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel UserName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton UserSettingsButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AcessoriesButton != null) {
                AcessoriesButton.Dispose ();
                AcessoriesButton = null;
            }

            if (BoatName != null) {
                BoatName.Dispose ();
                BoatName = null;
            }

            if (ControlsButton != null) {
                ControlsButton.Dispose ();
                ControlsButton = null;
            }

            if (DasboardButton != null) {
                DasboardButton.Dispose ();
                DasboardButton = null;
            }

            if (DeviceButton != null) {
                DeviceButton.Dispose ();
                DeviceButton = null;
            }

            if (FeedbackButton != null) {
                FeedbackButton.Dispose ();
                FeedbackButton = null;
            }

            if (FriendsButton != null) {
                FriendsButton.Dispose ();
                FriendsButton = null;
            }

            if (HelpButton != null) {
                HelpButton.Dispose ();
                HelpButton = null;
            }

            if (InviteButton != null) {
                InviteButton.Dispose ();
                InviteButton = null;
            }

            if (MapsButton != null) {
                MapsButton.Dispose ();
                MapsButton = null;
            }

            if (MessagesButton != null) {
                MessagesButton.Dispose ();
                MessagesButton = null;
            }

            if (NotificationButton != null) {
                NotificationButton.Dispose ();
                NotificationButton = null;
            }

            if (PairingButton != null) {
                PairingButton.Dispose ();
                PairingButton = null;
            }

            if (RaceButton != null) {
                RaceButton.Dispose ();
                RaceButton = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (SignOutButton != null) {
                SignOutButton.Dispose ();
                SignOutButton = null;
            }

            if (UserImage != null) {
                UserImage.Dispose ();
                UserImage = null;
            }

            if (UserName != null) {
                UserName.Dispose ();
                UserName = null;
            }

            if (UserSettingsButton != null) {
                UserSettingsButton.Dispose ();
                UserSettingsButton = null;
            }
        }
    }
}