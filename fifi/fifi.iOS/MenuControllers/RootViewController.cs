﻿using System;
using System.ComponentModel;
using System.Security.Policy;
using CoreGraphics;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.iOS.CustomControls;
using fifi.Utils.Helpers;
using Newtonsoft.Json;
using SidebarNavigation;
using UIKit;
using UserNotifications;

namespace fifi.iOS.MenuControllers
{
    public class RootViewController : UIViewController
    {
        private UIStoryboard _storyboard;

        private CustomNotifications notifications;

        public SidebarController SidebarController { get; private set; }

        public SeaDroidApi _api;

        private int NotficationNumber;

        private NavigationController NavigationController { get;  set; }

        public override UIStoryboard Storyboard => _storyboard ?? (_storyboard = UIStoryboard.FromName("Main", null));

        public RootViewController() : base(null, null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            NotficationNumber = 0;

            _api = new SeaDroidApi();

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width,
                statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.View.AddSubview(statusbar);

            NavigationController = new NavigationController();

            var dashboardController =
                (DashboardViewController) Storyboard.InstantiateViewController("dashboardViewController");
            var menuController = (MenuController) Storyboard.InstantiateViewController("menuController");


            NavigationController.PushViewController(dashboardController, false);

            var width = Convert.ToInt32(Convert.ToInt32(UIScreen.MainScreen.Bounds.Width) * 0.8);


            SidebarController = new SidebarController(this, NavigationController, menuController)
            {
                ReopenOnRotate = false,
                MenuLocation = MenuLocations.Left,
                HasShadowing = false,
                HasDarkOverlay = true,
                DarkOverlayAlpha = 0.85f,
                MenuWidth = Convert.ToInt32(Convert.ToInt32(UIScreen.MainScreen.Bounds.Width) * 0.8)
            };


            PubNubSingleton.Instance.PropertyChanged += InstanceOnPropertyChanged;

            notifications = (CustomNotifications) UNUserNotificationCenter.Current.Delegate;
            notifications.PropertyChanged += NotficaitoncenterOnPropertyChanged;
        }

       


        private async void NotficaitoncenterOnPropertyChanged(object sender,
            PropertyChangedEventArgs propertyChangedEventArgs)
        {
            try
            {
                var data = JsonConvert.DeserializeObject<SpecificUser>(await _api.GetSpecificUser(notifications.UserId));
                Friend friend = new Friend(data.data.Id, data.data.Firstname, data.data.Lastname, data.data.Email,
                    data.data.image, data.data.verified);
                var ChatView =
                    this.Storyboard.InstantiateViewController("FriendChatViewController") as FriendChatViewController;
                ChatView.Friend = friend;
                ChatView.WasOpenedWithNotfication = true;
                this.NavigationController.PushViewController(ChatView, true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

           
        }

        private async void InstanceOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (Settings.Current.ActiveChatId != PubNubSingleton.Instance.ChatMessage.Channel &&
                PubNubSingleton.Instance.ChatMessage.Sender_id != Settings.Current.UserId)
            {
                var user = JsonConvert.DeserializeObject<SpecificUser>(
                    await _api.GetSpecificUser(PubNubSingleton.Instance.ChatMessage.Sender_id));

                var content = new UNMutableNotificationContent();
                content.Title = user.data.Firstname + " " + user.data.Lastname;
                // content.Subtitle = PubNubSingleton.Instance.ChatMessage.Text;
                content.Body = PubNubSingleton.Instance.ChatMessage.Text;
                content.Badge = 1;


                var trigger = UNTimeIntervalNotificationTrigger.CreateTrigger(1, false);

                var requestID = PubNubSingleton.Instance.ChatMessage.Sender_id;
                ;
                var request = UNNotificationRequest.FromIdentifier(NotficationNumber.ToString(), content, trigger);
                NotficationNumber = NotficationNumber + 1;


                UNUserNotificationCenter.Current.AddNotificationRequest(request, (err) =>
                {
                    if (err != null)
                    {
                        var a = 2;
                        // Do something with error...
                    }
                });
            }
        }
    }
}