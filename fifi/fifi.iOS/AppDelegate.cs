﻿using System;
using System.Threading.Tasks;
using fifi.Client.DataObjects;
using fifi.Client.Interfaces;
using fifi.Helpers;
using fifi.iOS.CustomControls;
using fifi.iOS.Helpers;
using fifi.iOS.MenuControllers;
using fifi.Utils.Helpers;
using Foundation;
using Newtonsoft.Json;
using PubnubApi;
using SeaDroid.iOS.Helpers;
using UIKit;
using UserNotifications;

namespace fifi.iOS
{
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        // class-level declarations


        public override UIWindow Window
        {
            get;
            set;
        }

        public RootViewController RootViewController => Window.RootViewController as RootViewController;

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {

            SetUpRemoteNotifications();
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

           // Settings.Current.DeviceId = UIDevice.CurrentDevice.IdentifierForVendor.ToString();
            Window = new UIWindow(UIScreen.MainScreen.Bounds);
            ThemeManager.ApplyTheme();
            ViewModel.ViewModelBase.Init();
            application.IdleTimerDisabled = true;

           
            UNUserNotificationCenter.Current.Delegate = new CustomNotifications();
            Settings.Current.Platform = "IOS";


            if (Settings.Current.IsLoggedIn && Settings.Current.IsVerified)
            {
                var _api = new SeaDroidApi();

                //PubnubIos.Instance.Subcribe("2");
                //Task.Delay(1000);
                //PubnubIos.Instance.SubcribeForPushNotifications("");

                //if (Settings.Current.RandomNumber != string.Empty && Convert.ToInt32(Settings.Current.RandomNumber) == 1)
                //{ PubNubSingleton.Instance.Subcribe("0"); Settings.Current.RandomNumber = "0"; }
                //else
                //{ PubNubSingleton.Instance.Subcribe("1"); Settings.Current.RandomNumber = "1"; }



                PubNubSingleton.Instance.Subcribe(Settings.Current.UserId);
                PubNubSingleton.Instance.ClearLatestMessages();



                var a = new Task(async () =>
                {
                    var friends = JsonConvert.DeserializeObject<FriendList>(await _api.GetUserFriends());
                    foreach (var friend in friends.data)
                    {
                        PubNubSingleton.Instance.SubcribeToFriendChat(Convert.ToInt32(Settings.Current.UserId), friend.id);
                    }

                    var SentFriendRequests = JsonConvert.DeserializeObject<FriendList>(await _api.GetSentFriendRequests());

                    foreach (var friendrequest in SentFriendRequests.data)
                    {
                        PubNubSingleton.Instance.SubcribeToFriendChat(Convert.ToInt32(Settings.Current.UserId), friendrequest.id);
                    }

                    await Task.Delay(100);

                    PubNubSingleton.Instance.SubcribeForPushNotifications(Settings.Current.DeviceId);

                    await Task.Delay(100);

                    PubNubSingleton.Instance.GetLastestMessageFromSubcribedChannels();
                });
                a.Start();



             

                //if (Settings.Current.RandomNumber != string.Empty && Convert.ToInt32(Settings.Current.RandomNumber) == 1)
                //{ PubnubIos.Instance.Subcribe("0"); Settings.Current.RandomNumber = "0"; }
                //else
                //{ PubnubIos.Instance.Subcribe("1"); Settings.Current.RandomNumber = "1"; }



                //PubnubIos.Instance.Subcribe(Settings.Current.UserId);
                //PubnubIos.Instance.ClearLatestMessages();



                //var a = new Task(async () =>
                //{
                //    var friends = JsonConvert.DeserializeObject<FriendList>(await _api.GetUserFriends());
                //    foreach (var friend in friends.data)
                //    {
                //        PubnubIos.Instance.SubcribeToFriendChat(Convert.ToInt32(Settings.Current.UserId), friend.id);
                //    }

                //    var SentFriendRequests = JsonConvert.DeserializeObject<FriendList>(await _api.GetSentFriendRequests());

                //    foreach (var friendrequest in SentFriendRequests.data)
                //    {
                //        PubnubIos.Instance.SubcribeToFriendChat(Convert.ToInt32(Settings.Current.UserId), friendrequest.id);
                //    }

                //    await Task.Delay(100);

                //    PubnubIos.Instance.SubcribeForPushNotifications(Settings.Current.DeviceId);

                //    await Task.Delay(100);

                //    PubnubIos.Instance.GetLastestMessageFromSubcribedChannels();





                //});
                //a.Start();
                Window.RootViewController = new RootViewController();

                
            }
            else
            {
                var loginViewController = UIStoryboard.FromName("Main", null).InstantiateViewController("loginViewController");
                Window.RootViewController = loginViewController;
            }



        
            return true;
        }


        private void SetUpRemoteNotifications()
        {

            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var uiUserNotificationType = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;

                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                    uiUserNotificationType,
                    new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert |
                                                             UIRemoteNotificationType.Badge |
                                                             UIRemoteNotificationType.Sound;

                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }
        }

        public override void DidEnterBackground(UIApplication application)
        {
          
            
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            base.DidReceiveRemoteNotification(application, userInfo, completionHandler);
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            Settings.Current.DeviceId = deviceToken.ToString().Replace("<", "").Replace(">", "").Replace(" ", "");

            //base.RegisteredForRemoteNotifications(application, deviceToken);
        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            new UIAlertView("Registration failed", error.LocalizedDescription, null, "OK", null).Show();
        }


        public override void DidRegisterUserNotificationSettings(UIApplication application, UIUserNotificationSettings notificationSettings)
        {
            application.RegisterForRemoteNotifications();
        }

       
    }
}


