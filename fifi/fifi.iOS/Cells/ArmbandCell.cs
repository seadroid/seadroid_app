using Foundation;
using System;
using UIKit;


namespace SeaDroid.iOS
{
    public partial class ArmbandCell : UITableViewCell
    {
   

        public ArmbandCell (IntPtr handle) : base (handle)
        {
        }

        public string BatteryLevelText
        {
            get => BatteryLevel.Text;
            set => BatteryLevel.Text  = value;
        }

        public UIImageView BatteryImage
        {
            get => BatteryImageView;
            set => BatteryImageView = value;
        }

        public UIImageView SignalImage
        {
            get => SignalImangeView;
            set => SignalImangeView = value;
        }
       

        public string Name
        {
            get => ArmbandName.Text;
            set => ArmbandName.Text = value;
        }
    }
}