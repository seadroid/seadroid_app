// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace fifi.iOS
{
    [Register ("DeviceDetialsCell")]
    partial class DeviceDetialsCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel EndDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView MapView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel StartDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TripNumber { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (EndDate != null) {
                EndDate.Dispose ();
                EndDate = null;
            }

            if (MapView != null) {
                MapView.Dispose ();
                MapView = null;
            }

            if (StartDate != null) {
                StartDate.Dispose ();
                StartDate = null;
            }

            if (TripNumber != null) {
                TripNumber.Dispose ();
                TripNumber = null;
            }
        }
    }
}