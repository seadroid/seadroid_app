using Foundation;

using System;

using UIKit;



namespace fifi.iOS

{

    public partial class LeftChatCell : UITableViewCell

    {

        private UIImageView _image;

        private string _name;

       



		public ChatTextLabel TextView

        {
            get { return MessageText ; }

            set { MessageText = value; }

        }





        public UIImageView _Image

        {

            get { return UserImage; }

            set { UserImage = value; }

        }



        public string _Name

        {

            get { return Username.Text; }

            set { Username.Text = value; }

        }


        public string MessageDate
        {
            get { return Date.Text; }
            set { Date.Text = value; }
        }
       





        public LeftChatCell(IntPtr handle) : base (handle)

        {
			
            SelectionStyle = UITableViewCellSelectionStyle.None;

        }

    }

}