using Foundation;
using System;
using UIKit;

namespace fifi.iOS
{
    public partial class ChatFriendCell : UITableViewCell
    {


 

        public UIImageView Image
        {
            get { return UserImange; }
            set { UserImange = value; }
        }

        public string Name
        {
            get { return Username.Text; }
            set { Username.Text = value; }
        }

        public string Boat
        {
            get { return Boatname.Text; }
            set { Boatname.Text = value; }
        }

        public string Message
        {
            get { return MessageText.Text; }
            set { MessageText.Text = value; }
        }

        public ChatFriendCell (IntPtr handle) : base (handle)
        {
            SelectionStyle = UITableViewCellSelectionStyle.Gray;
        }
    }
}