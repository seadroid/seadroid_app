﻿using Foundation;
using System;
using UIKit;

namespace fifi.iOS
{
    public partial class DashboardTableViewCellHeading : UITableViewCell
    {
        public DashboardTableViewCellHeading(IntPtr handle) : base(handle) { }
        public DashboardTableViewCellHeading(NSString cellId) : base(UITableViewCellStyle.Default, cellId) { }

        public string Heading
        {
            get { return lblHeading.Text; }
            set { lblHeading.Text = value; }
        }
    }
}