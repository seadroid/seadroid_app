// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace fifi.iOS
{
    [Register ("DeviceCell")]
    partial class DeviceCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel DeviceCellDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel DeviceCellName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel DeviceCellTripNumber { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (DeviceCellDate != null) {
                DeviceCellDate.Dispose ();
                DeviceCellDate = null;
            }

            if (DeviceCellName != null) {
                DeviceCellName.Dispose ();
                DeviceCellName = null;
            }

            if (DeviceCellTripNumber != null) {
                DeviceCellTripNumber.Dispose ();
                DeviceCellTripNumber = null;
            }
        }
    }
}