using Foundation;
using System;
using UIKit;

namespace fifi.iOS
{
    public partial class RightChatCell : UITableViewCell
    {
       
     


        public string _Name
        {
            get { return Username.Text; }
            set { Username.Text = value; }
        }

        public ChatTextLabel _Text
        {
            get { return MessageText; }
            set { MessageText = value; }
        }

        public string MessageDate
        {
            get { return Date.Text; }
            set { Date.Text = value; }
        }

        public RightChatCell (IntPtr handle) : base (handle)
        {
            SelectionStyle = UITableViewCellSelectionStyle.None;
        }
    }
}