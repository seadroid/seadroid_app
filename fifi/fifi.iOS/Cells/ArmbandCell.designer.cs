// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SeaDroid.iOS
{
    [Register ("ArmbandCell")]
    partial class ArmbandCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ArmbandName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView BatteryImageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel BatteryLevel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView SignalImangeView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ArmbandName != null) {
                ArmbandName.Dispose ();
                ArmbandName = null;
            }

            if (BatteryImageView != null) {
                BatteryImageView.Dispose ();
                BatteryImageView = null;
            }

            if (BatteryLevel != null) {
                BatteryLevel.Dispose ();
                BatteryLevel = null;
            }

            if (SignalImangeView != null) {
                SignalImangeView.Dispose ();
                SignalImangeView = null;
            }
        }
    }
}