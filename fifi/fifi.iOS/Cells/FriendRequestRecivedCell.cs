using Foundation;
using System;
using fifi.Client.DataObjects;
using UIKit;

namespace fifi.iOS
{
    public sealed partial class FriendRequestRecivedCell : UITableViewCell
    {
        
        public Friend user { get; set; }


        public string Name
        {
            get { return Username.Text; }
            set { Username.Text = value; }
        }

        public string Boat
        {
            get { return Boatname.Text; }
            set { Boatname.Text = value; }
        }

        public UIImageView UiImage
        {
            get { return UserImange; }
            set { UserImange  = value; }
        }

        public UIButton Decline_Button
        {
            get { return DeclineButton; }
            set { DeclineButton = value; }
        }

        public UIButton Confirm_Button
        {
            get { return ConfirmButton; }
            set { ConfirmButton = value; }
        }

                 

        public FriendRequestRecivedCell (IntPtr handle) : base (handle)
        {
            SelectionStyle = UITableViewCellSelectionStyle.None;
        
        }
    }
}