﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("DashboardTableViewCellWeatherInfo")]
    partial class DashboardTableViewCellWeatherInfo
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblDegree { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblDownfall { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblWind { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (lblDegree != null) {
                lblDegree.Dispose ();
                lblDegree = null;
            }

            if (lblDownfall != null) {
                lblDownfall.Dispose ();
                lblDownfall = null;
            }

            if (lblWind != null) {
                lblWind.Dispose ();
                lblWind = null;
            }
        }
    }
}