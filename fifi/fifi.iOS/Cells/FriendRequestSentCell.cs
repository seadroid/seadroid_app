using Foundation;
using System;
using UIKit;
using System.ComponentModel;
using CallKit;
using CoreGraphics;
using fifi.Client.DataObjects;
using SidebarNavigation;
using fifi.Model;
using fifi.Utils.Helpers;
using fifi.ViewModel;
namespace fifi.iOS
{
    public partial class FriendRequestSentCell : UITableViewCell
    {
        public Friend user { get; set; }

        public string Name
        {
            get { return Username.Text; }
            set { Username.Text = value; }
        }

        public string Boat
        {
            get { return Boatname.Text; }
            set { Boatname.Text = value; }
        }

        public UIButton Cancel_button
        {
            get { return CancelButton; }
            set {CancelButton  = value; }
        }

        public UIImageView UiImage
        {
            get { return UserImange; }
            set { UserImange = value; }
        }

        public FriendRequestSentCell (IntPtr handle) : base (handle)
        {
            SelectionStyle = UITableViewCellSelectionStyle.None;
  
        }
    }
}