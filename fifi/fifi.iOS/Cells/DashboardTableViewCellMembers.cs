﻿using Foundation;
using System;
using UIKit;

namespace fifi.iOS
{
    public partial class DashboardTableViewCellMembers : UITableViewCell
    {
        public DashboardTableViewCellMembers(IntPtr handle) : base(handle) { }
        public DashboardTableViewCellMembers(NSString cellId) : base(UITableViewCellStyle.Default, cellId) { }

        public string AmountOnBoard
        {
            get { return lblAmountOnBoard.Text; }
            set { lblAmountOnBoard.Text = value; }
        }
    }
}