using Foundation;
using System;
using fifi.Client.DataObjects;
using UIKit;

namespace fifi.iOS
{
    public partial class AddFriendsListCell : UITableViewCell
    {
        private string _name;
        private string _boat;
        private UIButton _addFriendButton;
        private UIImageView _userImage;

        public Friend user { get; set; }
        
        public string name
        {
            get { return Username.Text; }
            set { Username.Text = value; }
        }

        public string boat
        {
            get { return Boatname.Text; }
            set { Boatname.Text = value; }
        }

        public UIButton Add_Friend_button
        {
            get { return AddFriendButton; }
            set { AddFriendButton = value; }
        }

        public UIImageView UserImage
        {
            get { return UserImange; }
            set { UserImange = value; }
        }


        public AddFriendsListCell (IntPtr handle) : base (handle)
        {
            SelectionStyle = UITableViewCellSelectionStyle.None;
           
        }

        public AddFriendsListCell()
        {
           
        }

        
    }
}