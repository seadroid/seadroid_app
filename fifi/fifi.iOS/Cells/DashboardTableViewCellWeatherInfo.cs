﻿using Foundation;
using System;
using UIKit;

namespace fifi.iOS
{
    public partial class DashboardTableViewCellWeatherInfo : UITableViewCell
    {
        public DashboardTableViewCellWeatherInfo(IntPtr handle) : base(handle) { }

        public DashboardTableViewCellWeatherInfo(NSString cellId) : base(UITableViewCellStyle.Default, cellId) { }

        public string Degrees
        {
            get { return lblDegree.Text; }
            set { lblDegree.Text = value; }
        }

        public string Downfall
        {
            get { return lblDownfall.Text; }
            set { lblDownfall.Text = value; }
        }

        public string WindSpeed
        {
            get { return lblWind.Text; }
            set { lblWind.Text = value; }
        }
    }
}