﻿using Foundation;
using System;
using UIKit;

namespace fifi.iOS
{
    public partial class DashboardTableViewCellHarbor : UITableViewCell
    {
        public DashboardTableViewCellHarbor(IntPtr handle) : base(handle) { }
        public DashboardTableViewCellHarbor(NSString cellId) : base(UITableViewCellStyle.Default, cellId) { }

        public string Harbor
        {
            get { return lblHarbor.Text; }
            set { lblHarbor.Text = value; }
        }
    }
}