using Foundation;
using System;
using fifi.Client.DataObjects;
using UIKit;

namespace fifi.iOS
{
    public partial class FriendsListCell : UITableViewCell
    {
        private UIImageView _image;
        private string Name;

        public Friend Friend { get; set; }

        public string boat
        {
            get { return Boatname.Text; }
            set { Boatname.Text = value; }
        }


        public UIImageView Image
        {
            get { return UserImange; }
            set { UserImange = value; }
        }

        public string name
        {
            get { return Username.Text; }
            set { Username.Text = value; }
        }


        public FriendsListCell (IntPtr handle) : base (handle)
        {
            SelectionStyle = UITableViewCellSelectionStyle.Gray;
        }


    }
}