﻿using System;
using MapKit;
using CoreLocation;

namespace fifi.iOS
{
	public class BoatAnnotation : MKAnnotation
	{
		string title;
		private CLLocationCoordinate2D _coordinate;

		public BoatAnnotation(string title, CLLocationCoordinate2D coord)
		{
			this.title = title;
			_coordinate = coord;
		}

		public override string Title
		{
			get
			{
				return title;
			}
		}

		public override CLLocationCoordinate2D Coordinate
		{
			get
			{
				{ return _coordinate;}
			}
		}

		public override void SetCoordinate(CLLocationCoordinate2D value)
		{

			_coordinate = value;
		}
	}
}
