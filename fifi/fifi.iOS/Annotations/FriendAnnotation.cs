﻿using CoreLocation;
using MapKit;

namespace fifi.iOS.Annotations
{
    public class FriendAnnotation: MKAnnotation
    {    
        string title;
        private CLLocationCoordinate2D _coordinate;

        public FriendAnnotation(string title, CLLocationCoordinate2D coord)
        {
            this.title = title;
            _coordinate = coord;
        }

        public override string Title
        {
            get
            {
                return title;
            }
        }

        public override CLLocationCoordinate2D Coordinate
        {
            get
            {
                { return _coordinate; }
            }
        }

        public override void SetCoordinate(CLLocationCoordinate2D value)
        {

            _coordinate = value;
        }
    }
}