// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("FriendChatViewController")]
    partial class FriendChatViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ChatInputView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView FriendChatTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView MessageTextView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SendMessageButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ChatInputView != null) {
                ChatInputView.Dispose ();
                ChatInputView = null;
            }

            if (FriendChatTableView != null) {
                FriendChatTableView.Dispose ();
                FriendChatTableView = null;
            }

            if (MessageTextView != null) {
                MessageTextView.Dispose ();
                MessageTextView = null;
            }

            if (SendMessageButton != null) {
                SendMessageButton.Dispose ();
                SendMessageButton = null;
            }
        }
    }
}