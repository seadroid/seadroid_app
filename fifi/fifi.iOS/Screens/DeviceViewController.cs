using Foundation;
using System;
using UIKit;
using CoreGraphics;
using fifi.iOS.MenuControllers;
using SidebarNavigation;
using fifi.ViewModel;
using fifi.Model;
using System.ComponentModel;
using fifi.iOS.Helpers;
using System.Linq;
using fifi.Client.DataObjects;
using fifi.Utils.Helpers;
using SeaDroid.iOS.Helpers;

namespace fifi.iOS
{
	public partial class DeviceViewController : UITableViewController
	{
		private DevicesViewModel _viewModel;

		protected SidebarController SidebarController
			=> (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.SidebarController;

	

		public override UIStoryboard Storyboard
			=> (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.Storyboard;

		DeviceDetailsViewController DeviceDetailsView;

	    private UIButton _addMasterButton;

		public DeviceViewController(IntPtr handle) : base(handle)
		{
			NavigationItem.SetLeftBarButtonItem(
	  new UIBarButtonItem(UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain,
		  (sender, args) =>
		  {
			  SidebarController.ToggleMenu();
		  }), true);

			var RefresControl = new UIRefreshControl();
			RefresControl.ValueChanged += refresh;

		}

		public async override void ViewDidLoad()
		{
			base.ViewDidLoad();

            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
			{
				TextAlignment = UITextAlignment.Left,
				Text = "      Masters",
			    Font = UIFont.BoldSystemFontOfSize(19.0f),
                TextColor = UIColor.White
			};
			NavigationItem.TitleView = lblNavTitle;

		    UIApplication app = UIApplication.SharedApplication;
		    var statusBarHeight = app.StatusBarFrame.Size.Height;
		    UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
		    statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
		    this.NavigationController.NavigationBar.AddSubview(statusbar);

            ImageResizer resizer = new ImageResizer();
		    var _addMasterButton = new UIBarButtonItem((UIImage.FromBundle("plus_icon"))
		        , UIBarButtonItemStyle.Plain
		        , (sender, args) =>
		        {
		            MasterPairingViewController MasterPairing = this.Storyboard.InstantiateViewController("MasterPairingViewController") as MasterPairingViewController;
		            MasterPairing.StartedByMastersController = true;
		            NavigationController.PushViewController(MasterPairing, true);
                });
            NavigationItem.SetRightBarButtonItem(_addMasterButton
                , true);
		    var ImageSize = _addMasterButton.Image;

		    _addMasterButton.Image = resizer.ResizeImage(UIImage.FromBundle("plus_icon"),
		        (float)ImageSize.Size.Height / (float)2.5, (float)ImageSize.Size.Width / (float)2.5);


            _viewModel = new DevicesViewModel();
			_viewModel.PropertyChanged += ViewModel_PropertyChanged;
			DeviceTable.Source = new DeviceViewSource(_viewModel, this);
			if (_viewModel.Masters.Count == 0)
			{
				Loading.StartAnimating();
				await _viewModel.ExecuteLoadDevicesCommandAsync();
				Loading.StopAnimating();
				Loading.Hidden = true;
			}

			this.RefreshControl = new UIRefreshControl();
			this.RefreshControl.ValueChanged += refresh;
			DeviceTable.Add(RefreshControl);

		    NSNotificationCenter.DefaultCenter.AddObserver(new Foundation.NSString("RefreshMasters"), NotficationRefresh);
		}

		private async void refresh(object sender, EventArgs e)
		{

			_viewModel.LoadDevicesCommand.Execute(null);

		}

	    private async void NotficationRefresh(NSNotification notificaiton)
	    {
	         _viewModel.LoadDevicesCommand.Execute(null);
	    }

		private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			DeviceTable.ReloadData();
		}


	public async void showDeviceDetails(Master master)
		{
			DeviceDetailsView = this.Storyboard.InstantiateViewController("deviceDetailsViewController") as DeviceDetailsViewController;
			DeviceDetailsViewModel DetailsViewModel = new DeviceDetailsViewModel();
			DetailsViewModel.Master = master;
			DeviceDetailsView._viewmodel = DetailsViewModel;
			NavigationController.PushViewController(DeviceDetailsView, true);
		}

	}



	internal class DeviceViewSource : UITableViewSource
	{
		DevicesViewModel _viewmodel;
		string CellIdentifier = "DeviceCell";
		DeviceViewController _controller;
		nint spacing = 5; 
		public DeviceViewSource(DevicesViewModel viewmodel, DeviceViewController controller)
		{
			_viewmodel = viewmodel;
			_controller = controller;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			//return _viewmodel.Masters.Count;
			return 1;
		}

		public override nint NumberOfSections(UITableView tableView)
		{
			return _viewmodel.Masters.Count;
		}

		public override nfloat GetHeightForHeader(UITableView tableView, nint section)
		{
			return spacing;
		}

		public override nfloat GetHeightForFooter(UITableView tableView, nint section)
		{
			return spacing;
		}


		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			
			DeviceCell cell = tableView.DequeueReusableCell(CellIdentifier) as DeviceCell;
			var item = _viewmodel.Masters[indexPath.Section];


			//---- if there are no cells to reuse, create a new one
		    if (cell == null)
		    {
		        cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier) as DeviceCell;
		    }

			cell.DeviceName = item.Name;
			cell.TripNumber = item.Trips.Count.ToString();
			cell.Date = item.Trips.LastOrDefault()?.UpdatedAt;
			return cell;
		}

		public async override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			var master = _viewmodel.Masters[indexPath.Section];
			_controller.showDeviceDetails(master);
			                  
		}

	}

    
}