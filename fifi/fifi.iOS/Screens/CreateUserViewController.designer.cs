// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("CreateUserViewController")]
    partial class CreateUserViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField ConfirmPassword { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView CreateUserScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextConfirmEmail { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextEmail { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextFirstname { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextLastname { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextPassword { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextPhoneNumber { get; set; }

        [Action ("TextEmailDidBeginEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextEmailDidBeginEditing (UIKit.UITextField sender);

        [Action ("TextEmailDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextEmailDidChange (UIKit.UITextField sender);

        [Action ("TextEmailEditingDidEnd:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextEmailEditingDidEnd (UIKit.UITextField sender);

        [Action ("TextfieldFirstNameDidBeginEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextfieldFirstNameDidBeginEditing (UIKit.UITextField sender);

        [Action ("TextfieldFirstNameDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextfieldFirstNameDidChange (UIKit.UITextField sender);

        [Action ("TextfukedFirstNameDidEndEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextfukedFirstNameDidEndEditing (UIKit.UITextField sender);

        [Action ("TextLastNameChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextLastNameChanged (UIKit.UITextField sender);

        [Action ("TextLastNameDidBeginEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextLastNameDidBeginEditing (UIKit.UITextField sender);

        [Action ("TextLastnameDidEndEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextLastnameDidEndEditing (UIKit.UITextField sender);

        [Action ("TextPasswordDidBeginEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextPasswordDidBeginEditing (UIKit.UITextField sender);

        [Action ("TextPasswordDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextPasswordDidChange (UIKit.UITextField sender);

        [Action ("TextPasswordDidEndEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextPasswordDidEndEditing (UIKit.UITextField sender);

        [Action ("TextPhonenumberdDidEndEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextPhonenumberdDidEndEdit (UIKit.UITextField sender);

        [Action ("TextPhonenumberDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextPhonenumberDidBeginEdit (UIKit.UITextField sender);

        [Action ("TextPhonenumberDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextPhonenumberDidChange (UIKit.UITextField sender);

        [Action ("TextReconfirmEmailDidBeginEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextReconfirmEmailDidBeginEditing (UIKit.UITextField sender);

        [Action ("TextReconfirmEmailDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextReconfirmEmailDidChange (UIKit.UITextField sender);

        [Action ("TextReconfirmEmailEditingDidEnd:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextReconfirmEmailEditingDidEnd (UIKit.UITextField sender);

        [Action ("TextReconfirmPasswordDidBeginEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextReconfirmPasswordDidBeginEditing (UIKit.UITextField sender);

        [Action ("TextReconfirmPasswordDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextReconfirmPasswordDidChange (UIKit.UITextField sender);

        [Action ("TextReconfirmPasswordDidEndEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextReconfirmPasswordDidEndEditing (UIKit.UITextField sender);

        void ReleaseDesignerOutlets ()
        {
            if (ConfirmPassword != null) {
                ConfirmPassword.Dispose ();
                ConfirmPassword = null;
            }

            if (CreateUserScrollView != null) {
                CreateUserScrollView.Dispose ();
                CreateUserScrollView = null;
            }

            if (TextConfirmEmail != null) {
                TextConfirmEmail.Dispose ();
                TextConfirmEmail = null;
            }

            if (TextEmail != null) {
                TextEmail.Dispose ();
                TextEmail = null;
            }

            if (TextFirstname != null) {
                TextFirstname.Dispose ();
                TextFirstname = null;
            }

            if (TextLastname != null) {
                TextLastname.Dispose ();
                TextLastname = null;
            }

            if (TextPassword != null) {
                TextPassword.Dispose ();
                TextPassword = null;
            }

            if (TextPhoneNumber != null) {
                TextPhoneNumber.Dispose ();
                TextPhoneNumber = null;
            }
        }
    }
}