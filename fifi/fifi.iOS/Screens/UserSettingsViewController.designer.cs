// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("UserSettingsViewController")]
    partial class UserSettingsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Save_Button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView ScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextFirstname { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextLastname { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView User_Imange { get; set; }

        [Action ("SaveButtonTouchDown:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void SaveButtonTouchDown (UIKit.UIButton sender);

        [Action ("SaveButtonTouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void SaveButtonTouchUpInside (UIKit.UIButton sender);

        [Action ("SaveButtonTouchUpOutside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void SaveButtonTouchUpOutside (UIKit.UIButton sender);

        [Action ("TexLastNameDidEndEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TexLastNameDidEndEdit (UIKit.UITextField sender);

        [Action ("TextFirstnameDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFirstnameDidBeginEdit (UIKit.UITextField sender);

        [Action ("TextFirstnameDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFirstnameDidChange (UIKit.UITextField sender);

        [Action ("TextFirstnameDidEndEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFirstnameDidEndEdit (UIKit.UITextField sender);

        [Action ("TextLastnameDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextLastnameDidBeginEdit (UIKit.UITextField sender);

        [Action ("TextLastNameDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextLastNameDidChange (UIKit.UITextField sender);

        void ReleaseDesignerOutlets ()
        {
            if (Save_Button != null) {
                Save_Button.Dispose ();
                Save_Button = null;
            }

            if (ScrollView != null) {
                ScrollView.Dispose ();
                ScrollView = null;
            }

            if (TextFirstname != null) {
                TextFirstname.Dispose ();
                TextFirstname = null;
            }

            if (TextLastname != null) {
                TextLastname.Dispose ();
                TextLastname = null;
            }

            if (User_Imange != null) {
                User_Imange.Dispose ();
                User_Imange = null;
            }
        }
    }
}