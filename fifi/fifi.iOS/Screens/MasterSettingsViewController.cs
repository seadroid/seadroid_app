using Foundation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using CoreGraphics;
using fifi.iOS;
using fifi.iOS.Helpers;
using UIKit;
using fifi.ViewModel;
using SeaDroid.iOS.Helpers;
using Wapps.TOCrop;

namespace SeaDroid.iOS
{
    public partial class MasterSettingsViewController : UIViewController
    {

        #region Parameters

        public MasterSettingsViewModel _viewModel;

        private UIImagePickerController _imagePicker;
        private bool ChangePending;
        private bool ImageChanged;
        private ToastIos _Toast;

        #region buttons
        private UIButton _nameOkButton;
        private UIButton _nameErrorButton;

        private UIButton _registryNumberOkButton;
        private UIButton _registryErrorButton;

        private UIButton _harborOkButton;
        private UIButton _harborErrorButton;

        private UIButton _countryOkButton;
        private UIButton _countryErrorButton;

        private UIButton _colorOkButton;
        private UIButton _colorErrorButton;

        

        #endregion

        #region Keyboard
        private NSObject _willShowNotificationObserver;
        private NSObject _willHideNotificationObserver;
        private bool _Keyboardisshown;
        private nfloat _currentKeyboardFrame;
        #endregion
        #endregion

        public MasterSettingsViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            if (_viewModel == null)
            {
                _viewModel = new MasterSettingsViewModel();

            }
            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;

            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "      Master Settings",
                Font = UIFont.BoldSystemFontOfSize(19.0f),
                TextColor = UIColor.White,


            };
            NavigationItem.TitleView = lblNavTitle;
            

            SaveButton.TouchUpInside += SaveButtonOnTouchUpInside;
            BoatImage.AddGestureRecognizer(new UITapGestureRecognizer(OnImageTap));
            BoatImage.UserInteractionEnabled = true;

            _Toast = new ToastIos();
            _imagePicker = new UIImagePickerController();


            #region button init
            _nameOkButton =  new UIButton(UIButtonType.Custom)  { BackgroundColor = "8ADB74".ToUiColor(),  UserInteractionEnabled = false,};
            _nameOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _nameErrorButton = new UIButton(UIButtonType.Custom)  { BackgroundColor = "F93334".ToUiColor(),UserInteractionEnabled = false  };
            _nameErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _registryNumberOkButton = new UIButton(UIButtonType.Custom) { BackgroundColor = "8ADB74".ToUiColor(), UserInteractionEnabled = false, };
            _registryNumberOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _registryErrorButton = new UIButton(UIButtonType.Custom) { BackgroundColor = "F93334".ToUiColor(), UserInteractionEnabled = false };
            _registryErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _harborOkButton = new UIButton(UIButtonType.Custom) { BackgroundColor = "8ADB74".ToUiColor(), UserInteractionEnabled = false, };
            _harborOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _harborErrorButton = new UIButton(UIButtonType.Custom) { BackgroundColor = "F93334".ToUiColor(), UserInteractionEnabled = false };
            _harborErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _countryOkButton= new UIButton(UIButtonType.Custom) { BackgroundColor = "8ADB74".ToUiColor(), UserInteractionEnabled = false, };
            _countryOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _countryErrorButton = new UIButton(UIButtonType.Custom) { BackgroundColor = "F93334".ToUiColor(), UserInteractionEnabled = false };
            _countryErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);


            _colorOkButton= new UIButton(UIButtonType.Custom) { BackgroundColor = "8ADB74".ToUiColor(), UserInteractionEnabled = false, };
            _colorOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _colorErrorButton= new UIButton(UIButtonType.Custom) { BackgroundColor = "F93334".ToUiColor(), UserInteractionEnabled = false };
            _colorErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);
            #endregion

            #region TextFieldInit
            InitalizeTextField(BoatnameTextField, _nameErrorButton, _nameOkButton);
            InitalizeTextField(RegistryNumberTextField, _registryErrorButton, _registryNumberOkButton);
            InitalizeTextField(HarborTextField, _harborErrorButton, _harborOkButton);
            InitalizeTextField(CountryTextField, _countryErrorButton, _countryOkButton);
            InitalizeTextField(ColorTextField, _colorErrorButton, _colorOkButton);

            #region PickerInit
            var LanguagePickerModel =
                new ListPickerModel(new List<string>
                {
                    "Denmark",
                    "Sweden",
                    "Norway",
                    "Finland",
                    "Germany",
                    "United Kingdom"
                });
            LanguagePickerModel.PropertyChanged += (sender, args) =>
            {
                CountryTextField.Text = "";
                CountryTextField.InsertText(LanguagePickerModel.selectedItem);
                _viewModel._boat.country_id = LanguagePickerModel.SelectedItemRow + 1;
            };
            var LanguagePicker = new UIPickerView() { Model = LanguagePickerModel };
            CountryTextField.InputView = LanguagePicker;
            #endregion


            #endregion

            
            BoatImage.Layer.CornerRadius = BoatImage.Frame.Size.Height / 2;
            BoatImage.Layer.MasksToBounds = true;

            _viewModel.GetBoatCommand.Execute(null);

        }

    

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            _willShowNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver( UIKeyboard.WillShowNotification, KeyBoardwillShow);
            _willHideNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, KeyBoardWillHide);
        }

        public override void ViewDidUnload()
        {
            base.ViewDidUnload();
            NSNotificationCenter.DefaultCenter.RemoveObserver(_willHideNotificationObserver);
            NSNotificationCenter.DefaultCenter.RemoveObserver(_willHideNotificationObserver);
        }

        private async void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (_viewModel.Updated)
            {
                _viewModel.Updated = false;
                this.NavigationController.PopViewController(true);
                return;
            }

            if (_viewModel._boat.picture != null)
            {
                BoatImage.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(new ImageUrlDownloader().GetImageBitmapFromUrl(_viewModel._boat.picture))));
                BoatnameTextField.Text = _viewModel._boat.name;
                RegistryNumberTextField.Text = _viewModel._boat.registry_no;
                HarborTextField.Text = _viewModel._boat.harbor;
                ColorTextField.Text = _viewModel._boat.color;

                switch (_viewModel._boat.country_id)
                {
                    case 1:
                        CountryTextField.Text = "Denmark";
                        break;
                    case 2:
                        CountryTextField.Text = "Sweden";
                        break;
                    case 3:
                        CountryTextField.Text = "Norway";
                        break;
                    case 4:
                        CountryTextField.Text = "Finland";
                        break;
                    case 5:
                        CountryTextField.Text = "Germany";
                        break;
                    case 6:
                        CountryTextField.Text = "United Kingdom";
                        break;
                }
            }
        }

        #region Functions

        private void OnImageTap()
        {
            UIAlertController actionSheetAlert = UIAlertController.Create("Change Profile Image", "", UIAlertControllerStyle.ActionSheet);
            actionSheetAlert.AddAction(UIAlertAction.Create("Camara", UIAlertActionStyle.Default, (action) =>
            {
                try
                {
                    _imagePicker.Delegate = new CameraDelegate(this);
                    _imagePicker.SourceType = UIImagePickerControllerSourceType.Camera;
                    NavigationController.PresentViewController(_imagePicker, true, null);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }

            }));

            actionSheetAlert.AddAction(UIAlertAction.Create("Gallery", UIAlertActionStyle.Default, (action) =>
            {
                try
                {
                    _imagePicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                    _imagePicker.MediaTypes = UIImagePickerController.AvailableMediaTypes((UIImagePickerControllerSourceType.PhotoLibrary));
                    _imagePicker.FinishedPickingMedia += ImagePickerOnFinishedPickingMedia;
                    _imagePicker.Canceled += ImagePickerOnCanceled;
                    NavigationController.PresentModalViewController(_imagePicker, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }

            }));

            actionSheetAlert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel,
                (action) => Console.WriteLine("Cancel button pressed.")));
            this.PresentViewController(actionSheetAlert, true, null);
        }


        private void SaveButtonOnTouchUpInside(object o, EventArgs eventArgs)
        {
            _viewModel._boat.color = ColorTextField.Text;
            _viewModel._boat.harbor = HarborTextField.Text;
            _viewModel._boat.name = BoatnameTextField.Text;
            _viewModel._boat.registry_no = RegistryNumberTextField.Text;
            _viewModel.UpdateBoatCommand.Execute(null);
            _viewModel.UpdateBoatImageCommand.Execute(null);

        }

        private void InitalizeTextField(UITextField textfield, UIButton ErrorButton, UIButton OkButton)
        {
           
                       
            textfield.Layer.BorderWidth = 1f;
            textfield.Layer.BorderColor = UIColor.White.CGColor;

            OkButton.Frame = new CGRect(0.0f, 0.0f, textfield.Frame.Size.Height, textfield.Frame.Size.Height);
            OkButton.ContentMode = UIViewContentMode.Center;
            textfield.RightView = OkButton;
            textfield.RightViewMode = UITextFieldViewMode.Always;

            textfield.EditingDidBegin += (sender, args) =>
            {
                textfield.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            };

            textfield.EditingChanged += (sender, args) =>
            {
                ChangePending = false;
                if (!ValidateText(textfield, ErrorButton, OkButton)) return;
                if (!ValidateText(BoatnameTextField, _nameErrorButton, _nameOkButton)) return;
                if (!ValidateText(RegistryNumberTextField, _registryErrorButton, _registryNumberOkButton)) return;
                if (!ValidateText(HarborTextField, _harborErrorButton, _harborOkButton)) return;
                if (!ValidateText(CountryTextField, _countryOkButton, _countryOkButton)) return;
                if (!ValidateText(ColorTextField, _colorErrorButton, _colorOkButton)) return;
                ChangePending = true;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.UserInteractionEnabled = true;
               
            };

            textfield.EditingDidEndOnExit += (sender, args) =>
            {
                textfield.BackgroundColor  = UIColor.Clear;
            };
        }


        

        #region Validation

       


        private bool ValidateText(UITextField text, UIButton ErrorButton, UIButton OkButton)
        {
            if (!string.IsNullOrEmpty(text.Text))
            {
                if (text.RightView != OkButton)
                {
                    OkButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    OkButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = OkButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                return true;
            }
            else
            {
                if (text.RightView != ErrorButton)
                {
                    ErrorButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    ErrorButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = ErrorButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                SaveButton.UserInteractionEnabled = false;
                SaveButton.BackgroundColor = UIColor.FromRGB(165, 55, 23);
                return false;
            }
        }


        #endregion
        

        #region keyboard

        private void KeyBoardwillShow(NSNotification notification)
        {
            if (!_Keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height - keyboardHeight;
                this.View.Frame = NewFrame;
                _currentKeyboardFrame = keyboardHeight;
                _Keyboardisshown = true;
            }
            else
            {
                var HeightDifference = UIKeyboard.BoundsFromNotification(notification).Height - _currentKeyboardFrame;
                _currentKeyboardFrame = UIKeyboard.BoundsFromNotification(notification).Height;
                var newFrame = this.View.Frame;
                newFrame.Height = newFrame.Height - HeightDifference;
                this.View.Frame = newFrame;
                _Keyboardisshown = true;
            }
        }


        private void KeyBoardWillHide(NSNotification notification)
        {
            if (_Keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, System.IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height + keyboardHeight;
                this.View.Frame = NewFrame;
                _Keyboardisshown = false;
            }
        }

        #endregion

        #endregion

        #region Imagehandling

        private void ImagePickerOnCanceled(object o, EventArgs eventArgs)
        {
            _imagePicker.DismissViewController(true, null);
        }

        private void ImagePickerOnFinishedPickingMedia(object o, UIImagePickerMediaPickedEventArgs e)
        {


            bool isImage = false;
            switch (e.Info[UIImagePickerController.MediaType].ToString())
            {
                case "public.image":
                    Console.WriteLine("Image selected");



                    isImage = true;
                    break;
                case "public.video":
                    var alert = new UIAlertView("Error", "Can not use video as profile picture", null, NSBundle.MainBundle.LocalizedString("OK", "OK"));
                    alert.Show();
                    break;
            }


            _imagePicker.DismissModalViewController(true);

            var CropVC = new TOCropViewController(TOCropViewCroppingStyle.Default, e.OriginalImage);
            CropVC.Delegate = new CropVCDelegate(this);
            CropVC.AspectRatioPreset = TOCropViewControllerAspectRatioPreset.Square;
            CropVC.AspectRatioLockEnabled = true;

            NavigationController.PresentViewController(CropVC, true, null);
        }

        class CropVCDelegate : TOCropViewControllerDelegate
        {
            private MasterSettingsViewController ViewController;
            public CropVCDelegate(MasterSettingsViewController viewController)
            {
                ViewController = viewController;
            }


            public override void DidCropImageToRect(TOCropViewController cropViewController, CGRect cropRect, nint angle)
            {
                cropViewController.PresentingViewController.DismissViewController(true, null);
                var myImage = cropViewController.FinalImage;
                myImage = new ImageCropper().ResizeImage(myImage, 500, 500);
                NSData imageData = myImage.AsPNG();
                string Base64image = imageData.GetBase64EncodedString(NSDataBase64EncodingOptions.None);
                var imagesize = System.Text.ASCIIEncoding.ASCII.GetByteCount(Base64image) / 1024;
                var size = 500;
                while (imagesize > 1024)
                {
                    myImage = new ImageCropper().ResizeImage(myImage, size, size);
                    imageData = myImage.AsPNG();
                    Base64image = imageData.GetBase64EncodedString(NSDataBase64EncodingOptions.None);
                    imagesize = System.Text.ASCIIEncoding.ASCII.GetByteCount(Base64image) / 1024;
                    size = size - 50;
                }
                ViewController.BoatImage.Image = myImage;
                ViewController._viewModel._base64Image = Base64image;
                ViewController.ChangePending = true;

            }
        }
        class CameraDelegate : UIImagePickerControllerDelegate
        {
            private MasterSettingsViewController Controller;

            public CameraDelegate(MasterSettingsViewController controller)
            {
                Controller = controller;
            }

            public override void FinishedPickingMedia(UIImagePickerController picker, NSDictionary info)
            {
                picker.DismissModalViewController(true);
                var image = info.ValueForKey(new NSString("UIImagePickerControllerOriginalImage")) as UIImage;

                var CropVC = new TOCropViewController(TOCropViewCroppingStyle.Default, image);
                CropVC.Delegate = new MasterSettingsViewController.CropVCDelegate(Controller);
                CropVC.AspectRatioPreset = TOCropViewControllerAspectRatioPreset.Square;
                CropVC.AspectRatioLockEnabled = true;
                Controller.NavigationController.PresentViewController(CropVC, true, null);

            }
        }

        #endregion
    }
}