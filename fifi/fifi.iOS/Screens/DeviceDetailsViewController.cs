using Foundation;
using System;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using CoreGraphics;
using CoreImage;
using CoreLocation;
using fifi.Client.DataObjects;
using fifi.Helpers;
using fifi.iOS;
using UIKit;
using fifi.ViewModel;
using MapKit;
using fifi.iOS.CustomControls;
using fifi.iOS.Helpers;
using SeaDroid.iOS;
using SeaDroid.iOS.Helpers;

//using Mapbox;

namespace fifi.iOS
{
    public partial class DeviceDetailsViewController : UITableViewController
    {
		public DeviceDetailsViewModel _viewmodel;
		private DeviceTripViewController DeviceTripView;
        public DeviceDetailsViewController (IntPtr handle) : base (handle){ }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "      Trips",
                Font = UIFont.BoldSystemFontOfSize(19.0f),
                TextColor = UIColor.White,
                
                
            };

            var _masterSettingsButton = new UIBarButtonItem((UIImage.FromBundle("Settings"))
                , UIBarButtonItemStyle.Plain
                , (sender, args) =>
                {
                    MasterSettingsViewController MasterSettings = this.Storyboard.InstantiateViewController("MasterSettingsViewController") as MasterSettingsViewController;
                    MasterSettings._viewModel = new MasterSettingsViewModel(){ _master = _viewmodel.Master};
                    NavigationController.PushViewController(MasterSettings, true);
                });
            NavigationItem.SetRightBarButtonItem(_masterSettingsButton
                , true);
            ImageResizer resizer = new ImageResizer();
            _masterSettingsButton.Image = resizer.ResizeImage(UIImage.FromBundle("Settings"),
                (float)_masterSettingsButton.Image.Size.Height / (float)5, (float)_masterSettingsButton.Image.Size.Width / (float)5);

            NavigationItem.TitleView = lblNavTitle;

            if (_viewmodel == null)
            {
                _viewmodel = new DeviceDetailsViewModel();
            }

            _viewmodel.PropertyChanged += ViewmodelOnPropertyChanged;

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);


            Loading.Hidden = true;


            TripsTable.Source = new DeviceDetialsViewSource(_viewmodel, this);


        }

        private void ViewmodelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            DeviceTripView = this.Storyboard.InstantiateViewController("DeviceTripViewController") as DeviceTripViewController;
            PastTripDetailsViewModel TripViewModel = new PastTripDetailsViewModel();
            TripViewModel.Trip = _viewmodel.SelectedTrip;
            DeviceTripView._viewmodel = TripViewModel;
            NavigationController.PushViewController(DeviceTripView, true);
        }

      
    }



   
    internal class DeviceDetialsViewSource : UITableViewSource 
    {
        DeviceDetailsViewModel _viewmodel;
        string CellIdentifier = "DeviceDetailsCell";
		private nint spacing = 5;
        DeviceDetailsViewController _controller;

        public DeviceDetialsViewSource(DeviceDetailsViewModel viewmodel, DeviceDetailsViewController controller)
        {
            _viewmodel = viewmodel;
            _controller = controller;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
			return 1;
        }

		public override nint NumberOfSections(UITableView tableView)
		{
			return _viewmodel.Master.Trips.Count;
		}

		public override nfloat GetHeightForFooter(UITableView tableView, nint section)
		{
			return spacing;
		}

        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            return spacing;
        }


        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            DeviceDetialsCell cell = tableView.DequeueReusableCell(CellIdentifier) as DeviceDetialsCell;
			var item = _viewmodel.Master.Trips[indexPath.Section];

            //---- if there are no cells to reuse, create a new one
            if (cell == null)
            {
                cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier) as DeviceDetialsCell;
            }

            cell.Number = indexPath.Section.ToString();
            cell.Start = item.StartedAt;

            var tripTime = Convert.ToDateTime(item.EndedAt) - Convert.ToDateTime(item.StartedAt);
            cell.End = string.IsNullOrWhiteSpace(item.EndedAt) ? "Not Ended" : Convert.ToString(tripTime);

			//var mapView = new CustomMap(cell.MapUiView.Bounds)
   //         {
   //             CenterCoordinate = new CLLocationCoordinate2D(56.2639, 9.5018),
   //         };
			//mapView.ZoomEnabled = true;		
   //         cell.MapUiView.InsertSubview(mapView, 0);
   //         UpdateMapContents(mapView, item);




            return cell;
        }




		public async override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			//var Trip = _viewmodel.Master.Trips[indexPath.Row];
			_viewmodel.Trip_id = _viewmodel.Master.Trips[indexPath.Section].Id.ToString();
            _viewmodel.LoadTripCommand.Execute(null);

        }

  //      private void UpdateMapContents(MKMapView map, Trip trip)
  //      {

  //          if (trip.Coordinates.Count == 0 || trip.Coordinates.Count < 0)
  //          {
  //              return;
  //          }

  //          if (trip.Coordinates.Count <= 0)
  //          {
  //              return;
  //          }

  //          trip.Coordinates.HaversineDistance();
  //          var start =
  //              trip.Coordinates.LastOrDefault(
  //                  c =>
  //                      c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
  //                      c.Longitude.Where(char.IsDigit).Any(x => x != '0'));
  //          var end =
  //              trip.Coordinates.FirstOrDefault(
  //                  c =>
  //                      c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
  //                      c.Longitude.Where(char.IsDigit).Any(x => x != '0'));

  //          if (start == null || end == null)
  //          {
  //              return;
  //          }

  //          var OrderedById = trip.Coordinates.OrderBy(x => x.Id);

  //          var startLatitude = double.Parse(start.Latitude);
  //          var startLongitude = double.Parse(start.Longitude);
  //          var endLatitude = double.Parse(end.Latitude);
  //          var endLongitude = double.Parse(end.Longitude);

		//	//var startpin = new StartAnnotation("startmarker", new CLLocationCoordinate2D(startLatitude, startLongitude));
		////	var endpin = new EndAnnotation("endmarker", new CLLocationCoordinate2D(endLatitude, endLongitude));
		//	var testpin = new MKPointAnnotation();
		//	//testpin.Coordinate = new CLLocationCoordinate2D(startLatitude, startLongitude);
		//	//map.AddAnnotation(testpin);
		//	map.AddAnnotations(new StartAnnotation("startMarker", new CLLocationCoordinate2D(startLatitude, startLongitude)));
		//	map.AddAnnotation(new EndAnnotation("endmarker", new CLLocationCoordinate2D(endLatitude, endLongitude)));

  //          if (!OrderedById.Any())
  //          {
  //              return;
  //          }



  //          var points = OrderedById.Where(
  //                  c =>
  //                      c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
  //                      c.Longitude.Where(char.IsDigit).Any(x => x != '0'))
  //              .Select(
  //                  s =>
  //                      new CLLocationCoordinate2D(double.Parse(s.Latitude),
  //                          double.Parse(s.Longitude))).ToArray();
  //         var Polyline = MKPolyline.FromCoordinates(points);       

  //         map.AddOverlay(Polyline);
         
          


  //          MKMapRect zoomRect = MKMapRect.Null;
  //          //foreach (var point in points)
  //          //{
  //          //    MKMapPoint mappoint = MKMapPoint.FromCoordinate(point);
  //          //    MKMapRect PointRect = new MKMapRect(mappoint.X, mappoint.Y, 0.1, 0.1);     
  //          //    zoomRect = MKMapRect.Union(zoomRect, PointRect);           
  //          //}
                 
  //         map.SetVisibleMapRect(Polyline.BoundingMapRect, true);
            







  //      }
    }

}
