using Foundation;
using System;
using System.Collections.Generic;
using Acr.UserDialogs;
using CoreGraphics;
using fifi.Client.DataObjects;
using fifi.iOS.Helpers;
using fifi.Model;
using UIKit;
using fifi.ViewModel;
using SidebarNavigation;

namespace fifi.iOS
{
    public partial class ArmbandDetailsController : UIViewController
    {

     
        #region keyboard

        private NSObject _willShowNotificationObserver;
        private NSObject _willHideNotificationObserver;
        private bool _Keyboardisshown;
        private nfloat _currentKeyboardFrame;

        #endregion

        #region  Properties

        public PairingDetailsViewModel _viewModel;
        private int SelectedLanguage;
        private int SelectedSwimExp;
        private int SelectedSeaExp;

        #endregion

        #region Buttons

        private UIButton _FirstnameOkButton;
        private UIButton _FirstnameErrorButton;
        private UIButton _LastnameOkButton;
        private UIButton _LastnameErrorButton;
        private UIButton _GenderOkButton;
        private UIButton _GenderErrorButton;
        private UIButton _BirthdayOkButton;
        private UIButton _BirthdayErrorButton;
        private UIButton _HeightOkButton;
        private UIButton _HeightErrorButton;
        private UIButton _WeightOkButton;
        private UIButton _WeightErrorButton;
        private UIButton _LanguageOkButton;
        private UIButton _LangaugeErrorButton;
        private UIButton _HaircolorErrorButton;
        private UIButton _HaircolorOkButton;
        private UIButton _SwimExpOkButton;
        private UIButton _SwinExpErrorButton;
        private UIButton _SeaExpOkButton;
        private UIButton _SeaExpErrorButton;

        #endregion

        public ArmbandDetailsController(IntPtr handle) : base(handle)
        {
        }

        public async override void ViewDidLoad()
        {
            NavigationController.InteractivePopGestureRecognizer.Enabled = false;

            base.ViewDidLoad();

            var title = _viewModel.BluetoothDeviceId.Split(' ');

            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
               
                Text = "Armband ID: " + title[1],
                TextColor = UIColor.White
            };
            NavigationItem.TitleView = lblNavTitle;

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);

            NavigationController.BarHideOnSwipeGestureRecognizer.Enabled = false;
     

            Scrollview.AddGestureRecognizer(new UIScreenEdgePanGestureRecognizer(() =>
            {
                TextFirstname.ResignFirstResponder();
                TextLastname.ResignFirstResponder();
                TextBirthday.ResignFirstResponder();
                TextGender.ResignFirstResponder();
                TextHaircolor.ResignFirstResponder();
                TextHeight.ResignFirstResponder();
                TextWeight.ResignFirstResponder();
                TextLanguage.ResignFirstResponder();
                TextSeaExp.ResignFirstResponder();
                TextSwimExp.ResignFirstResponder();
            }));

            Scrollview.AddGestureRecognizer(new UITapGestureRecognizer(() =>
            {
                TextFirstname.ResignFirstResponder();
                TextLastname.ResignFirstResponder();
                TextBirthday.ResignFirstResponder();
                TextGender.ResignFirstResponder();
                TextHaircolor.ResignFirstResponder();
                TextHeight.ResignFirstResponder();
                TextWeight.ResignFirstResponder();
                TextLanguage.ResignFirstResponder();
                TextSeaExp.ResignFirstResponder();
                TextSwimExp.ResignFirstResponder();
            }));

            #region TextFieldInfoInit
            
            if (!string.IsNullOrEmpty(_viewModel.Wristband.firstname))
            {
                try
                {
                    TextFirstname.Text = _viewModel.Wristband.firstname;
                    TextLastname.Text = _viewModel.Wristband.lastname;
                    TextBirthday.Text = _viewModel.Wristband.birthday;
                    TextHeight.Text = Convert.ToString(_viewModel.Wristband.height);
                    TextWeight.Text = Convert.ToString(_viewModel.Wristband.weight);
                    TextHaircolor.Text = _viewModel.Wristband.haircolor;
                    TextBirthday.Text = _viewModel.Wristband.birthday;
                    TextGender.Text = _viewModel.Wristband.gender;
                    SelectedLanguage = (int)_viewModel.Wristband.country_id;
                    SelectedSeaExp = (int)_viewModel.Wristband.sea_exp;
                    SelectedSwimExp = (int)_viewModel.Wristband.swim_exp;
                    switch (_viewModel.Wristband.country_id)
                    {
                        case 1:
                            TextLanguage.Text = "Danish";
                            break;
                        case 2:
                            TextLanguage.Text = "Swedish";
                            break;
                        case 3:
                            TextLanguage.Text = "Norwegian";
                            break;
                        case 4:
                            TextLanguage.Text = "Finnish";
                            break;
                        case 5:
                            TextLanguage.Text = "German";
                            break;
                        case 6:
                            TextLanguage.Text = "English";
                            break;
                    }

                    switch (_viewModel.Wristband.sea_exp)
                    {
                        case 0:
                            TextSeaExp.Text = "No Experience";
                            break;
                        case 1:
                            TextSeaExp.Text = "Some experience";
                            break;
                        case 2:
                            TextSeaExp.Text = "Lots of experience";
                            break;
                        case 3:
                            TextSeaExp.Text = "Professional";
                            break;
                    }
                    switch (_viewModel.Wristband.swim_exp)
                    {
                        case 0:
                            TextSwimExp.Text = "No experience";
                            break;
                        case 1:
                            TextSwimExp.Text = "can swim 200m";
                            break;
                        case 2:
                            TextSwimExp.Text = "can swim +200m";
                            break;
                    }
                }
              
                catch (Exception e)
                {
                    TextFirstname.Text = string.Empty;
                    TextLastname.Text = string.Empty;
                    TextBirthday.Text = string.Empty;
                    TextGender.Text = string.Empty;
                    TextHeight.Text = string.Empty;
                    TextWeight.Text = string.Empty;
                    TextLanguage.Text = string.Empty;
                    SelectedLanguage = 1;
                    TextSeaExp.Text = string.Empty;
                    SelectedSeaExp = 0;
                    TextSwimExp.Text = string.Empty;
                    SelectedSwimExp = 0;
                    SaveButton.UserInteractionEnabled = false;
                    SaveButton.BackgroundColor = UIColor.FromRGB(165, 55, 23);
                    Console.WriteLine(e);
                }          
            }
            else
            {
                TextFirstname.Text = string.Empty;
                TextLastname.Text = string.Empty;
                TextBirthday.Text = string.Empty;
                TextGender.Text = string.Empty;
                TextHeight.Text = string.Empty;
                TextWeight.Text = string.Empty;
                TextLanguage.Text = string.Empty;
                SelectedLanguage = 1;
                TextSeaExp.Text = string.Empty;
                SelectedSeaExp = 0;
                TextSwimExp.Text = string.Empty;
                SelectedSwimExp = 0;
                SaveButton.UserInteractionEnabled = false;
                SaveButton.BackgroundColor = UIColor.FromRGB(165, 55, 23);
            }

            #endregion

            #region InitPickers

            var GenderPickerModel = new ListPickerModel(new List<string> {"Male", "Female"});
            GenderPickerModel.PropertyChanged += (sender, args) =>
            {
                TextGender.Text = "";
                TextGender.InsertText(GenderPickerModel.selectedItem);
            };
            var GenderPicker = new UIPickerView() {Model = GenderPickerModel};
            TextGender.InputView = GenderPicker;


            var LanguagePickerModel =
                new ListPickerModel(new List<string>
                {
                    "Danish",
                    "Swedish",
                    "Norwegian",
                    "Finnish",
                    "German",
                    "English"
                });
            LanguagePickerModel.PropertyChanged += (sender, args) =>
            {
                TextLanguage.Text = "";
                TextLanguage.InsertText(LanguagePickerModel.selectedItem);
                SelectedLanguage = LanguagePickerModel.SelectedItemRow + 1;
            };
            var LanguagePicker = new UIPickerView() {Model = LanguagePickerModel};
            TextLanguage.InputView = LanguagePicker;

            var SwimExpPickerModel =
                new ListPickerModel(new List<string>
                {
                    "No experience",
                    "can swim 200m",
                    "can swim +200m"
                });
            SwimExpPickerModel.PropertyChanged += (sender, args) =>
            {
                TextSwimExp.Text = "";
                TextSwimExp.InsertText(SwimExpPickerModel.selectedItem);
                TextSwimExp.SendActionForControlEvents(UIControlEvent.EditingChanged);
                SelectedSwimExp = SwimExpPickerModel.SelectedItemRow;
            };
            var SwimExpPicker = new UIPickerView() {Model = SwimExpPickerModel};
            TextSwimExp.InputView = SwimExpPicker;

            var SeaExpPickerModel =
                new ListPickerModel(new List<string>
                {
                    "No experience",
                    "Some experience",
                    " lots of experience",
                    "Professional"
                });
            SeaExpPickerModel.PropertyChanged += (sender, args) =>
            {
                TextSeaExp.Text = "";
                TextSeaExp.InsertText(SeaExpPickerModel.selectedItem);
                TextSeaExp.SendActionForControlEvents(UIControlEvent.EditingChanged);
                SelectedSeaExp = SeaExpPickerModel.SelectedItemRow;
            };
            var SeaExpPicker = new UIPickerView() {Model = SeaExpPickerModel};
            TextSeaExp.InputView = SeaExpPicker;

            var Birthdaypicker = new UIDatePicker() {Mode = UIDatePickerMode.Date};
            Birthdaypicker.ValueChanged += (sender, args) =>
            {
                TextBirthday.Text = "";
                TextBirthday.InsertText(Birthdaypicker.Date.ToDateTime().Year + "/" +
                                        Birthdaypicker.Date.ToDateTime().Month + "/" +
                                        Birthdaypicker.Date.ToDateTime().Day);
            };
            TextBirthday.InputView = Birthdaypicker;

            #endregion

            #region InitButtons

            _FirstnameOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false,
                                      
                };
            _FirstnameOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _FirstnameErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _FirstnameErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            TextFirstname.Layer.BorderWidth = 1f;
            TextFirstname.Layer.BorderColor = UIColor.White.CGColor;

            if (!string.IsNullOrEmpty(TextFirstname.Text))
            {
                _FirstnameOkButton.Frame = new CGRect(0.0f, 0.0f, TextFirstname.Frame.Size.Height,
                    TextFirstname.Frame.Size.Height);
                _FirstnameOkButton.ContentMode = UIViewContentMode.Center;
                TextFirstname.RightView = _FirstnameOkButton;
                TextFirstname.RightViewMode = UITextFieldViewMode.Always;
            }
            else
            {
                _FirstnameErrorButton.Frame = new CGRect(0.0f, 0.0f, TextFirstname.Frame.Size.Height,
                    TextFirstname.Frame.Size.Height);
                _FirstnameErrorButton.ContentMode = UIViewContentMode.Center;
                TextFirstname.RightView = _FirstnameErrorButton;
                TextFirstname.RightViewMode = UITextFieldViewMode.Always;
            }


            _LastnameOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _LastnameOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _LastnameErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _LastnameErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            TextLastname.Layer.BorderWidth = 1f;
            TextLastname.Layer.BorderColor = UIColor.White.CGColor;

            if (!string.IsNullOrEmpty(TextFirstname.Text))
            {
                _LastnameOkButton.Frame = new CGRect(0.0f, 0.0f, TextLastname.Frame.Size.Height,
                    TextLastname.Frame.Size.Height);
                _LastnameOkButton.ContentMode = UIViewContentMode.Center;
                TextLastname.RightView = _LastnameOkButton;
                TextLastname.RightViewMode = UITextFieldViewMode.Always;
            }
            else
            {
                _LastnameErrorButton.Frame = new CGRect(0.0f, 0.0f, TextLastname.Frame.Size.Height,
                    TextLastname.Frame.Size.Height);
                _LastnameErrorButton.ContentMode = UIViewContentMode.Center;
                TextLastname.RightView = _LastnameErrorButton;
                TextLastname.RightViewMode = UITextFieldViewMode.Always;
            }


            _GenderOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _GenderOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _GenderErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _GenderErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            TextGender.Layer.BorderWidth = 1f;
            TextGender.Layer.BorderColor = UIColor.White.CGColor;

            if (!string.IsNullOrEmpty(TextFirstname.Text))
            {
                _GenderOkButton.Frame = new CGRect(0.0f, 0.0f, TextGender.Frame.Size.Height,
                    TextGender.Frame.Size.Height);
                _GenderOkButton.ContentMode = UIViewContentMode.Center;
                TextGender.RightView = _GenderOkButton;
                TextGender.RightViewMode = UITextFieldViewMode.Always;
            }
            else
            {
                _GenderErrorButton.Frame = new CGRect(0.0f, 0.0f, TextGender.Frame.Size.Height,
                    TextGender.Frame.Size.Height);
                _GenderErrorButton.ContentMode = UIViewContentMode.Center;
                TextGender.RightView = _GenderErrorButton;
                TextGender.RightViewMode = UITextFieldViewMode.Always;
            }


            _BirthdayOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _BirthdayOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _BirthdayErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _BirthdayErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);


            TextBirthday.Layer.BorderWidth = 1f;
            TextBirthday.Layer.BorderColor = UIColor.White.CGColor;

            if (!string.IsNullOrEmpty(TextBirthday.Text))
            {
                _BirthdayOkButton.Frame = new CGRect(0.0f, 0.0f, TextBirthday.Frame.Size.Height,
                    TextBirthday.Frame.Size.Height);
                _BirthdayOkButton.ContentMode = UIViewContentMode.Center;
                TextBirthday.RightView = _BirthdayOkButton;
                TextBirthday.RightViewMode = UITextFieldViewMode.Always;
            }
            else
            {
                _BirthdayErrorButton.Frame = new CGRect(0.0f, 0.0f, TextBirthday.Frame.Size.Height,
                    TextBirthday.Frame.Size.Height);
                _BirthdayErrorButton.ContentMode = UIViewContentMode.Center;
                TextBirthday.RightView = _BirthdayErrorButton;
                TextBirthday.RightViewMode = UITextFieldViewMode.Always;
            }


            _HeightOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _HeightOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _HeightErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _HeightErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            TextHeight.Layer.BorderWidth = 1f;
            TextHeight.Layer.BorderColor = UIColor.White.CGColor;

            if (!string.IsNullOrEmpty(TextHeight.Text))
            {
                _HeightOkButton.Frame = new CGRect(0.0f, 0.0f, TextHeight.Frame.Size.Height,
                    TextHeight.Frame.Size.Height);
                _HeightOkButton.ContentMode = UIViewContentMode.Center;
                TextHeight.RightView = _HeightOkButton;
                TextHeight.RightViewMode = UITextFieldViewMode.Always;
            }
            else
            {
                _HeightErrorButton.Frame = new CGRect(0.0f, 0.0f, TextHeight.Frame.Size.Height,
                    TextHeight.Frame.Size.Height);
                _HeightErrorButton.ContentMode = UIViewContentMode.Center;
                TextHeight.RightView = _HeightErrorButton;
                TextHeight.RightViewMode = UITextFieldViewMode.Always;
            }


            _WeightOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _WeightOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _WeightErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _WeightErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            TextWeight.Layer.BorderWidth = 1f;
            TextWeight.Layer.BorderColor = UIColor.White.CGColor;

            if (!string.IsNullOrEmpty(TextWeight.Text))
            {
                _WeightOkButton.Frame = new CGRect(0.0f, 0.0f, TextWeight.Frame.Size.Height,
                    TextWeight.Frame.Size.Height);
                _WeightOkButton.ContentMode = UIViewContentMode.Center;
                TextWeight.RightView = _WeightOkButton;
                TextWeight.RightViewMode = UITextFieldViewMode.Always;
            }
            else
            {
                _WeightErrorButton.Frame = new CGRect(0.0f, 0.0f, TextWeight.Frame.Size.Height,
                    TextWeight.Frame.Size.Height);
                _WeightErrorButton.ContentMode = UIViewContentMode.Center;
                TextWeight.RightView = _WeightErrorButton;
                TextWeight.RightViewMode = UITextFieldViewMode.Always;
            }


            _LanguageOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _LanguageOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _LangaugeErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _LangaugeErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            TextLanguage.Layer.BorderWidth = 1f;
            TextLanguage.Layer.BorderColor = UIColor.White.CGColor;

            if (!string.IsNullOrEmpty(TextLanguage.Text))
            {
                _LanguageOkButton.Frame = new CGRect(0.0f, 0.0f, TextLanguage.Frame.Size.Height,
                    TextLanguage.Frame.Size.Height);
                _LanguageOkButton.ContentMode = UIViewContentMode.Center;
                TextLanguage.RightView = _LanguageOkButton;
                TextLanguage.RightViewMode = UITextFieldViewMode.Always;
            }
            else
            {
                _LangaugeErrorButton.Frame = new CGRect(0.0f, 0.0f, TextLanguage.Frame.Size.Height,
                    TextLanguage.Frame.Size.Height);
                _LangaugeErrorButton.ContentMode = UIViewContentMode.Center;
                TextLanguage.RightView = _LangaugeErrorButton;
                TextLanguage.RightViewMode = UITextFieldViewMode.Always;
            }


            _HaircolorOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _HaircolorOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _HaircolorErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _HaircolorErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            TextHaircolor.Layer.BorderWidth = 1f;
            TextHaircolor.Layer.BorderColor = UIColor.White.CGColor;

            if (!string.IsNullOrEmpty(TextHaircolor.Text))
            {
                _HaircolorOkButton.Frame = new CGRect(0.0f, 0.0f, TextHaircolor.Frame.Size.Height,
                    TextHaircolor.Frame.Size.Height);
                _HaircolorOkButton.ContentMode = UIViewContentMode.Center;
                TextHaircolor.RightView = _HaircolorOkButton;
                TextHaircolor.RightViewMode = UITextFieldViewMode.Always;
            }
            else
            {
                _HaircolorErrorButton.Frame = new CGRect(0.0f, 0.0f, TextHaircolor.Frame.Size.Height,
                    TextHaircolor.Frame.Size.Height);
                _HaircolorErrorButton.ContentMode = UIViewContentMode.Center;
                TextHaircolor.RightView = _HaircolorErrorButton;
                TextHaircolor.RightViewMode = UITextFieldViewMode.Always;
            }

            _SeaExpOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _SeaExpOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _SeaExpErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _SeaExpErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            TextSeaExp.Layer.BorderWidth = 1f;
            TextSeaExp.Layer.BorderColor = UIColor.White.CGColor;

            if (!string.IsNullOrEmpty(TextSeaExp.Text))
            {
                _SeaExpOkButton.Frame = new CGRect(0.0f, 0.0f, TextSeaExp.Frame.Size.Height,
                    TextSeaExp.Frame.Size.Height);
                _SeaExpOkButton.ContentMode = UIViewContentMode.Center;
                TextSeaExp.RightView = _SeaExpOkButton;
                TextSeaExp.RightViewMode = UITextFieldViewMode.Always;
            }
            else
            {
                _SeaExpErrorButton.Frame = new CGRect(0.0f, 0.0f, TextSeaExp.Frame.Size.Height,
                    TextSeaExp.Frame.Size.Height);
                _SeaExpErrorButton.ContentMode = UIViewContentMode.Center;
                TextSeaExp.RightView = _SeaExpErrorButton;
                TextSeaExp.RightViewMode = UITextFieldViewMode.Always;
            }


            _SwimExpOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _SwimExpOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _SwinExpErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _SwinExpErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            TextSwimExp.Layer.BorderWidth = 1f;
            TextSwimExp.Layer.BorderColor = UIColor.White.CGColor;

            if (!string.IsNullOrEmpty(TextSwimExp.Text))
            {
                _SwimExpOkButton.Frame = new CGRect(0.0f, 0.0f, TextSwimExp.Frame.Size.Height,
                    TextSwimExp.Frame.Size.Height);
                _SwimExpOkButton.ContentMode = UIViewContentMode.Center;
                TextSwimExp.RightView = _SwimExpOkButton;
                TextSwimExp.RightViewMode = UITextFieldViewMode.Always;
            }
            else
            {
                _SwinExpErrorButton.Frame = new CGRect(0.0f, 0.0f, TextSwimExp.Frame.Size.Height,
                    TextSwimExp.Frame.Size.Height);
                _SwinExpErrorButton.ContentMode = UIViewContentMode.Center;
                TextSwimExp.RightView = _SwinExpErrorButton;
                TextSwimExp.RightViewMode = UITextFieldViewMode.Always;
            }

            #endregion

            SaveButton.TouchUpInside += SaveButtonOnTouchUpInside;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            _willShowNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(
                UIKeyboard.WillShowNotification, KeyBoardwillShow);
            _willHideNotificationObserver =
                NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, KeyBoardWillHide);
        }

        public override void ViewDidDisappear(bool animated)
        {
           base.ViewDidDisappear(animated);
            
           NSNotificationCenter.DefaultCenter.RemoveObserver(_willShowNotificationObserver);          
           NSNotificationCenter.DefaultCenter.RemoveObserver(_willHideNotificationObserver);
            
        }


        private void SaveButtonOnTouchUpInside(object o, EventArgs eventArgs)
        {
            UIAlertView alert = new UIAlertView("Edit Wristband", "Confirm Eddit", null,
                NSBundle.MainBundle.LocalizedString("Cancel", "Cancel"),
                NSBundle.MainBundle.LocalizedString("OK", "OK"));
            alert.Clicked += (object a, UIButtonEventArgs b) =>
            {
                var answer = (b.ButtonIndex != alert.CancelButtonIndex);


                if (answer == true)
                {
                    _viewModel.Wristband.firstname = TextFirstname.Text;
                    _viewModel.Wristband.lastname = TextLastname.Text;
                    _viewModel.Wristband.birthday = TextBirthday.Text;
                    _viewModel.Wristband.height = Convert.ToInt32(TextHeight.Text);
                    _viewModel.Wristband.weight = Convert.ToInt32(TextWeight.Text);
                    _viewModel.Wristband.gender = TextGender.Text;
                    _viewModel.Wristband.country_id = SelectedLanguage;
                    _viewModel.Wristband.sea_exp = SelectedSeaExp;
                    _viewModel.Wristband.swim_exp = SelectedSwimExp;
                    _viewModel.Wristband.haircolor = TextHaircolor.Text;
                    _viewModel.Wristband.serial = _viewModel.BluetoothDeviceId;
              
                    NSNotificationCenter.DefaultCenter.PostNotificationName("Done", this);
                    this.NavigationController.PopViewController(true);
                }
            };
            alert.Show();
        }

        partial void TextFirstnameEditDidBegin(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
        }

        partial void TextFirstnameChanged(UITextField sender)
        {
            if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
            if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
            if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
            if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
            if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
            if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
            if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
            if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
            if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
            if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
            SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
            SaveButton.UserInteractionEnabled = true;
        }

        partial void TextFirstnameEditEnded(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextLastnameEditDidBegin(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
        }

        partial void TextLastnameChanged(UITextField sender)
        {
            if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
            if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
            if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
            if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
            if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
            if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
            if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
            if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
            if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
            if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
            SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
            SaveButton.UserInteractionEnabled = true;
        }

        partial void TextLastnameEditEnded(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextGenderDidBeginEdit(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            if (string.IsNullOrEmpty(sender.Text))
            {
                sender.Text = "Male";
                if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
                if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
                if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
                if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
                if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
                if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
                if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
                if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
                if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
                if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.UserInteractionEnabled = true;
            }
        }

        partial void TextGenderDidChange(UITextField sender)
        {
            if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
            if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
            if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
            if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
            if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
            if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
            if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
            if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
            if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
            if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
            SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
            SaveButton.UserInteractionEnabled = true;
        }

        partial void TextGenderDidEndEdit(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextBirthdayDidBeginEdit(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
        }

        partial void TextBirthdayChanged(UITextField sender)
        {
            if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
            if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
            if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
            if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
            if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
            if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
            if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
            if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
            if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
            if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
            SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
            SaveButton.UserInteractionEnabled = true;
        }

        partial void TextBirthdayEditDidEnd(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextHeightDidBeginEdit(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
        }

        partial void TextHeightDidChange(UITextField sender)
        {
            if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
            if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
            if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
            if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
            if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
            if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
            if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
            if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
            if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
            if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
            SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
            SaveButton.UserInteractionEnabled = true;
        }

        partial void TextHeightDidEndEdit(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextWeightDidBeginEdit(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
        }


        partial void TextWeightChanged(UITextField sender)
        {
            var SplitText = TextWeight.Text.Split();

            if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
            if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
            if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
            if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
            if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
            if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
            if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
            if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
            if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
            if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
            SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
            SaveButton.UserInteractionEnabled = true;
        }

        partial void TextWeightEditEnded(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextLanguageDidBeginEdit(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            if (string.IsNullOrEmpty(sender.Text))
            {
                sender.Text = "Danish";
                SelectedLanguage = 1;
                if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
                if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
                if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
                if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
                if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
                if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
                if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
                if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
                if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
                if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.UserInteractionEnabled = true;
            }
        }

        partial void TextLanguageChanged(UITextField sender)
        {
            if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
            if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
            if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
            if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
            if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
            if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
            if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
            if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
            if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
            if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
            SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
            SaveButton.UserInteractionEnabled = true;
        }

        partial void TextLanguageEditEnded(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextHaircolorDidBeginEdit(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
        }

        partial void TextHaircolorChanged(UITextField sender)
        {
            if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
            if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
            if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
            if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
            if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
            if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
            if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
            if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
            if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
            if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
            SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
            SaveButton.UserInteractionEnabled = true;
        }

        partial void TextHairColorEditEnded(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextSwimExpDidBeginEdit(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            if (string.IsNullOrEmpty(sender.Text))
            {
                sender.Text = "no experience";
                if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
                if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
                if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
                if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
                if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
                if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
                if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
                if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
                if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
                if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.UserInteractionEnabled = true;

                SelectedSwimExp = 0;
            }
        }

        partial void TextSwimDidChange(UITextField sender)
        {
            if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
            if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
            if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
            if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
            if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
            if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
            if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
            if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
            if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
            if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
            SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
            SaveButton.UserInteractionEnabled = true;
        }

        partial void TextSwimDidEndEdit(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextSeaExpDidBeginEdit(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            if (string.IsNullOrEmpty(sender.Text))
            {
                sender.Text = "no experience";
                SelectedSeaExp = 0;
                if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
                if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
                if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
                if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
                if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
                if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
                if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
                if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
                if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
                if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.UserInteractionEnabled = true;
            }
        }

        partial void TextSeaExpChanged(UITextField sender)
        {
            if (!ValidateText(TextSeaExp, _SeaExpErrorButton, _SeaExpOkButton)) return;
            if (!ValidateText(TextSwimExp, _SwinExpErrorButton, _SwimExpOkButton)) return;
            if (!ValidateText(TextHaircolor, _HaircolorErrorButton, _HaircolorOkButton)) return;
            if (!ValidateText(TextLanguage, _LangaugeErrorButton, _LanguageOkButton)) return;
            if (!ValidateNumber(TextWeight, _WeightErrorButton, _WeightOkButton)) return;
            if (!ValidateNumber(TextHeight, _HeightErrorButton, _HeightOkButton)) return;
            if (!ValidateText(TextBirthday, _BirthdayErrorButton, _BirthdayOkButton)) return;
            if (!ValidateText(TextGender, _GenderErrorButton, _GenderOkButton)) return;
            if (!ValidateText(TextLastname, _LastnameErrorButton, _LastnameOkButton)) return;
            if (!ValidateText(TextFirstname, _FirstnameErrorButton, _FirstnameOkButton)) return;
            SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
            SaveButton.UserInteractionEnabled = true;
        }

        partial void TextSeaExpEditEnded(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        #region Validation

        private bool ValidateText(UITextField text, UIButton ErrorButton, UIButton OkButton)
        {
            if (!string.IsNullOrEmpty(text.Text))
            {
                if (text.RightView != OkButton)
                {
                    OkButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    OkButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = OkButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                return true;
            }
            else
            {
                if (text.RightView != ErrorButton)
                {
                    ErrorButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    ErrorButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = ErrorButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                SaveButton.UserInteractionEnabled = false;
                SaveButton.BackgroundColor = UIColor.FromRGB(165, 55, 23);
                return false;
            }
        }


        private bool ValidateNumber(UITextField text, UIButton ErrorButton, UIButton OkButton)
        {
            if (CheckIfNumber(text.Text) && !string.IsNullOrEmpty(text.Text))
            {
                if (text.RightView != OkButton)
                {
                    OkButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    OkButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = OkButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                return true;
            }
            else
            {
                if (text.RightView != ErrorButton)
                {
                    ErrorButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    ErrorButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = ErrorButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                SaveButton.UserInteractionEnabled = false;
                SaveButton.BackgroundColor = UIColor.FromRGB(165, 55, 23);
            }
            return false;
        }

        private bool CheckIfNumber(string text)
        {
            foreach (char c in text)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }

        #endregion


        #region KeyboardHandling

      


        private void KeyBoardwillShow(NSNotification notification)
        {
            if (!_Keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height - keyboardHeight;
                this.View.Frame = NewFrame;
                _currentKeyboardFrame = keyboardHeight;
                _Keyboardisshown = true;
            }
            else
            {
                var HeightDifference = UIKeyboard.BoundsFromNotification(notification).Height - _currentKeyboardFrame;
                _currentKeyboardFrame = UIKeyboard.BoundsFromNotification(notification).Height;
                var newFrame = this.View.Frame;
                newFrame.Height = newFrame.Height - HeightDifference;
                this.View.Frame = newFrame;
                _Keyboardisshown = true;
            }
        }


        private void KeyBoardWillHide(NSNotification notification)
        {
            if (_Keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, System.IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height + keyboardHeight;
                this.View.Frame = NewFrame;
                _Keyboardisshown = false;
            }
        }

        #endregion
    }
}