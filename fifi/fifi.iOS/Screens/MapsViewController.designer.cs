// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace fifi.iOS
{
    [Register ("MapsViewController")]
    partial class MapsViewController
    {
        [Outlet]
        UIKit.UIView alertView { get; set; }


        [Outlet]
        UIKit.UIButton btnSubmit { get; set; }


        [Outlet]
        UIKit.UIImageView ivAlert { get; set; }


        [Outlet]
        fifi.iOS.PlaceholderTextView txtDescription { get; set; }


        [Outlet]
        UIKit.UITextField txtTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AddFriendButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Background_speedline { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton close_alert_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton close_markermenu_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Closed_markermenu { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Closed_markermenu_boatname { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem FriendsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem GlobalButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView MarkerMenu { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView maxbar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView minbar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Open_markermenu_bar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton open_markermenu_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem SoloButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Text_boat_direction { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Text_boat_name { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Text_boat_type { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Text_currentspeed { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Text_maxspeed { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Text_minspeed { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Text_wind_direction { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Text_windspeed { get; set; }


        [Action ("BtnSubmitDragOutside:")]
        partial void BtnSubmitDragOutside (Foundation.NSObject sender);


        [Action ("BtnSubmitOnDown:")]
        partial void BtnSubmitOnDown (Foundation.NSObject sender);


        [Action ("BtnSubmitOnUp:")]
        partial void BtnSubmitOnUp (Foundation.NSObject sender);

        [Action ("AddFriendButtonTouchedDown:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void AddFriendButtonTouchedDown (UIKit.UIButton sender);

        [Action ("AddFriendButtonTouchedUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void AddFriendButtonTouchedUpInside (UIKit.UIButton sender);

        [Action ("AddFriendButtonTouchedUpOutside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void AddFriendButtonTouchedUpOutside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (AddFriendButton != null) {
                AddFriendButton.Dispose ();
                AddFriendButton = null;
            }

            if (alertView != null) {
                alertView.Dispose ();
                alertView = null;
            }

            if (Background_speedline != null) {
                Background_speedline.Dispose ();
                Background_speedline = null;
            }

            if (btnSubmit != null) {
                btnSubmit.Dispose ();
                btnSubmit = null;
            }

            if (close_alert_button != null) {
                close_alert_button.Dispose ();
                close_alert_button = null;
            }

            if (close_markermenu_button != null) {
                close_markermenu_button.Dispose ();
                close_markermenu_button = null;
            }

            if (Closed_markermenu != null) {
                Closed_markermenu.Dispose ();
                Closed_markermenu = null;
            }

            if (Closed_markermenu_boatname != null) {
                Closed_markermenu_boatname.Dispose ();
                Closed_markermenu_boatname = null;
            }

            if (FriendsButton != null) {
                FriendsButton.Dispose ();
                FriendsButton = null;
            }

            if (GlobalButton != null) {
                GlobalButton.Dispose ();
                GlobalButton = null;
            }

            if (ivAlert != null) {
                ivAlert.Dispose ();
                ivAlert = null;
            }

            if (MarkerMenu != null) {
                MarkerMenu.Dispose ();
                MarkerMenu = null;
            }

            if (maxbar != null) {
                maxbar.Dispose ();
                maxbar = null;
            }

            if (minbar != null) {
                minbar.Dispose ();
                minbar = null;
            }

            if (Open_markermenu_bar != null) {
                Open_markermenu_bar.Dispose ();
                Open_markermenu_bar = null;
            }

            if (open_markermenu_button != null) {
                open_markermenu_button.Dispose ();
                open_markermenu_button = null;
            }

            if (SoloButton != null) {
                SoloButton.Dispose ();
                SoloButton = null;
            }

            if (Text_boat_direction != null) {
                Text_boat_direction.Dispose ();
                Text_boat_direction = null;
            }

            if (Text_boat_name != null) {
                Text_boat_name.Dispose ();
                Text_boat_name = null;
            }

            if (Text_boat_type != null) {
                Text_boat_type.Dispose ();
                Text_boat_type = null;
            }

            if (Text_currentspeed != null) {
                Text_currentspeed.Dispose ();
                Text_currentspeed = null;
            }

            if (Text_maxspeed != null) {
                Text_maxspeed.Dispose ();
                Text_maxspeed = null;
            }

            if (Text_minspeed != null) {
                Text_minspeed.Dispose ();
                Text_minspeed = null;
            }

            if (Text_wind_direction != null) {
                Text_wind_direction.Dispose ();
                Text_wind_direction = null;
            }

            if (Text_windspeed != null) {
                Text_windspeed.Dispose ();
                Text_windspeed = null;
            }

            if (txtDescription != null) {
                txtDescription.Dispose ();
                txtDescription = null;
            }

            if (txtTitle != null) {
                txtTitle.Dispose ();
                txtTitle = null;
            }
        }
    }
}