// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("SmsVerificationViewController")]
    partial class SmsVerificationViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Invite_Email { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField VerificationPassword { get; set; }

        [Action ("VerficationPasswordDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void VerficationPasswordDidBeginEdit (UIKit.UITextField sender);

        [Action ("VerficationPasswordDidEnd:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void VerficationPasswordDidEnd (UIKit.UITextField sender);

        [Action ("VerificationPasswordDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void VerificationPasswordDidChange (UIKit.UITextField sender);

        void ReleaseDesignerOutlets ()
        {
            if (Invite_Email != null) {
                Invite_Email.Dispose ();
                Invite_Email = null;
            }

            if (VerificationPassword != null) {
                VerificationPassword.Dispose ();
                VerificationPassword = null;
            }
        }
    }
}