// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("ArmbandDetailsController")]
    partial class ArmbandDetailsController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SaveButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView Scrollview { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextBirthday { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextFirstname { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextGender { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextHaircolor { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextHeight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextLanguage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextLastname { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextSeaExp { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextSwimExp { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextWeight { get; set; }

        [Action ("TextBirthdayChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextBirthdayChanged (UIKit.UITextField sender);

        [Action ("TextBirthdayDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextBirthdayDidBeginEdit (UIKit.UITextField sender);

        [Action ("TextBirthdayEditDidEnd:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextBirthdayEditDidEnd (UIKit.UITextField sender);

        [Action ("TextFirstnameChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFirstnameChanged (UIKit.UITextField sender);

        [Action ("TextFirstnameEditDidBegin:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFirstnameEditDidBegin (UIKit.UITextField sender);

        [Action ("TextFirstnameEditEnded:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFirstnameEditEnded (UIKit.UITextField sender);

        [Action ("TextGenderDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextGenderDidBeginEdit (UIKit.UITextField sender);

        [Action ("TextGenderDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextGenderDidChange (UIKit.UITextField sender);

        [Action ("TextGenderDidEndEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextGenderDidEndEdit (UIKit.UITextField sender);

        [Action ("TextHaircolorChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextHaircolorChanged (UIKit.UITextField sender);

        [Action ("TextHaircolorDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextHaircolorDidBeginEdit (UIKit.UITextField sender);

        [Action ("TextHairColorEditEnded:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextHairColorEditEnded (UIKit.UITextField sender);

        [Action ("TextHeightDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextHeightDidBeginEdit (UIKit.UITextField sender);

        [Action ("TextHeightDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextHeightDidChange (UIKit.UITextField sender);

        [Action ("TextHeightDidEndEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextHeightDidEndEdit (UIKit.UITextField sender);

        [Action ("TextLanguageChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextLanguageChanged (UIKit.UITextField sender);

        [Action ("TextLanguageDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextLanguageDidBeginEdit (UIKit.UITextField sender);

        [Action ("TextLanguageEditEnded:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextLanguageEditEnded (UIKit.UITextField sender);

        [Action ("TextLastnameChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextLastnameChanged (UIKit.UITextField sender);

        [Action ("TextLastnameEditDidBegin:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextLastnameEditDidBegin (UIKit.UITextField sender);

        [Action ("TextLastnameEditEnded:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextLastnameEditEnded (UIKit.UITextField sender);

        [Action ("TextSeaExpChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextSeaExpChanged (UIKit.UITextField sender);

        [Action ("TextSeaExpDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextSeaExpDidBeginEdit (UIKit.UITextField sender);

        [Action ("TextSeaExpEditEnded:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextSeaExpEditEnded (UIKit.UITextField sender);

        [Action ("TextSwimDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextSwimDidChange (UIKit.UITextField sender);

        [Action ("TextSwimDidEndEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextSwimDidEndEdit (UIKit.UITextField sender);

        [Action ("TextSwimExpDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextSwimExpDidBeginEdit (UIKit.UITextField sender);

        [Action ("TextWeightChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextWeightChanged (UIKit.UITextField sender);

        [Action ("TextWeightDidBeginEdit:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextWeightDidBeginEdit (UIKit.UITextField sender);

        [Action ("TextWeightEditEnded:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextWeightEditEnded (UIKit.UITextField sender);

        void ReleaseDesignerOutlets ()
        {
            if (SaveButton != null) {
                SaveButton.Dispose ();
                SaveButton = null;
            }

            if (Scrollview != null) {
                Scrollview.Dispose ();
                Scrollview = null;
            }

            if (TextBirthday != null) {
                TextBirthday.Dispose ();
                TextBirthday = null;
            }

            if (TextFirstname != null) {
                TextFirstname.Dispose ();
                TextFirstname = null;
            }

            if (TextGender != null) {
                TextGender.Dispose ();
                TextGender = null;
            }

            if (TextHaircolor != null) {
                TextHaircolor.Dispose ();
                TextHaircolor = null;
            }

            if (TextHeight != null) {
                TextHeight.Dispose ();
                TextHeight = null;
            }

            if (TextLanguage != null) {
                TextLanguage.Dispose ();
                TextLanguage = null;
            }

            if (TextLastname != null) {
                TextLastname.Dispose ();
                TextLastname = null;
            }

            if (TextSeaExp != null) {
                TextSeaExp.Dispose ();
                TextSeaExp = null;
            }

            if (TextSwimExp != null) {
                TextSwimExp.Dispose ();
                TextSwimExp = null;
            }

            if (TextWeight != null) {
                TextWeight.Dispose ();
                TextWeight = null;
            }
        }
    }
}