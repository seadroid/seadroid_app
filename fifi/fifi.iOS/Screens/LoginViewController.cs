﻿using System;
using System.ComponentModel;
using System.Net.Mime;
using CoreGraphics;
using fifi.Helpers;
using fifi.iOS.Helpers;
using fifi.iOS.MenuControllers;
using fifi.Utils.Helpers;
using fifi.ViewModel;
using Foundation;
using SeaDroid.iOS;
using SeaDroid.iOS.Helpers;
using UIKit;

namespace fifi.iOS
{
    public partial class LoginViewController : UIViewController
    {

        private ImageUrlDownloader ImageDownloader;
        private LoginViewModel _viewModel;
		private UINavigationController NavController;
        private UIButton _EmailErrorButton;
        private UIButton _continueButton;
		private UIButton _passwordErrorButton;
        private UIButton _okButton;
        private NSObject _didShowNotificationObserver;
        private NSObject _willHideNotificationObserver;
        private UIView activeTextFieldView;
        private nfloat amountToScroll = 0.0f;
        private nfloat alreadyScrolledAmount = 0.0f;
        private nfloat bottomOfTheActiveTextField = 0.0f;
        private nfloat offsetBetweenKeybordAndTextField = 10.0f;
        private bool isMoveRequired = false;
        private bool isverifying;
    //   protected new NavigationController NavigationController
      //    => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.NavigationController;

        CreateUserViewController createUserView;
        private bool _didAnimate;

        public bool HasPasswordChanged { get; set; }

        public LoginViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            //base.ViewDidLoad();
            isverifying = false;
            
            

            #region Move UI View Up Handling
            // Keyboard popup
            _didShowNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver
            (UIKeyboard.DidShowNotification, KeyBoardDidShow, this);

            // Keyboard Down
            _willHideNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver
            (UIKeyboard.WillHideNotification, KeyBoardWillHide, this);
            #endregion

            _viewModel = new LoginViewModel();
            _viewModel.PropertyChanged += ViewModel_PropertyChanged;

            _EmailErrorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };
            _EmailErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

			_passwordErrorButton= new UIButton(UIButtonType.Custom)
			{
				BackgroundColor = "F93334".ToUiColor(),
				UserInteractionEnabled = false
			};
			_passwordErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);


            _continueButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "9FFFFF".ToUiColor(),
                UserInteractionEnabled = true
            };
            _continueButton.SetImage(UIImage.FromBundle("ic_continue"), UIControlState.Normal);

            _continueButton.TouchUpInside += (sender, e) =>
            {
                ((UIButton)sender).BackgroundColor = "9FFFFF".ToUiColor();
            };

            _continueButton.TouchDown += (sender, e) =>
            {
                ((UIButton)sender).BackgroundColor = "608CFF".ToUiColor();
                _viewModel.LoginCommand.Execute(null);
            };

            _okButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _okButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            // prepare for fade
            textEmail.Alpha = 0;
            textPassword.Alpha = 0;

            textEmail.Layer.BorderWidth = 1f;
            textEmail.Layer.BorderColor = UIColor.White.CGColor;
            textPassword.Layer.BorderWidth = 1f;
            textPassword.Layer.BorderColor = UIColor.White.CGColor;

            
            textEmail.ShouldReturn += sender =>
            {
                textEmail.ResignFirstResponder();
                return true;
            };

            textPassword.ShouldReturn += sender =>
            {
                textPassword.ResignFirstResponder();
                return true;
            };

            ImageDownloader = new ImageUrlDownloader();
            
            this.View.AddGestureRecognizer(new UITapGestureRecognizer(HideKeyboard));

           
        }

        private async void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
            if (!_viewModel.IsLoggedIn)
            {
                return;
            }

            if (isverifying == false)
            {
                isverifying = true;
                await _viewModel.ExecuteVerifUserAsync();
                bool answer = false;

                if (!_viewModel.IsVerified)
                {
                    var alert = new UIAlertView();
                    if (_viewModel.MasterMissingBoatProfile)
                    {
                        alert = new UIAlertView("master is missing vessel info", "Please finish activating your master unit by filling out the information about the vessel on the next page", null, NSBundle.MainBundle.LocalizedString("Cancel", "Cancel"), NSBundle.MainBundle.LocalizedString("OK", "OK"));
                        alert.Clicked += (object s, UIButtonEventArgs ex) =>
                        {
                            answer = (ex.ButtonIndex != alert.CancelButtonIndex);
                            if (answer == true)
                            {
                                CreateBoatViewController createBoatViewController = this.Storyboard.InstantiateViewController("CreateBoatViewController") as CreateBoatViewController;
                                NavController = new UINavigationController(createBoatViewController);
                                var app = (AppDelegate)UIApplication.SharedApplication.Delegate;
                                app.Window.RootViewController = NavController;
                            }
                        };
                        alert.Show();
                    }

                    else
                    {
                        alert = new UIAlertView("Account Is not Activated", "Please activate your account with one of the options on the following screen", null, NSBundle.MainBundle.LocalizedString("Cancel", "Cancel"), NSBundle.MainBundle.LocalizedString("OK", "OK"));
                        alert.Clicked += (object s, UIButtonEventArgs ex) =>
                        {
                            answer = (ex.ButtonIndex != alert.CancelButtonIndex);
                            if (answer == true)
                            {
                                AccountPairingViewController accountPairingView = this.Storyboard.InstantiateViewController("AccountPairingViewController") as AccountPairingViewController;
                                accountPairingView._viewModel.IsBaseView = true;
                                NavController = new UINavigationController(accountPairingView);
                                var app = (AppDelegate)UIApplication.SharedApplication.Delegate;
                                app.Window.RootViewController = NavController;
                            }
                            if (answer == false)
                            {
                                _viewModel.IsLoggedIn = false;
                                _viewModel.IsBusy = false;
                            }
                        };
                        alert.Show();
                    }
                    
                  
                }
                else
                {
                    if (!string.IsNullOrEmpty(Settings.Current.UserImageUrl))
                    {
                        Settings.Current.UserImageBase64 = ImageDownloader.GetImageBitmapFromUrl(Settings.Current.UserImageUrl);
                    }
                    else
                    {
                        Settings.Current.UserImageBase64 = string.Empty;
                    }            
                    NavigateToMain();
                }

                isverifying = false;
            }
           
           
            
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            if (_didAnimate)
            {
                return;
            }

            _didAnimate = true;
            textEmail.FadeIn(0.3, 0.3f);
            textPassword.FadeIn(0.3, 0.5f);
        }

        private void HideKeyboard()
        {
            textEmail.ResignFirstResponder();
            textPassword.ResignFirstResponder();
        }


        partial void CreateUserTouchDown(UIButton sender)
        {

            createUserView = this.Storyboard.InstantiateViewController("CreateUserViewController") as CreateUserViewController;
			NavController = new UINavigationController(createUserView);
			var app = (AppDelegate)UIApplication.SharedApplication.Delegate;
			app.Window.RootViewController = NavController;

        }

      

        partial void TextFieldEmailDidBeginEditing(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
		}

        partial void TextFieldEmailChangedEditing(UITextField sender)
        {
            _viewModel.Email = sender.Text;
            if (!ValidateEmail()) return;
            if (!HasPasswordChanged) return;
            if (!ValidatePassword()) return;
            // enable continue button
            _continueButton.Frame = new CGRect(0.0f, 0.0f, textPassword.Frame.Size.Height,
                textPassword.Frame.Size.Height);
            _continueButton.ContentMode = UIViewContentMode.Center;
            textPassword.RightView = _continueButton;
            textPassword.RightViewMode = UITextFieldViewMode.Always;
        }

        private void NavigateToMain()
        {
            InvokeOnMainThread(() =>
            {
                var app = (AppDelegate)UIApplication.SharedApplication.Delegate;
                //var viewController =
                //    UIStoryboard.FromName("Main", null).InstantiateViewController("mainViewController") as
                //        UINavigationController;
                app.Window.RootViewController = new RootViewController();
            });
        }


        partial void TextFieldEmailDidEndEditing(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextFieldPasswordDidBeginEditing(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
        }

        partial void TextFieldPasswordDidEndEditing(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextFieldPasswordChangedEditing(UITextField sender)
        {
            _viewModel.Password = sender.Text;
            HasPasswordChanged = true;
            if (!ValidateEmail()) return;
            if (!ValidatePassword()) return;
            if (ValidateEmail())
            {
                // enable continue compound
                _continueButton.Frame = new CGRect(0.0f, 0.0f, textPassword.Frame.Size.Height, textPassword.Frame.Size.Height);
                _continueButton.ContentMode = UIViewContentMode.Center;
                textPassword.RightView = _continueButton;
                textPassword.RightViewMode = UITextFieldViewMode.Always;
            }
        }

        private bool ValidatePassword()
        {
            if (!string.IsNullOrWhiteSpace(textPassword.Text) && textPassword.Text.Length >= 6)
            {
                textPassword.RightViewMode = UITextFieldViewMode.Never;
                return true;
            }
            if (textPassword.RightView != _passwordErrorButton)
            {
                _passwordErrorButton.Frame = new CGRect(0.0f, 0.0f, textPassword.Frame.Size.Height, textPassword.Frame.Size.Height);
                _passwordErrorButton.ContentMode = UIViewContentMode.Center;
                textPassword.RightView = _passwordErrorButton;
                textPassword.RightViewMode = UITextFieldViewMode.Always;
            }	
            return false;
        }

        private bool ValidateEmail()
        {
            if (textEmail.Text.IsEmail())
            {

                if (textEmail.RightView != _okButton)
                {
                    _okButton.Frame = new CGRect(0.0f, 0.0f, textEmail.Frame.Size.Height, textEmail.Frame.Size.Height);
                    _okButton.ContentMode = UIViewContentMode.Center;
                    textEmail.RightView = _okButton;
                    textEmail.RightViewMode = UITextFieldViewMode.Always;
                }
            
                return true;
            }

            if (textEmail.RightView != _EmailErrorButton)
            {
                _EmailErrorButton.Frame = new CGRect(0.0f, 0.0f, textEmail.Frame.Size.Height, textEmail.Frame.Size.Height);
                _EmailErrorButton.ContentMode = UIViewContentMode.Center;
                textEmail.RightView = _EmailErrorButton;
                textEmail.RightViewMode = UITextFieldViewMode.Always;

                _passwordErrorButton.Frame = new CGRect(0.0f, 0.0f, textPassword.Frame.Size.Height, textPassword.Frame.Size.Height);
                _passwordErrorButton.ContentMode = UIViewContentMode.Center;
                textPassword.RightView = _passwordErrorButton;
                textPassword.RightViewMode = UITextFieldViewMode.Always;
            }		
			return false;
        }
        #region Move UI View Up Handling
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            _didShowNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, KeyBoardDidShow);

            _willHideNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, KeyBoardWillHide);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
            if (_didShowNotificationObserver != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_didShowNotificationObserver);
            }

            if (_willHideNotificationObserver != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_willHideNotificationObserver);
            }
        }

        private void KeyBoardDidShow(NSNotification notification)
        {
            // get the keyboard size
            CoreGraphics.CGRect notificationBounds = UIKeyboard.BoundsFromNotification(notification);

            // Find what opened the keyboard
            foreach (UIView view in this.View.Subviews)
            {
               // if (view.IsFirstResponder)
                    activeTextFieldView = textPassword;
            }

            // Bottom of the controller = initial position + height + offset
            bottomOfTheActiveTextField = (activeTextFieldView.Frame.Y + activeTextFieldView.Frame.Height + offsetBetweenKeybordAndTextField);

            // Calculate how far we need to scroll
            amountToScroll = (notificationBounds.Height - (View.Frame.Size.Height - bottomOfTheActiveTextField));

            // Perform the scrolling
            if (amountToScroll > 0)
            {
                bottomOfTheActiveTextField -= alreadyScrolledAmount;
                amountToScroll = (notificationBounds.Height - (View.Frame.Size.Height - bottomOfTheActiveTextField));
                alreadyScrolledAmount += amountToScroll;
                isMoveRequired = true;
                ScrollTheView(isMoveRequired);
            }
            else
            {
                isMoveRequired = false;
            }

        }

        private void KeyBoardWillHide(NSNotification notification)
        {
            bool wasViewMoved = !isMoveRequired;
            if (isMoveRequired) { ScrollTheView(wasViewMoved); }
        }

        private void ScrollTheView(bool move)
        {

            // scroll the view up or down
            UIView.BeginAnimations(string.Empty, System.IntPtr.Zero);
            UIView.SetAnimationDuration(0.3);

            CoreGraphics.CGRect frame = View.Frame;

            if (move)
            {
                frame.Y -= amountToScroll;
            }
            else
            {
                frame.Y += alreadyScrolledAmount;
                amountToScroll = 0;
                alreadyScrolledAmount = 0;
            }

            View.Frame = frame;
            UIView.CommitAnimations();
        }
        #endregion
    }
}
