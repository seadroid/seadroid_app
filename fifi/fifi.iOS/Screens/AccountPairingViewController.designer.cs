// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("AccountPairingViewController")]
    partial class AccountPairingViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton armband_pairing_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton friend_invite_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton master_pairing_button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton shop_button { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (armband_pairing_button != null) {
                armband_pairing_button.Dispose ();
                armband_pairing_button = null;
            }

            if (friend_invite_button != null) {
                friend_invite_button.Dispose ();
                friend_invite_button = null;
            }

            if (master_pairing_button != null) {
                master_pairing_button.Dispose ();
                master_pairing_button = null;
            }

            if (shop_button != null) {
                shop_button.Dispose ();
                shop_button = null;
            }
        }
    }
}