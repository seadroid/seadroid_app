// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("DeviceTripViewController")]
    partial class DeviceTripViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Altitude { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Altitude_Units { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISlider CoordinatSlider { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel EndDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView EndPictureFrame { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView MapViewFrame { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Speed { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Speed_Units { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel StartDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView StartPictureFrame { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel WindHeading { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Windheading_units { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel WindSpeed { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel WindSpeed_Units { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Altitude != null) {
                Altitude.Dispose ();
                Altitude = null;
            }

            if (Altitude_Units != null) {
                Altitude_Units.Dispose ();
                Altitude_Units = null;
            }

            if (CoordinatSlider != null) {
                CoordinatSlider.Dispose ();
                CoordinatSlider = null;
            }

            if (EndDate != null) {
                EndDate.Dispose ();
                EndDate = null;
            }

            if (EndPictureFrame != null) {
                EndPictureFrame.Dispose ();
                EndPictureFrame = null;
            }

            if (MapViewFrame != null) {
                MapViewFrame.Dispose ();
                MapViewFrame = null;
            }

            if (Speed != null) {
                Speed.Dispose ();
                Speed = null;
            }

            if (Speed_Units != null) {
                Speed_Units.Dispose ();
                Speed_Units = null;
            }

            if (StartDate != null) {
                StartDate.Dispose ();
                StartDate = null;
            }

            if (StartPictureFrame != null) {
                StartPictureFrame.Dispose ();
                StartPictureFrame = null;
            }

            if (WindHeading != null) {
                WindHeading.Dispose ();
                WindHeading = null;
            }

            if (Windheading_units != null) {
                Windheading_units.Dispose ();
                Windheading_units = null;
            }

            if (WindSpeed != null) {
                WindSpeed.Dispose ();
                WindSpeed = null;
            }

            if (WindSpeed_Units != null) {
                WindSpeed_Units.Dispose ();
                WindSpeed_Units = null;
            }
        }
    }
}