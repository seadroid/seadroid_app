using System;
using System;
using CoreGraphics;
using fifi.iOS.MenuControllers;
using Foundation;
using SidebarNavigation;
using UIKit;
using fifi.ViewModel;
using fifi.Model;
using System.ComponentModel;
using fifi.Client.DataObjects;
using fifi.iOS.Helpers;
using SeaDroid.iOS;


namespace fifi.iOS
{
	public partial class PairingController : UITableViewController, IUITableViewDataSource, IUITableViewDelegate
	{
		private NSObject _notifyEditDone;
		UITableView table;
		private PairingViewModel _viewModel;
		protected SidebarController SidebarController
			=> (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.SidebarController;

		//protected new NavigationController NavigationController
		//	=> (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.NavigationController;

		public override UIStoryboard Storyboard
			=> (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.Storyboard;

		//pairingDetailsViewController pairingDetailsView;
	    private ArmbandDetailsController pairingDetailsView;

		public PairingController(IntPtr handle) : base(handle)
		{
			//NavigationItem.SetLeftBarButtonItem(
			//  new UIBarButtonItem(UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain,
			//	  (sender, args) =>
			//	  {
			//		  SidebarController.ToggleMenu();
			//	  }), true);

		}




		public async override void ViewDidLoad()
		{
			base.ViewDidLoad();

			var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
			{
				TextAlignment = UITextAlignment.Left,
				Text = "Pairing",
				TextColor = UIColor.White
			};
			NavigationItem.TitleView = lblNavTitle;

		    UIApplication app = UIApplication.SharedApplication;
		    var statusBarHeight = app.StatusBarFrame.Size.Height;
		    UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
		    statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
		    this.NavigationController.NavigationBar.AddSubview(statusbar);

            pairingDetailsView = this.Storyboard.InstantiateViewController("ArmbandDetailsController") as ArmbandDetailsController;

            _viewModel = new PairingViewModel();
			_viewModel.PropertyChanged += ViewModel_PropertyChanged;
			BluetoothTable.Source = new PairingTablesource(_viewModel, this);
			await _viewModel.ExecuteCheckBluetoothCommandAsync();
			await _viewModel.ExecuteLoadBluetoothCommandAsync();		
			this.RefreshControl = new UIRefreshControl();
			this.RefreshControl.ValueChanged += refresh;
			BluetoothTable.Add(RefreshControl);
  

		    RefreshControl.Enabled = false;
            

		}

	    public override void ViewDidDisappear(bool animated)
	    {
	        _viewModel.StopBluetoothCommand.Execute(null);
            base.ViewDidDisappear(animated);
	    }

	    public async void refresh(object sender, EventArgs e)
		{
			//await _viewModel.ExecuteRefreshBluetoothCommandAsync();
			BluetoothTable.ReloadData();
			RefreshControl.EndRefreshing();
		}
	
		public async void HandleEddittingDone(NSNotification notificaiton)
		{
			_viewModel.wristband = pairingDetailsView._viewModel.Wristband;
			NSNotificationCenter.DefaultCenter.RemoveObserver(_notifyEditDone);
			await _viewModel.ExecuteUpdateWristbandCommandAsync();
			NavigationController.PopViewController(false);
            NSNotificationCenter.DefaultCenter.PostNotificationName("PairingDone", this);

        }


		public void ShowPairingDetailsView(BluetoothDevice bluetoothdevice)

        {
			try
			{ 
				NSNotificationCenter.DefaultCenter.RemoveObserver(_notifyEditDone);
			}

			catch (Exception e) 
			{
				var a = e;
			}
			_notifyEditDone =	NSNotificationCenter.DefaultCenter.AddObserver(new Foundation.NSString("Done"), HandleEddittingDone);
            var wristbandfound = false;
			var PairingDetials = new PairingDetailsViewModel();
			var user_id = 0;
			bool answer = false;
			PairingDetials.BluetoothDeviceId = bluetoothdevice.Device.Name;
			var alert = new UIAlertView();

			foreach (var wristband in _viewModel.Armbands.data)
			{
				if (bluetoothdevice.Device.Name == wristband.serial)
				{
				    PairingDetials.Wristband = wristband;
					wristbandfound = true;
					user_id = wristband.user_id;
				}

			}
			if (wristbandfound == false)
			{
				PairingDetials.Wristband = new Armband();
			}

			pairingDetailsView._viewModel = PairingDetials;



			if (user_id == 0)
			{
				 alert = new UIAlertView("Create Profile", "Do you whant to create a profile for this Armband? Creating a profile will bind this Armband to your account", null, NSBundle.MainBundle.LocalizedString("NO", "NO"), NSBundle.MainBundle.LocalizedString("YES", "YES"));
				alert.Clicked += (object sender, UIButtonEventArgs e) =>
			{
				answer = (e.ButtonIndex != alert.CancelButtonIndex);
				if (answer == true)
				{
						NavigationController.PushViewController(pairingDetailsView, true);
				        _viewModel.StopBluetoothCommand.Execute(null);

                }
            };
				alert.Show();
			}


			if (user_id == _viewModel.UserProfile.Id) 
			{
               alert = new UIAlertView("Edit Wristband", "Do you whant to eddit selected wristband?", null, NSBundle.MainBundle.LocalizedString("NO", "NO"), NSBundle.MainBundle.LocalizedString("YES", "YES"));
				alert.Clicked += (object sender, UIButtonEventArgs e) =>
				{
					answer = (e.ButtonIndex != alert.CancelButtonIndex);
					if (answer == true)
					{
						NavigationController.PushViewController(pairingDetailsView, true);
                        _viewModel.StopBluetoothCommand.Execute(null);
					}
				};
				alert.Show();

			}

			if(user_id !=0 && user_id != _viewModel.UserProfile.Id)
					{
               alert = new UIAlertView("Insufficient permission", "Cant edit selected Armband, the amband belongs to someone else", null, NSBundle.MainBundle.LocalizedString("Ok", "Ok"));
				alert.Clicked += (object sender, UIButtonEventArgs e) =>
				{
					answer = (e.ButtonIndex != alert.CancelButtonIndex);
					if (answer == true)
					{
						
					}
				};
				alert.Show();

			}


		}



		private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
		
		//	BluetoothTable.Source = new PairingTablesource(_viewModel, this);
			BluetoothTable.ReloadData();
		}


	}

	internal class PairingTablesource : UITableViewSource
	{
	    readonly PairingViewModel _viewmodel;
		string CellIdentifier = "ArmbandCell";
	    readonly PairingController _controller;
		private nint spacing = 5;

		public PairingTablesource(PairingViewModel viewmodel, PairingController controller)
		{
			_viewmodel = viewmodel;
			_controller = controller;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return 1;
		}

		public override nint NumberOfSections(UITableView tableView)
		{
			return _viewmodel.Bluetoothdevices.Count;
		}

		public override nfloat GetHeightForFooter(UITableView tableView, nint section)
		{
			return spacing;
		}

		public override nfloat GetHeightForHeader(UITableView tableView, nint section)
		{
			return spacing;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
		    ArmbandCell cell = tableView.DequeueReusableCell(CellIdentifier) as ArmbandCell;
			var item = _viewmodel.Bluetoothdevices[indexPath.Section];

			//---- if there are no cells to reuse, create a new one
		    if (cell == null)
		    {
		        cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier) as ArmbandCell;
		    }

            cell.Name = item.DeviceName;
		    cell.BatteryLevelText = string.Empty;
		    cell.BatteryImage.TintColor = UIColor.White;
		    cell.SignalImage.TintColor = UIColor.White;

            if (!string.IsNullOrEmpty(item.BatteryLevel))
		    {

		        cell.BatteryLevelText = item.BatteryLevel + "%";

		        cell.BatteryImage.Image = cell.BatteryImage.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
		        cell.SignalImage.Image = cell.SignalImage.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);


		        if (Convert.ToInt32(item.BatteryLevel) <= 20)
		        {
		            var image = cell.BatteryImage.Image;
		            cell.BatteryImage.TintColor = "#cf3619".ToUiColor();

		        }
		        if (Convert.ToInt32(item.BatteryLevel) > 20 && Convert.ToInt32(item.BatteryLevel) <= 30)
		        {
		            cell.BatteryImage.TintColor = "#ffe70f".ToUiColor();

		        }
		        if (Convert.ToInt32(item.BatteryLevel) > 30)
		        {
		            cell.BatteryImage.TintColor = "#00b157".ToUiColor();
		        }

		        cell.SignalImage.TintColor = "#00b157".ToUiColor();
		    }


            return cell;
		}

      

		public async override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			
			BluetoothDevice blue = _viewmodel.Bluetoothdevices[indexPath.Section];
			await _viewmodel.ExecutePingBluetoothdeviceCommandAsync(blue.Device);
			_controller.ShowPairingDetailsView(blue);

		} 

	}

}