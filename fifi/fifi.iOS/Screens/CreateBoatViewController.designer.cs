// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SeaDroid.iOS
{
    [Register ("CreateBoatViewController")]
    partial class CreateBoatViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView BoatImange { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SaveButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextBoatColor { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextBoatLenght { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextBoatName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextBoatType { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextCountry { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextHarbor { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextModel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextProductionYear { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TextRegister_No { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BoatImange != null) {
                BoatImange.Dispose ();
                BoatImange = null;
            }

            if (SaveButton != null) {
                SaveButton.Dispose ();
                SaveButton = null;
            }

            if (TextBoatColor != null) {
                TextBoatColor.Dispose ();
                TextBoatColor = null;
            }

            if (TextBoatLenght != null) {
                TextBoatLenght.Dispose ();
                TextBoatLenght = null;
            }

            if (TextBoatName != null) {
                TextBoatName.Dispose ();
                TextBoatName = null;
            }

            if (TextBoatType != null) {
                TextBoatType.Dispose ();
                TextBoatType = null;
            }

            if (TextCountry != null) {
                TextCountry.Dispose ();
                TextCountry = null;
            }

            if (TextHarbor != null) {
                TextHarbor.Dispose ();
                TextHarbor = null;
            }

            if (TextModel != null) {
                TextModel.Dispose ();
                TextModel = null;
            }

            if (TextProductionYear != null) {
                TextProductionYear.Dispose ();
                TextProductionYear = null;
            }

            if (TextRegister_No != null) {
                TextRegister_No.Dispose ();
                TextRegister_No = null;
            }
        }
    }
}