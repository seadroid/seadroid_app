using Foundation;
using System;
using CoreGraphics;
using UIKit;
using fifi.ViewModel;

namespace fifi.iOS
{
    public partial class AccountPairingViewController : UIViewController
    {

        public AccountPairingViewModel _viewModel = new AccountPairingViewModel();

        public AccountPairingViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{

		    if ( _viewModel.IsBaseView == true)
		    {
                this.NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(UIBarButtonSystemItem.Cancel, (sender, e) =>
                {
                    var app = (AppDelegate)UIApplication.SharedApplication.Delegate;
                    var loginViewController = UIStoryboard.FromName("Main", null).InstantiateViewController("loginViewController");
                    app.Window.RootViewController = loginViewController;
                }), true);
            }
            friend_invite_button.TouchUpInside += (sender, args) =>
		    {

		    };

		    armband_pairing_button.TouchUpInside += (sender, args) =>
		    {
                AccountArmbandPairing armbandPairing = this.Storyboard.InstantiateViewController("AccountArmbandPairing") as AccountArmbandPairing;
                NavigationController.PushViewController(armbandPairing, true);
            };

		    master_pairing_button.TouchUpInside += (sender, args) =>
		    {
               MasterPairingViewController MasterPairing = this.Storyboard.InstantiateViewController("MasterPairingViewController") as MasterPairingViewController;
                NavigationController.PushViewController(MasterPairing, true);
		    };

		    shop_button.TouchUpInside += (sender, args) =>
		    {

		    };

		    UIApplication app2 = UIApplication.SharedApplication;
		    var statusBarHeight = app2.StatusBarFrame.Size.Height;
		    UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
		    statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
		    this.NavigationController.NavigationBar.AddSubview(statusbar);

            base.ViewDidLoad();

		}
    }
}