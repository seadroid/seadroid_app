// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("PairingController")]
    partial class PairingController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView BluetoothTable { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIActivityIndicatorView Loading { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BluetoothTable != null) {
                BluetoothTable.Dispose ();
                BluetoothTable = null;
            }

            if (Loading != null) {
                Loading.Dispose ();
                Loading = null;
            }
        }
    }
}