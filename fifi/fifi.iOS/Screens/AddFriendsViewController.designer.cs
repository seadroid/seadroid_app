// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("AddFriendsViewController")]
    partial class AddFriendsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AddFriendsButotn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AddFriendsSearchButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField AddFriendsSearchText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView AddFriendsTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FriendRequestsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FriendsButton { get; set; }

        [Action ("AddFriendsButtonPressed:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void AddFriendsButtonPressed (UIKit.UIButton sender);

        [Action ("FriendRequestButtonPressed:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void FriendRequestButtonPressed (UIKit.UIButton sender);

        [Action ("FriendsButtonPressed:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void FriendsButtonPressed (UIKit.UIButton sender);

        [Action ("SearchButtonPressed:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void SearchButtonPressed (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (AddFriendsButotn != null) {
                AddFriendsButotn.Dispose ();
                AddFriendsButotn = null;
            }

            if (AddFriendsSearchButton != null) {
                AddFriendsSearchButton.Dispose ();
                AddFriendsSearchButton = null;
            }

            if (AddFriendsSearchText != null) {
                AddFriendsSearchText.Dispose ();
                AddFriendsSearchText = null;
            }

            if (AddFriendsTableView != null) {
                AddFriendsTableView.Dispose ();
                AddFriendsTableView = null;
            }

            if (FriendRequestsButton != null) {
                FriendRequestsButton.Dispose ();
                FriendRequestsButton = null;
            }

            if (FriendsButton != null) {
                FriendsButton.Dispose ();
                FriendsButton = null;
            }
        }
    }
}