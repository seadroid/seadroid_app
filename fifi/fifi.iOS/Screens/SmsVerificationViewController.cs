using Foundation;
using System;
using CoreGraphics;
using fifi.iOS.Helpers;
using fifi.ViewModel;
using UIKit;

namespace fifi.iOS
{
    public partial class SmsVerificationViewController : UIViewController
    {
        private UIButton _VerificationPasswordErrorButton;
        private UIButton _VerificationPasswordOkButton;
        private UIButton _VerfircationContinueButton;
        private CreateUserViewModel _viewModel;
        public string Phonenumber { get; set; }

        public SmsVerificationViewController(IntPtr handle) : base(handle)
        {
        }


        public override void ViewDidLoad()
        {
            _viewModel = new CreateUserViewModel();
            _viewModel.PhoneNumber = Phonenumber;
            _viewModel.SendVerificationCodeCommand.Execute(null);

            try
            {
                UIApplication app = UIApplication.SharedApplication;
                var statusBarHeight = app.StatusBarFrame.Size.Height;
                UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
                statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
                this.NavigationController.NavigationBar.AddSubview(statusbar);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            //VerificationPassword.Alpha = 0;
            //VerificationPassword.Layer.BorderWidth = 1f;
            //VerificationPassword.Layer.BorderColor = UIColor.White.CGColor;

            _VerificationPasswordErrorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };
            _VerificationPasswordErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);


            _VerificationPasswordOkButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _VerificationPasswordOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);


            _VerfircationContinueButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "9FFFFF".ToUiColor(),
                UserInteractionEnabled = true
            };
            _VerfircationContinueButton.SetImage(UIImage.FromBundle("ic_continue"), UIControlState.Normal);
            _VerfircationContinueButton.TouchUpInside += (sender, e) =>
            {

                if (VerificationPassword.Text == _viewModel.VerificationPassword)
                {
                    NSNotificationCenter.DefaultCenter.PostNotificationName("Done", this);
                    this.NavigationController.PopViewController(true);
                }
                else
                {
                    var toaster = new ToastIos();
                    toaster.ShowToast("wrong password", this.View);       
                }

            };

            _VerificationPasswordErrorButton.Frame = new CGRect(0.0f, 0.0f, VerificationPassword.Frame.Size.Height,
                VerificationPassword.Frame.Size.Height);
            _VerificationPasswordErrorButton.ContentMode = UIViewContentMode.Center;
            VerificationPassword.RightViewMode = UITextFieldViewMode.Always;
            VerificationPassword.RightView = _VerificationPasswordErrorButton;
        }


        partial void VerficationPasswordDidBeginEdit(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
        }

        partial void VerificationPasswordDidChange(UITextField sender)
        {
            if(!CheckPasswordLength()) return;
            _VerfircationContinueButton.Frame = new CGRect(0.0f, 0.0f, VerificationPassword.Frame.Size.Height,
            VerificationPassword.Frame.Size.Height);
            _VerfircationContinueButton.ContentMode = UIViewContentMode.Center;
            VerificationPassword.RightView = _VerfircationContinueButton;
            VerificationPassword.RightViewMode = UITextFieldViewMode.Always;
        }

        partial void VerficationPasswordDidEnd(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }


        private bool CheckPasswordLength()
        {
            if (VerificationPassword.Text.Length >= 4)
            {
                _VerificationPasswordOkButton.Frame = new CGRect(0.0f, 0.0f, VerificationPassword.Frame.Size.Height,
                VerificationPassword.Frame.Size.Height);
                _VerificationPasswordOkButton.ContentMode = UIViewContentMode.Center;
                VerificationPassword.RightView = _VerificationPasswordOkButton;
                VerificationPassword.RightViewMode = UITextFieldViewMode.Always;
                return true;
            }
            _VerificationPasswordErrorButton.Frame = new CGRect(0.0f, 0.0f, VerificationPassword.Frame.Size.Height,
                 VerificationPassword.Frame.Size.Height);
            _VerificationPasswordErrorButton.ContentMode = UIViewContentMode.Center;
            VerificationPassword.RightView = _VerificationPasswordErrorButton;
            VerificationPassword.RightViewMode = UITextFieldViewMode.Always;
            return false;
        }

        
    }
}