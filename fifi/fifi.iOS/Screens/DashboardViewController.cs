﻿using System;
using CoreGraphics;
using fifi.iOS.MenuControllers;
using Foundation;
using SidebarNavigation;
using UIKit;
using UserNotifications;

namespace fifi.iOS
{
    public partial class DashboardViewController : BaseController, IUITableViewDataSource, IUITableViewDelegate
    {
        private const string DashboardCellHeadingIdentifier = "DASHBOARD_CELL_HEADING_IDENTIFIER";
        private const string DashboardCellHarborIdentifier = "DASHBOARD_CELL_HARBOR_IDENTIFIER";
        private const string DashboardCellWeatherInfoIdentifier = "DASHBOARD_CELL_WEATHERINFO_IDENTIFIER";
        private const string DashboardCellMembersIdentifier = "DASHBOARD_CELL_MEMBERS_IDENTIFIER";

        protected SidebarController SidebarController
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.SidebarController;
   

        public override UIStoryboard Storyboard
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.Storyboard;

        public DashboardViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

         

            NavigationItem.SetLeftBarButtonItem(
                new UIBarButtonItem(UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain,
                    (sender, args) =>
                    {
                        SidebarController.ToggleMenu();
                    }), true);

            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "Cacao",
                TextColor = UIColor.White
            };
            NavigationItem.TitleView = lblNavTitle;
            lblNavTitle.DangerousRelease();

            DashboardTableView.WeakDataSource = this;
            DashboardTableView.TableFooterView = new UIView(new CGRect(0, 0, 0, 0));

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);


        }

        public override void ViewDidAppear(bool animated)
        {

            UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert, (approved, err) => {
                // Handle approval
            });

            base.ViewDidAppear(animated);
        }

        #region UITableViewSource

        public nint RowsInSection(UITableView tableView, nint section)
        {
            return 4;
        }

        public UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            switch (indexPath.Row)
            {
                case 0:
                    {
                        var cell =
                            tableView.DequeueReusableCell(DashboardCellHeadingIdentifier) as DashboardTableViewCellHeading ??
                            new DashboardTableViewCellHeading(new NSString(DashboardCellHeadingIdentifier));

                        cell.Heading = "37° 22,8° N 12° 19,4° Ø";
                        return cell;
                    }
                case 1:
                    {
                        var cell =
                            tableView.DequeueReusableCell(DashboardCellHarborIdentifier) as DashboardTableViewCellHarbor ??
                            new DashboardTableViewCellHarbor(new NSString(DashboardCellHarborIdentifier));

                        cell.Harbor = "≈ Nivå Havn, Denmark";
                        return cell;
                    }
                case 2:
                    {
                        var cell =
                            tableView.DequeueReusableCell(DashboardCellWeatherInfoIdentifier) as
                                DashboardTableViewCellWeatherInfo ??
                            new DashboardTableViewCellWeatherInfo(new NSString(DashboardCellWeatherInfoIdentifier));

                        cell.Degrees = "8°";
                        cell.Downfall = "0 mm";
                        cell.WindSpeed = "3 m/s";
                        return cell;
                    }
                case 3:
                    {
                        var cell =
                            tableView.DequeueReusableCell(DashboardCellMembersIdentifier) as DashboardTableViewCellMembers ??
                            new DashboardTableViewCellMembers(new NSString(DashboardCellMembersIdentifier));

                        cell.AmountOnBoard = "6 x";
                        return cell;
                    }
                default:
                    return null;
            }
        }
        #endregion

        public override void UpdateViewConstraints()
        {
            base.UpdateViewConstraints();
            DashboardTableViewHeightConstraint.Constant = DashboardTableView.ContentSize.Height;
        }
    }
}