using Foundation;
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using CoreAnimation;
using CoreGraphics;
using fifi.Client.DataObjects;
using fifi.iOS.Helpers;
using fifi.iOS.MenuControllers;
using fifi.Utils.Helpers;
using fifi.ViewModel;
using SidebarNavigation;
using UIKit;

namespace fifi.iOS
{
    public partial class ChatHunViewController : BaseController
    {
      

   

        public Friend Friend { get; set; }
        private ChatHubViewModel _viewModel;
        private UIView _coverView;

        private NSObject _chatListener;

        public ChatHunViewController (IntPtr handle) : base (handle)
        {
            NavigationItem.SetLeftBarButtonItem(
                new UIBarButtonItem(UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain,
                    (sender, args) => { SidebarController.ToggleMenu();
                       // ToggleOverlay();
                    }), true);
             
            


           
        }



        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);

            //_coverView.Frame = this.View.Frame;
            //_coverView.BackgroundColor = UIColor.Black.ColorWithAlpha(0.6f);
            _viewModel = new ChatHubViewModel();
            ChatHubTableView.Source = new ChatHubTableSource(_viewModel, this);
            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "Messages",
                TextColor = UIColor.White
            };
            NavigationItem.TitleView = lblNavTitle;
            _viewModel.Friends.CollectionChanged += FriendsOnCollectionChanged;

            



            //SidebarController.StateChangeHandler += (sender, b) =>
            //{
            //    if (SidebarController.IsOpen)
            //    {
            //       this.View.AddSubview(_coverView);
            //    }
            //    else
            //    {
            //        _coverView.RemoveFromSuperview();
            //    }
            //};
        }


        private void ToggleOverlay()
        {
            if (SidebarController.IsOpen)
            {
                _coverView?.RemoveFromSuperview();
            }
            else
            {
                UIApplication app = UIApplication.SharedApplication;
                _coverView = new UIView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height));
                _coverView.BackgroundColor = UIColor.Black.ColorWithAlpha(0.85f);
                this.View.AddSubview(_coverView);
            }
        }

        private void FriendsOnCollectionChanged(object o, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            ChatHubTableView.ReloadData();

        }

        public override void ViewDidAppear(bool animated)
        {
            _viewModel.LoadFriendsCommand.Execute(null);

            base.ViewDidAppear(animated);
        }

        


        internal class ChatHubTableSource : UITableViewSource
        {
            private ChatHubViewModel _viewModel;
            private string CellIdentifier = "ChatFriendCell";
            private ChatHunViewController _controller;



            public ChatHubTableSource(ChatHubViewModel viewModel, ChatHunViewController controller)
            {
                _controller = controller;
                _viewModel = viewModel;
            }


            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                ChatFriendCell cell = tableView.DequeueReusableCell(CellIdentifier) as ChatFriendCell;

                var item = _viewModel.Friends[indexPath.Row];

                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier) as ChatFriendCell;
                }

                cell.Name = item.firstname + " " + item.lastname;
                cell.Message = item.latest_message;

                if (!string.IsNullOrEmpty(item.image))
                {
                    InvokeOnMainThread((() =>
                    {
                        cell.Image.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(new ImageUrlDownloader().GetImageBitmapFromUrl(item.image))));
                    }));
                    
                }
                else
                {
                    cell.Image.Image = UIImage.FromBundle("friends_ghost_image_blue");
                }
                cell.Image.Layer.CornerRadius = cell.Image.Frame.Size.Height / 2;
                cell.Image.Layer.MasksToBounds = true;
                return cell;
            }

           

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                try
                {
                    var ChatView = _controller.Storyboard.InstantiateViewController("FriendChatViewController") as FriendChatViewController;
                    
                                    
                    ChatView.Friend = _viewModel.Friends[indexPath.Row];

                    _controller.NavigationController.PushViewController(ChatView, true);
                }

                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
               
               // ChatHunViewController chat = _controller.Storyboard.InstantiateViewController("ChatHunViewController") as ChatHunViewController;
               
                //_controller.NavigationController.PushViewController(ChatView, true);
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return _viewModel.Friends.Count;
            }



         
        }
    }
}