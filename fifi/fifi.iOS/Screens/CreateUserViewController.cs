using Foundation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using CoreGraphics;
using CoreMedia;
using fifi.iOS.Helpers;
using fifi.ViewModel;
using UIKit;
using fifi.Client.Interfaces;
using fifi.Client.DataObjects;
using fifi.iOS.CustomControls;

namespace fifi.iOS
{
    public partial class CreateUserViewController : UIViewController
    {
        #region Params

        

   
        private UIButton _continueButton;
        private UIButton _ConfirmEmailErrorButton;
        private UIButton _ConfirmEmailOkkButton;
        private UIButton _PhoneNumberErrorButton;
        private UIButton _PhoneNumberOkButton;
        private UIButton _EmailerrorButton;
        private UIButton _EmailokButton;
        private UIButton _PasswordErrorButton;
        private UIButton _PasswordKkButton;
        private UIButton _ConfirmPassworderrorButton;
        private UIButton _ConfirmPasswordokButton;
        private UIButton _LastnameErrorButton;
        private UIButton _LastnameokButton;
        private UIButton _FirstnameErrorButton;
        private UIButton _FirstnameOkButton;
        private NSObject _willShowNotificationObserver;
        private NSObject _willHideNotificationObserver;
        private UIButton _Flag;

        private SmsVerificationViewController SmsController;
        private NSObject _notifyEditDone;
        private UIPickerView FlagPicker;
        private CustomUIpickerViewModel flagsViewModel;

        private UIView activeTextFieldView;
        private nfloat amountToScroll = 0.0f;
        private nfloat alreadyScrolledAmount = 0.0f;
        private nfloat bottomOfTheActiveTextField = 0.0f;
        private nfloat offsetBetweenKeybordAndTextField = 10.0f;
        private bool isMoveRequired = false;
        private CreateUserViewModel _viewModel;
        private bool IsCreatinUser = false;
        private numverifyApi _numverifyApi;
        private GatewayApi _gatewayApi;
        private bool PhoneVerified = false;
       
        private string selectedPhoneCode;

        #region keyboard

        private nfloat _currentKeyboardFrame;
        private bool _keyboardisshown;


        #endregion

        #endregion


        public override void ViewDidLoad()
        {
            UIApplication app2 = UIApplication.SharedApplication;
            var statusBarHeight = app2.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);


            var flags = new List<IosFlag>();
            flags.Add(new IosFlag(UIImage.FromBundle("DK"),"DK","+45"));
            flags.Add(new IosFlag(UIImage.FromBundle("SE"), "SE", "+46"));
            flags.Add(new IosFlag(UIImage.FromBundle("NO"), "NO", "+47"));
            flags.Add(new IosFlag(UIImage.FromBundle("FI"), "FI", "+358"));
            flags.Add(new IosFlag(UIImage.FromBundle("DE"), "DE", "+49"));
            flags.Add(new IosFlag(UIImage.FromBundle("UK"), "UK", "+44"));


          
            FlagPicker = new UIPickerView();
            FlagPicker.Center = CGPoint.Empty;
            flagsViewModel = new CustomUIpickerViewModel(flags) ;

            flagsViewModel.ValueChanged += (sender, args) =>
            {
                _Flag.SetImage(flagsViewModel.SelectedItem.flagImage, UIControlState.Normal);
                selectedPhoneCode = flagsViewModel.SelectedItem.PhoneCode;
                ValidatePhonenumber();

            };


            FlagPicker.Model = flagsViewModel;
            

            var toolbar = new UIToolbar
            {
                BarStyle = UIBarStyle.BlackTranslucent,
                Translucent = true,
                Hidden = false
            };
            toolbar.SizeToFit();

            UIButton doneButton = new UIButton(UIButtonType.Custom)
            {
                Frame = new RectangleF(260, 8, 50, 30),
                UserInteractionEnabled = true                
            };

            doneButton.SetTitle("Done", UIControlState.Normal);
            doneButton.BackgroundColor = UIColor.White;
            doneButton.Layer.CornerRadius = 5f;
            doneButton.SetTitleColor(UIColor.Black, UIControlState.Normal);
            
            doneButton.TouchUpInside += (sender, e) =>
            {               
                TextPhoneNumber.InputAccessoryView = null;
                TextPhoneNumber.InputView = null;
                TextPhoneNumber.ReloadInputViews();
            };

            toolbar.AddSubview(doneButton);
        

            // Keyboard popup
            _willShowNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver
                (UIKeyboard.WillShowNotification,KeyBoardwillShow  , this);

            // Keyboard Down
            _willHideNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver
                (UIKeyboard.WillHideNotification, KeyBoardWillHide, this);

           
           

            _numverifyApi = new numverifyApi();
            _gatewayApi = new GatewayApi();

            this.NavigationController.NavigationBar.Translucent = true;
            this.NavigationController.NavigationBar.BackgroundColor = UIColor.Blue;

            this.NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(UIBarButtonSystemItem.Cancel, (sender, e) =>
            {
                var app = (AppDelegate) UIApplication.SharedApplication.Delegate;
                var loginViewController =
                    UIStoryboard.FromName("Main", null).InstantiateViewController("loginViewController");
                app.Window.RootViewController = loginViewController;
            }), true);
            _viewModel = new CreateUserViewModel();


         

            TextFirstname.EditingDidEnd += (sender, args) =>
            {
                TextFirstname.BackgroundColor = UIColor.Clear;
               
            };

            TextLastname.EditingDidEnd += (sender, args) =>
            {
                TextLastname.BackgroundColor = UIColor.Clear;
                activeTextFieldView = null;
            };

            TextPhoneNumber.EditingDidEnd += (sender, args) =>
            {
                TextPhoneNumber.BackgroundColor = UIColor.Clear;
                TextPhoneNumber.InputView = null;
                TextPhoneNumber.InputAccessoryView = null;
                activeTextFieldView = null;
            };
            TextPhoneNumber.KeyboardType = UIKeyboardType.NumberPad;

            TextEmail.EditingDidEnd += (sender, args) =>
            {
                TextEmail.BackgroundColor = UIColor.Clear;
                activeTextFieldView = null;
            };

            TextConfirmEmail.EditingDidEnd += (sender, args) =>
            {
                TextConfirmEmail.BackgroundColor = UIColor.Clear;
                activeTextFieldView = null;
            };

            TextPassword.EditingDidEnd += (sender, args) =>
            {
                TextPassword.BackgroundColor = UIColor.Clear;
                activeTextFieldView = null;
            };

            ConfirmPassword.EditingDidEnd += (sender, args) =>
            {
                ConfirmPassword.BackgroundColor = UIColor.Clear;
                activeTextFieldView = null;
            };

            _FirstnameErrorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };
            _FirstnameErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);


            _LastnameErrorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };
            _LastnameErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _PhoneNumberErrorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };

            _PhoneNumberErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _EmailerrorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };
            _EmailerrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _ConfirmEmailErrorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };
            _ConfirmEmailErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _PasswordErrorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };
            _PasswordErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _ConfirmPassworderrorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };
            _ConfirmPassworderrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _continueButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "9FFFFF".ToUiColor(),
                UserInteractionEnabled = true
            };
            _continueButton.SetImage(UIImage.FromBundle("ic_continue"), UIControlState.Normal);

            _continueButton.TouchUpInside +=  (sender, e) =>
            {
                _notifyEditDone = NSNotificationCenter.DefaultCenter.AddObserver(new Foundation.NSString("Done"), HandleEddittingDone);
                ((UIButton)sender).BackgroundColor = "9FFFFF".ToUiColor();
                SmsController = this.Storyboard.InstantiateViewController("SmsVerificationViewController") as SmsVerificationViewController;
                SmsController.Phonenumber = selectedPhoneCode + TextPhoneNumber.Text;           
                NavigationController.PushViewController(SmsController, true);
              
            };


            _FirstnameOkButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _FirstnameOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);


            _LastnameokButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _LastnameokButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _PhoneNumberOkButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _PhoneNumberOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _EmailokButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _EmailokButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);


            _Flag = new UIButton(UIButtonType.Custom)
            {
                UserInteractionEnabled = true
            };
            _Flag.SetImage(UIImage.FromBundle("DK"), UIControlState.Normal);
            
           

            _EmailokButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);


            _ConfirmEmailOkkButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _ConfirmEmailOkkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);


            _PasswordKkButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _PasswordKkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);


            _ConfirmPasswordokButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _ConfirmPasswordokButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _continueButton.TouchUpInside +=
                (sender, e) => { ((UIButton) sender).BackgroundColor = "9FFFFF".ToUiColor(); };

            _continueButton.TouchDown += (sender, e) => { ((UIButton) sender).BackgroundColor = "608CFF".ToUiColor(); };




            #region Initalize Errorbuttons

            _PhoneNumberErrorButton.Frame = new CGRect(0.0f, 0.0f, TextPhoneNumber.Frame.Size.Height,
              TextPhoneNumber.Frame.Size.Height);
            _PhoneNumberErrorButton.ContentMode = UIViewContentMode.Center;
            TextPhoneNumber.RightViewMode = UITextFieldViewMode.Always;
            TextPhoneNumber.RightView = _PhoneNumberErrorButton;




            _Flag.Frame = new CGRect(0.0f, 0.0f, TextPhoneNumber.Frame.Size.Height / 1.5, TextPhoneNumber.Frame.Size.Height/2);
            _Flag.BackgroundColor = UIColor.LightGray;
            _Flag.ContentMode = UIViewContentMode.Center;
            TextPhoneNumber.LeftViewMode = UITextFieldViewMode.Always;
            TextPhoneNumber.LeftView = _Flag;

            _Flag.TouchDown += (sender, args) =>
            {
                TextPhoneNumber.InputView = FlagPicker;
                TextPhoneNumber.InputAccessoryView = toolbar;
                TextPhoneNumber.ReloadInputViews();
                TextPhoneNumber.BecomeFirstResponder();

                foreach (var view in CreateUserScrollView.Subviews)
                {
                    if (view.IsFirstResponder)
                    {
                        ResignFirstResponder();
                    }
                }
            };

         


            _FirstnameErrorButton.Frame = new CGRect(0.0f, 0.0f, TextFirstname.Frame.Size.Height,
               TextFirstname.Frame.Size.Height);
            _FirstnameErrorButton.ContentMode = UIViewContentMode.Center;
            TextFirstname.RightView = _FirstnameErrorButton;
            TextFirstname.RightViewMode = UITextFieldViewMode.Always;

            _LastnameErrorButton.Frame = new CGRect(0.0f, 0.0f, TextLastname.Frame.Size.Height,
                TextLastname.Frame.Size.Height);
            _LastnameErrorButton.ContentMode = UIViewContentMode.Center;
            TextLastname.RightView = _LastnameErrorButton;
            TextLastname.RightViewMode = UITextFieldViewMode.Always;

            _PasswordErrorButton.Frame = new CGRect(0.0f, 0.0f, TextPassword.Frame.Size.Height,
          TextPassword.Frame.Size.Height);
            _PasswordErrorButton.ContentMode = UIViewContentMode.Center;
            TextPassword.RightView = _PasswordErrorButton;
            TextPassword.RightViewMode = UITextFieldViewMode.Always;


            _EmailerrorButton.Frame = new CGRect(0.0f, 0.0f, TextEmail.Frame.Size.Height, TextEmail.Frame.Size.Height);
            _EmailerrorButton.ContentMode = UIViewContentMode.Center;
            TextEmail.RightView = _EmailerrorButton;
            TextEmail.RightViewMode = UITextFieldViewMode.Always;

            _ConfirmEmailErrorButton.Frame = new CGRect(0.0f, 0.0f, TextConfirmEmail.Frame.Size.Height,
               TextConfirmEmail.Frame.Size.Height);
            _ConfirmEmailErrorButton.ContentMode = UIViewContentMode.Center;
            TextConfirmEmail.RightView = _ConfirmEmailErrorButton;
            TextConfirmEmail.RightViewMode = UITextFieldViewMode.Always;
            #endregion

            _ConfirmPassworderrorButton.Frame = new CGRect(0.0f, 0.0f, TextConfirmEmail.Frame.Size.Height,
                 ConfirmPassword.Frame.Size.Height);
            _ConfirmPassworderrorButton.ContentMode = UIViewContentMode.Center;
            ConfirmPassword.RightView = _ConfirmPassworderrorButton;
            ConfirmPassword.RightViewMode = UITextFieldViewMode.Always;

        }
        public async void HandleEddittingDone(NSNotification notificaiton)
        {
            if (IsCreatinUser == false)
            {
                try
                {
                    NSNotificationCenter.DefaultCenter.RemoveObserver(_notifyEditDone);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);                 
                }
                IsCreatinUser = true;
                
                _viewModel.Email = TextEmail.Text;
                _viewModel.Firstname = TextFirstname.Text;
                _viewModel.Lastname = TextLastname.Text;
                _viewModel.Password = TextPassword.Text;
                _viewModel.PhoneNumber = TextPhoneNumber.Text;
                _viewModel.Country_id = flagsViewModel.SelectedRow + 1;
                

                var result = await _viewModel.ExecuteCreateUserAsync();

                if (result)
                {

                    var alert = new UIAlertView();

                    alert = new UIAlertView("Account was created",
                        "To Activate the account please use one of the following options on the next screen", null,
                        NSBundle.MainBundle.LocalizedString("OK", "OK"));

                    alert.Clicked += (object s, UIButtonEventArgs ex) =>
                    {
                        AccountPairingViewController accountPairing =
                            this.Storyboard.InstantiateViewController("AccountPairingViewController") as
                                AccountPairingViewController;
                        this.NavigationController.PushViewController(accountPairing, true);
                    };
                    alert.Show();
                }
                else
                {
                    //var alert = new UIAlertView();
                    //alert = new UIAlertView("Account Not Created",
                    //    "Email or Phonenumber is already used", null,
                    //    NSBundle.MainBundle.LocalizedString("OK", "OK"));
                    //alert.Show();
                }
                IsCreatinUser = false;
            }
        }

        public CreateUserViewController(IntPtr handle) : base(handle)
        {
        }

        partial void TextPhonenumberDidBeginEdit(UITextField sender)
        {
 
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            activeTextFieldView = sender;
        }

       async partial void TextPhonenumberDidChange(UITextField sender)
        {         
            ValidateReconfirmPassword();
            if (!await ValidatePhonenumber()) return;
            if (!ValidateFirstname()) return;
            if (!ValidateLastname()) return;          
            if (!ReconfirmValidateEmail()) return;
            if (!ValidateEmail()) return;
            if (!ValidatePassword()) return;
            if (!ValidateReconfirmPassword()) return;
            {
                // enable continue compound
                _continueButton.Frame = new CGRect(0.0f, 0.0f, ConfirmPassword.Frame.Size.Height,
                    ConfirmPassword.Frame.Size.Height);
                _continueButton.ContentMode = UIViewContentMode.Center;
                ConfirmPassword.RightView = _continueButton;
                ConfirmPassword.RightViewMode = UITextFieldViewMode.Always;
            }
        }

        partial void TextPhonenumberdDidEndEdit(UITextField sender)
        {
            
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextfieldFirstNameDidBeginEditing(UITextField sender)
        {
           
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            activeTextFieldView = sender;
        }

        partial void TextfieldFirstNameDidChange(UITextField sender)
        {            
            _viewModel.Firstname = TextFirstname.Text;
            ValidateReconfirmPassword();
            if (!ValidateFirstname()) return;
            if (!ValidateLastname()) return;
            if(!PhoneVerified) return;
            if (!ReconfirmValidateEmail()) return;
            if (!ValidateEmail()) return;
            if (!ValidatePassword()) return;
            if (!ValidateReconfirmPassword()) return;
            {
                // enable continue compound
                _continueButton.Frame = new CGRect(0.0f, 0.0f, ConfirmPassword.Frame.Size.Height,
                    ConfirmPassword.Frame.Size.Height);
                _continueButton.ContentMode = UIViewContentMode.Center;
                ConfirmPassword.RightView = _continueButton;
                ConfirmPassword.RightViewMode = UITextFieldViewMode.Always;
            }
        }

        partial void TextfukedFirstNameDidEndEditing(UITextField sender)
        {
            
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextLastNameDidBeginEditing(UITextField sender)
        {
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            activeTextFieldView = sender;
        }

        partial void TextLastNameChanged(UITextField sender)
        {
            _viewModel.Lastname = TextLastname.Text;
            ValidateReconfirmPassword();
            if (!ValidateLastname()) return;
            if (!PhoneVerified) return;
            if (!ValidateEmail()) return;
            if (!ReconfirmValidateEmail()) return;
            if (!ValidateFirstname()) return;
            if (!ValidatePassword()) return;
            if (!ValidateReconfirmPassword()) return;
            {
                // enable continue compound
                _continueButton.Frame = new CGRect(0.0f, 0.0f, ConfirmPassword.Frame.Size.Height,
                    ConfirmPassword.Frame.Size.Height);
                _continueButton.ContentMode = UIViewContentMode.Center;
                ConfirmPassword.RightView = _continueButton;
                ConfirmPassword.RightViewMode = UITextFieldViewMode.Always;
            }
        }

        partial void TextLastnameDidEndEditing(UITextField sender)
        {           
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextEmailDidBeginEditing(UITextField sender)
        {
            
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            activeTextFieldView = sender;
        }

       partial void TextEmailDidChange(UITextField sender)
        {
            _viewModel.Email = TextEmail.Text;
            ValidateReconfirmPassword();
            if (!ValidateEmail()) return;
            if (!ReconfirmValidateEmail()) return;
            if (!ValidateFirstname()) return;
            if (!ValidateLastname()) return;
            if (!PhoneVerified) return;
            if (!ValidatePassword()) return;
            if (!ValidateReconfirmPassword()) return;
            {
                // enable continue compound
                _continueButton.Frame = new CGRect(0.0f, 0.0f, ConfirmPassword.Frame.Size.Height,
                    ConfirmPassword.Frame.Size.Height);
                _continueButton.ContentMode = UIViewContentMode.Center;
                ConfirmPassword.RightView = _continueButton;
                ConfirmPassword.RightViewMode = UITextFieldViewMode.Always;
            }
        }

        partial void TextEmailEditingDidEnd(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextReconfirmEmailDidChange(UITextField sender)
        {
            _viewModel.ReconfirmEmail = TextConfirmEmail.Text;
            ValidateReconfirmPassword();
            if (!ReconfirmValidateEmail()) return;
            if (!ValidateEmail()) return;
            if (!ValidatePassword()) return;
            if (!ValidateFirstname()) return;
            if (!ValidateLastname()) return;
            if (!PhoneVerified) return;
            {
                // enable continue compound
                _continueButton.Frame = new CGRect(0.0f, 0.0f, ConfirmPassword.Frame.Size.Height,
                    ConfirmPassword.Frame.Size.Height);
                _continueButton.ContentMode = UIViewContentMode.Center;
                ConfirmPassword.RightView = _continueButton;
                ConfirmPassword.RightViewMode = UITextFieldViewMode.Always;
            }
        }

        partial void TextReconfirmEmailEditingDidEnd(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextReconfirmEmailDidBeginEditing(UITextField sender)
        {
     
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            activeTextFieldView = sender;
        }

        partial void TextPasswordDidBeginEditing(UITextField sender)
        {
           
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            activeTextFieldView = sender;
        }

        partial void TextPasswordDidChange(UITextField sender)
        {
            _viewModel.Password = TextPassword.Text;
            ValidateReconfirmPassword();
            if (!ValidatePassword()) return;
            if (!ValidateFirstname()) return;
            if (!ValidateLastname()) return;
            if (!PhoneVerified) return;
            if (!ReconfirmValidateEmail()) return;
            if (!ValidateReconfirmPassword()) return;
            {
                // enable continue compound
                _continueButton.Frame = new CGRect(0.0f, 0.0f, ConfirmPassword.Frame.Size.Height,
                    ConfirmPassword.Frame.Size.Height);
                _continueButton.ContentMode = UIViewContentMode.Center;
                ConfirmPassword.RightView = _continueButton;
                ConfirmPassword.RightViewMode = UITextFieldViewMode.Always;
            }
        }

        partial void TextPasswordDidEndEditing(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextReconfirmPasswordDidBeginEditing(UITextField sender)
        {
        
            sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            activeTextFieldView = sender;
        }

        partial void TextReconfirmPasswordDidEndEditing(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextReconfirmPasswordDidChange(UITextField sender)
        {
            _viewModel.Password = sender.Text;


            if (!ValidateReconfirmPassword()) return;
            if (!ValidateFirstname()) return;
            if (!ValidateLastname()) return;
            if (!PhoneVerified) return;
            if (!ValidateEmail()) return;
            if (!ReconfirmValidateEmail()) return;
            if (!ValidatePassword()) return;
            {
                // enable continue compound
                _continueButton.Frame = new CGRect(0.0f, 0.0f, ConfirmPassword.Frame.Size.Height,
                    ConfirmPassword.Frame.Size.Height);
                _continueButton.ContentMode = UIViewContentMode.Center;
                ConfirmPassword.RightView = _continueButton;
                ConfirmPassword.RightViewMode = UITextFieldViewMode.Always;
            }
        }


        private async Task<bool> ValidatePhonenumber()
        {
            try
            {
                if (selectedPhoneCode == null)
                {
                    selectedPhoneCode = "+45";
                }
                var result = await _numverifyApi.VerifyNumber(selectedPhoneCode + TextPhoneNumber.Text);

                if (result.valid)
                {
                    if (TextPhoneNumber.RightView != _PhoneNumberOkButton)
                    {
                        _PhoneNumberOkButton.Frame = new CGRect(0.0f, 0.0f, TextPhoneNumber.Frame.Size.Height,
                       TextPhoneNumber.Frame.Size.Height);
                        _PhoneNumberOkButton.ContentMode = UIViewContentMode.Center;
                        TextPhoneNumber.RightView = _PhoneNumberOkButton;
                        TextPhoneNumber.RightViewMode = UITextFieldViewMode.Always;
                    }
                    PhoneVerified = true;
                    return true;
                }

                if (TextPhoneNumber.RightView != _PhoneNumberErrorButton)
                {
                    _PhoneNumberErrorButton.Frame = new CGRect(0.0f, 0.0f, TextPhoneNumber.Frame.Size.Height,
                   TextPhoneNumber.Frame.Size.Height);
                    _PhoneNumberErrorButton.ContentMode = UIViewContentMode.Center;
                    TextPhoneNumber.RightViewMode = UITextFieldViewMode.Always;
                    TextPhoneNumber.RightView = _PhoneNumberErrorButton;
                }
                PhoneVerified = false;
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                var result = await ValidatePhonenumber();
                return result;
            }
            




        }


        private bool ValidateFirstname()
        {
            if (!string.IsNullOrWhiteSpace(TextFirstname.Text) && TextFirstname.Text.Length > 1 && TextFirstname.Text.Length <60)
            {
                if (TextFirstname.RightView != _FirstnameOkButton)
                {
                    _FirstnameOkButton.Frame = new CGRect(0.0f, 0.0f, TextFirstname.Frame.Size.Height,
                    TextFirstname.Frame.Size.Height);
                    _FirstnameOkButton.ContentMode = UIViewContentMode.Center;
                    TextFirstname.RightView = _FirstnameOkButton;
                    TextFirstname.RightViewMode = UITextFieldViewMode.Always;
                }
                return true;
            }
            if (TextFirstname.RightView != _FirstnameErrorButton)
            {
                _FirstnameErrorButton.Frame = new CGRect(0.0f, 0.0f, TextFirstname.Frame.Size.Height,
                TextFirstname.Frame.Size.Height);
                _FirstnameErrorButton.ContentMode = UIViewContentMode.Center;
                TextFirstname.RightView = _FirstnameErrorButton;
                TextFirstname.RightViewMode = UITextFieldViewMode.Always;
            }           
            return false;
        }


        private bool ValidateLastname()
        {
            if (!string.IsNullOrWhiteSpace(TextLastname.Text) && TextLastname.Text.Length > 0 && TextLastname.Text.Length <60)
            {
                if (TextLastname.RightView != _LastnameokButton)
                {
                    _LastnameokButton.Frame = new CGRect(0.0f, 0.0f, TextLastname.Frame.Size.Height,
                   TextLastname.Frame.Size.Height);
                    _LastnameokButton.ContentMode = UIViewContentMode.Center;
                    TextLastname.RightView = _LastnameokButton;
                    TextLastname.RightViewMode = UITextFieldViewMode.Always;
                }       
                return true;
            }
            if (TextLastname.RightView != _LastnameErrorButton)
            {
                _LastnameErrorButton.Frame = new CGRect(0.0f, 0.0f, TextLastname.Frame.Size.Height,
                TextLastname.Frame.Size.Height);
                _LastnameErrorButton.ContentMode = UIViewContentMode.Center;
                TextLastname.RightView = _LastnameErrorButton;
                TextLastname.RightViewMode = UITextFieldViewMode.Always;
            }
            return false;
        }

        private bool ValidatePassword()
        {
            if (!string.IsNullOrWhiteSpace(TextPassword.Text) && TextPassword.Text.Length >= 6)
            {
                if (TextPassword.RightView != _PasswordKkButton)
                {
                    _PasswordKkButton.Frame = new CGRect(0.0f, 0.0f, TextPassword.Frame.Size.Height,
                    TextPassword.Frame.Size.Height);
                    _PasswordKkButton.ContentMode = UIViewContentMode.Center;
                    TextPassword.RightView = _PasswordKkButton;
                    TextPassword.RightViewMode = UITextFieldViewMode.Always;
                }
                               
                return true;
            }
            if (TextPassword.RightView != _PasswordErrorButton)
            {
                _PasswordErrorButton.Frame = new CGRect(0.0f, 0.0f, TextPassword.Frame.Size.Height,
           TextPassword.Frame.Size.Height);
                _PasswordErrorButton.ContentMode = UIViewContentMode.Center;
                TextPassword.RightView = _PasswordErrorButton;
                TextPassword.RightViewMode = UITextFieldViewMode.Always;
            }
                
            return false;
        }


        private bool ValidateEmail()
        {
            if (TextEmail.Text.IsEmail())
            {
                if (TextEmail.RightView != _EmailokButton)
                {
                    _EmailokButton.Frame = new CGRect(0.0f, 0.0f, TextEmail.Frame.Size.Height, TextEmail.Frame.Size.Height);
                    _EmailokButton.ContentMode = UIViewContentMode.Center;
                    TextEmail.RightView = _EmailokButton;
                    TextEmail.RightViewMode = UITextFieldViewMode.Always;
                }
                          
                return true;
            }
            if (TextEmail.RightView != _EmailerrorButton)
            {
                _EmailerrorButton.Frame = new CGRect(0.0f, 0.0f, TextEmail.Frame.Size.Height, TextEmail.Frame.Size.Height);
                _EmailerrorButton.ContentMode = UIViewContentMode.Center;
                TextEmail.RightView = _EmailerrorButton;
                TextEmail.RightViewMode = UITextFieldViewMode.Always;
            }  
            return false;
        }

        private bool ReconfirmValidateEmail()
        {
            if (TextConfirmEmail.Text.IsEmail() && TextConfirmEmail.Text == TextEmail.Text)
            {
                if (TextConfirmEmail.RightView != _ConfirmEmailOkkButton)
                {
                    _ConfirmEmailOkkButton.Frame = new CGRect(0.0f, 0.0f, TextConfirmEmail.Frame.Size.Height,
                    TextConfirmEmail.Frame.Size.Height);
                    _ConfirmEmailOkkButton.ContentMode = UIViewContentMode.Center;
                    TextConfirmEmail.RightView = _ConfirmEmailOkkButton;
                    TextConfirmEmail.RightViewMode = UITextFieldViewMode.Always;
                }                
                return true;
            }
            if (TextConfirmEmail.RightView != _ConfirmEmailErrorButton)
            {
                _ConfirmEmailErrorButton.Frame = new CGRect(0.0f, 0.0f, TextConfirmEmail.Frame.Size.Height,
               TextConfirmEmail.Frame.Size.Height);
                _ConfirmEmailErrorButton.ContentMode = UIViewContentMode.Center;
                TextConfirmEmail.RightView = _ConfirmEmailErrorButton;
                TextConfirmEmail.RightViewMode = UITextFieldViewMode.Always;
            }          
            return false;
        }

        private bool ValidateReconfirmPassword()
        {
            if (ConfirmPassword.Text == TextPassword.Text && ConfirmPassword.Text.Length >= 6)
            {
                if (ConfirmPassword.RightView != _PhoneNumberOkButton)
                {
                    _ConfirmPasswordokButton.Frame = new CGRect(0.0f, 0.0f, ConfirmPassword.Frame.Size.Height,
                  ConfirmPassword.Frame.Size.Height);
                    _ConfirmPasswordokButton.ContentMode = UIViewContentMode.Center;
                    ConfirmPassword.RightView = _ConfirmPasswordokButton;
                    ConfirmPassword.RightViewMode = UITextFieldViewMode.Always;
                }           
                return true;
            }

            if (ConfirmPassword.RightView != _ConfirmPassworderrorButton)
            {
                _ConfirmPassworderrorButton.Frame = new CGRect(0.0f, 0.0f, TextConfirmEmail.Frame.Size.Height,
                  ConfirmPassword.Frame.Size.Height);
                _ConfirmPassworderrorButton.ContentMode = UIViewContentMode.Center;
                ConfirmPassword.RightView = _ConfirmPassworderrorButton;
                ConfirmPassword.RightViewMode = UITextFieldViewMode.Always;
            }          
            return false;
        }

 

        #region Move UI View Up Handling

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            _willShowNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(
                UIKeyboard.WillShowNotification, KeyBoardwillShow);
            _willHideNotificationObserver =
                NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, KeyBoardWillHide);
        }


        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            if (_willShowNotificationObserver != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_willShowNotificationObserver);
            }

            if (_willHideNotificationObserver != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_willHideNotificationObserver);
            }
        }


        private void KeyBoardwillShow(NSNotification notification)
        {
            if (!_keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height - keyboardHeight;
                this.View.Frame = NewFrame;
                _currentKeyboardFrame = keyboardHeight;
                _keyboardisshown = true;
            }
            else
            {
                var HeightDifference = UIKeyboard.BoundsFromNotification(notification).Height - _currentKeyboardFrame;
                _currentKeyboardFrame = UIKeyboard.BoundsFromNotification(notification).Height;
                var newFrame = this.View.Frame;
                newFrame.Height = newFrame.Height - HeightDifference;
                this.View.Frame = newFrame;
                _keyboardisshown = true;
            }
        }

        //private void KeyBoardDidShow(NSNotification notification)
        //{

        //     UIKeyboard.Notifications.ObserveWillShow((s, e) =>
        //    {
        //        //finds the height of the keyboard
        //        var r = UIKeyboard.FrameBeginFromNotification(e.Notification);
        //        var keyboardHeight = r.Height;

        //        // adjusts the buttom content inset of the scroll view by the keyboards size
        //        UIEdgeInsets contentInsets = new UIEdgeInsets((float)0.0,(float)0.0, keyboardHeight + 30, (float)0.0);
        //        CreateUserScrollView.ContentInset = contentInsets;
        //        CreateUserScrollView.ScrollIndicatorInsets = contentInsets;
        //        CGRect aRect = View.Frame;
        //        foreach (UIView view in this.CreateUserScrollView.Subviews)
        //        {
        //            if (view.IsFirstResponder)
        //                activeTextFieldView = view;
        //        }
        //        if(!View.Frame.Contains(aRect))
        //        {
        //            CGPoint scrollPoint = new CGPoint(0.0, activeTextFieldView.Frame.Y - (keyboardHeight + 15));
        //            CreateUserScrollView.SetContentOffset(scrollPoint, true);
        //        }

                
        //    });

        //}



        private void KeyBoardWillHide(NSNotification notification)
        {

            if (_keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, System.IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height + keyboardHeight;
                this.View.Frame = NewFrame;
                _keyboardisshown = false;
            }

        }

      

        #endregion


       

    }
}