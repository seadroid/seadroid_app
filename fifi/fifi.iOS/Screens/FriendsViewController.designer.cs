// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("FriendsViewController")]
    partial class FriendsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AddFriendsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FriendsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        fifi.iOS.FriendsListTableView FriendsListListView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView FriendsListSearchBar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FriendsListSearchButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField FriendsListSearchTextInput { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView FriendsListView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FriendsRequestButton { get; set; }

        [Action ("AddFriendsButtonPressed:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void AddFriendsButtonPressed (UIKit.UIButton sender);

        [Action ("FriendRequestButtonPressed:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void FriendRequestButtonPressed (UIKit.UIButton sender);

        [Action ("FriendsButtonPressed:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void FriendsButtonPressed (UIKit.UIButton sender);

        [Action ("SearchButtonPressed:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void SearchButtonPressed (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (AddFriendsButton != null) {
                AddFriendsButton.Dispose ();
                AddFriendsButton = null;
            }

            if (FriendsButton != null) {
                FriendsButton.Dispose ();
                FriendsButton = null;
            }

            if (FriendsListListView != null) {
                FriendsListListView.Dispose ();
                FriendsListListView = null;
            }

            if (FriendsListSearchBar != null) {
                FriendsListSearchBar.Dispose ();
                FriendsListSearchBar = null;
            }

            if (FriendsListSearchButton != null) {
                FriendsListSearchButton.Dispose ();
                FriendsListSearchButton = null;
            }

            if (FriendsListSearchTextInput != null) {
                FriendsListSearchTextInput.Dispose ();
                FriendsListSearchTextInput = null;
            }

            if (FriendsListView != null) {
                FriendsListView.Dispose ();
                FriendsListView = null;
            }

            if (FriendsRequestButton != null) {
                FriendsRequestButton.Dispose ();
                FriendsRequestButton = null;
            }
        }
    }
}