using Foundation;
using System;
using System.ComponentModel;
using CoreGraphics;
using fifi.iOS.Helpers;
using UIKit;
using fifi.Model;
using fifi.ViewModel;
using SidebarNavigation;
using SeaDroid.iOS.Helpers;


namespace fifi.iOS
{

    public partial class FriendRequestsViewControlller : UIViewController
    {

        private FriendViewModel _viewModel;
        protected SidebarController SidebarController
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.SidebarController;

        //protected new NavigationController NavigationController
        //    => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.NavigationController;

        public override UIStoryboard Storyboard
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.Storyboard;

        public bool AlertIsShown;

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            AlertIsShown = false;
            FriendRequestsButton.BackgroundColor = UIColor.FromRGB(206, 208, 206);

            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "     Friends",
                Font = UIFont.BoldSystemFontOfSize(19.0f),
                TextColor = UIColor.White
            };
            NavigationItem.TitleView = lblNavTitle;

ImageResizer resizer = new ImageResizer();
var image = UIImage.FromBundle("neg_three_men");
var ThreeMen = resizer.ResizeImage(UIImage.FromBundle("neg_three_men"), (float)image.Size.Height / (float)2.5, (float)image.Size.Width / (float)2.5);

var ThreeMenButton = new UIBarButtonItem((UIImage.FromBundle("ThreeMen"))
	, UIBarButtonItemStyle.Plain
	, (sender, args) =>
	{

	});
ThreeMenButton.Enabled = false;
            

            ThreeMenButton.Image = ThreeMen;
            NavigationItem.SetRightBarButtonItem(ThreeMenButton, false);

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);

            _viewModel = new FriendViewModel();
            _viewModel.PropertyChanged += ViewModel_propertychanged;
        
             FriendRequestRecivedTableView.Source = new FriendRequestRecievedTableSource(_viewModel, this);
             FriendRequestSentTableView.Source = new FriendRequestSentTableSource(_viewModel, this);

            _viewModel.LoadRecivedFriendRequestsCommand.Execute(null);
            _viewModel.LoadSentFriendRequestsCommand.Execute(null);
        }

        public FriendRequestsViewControlller (IntPtr handle) : base (handle)
        {
            NavigationItem.SetLeftBarButtonItem(
             new UIBarButtonItem(UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain,
                 (sender, args) =>
                 {
                     SidebarController.ToggleMenu();
                 }), true);
        }

        public void ViewModel_propertychanged(object sender, PropertyChangedEventArgs e)
        {
            FriendRequestRecivedTableView.ReloadData();
            FriendRequestSentTableView.ReloadData();
        }

        partial void FriendRequestButton_TouchedUpInside(UIButton sender)
        {
           
        }

        partial void FriendsButtonPressed(UIButton sender)
		{
            FriendsViewController FriendsView = this.Storyboard.InstantiateViewController("FriendsViewController") as FriendsViewController;
			this.NavigationController.PushViewController(FriendsView, false);
            this.SidebarController.CloseMenu(true);
        }

	

partial void AddFriendsButtonPressed(UIButton sender)
		{
            AddFriendsViewController AddFriendsView = this.Storyboard.InstantiateViewController("AddFriendsViewController") as AddFriendsViewController;
            this.NavigationController.PushViewController(AddFriendsView, false);
            this.SidebarController.CloseMenu(true);
        }



        internal class FriendRequestRecievedTableSource : UITableViewSource
        {
            private FriendViewModel _viewModel;
            private string CellIdentifier = "FriendRequestRecivedCell";
            private FriendRequestsViewControlller _controller;

            public FriendRequestRecievedTableSource(FriendViewModel viewModel, FriendRequestsViewControlller controller)
            {
                _viewModel = viewModel;
                _controller = controller;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                FriendRequestRecivedCell cell = tableView.DequeueReusableCell(CellIdentifier) as FriendRequestRecivedCell;
                var item = _viewModel.RecivedFriendRequests[indexPath.Row];

                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier) as FriendRequestRecivedCell;
                }

                cell.Confirm_Button.TouchDown += (sender, args) =>
                {
                    cell.Confirm_Button.BackgroundColor = UIColor.FromRGB(0, 126, 41);
                };
                cell.Confirm_Button.TouchUpInside += (sender, args) =>
                {
                    cell.Confirm_Button.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                    _viewModel.user_id = cell.user.id.ToString();
                    _viewModel.user = cell.user;
                    _viewModel.AcceptFriendCommand.Execute(null);
                    _viewModel.RecivedFriendRequests.Remove(cell.user);
                    _controller.FriendRequestRecivedTableView.ReloadData();
                };
                cell.Confirm_Button.TouchUpOutside += (sender, args) =>
                {
                    cell.Confirm_Button.BackgroundColor = UIColor.FromRGB(0, 177, 87);                
                };

                cell.Decline_Button.TouchDown += (sender, args) =>
                {
                    cell.Decline_Button.BackgroundColor = UIColor.FromRGB(165, 53, 23);
                };
                cell.Decline_Button.TouchUpInside += (sender, args) =>
                {
                    cell.Decline_Button.BackgroundColor = UIColor.FromRGB(207, 52, 25);
                    var alert = new UIAlertView("Decline Friend request?", "Do you whant to Decline the friend request from " + cell.user.firstname + " " + cell.user.lastname + "?", null, NSBundle.MainBundle.LocalizedString("NO", "NO"), NSBundle.MainBundle.LocalizedString("YES", "YES"));
                    alert.Clicked += (object s, UIButtonEventArgs e) =>
                    {
                        if (_controller.AlertIsShown)
                        {
                            return;
                        }
                        _controller.AlertIsShown = true;
                        bool answer = (e.ButtonIndex != alert.CancelButtonIndex);
                        if (answer)
                        {
                            _viewModel.user_id = item.id.ToString();
                            _viewModel.user = item;
                            _viewModel.DeclineFriendRequestCommand.Execute(null);
                            _viewModel.RecivedFriendRequests.Remove(cell.user);
                            _controller.FriendRequestRecivedTableView.ReloadData();
                            _controller.AlertIsShown = false;
                        }
                        else
                        {
                            _controller.AlertIsShown = false;
                        }
                    };
                    alert.Show();

                };
                cell.Decline_Button.TouchUpOutside += (sender, args) =>
                {
                    cell.Decline_Button.BackgroundColor = UIColor.FromRGB(207, 52, 25);
                };
                cell.user = item;
                cell.Name = item.firstname + " " + item.lastname;
                if (!string.IsNullOrEmpty(item.image))
                {
                    cell.UiImage.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(new ImageUrlDownloader().GetImageBitmapFromUrl(item.image))));
                }
                else
                {
                    cell.UiImage.Image = UIImage.FromBundle("friends_ghost_image_blue");
                }
                cell.UiImage.Layer.CornerRadius = cell.UiImage.Frame.Size.Height / 2;
                cell.UiImage.Layer.MasksToBounds = true;
                return cell;
            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                var Trip = _viewModel.RecivedFriendRequests[indexPath.Row];               
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return _viewModel.RecivedFriendRequests.Count;
            }
        }



        internal class FriendRequestSentTableSource : UITableViewSource
        {
            private FriendViewModel _viewModel;
            private string CellIdentifier = "FriendRequestSentCell";
            private FriendRequestsViewControlller _controller;

            public FriendRequestSentTableSource(FriendViewModel viewModel, FriendRequestsViewControlller controller)
            {
                _viewModel = viewModel;
                _controller = controller;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                FriendRequestSentCell cell = tableView.DequeueReusableCell(CellIdentifier) as FriendRequestSentCell;
                var item = _viewModel.SentFriendRequests[indexPath.Row];

                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier) as FriendRequestSentCell;
                }
                cell.Cancel_button.TouchDown += (sender, args) =>
                {
                    cell.Cancel_button.BackgroundColor = UIColor.FromRGB(165, 55, 23);
                };

                cell.Cancel_button.TouchUpInside += (sender, args) =>
                {
                    if (_controller.AlertIsShown)
                    {
                        return;
                    }
                    _controller.AlertIsShown = true;
                    cell.Cancel_button.BackgroundColor = UIColor.FromRGB(207, 54, 25);
                    var alert = new UIAlertView("Cancel Friend request", "Do you whant to cancel the friend request to " + cell.user.firstname + " " + cell.user.lastname + "?", null, NSBundle.MainBundle.LocalizedString("NO", "NO"), NSBundle.MainBundle.LocalizedString("YES", "YES"));
                    alert.Clicked += (object s, UIButtonEventArgs e) =>
                    {                       
                        bool answer = (e.ButtonIndex != alert.CancelButtonIndex);
                        if (answer)
                        {
                            _viewModel.user_id = cell.user.id.ToString();
                            _viewModel.user = cell.user;
                            _viewModel.DeleteFriendCommand.Execute(null);
                            _viewModel.SentFriendRequests.Remove(cell.user);
                            _controller.FriendRequestSentTableView.ReloadData();
                            _controller.AlertIsShown = false;
                        }
                        else
                        {
                            _controller.AlertIsShown = false;
                        }
                    };
                    alert.Show();

                   
                };

                cell.Cancel_button.TouchUpOutside += (sender, args) =>
                {
                    cell.Cancel_button.BackgroundColor = UIColor.FromRGB(207, 54, 25);
                };
                cell.Name = item.firstname + " " + item.lastname;
                cell.user = item;
                if (!string.IsNullOrEmpty(item.image))
                {
                    cell.UiImage.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(new ImageUrlDownloader().GetImageBitmapFromUrl(item.image))));
                }
                else
                {
                    cell.UiImage.Image = UIImage.FromBundle("friends_ghost_image_blue");
                }
                cell.UiImage.Layer.CornerRadius = cell.UiImage.Frame.Size.Height / 2;
                cell.UiImage.Layer.MasksToBounds = true;
                return cell;
            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                var Trip = _viewModel.SentFriendRequests[indexPath.Row];
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return _viewModel.SentFriendRequests.Count;
            }
        }
    }
}