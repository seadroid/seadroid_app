using Foundation;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using CoreGraphics;
using fifi.Client.DataObjects;
using fifi.iOS.Annotations;
using fifi.iOS.Helpers;
using fifi.Utils.Helpers;
using fifi.ViewModel;
using Org.BouncyCastle.Asn1.Cms;
using UIKit;

namespace fifi.iOS
{
    public partial class FriendChatViewController : UIViewController
    {
       

        public override UIStoryboard Storyboard
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.Storyboard;

        private string _friendBase64Image;
    
        private NSObject _willShowNotificationObserver;
        private NSObject _didShowNotificationObserver;
        private NSObject _willHideNotificationObserver;
        private NSObject _AppEnteredBackgroud;
        private NSIndexPath LastVisibleRow;
        private bool _loadingMoreMessages;
        private bool _hasSavedMessageInputFrame;
        private CGRect _originalMessageInputFrame;
        public ChatViewModel _viewModel { get; set; }
        public Friend Friend { get; set; }
        private bool _Keyboardisshown;
        private nfloat _currentKeyboardFrame;
        public Dictionary<NSIndexPath,nfloat> CellHeights { get; set; }

     


        public bool WasOpenedWithNotfication = false;
        private bool _canceled;

        public FriendChatViewController(IntPtr handle) : base(handle)
        {
        }

        public FriendChatViewController()
        {
        }

        public override void ViewDidLoad()
        {
            _viewModel = new ChatViewModel(Friend.id.ToString());
            Settings.Current.ActiveChatId = _viewModel.Channel;
            _viewModel.Messages.CollectionChanged += MessagesOnCollectionChanged;
            NavigationController.BarHideOnSwipeGestureRecognizer.Enabled = false;

         


            base.ViewDidLoad();
            _Keyboardisshown = false;
           
            
            _friendBase64Image = new ImageUrlDownloader().GetImageBitmapFromUrl(Friend.image);
            CellHeights = new Dictionary<NSIndexPath, nfloat>();
            FriendChatTableView.Source = new ChatTableView(_viewModel, this);
            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = Friend.firstname + " " + Friend.lastname,
                TextColor = UIColor.White
            };
            NavigationItem.TitleView = lblNavTitle;

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);

            if (WasOpenedWithNotfication)
            {
                NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
                    UIImage.FromFile("ic_clear.png"), UIBarButtonItemStyle.Plain, (sender, args) => {
                        DashboardViewController DasboardController = this.Storyboard.InstantiateViewController("dashboardViewController") as DashboardViewController;
                        this.NavigationController.PushViewController(DasboardController, true);
                    }), true);

            }
         
          
            FriendChatTableView.Delegate = new ChatDelegate(MessageTextView, this);
            FriendChatTableView.RowHeight = UITableView.AutomaticDimension;
            FriendChatTableView.EstimatedRowHeight = 40f;

        

            SendMessageButton.TouchUpInside += (sender, args) =>
            {
                var foundtext = false;
                var textlines = MessageTextView.Text.Split(Environment.NewLine.ToCharArray());

                var TextlistRemoveExtraLines = new List<string>();
                var cleanedTextlines = new List<string>();
                foreach (var textline in textlines)
                {
                    if (!foundtext && !string.IsNullOrEmpty(textline))
                    {
                        TextlistRemoveExtraLines.Add(textline);
                        foundtext = true;
                    }
                    else
                    {
                        if (foundtext)
                        {
                            TextlistRemoveExtraLines.Add(textline);
                        }

                    }
                }

                var cleanedForEmptyStartLines = string.Join("\n", TextlistRemoveExtraLines);
                var listexstraspaces = cleanedForEmptyStartLines.Split(' ');
                listexstraspaces = listexstraspaces.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                var newtext = string.Join(" ", listexstraspaces);

                if (newtext.Length > 0)
                {
                    _viewModel.Time = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);
                    _viewModel.MessageText = newtext;
                    _viewModel.SendMessageCommand.Execute(null);
                    MessageTextView.Text = string.Empty;
                    MessageTextView.Frame = _originalMessageInputFrame;
                }

               
               
            };


            
            MessageTextView.Changed += (sender, args) =>
            {
                if (!_hasSavedMessageInputFrame)
                {
                    _originalMessageInputFrame = MessageTextView.Frame;
                    _hasSavedMessageInputFrame = true;
                }

                var frame = MessageTextView.Frame;
                frame.Y = frame.Bottom - MessageTextView.ContentSize.Height;
                frame.Height = MessageTextView.ContentSize.Height;
                MessageTextView.Frame = frame;
            };

            FriendChatTableView.RefreshControl = new UIRefreshControl();
            FriendChatTableView.RefreshControl.ValueChanged += (sender, args) =>
            {
                _viewModel.LoadMoreMessagesCommand.Execute(null);
                FriendChatTableView.RefreshControl.EndRefreshing();
            };

            FriendChatTableView.RefreshControl = new UIRefreshControl();
            
            

            FriendChatTableView.RefreshControl.PrimaryActionTriggered += (sender, args) =>
            {
                _loadingMoreMessages = true;
                _viewModel.LoadMoreMessagesCommand.Execute(null);
                FriendChatTableView.RefreshControl.EndRefreshing();
             
            };

            _loadingMoreMessages = false;

            
        }

        private void AppEnteredBackgroud(NSNotification notification)
        {
            NavigationController.PopViewController(true);
        }

       

        private void FriendChatTableViewOnScrolled(object sender, EventArgs eventArgs)
        {
        }

        private void MessagesOnCollectionChanged(object sender,
            NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            InvokeOnMainThread(delegate
            {
                
                if (_loadingMoreMessages)
                {
                    FriendChatTableView.ReloadData();
                    //NSIndexPath path = NSIndexPath.FromRowSection(10, 0);
                    //FriendChatTableView.ScrollToRow(path, UITableViewScrollPosition.Bottom, true);
                    // FriendChatTableView.ScrollToRow(FriendChatTableView.IndexPathsForVisibleRows.Last(), UITableViewScrollPosition.Bottom, true);
                    _loadingMoreMessages = false;
                }
                else
                {

                    if (_viewModel.Messages.Count > 0)
                    {
                        var offset = FriendChatTableView.ContentOffset;
                        FriendChatTableView.ReloadData();
                        var a = FriendChatTableView.ContentOffset;
                        FriendChatTableView.ContentOffset = offset;
                        //FriendChatTableView.ScrollToRow(FriendChatTableView.IndexPathsForVisibleRows.Last(), UITableViewScrollPosition.Bottom, true);
                        //FriendChatTableView.SetContentOffset(new CGPoint(0, FriendChatTableView.ContentSize.Height), true);
                        //int lastrow = (int)FriendChatTableView.NumberOfRowsInSection(0) -1;
                        NSIndexPath path = NSIndexPath.FromRowSection(_viewModel.Messages.Count - 1, 0);
                        FriendChatTableView.ScrollToRow(path, UITableViewScrollPosition.Bottom, true);
                        _loadingMoreMessages = false;
                    }
                  

                 //   FriendChatTableView.SetContentOffset(new CGPoint(0, FriendChatTableView.ContentSize.Height), true);
                }
            


            });
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            
            _viewModel.StartListeningForMessagesCommand.Execute(null);

            _AppEnteredBackgroud = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidEnterBackgroundNotification, AppEnteredBackgroud);
            _willShowNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(
                UIKeyboard.WillShowNotification, KeyBoardwillShow);
            _didShowNotificationObserver =
                NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, KeyBoardDidShow);
            _willHideNotificationObserver =
                NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, KeyBoardWillHide);
        }

        

        public override void ViewDidDisappear(bool animated)
        {

            base.ViewWillDisappear(animated);

            Settings.Current.ActiveChatId = "";
            _viewModel.StopListeningForMessagesCommand.Execute(null);
            if (_didShowNotificationObserver != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_willShowNotificationObserver);
            }

            if (_willHideNotificationObserver != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_willHideNotificationObserver);
            }
            if (_AppEnteredBackgroud != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_AppEnteredBackgroud);
            }
            
        }




        private void KeyBoardwillShow(NSNotification notification)
        {
            if (!_Keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height - keyboardHeight;
                this.View.Frame = NewFrame;
                _currentKeyboardFrame = keyboardHeight;
            }
            else
            {
                var HeightDifference = UIKeyboard.BoundsFromNotification(notification).Height - _currentKeyboardFrame;
                _currentKeyboardFrame = UIKeyboard.BoundsFromNotification(notification).Height;
                var newFrame = this.View.Frame;
                newFrame.Height = newFrame.Height - HeightDifference;
                this.View.Frame = newFrame;

            }
        }

        private void KeyBoardDidShow(NSNotification notification)
        {
            if (_viewModel.Messages.Count > 0)
            {
                NSIndexPath path = NSIndexPath.FromRowSection(_viewModel.Messages.Count - 1, 0);
                FriendChatTableView.ScrollToRow(path, UITableViewScrollPosition.Bottom, false);
            }         
            _Keyboardisshown = true;
        }

        private void KeyBoardWillHide(NSNotification notification)
        {
            if (_Keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, System.IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height + keyboardHeight;
                this.View.Frame = NewFrame;
                _Keyboardisshown = false;
            }          
        }

        internal class ChatTableView : UITableViewSource
        {
            private ChatViewModel _viewModel;
            private FriendChatViewController _controller;


            public ChatTableView(ChatViewModel viewModel, FriendChatViewController controller)
            {
                _controller = controller;
                _viewModel = viewModel;
            }


            //public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
            //{
            //    var item = _viewModel.Messages[indexPath.Row];
            //    var LinesArrys = item.Text.Split('\n');


            //    return 44 + 7 * LinesArrys.Length;
            //}

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                var item = _viewModel.Messages[indexPath.Row];

                if (item.Sender_id == Settings.Current.UserId)
                {
                    RightChatCell cell = tableView.DequeueReusableCell("RightChatCell") as RightChatCell;


                    if (cell == null)
                    {
                        cell = new UITableViewCell(UITableViewCellStyle.Default, "RightChatCell") as RightChatCell;
                    }

                    cell._Name = Settings.Current.UserFirstname + " " + Settings.Current.UserLastname;
                    cell._Text.Text = item.Text;


                    cell.MessageDate = Convert.ToString(TimeZoneInfo.ConvertTimeFromUtc(item.Date, TimeZoneInfo.Local));

                    //var DateArrey = item.Date.Split(' ');
                    //var date = DateArrey[0].Split('/');
                    //var messsageDateTime = new DateTime(Convert.ToInt16(date[2]), Convert.ToInt16(date[0]), Convert.ToInt16(date[1]));

                    //var Time = DateArrey[1].Split(':');

                    //if (DateArrey[2] == "PM")
                    //{
                    //    Time[0] = (Convert.ToInt16(Time[0]) + 12).ToString();
                    //}


                    //if (messsageDateTime == DateTime.Today)
                    //{
                    //    cell.MessageDate = "Today" + date[2] + " " + Time[0] + ":" + Time[1];
                    //}
                    //else
                    //{
                    //    cell.MessageDate = date[1] + "/" + date[0] + "/" + date[2] + " " + Time[0] + ":" + Time[1];
                    //}
                    _controller.CellHeights.Remove(indexPath);
                    _controller.CellHeights.Add(indexPath, cell.Frame.Height);

                    return cell;
                }
                else
                {
                    LeftChatCell cell = tableView.DequeueReusableCell("LeftChatCell") as LeftChatCell;

                    if (cell == null)
                    {
                        cell = new UITableViewCell(UITableViewCellStyle.Default, "LeftChatCell") as LeftChatCell;
                    }
                    


                    cell.TextView.Text = item.Text;
                    cell._Name = _controller.Friend.firstname + " " + _controller.Friend.lastname;



                    try
                    {
                        cell.MessageDate = Convert.ToString(TimeZoneInfo.ConvertTimeFromUtc(item.Date, TimeZoneInfo.Local));
                    }
                    catch (Exception e)
                    {
                        cell.MessageDate = Convert.ToString(item.Date);

                    }



                    //var DateArrey = item.Date.Split(' ');
                    //var date = DateArrey[0].Split('/');
                    //var messsageDateTime = new DateTime(Convert.ToInt16(date[2]), Convert.ToInt16(date[0]), Convert.ToInt16(date[1]));

                    //var Time = DateArrey[1].Split(':');

                    //if (DateArrey[2] == "PM")
                    //{
                    //    Time[0] = (Convert.ToInt16(Time[0]) + 12).ToString();
                    //}


                    //if (messsageDateTime == DateTime.Today)
                    //{
                    //    cell.MessageDate = "Today" + date[2] + " " + Time[0] + ":" + Time[1];
                    //}
                    //else
                    //{
                    //    cell.MessageDate = date[1] + "/" + date[0] + "/" + date[2] + " " + Time[0] + ":" + Time[1];
                    //}

                    if (!string.IsNullOrEmpty(_controller.Friend.image))
                    {
                        cell._Image.Image =
                            UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(_controller._friendBase64Image)));
                    }
                    else
                    {
                        cell._Image.Image = UIImage.FromBundle("friends_ghost_image_blue");
                    }
                    cell._Image.Layer.CornerRadius = cell._Image.Frame.Size.Height / 2;
                    cell._Image.Layer.MasksToBounds = true;

                    _controller.CellHeights.Remove(indexPath);
                    _controller.CellHeights.Add(indexPath, cell.Frame.Height);

                    return cell;
                }

                
            }


            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                
            }

            

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return _viewModel.Messages.Count;
            }
        }


        internal class ChatDelegate : UITableViewDelegate
        {
            public IntPtr Handle { get; }
            private UITextView TextField;
            private FriendChatViewController _controller;

            public ChatDelegate(UITextView textField, FriendChatViewController controller)
            {
                TextField = textField;
                _controller = controller;
            }


            public override nfloat EstimatedHeight(UITableView tableView, NSIndexPath indexPath)
            {
                if (_controller.CellHeights.ContainsKey(indexPath))
                {
                    return _controller.CellHeights[indexPath];
                }
                return UITableView.AutomaticDimension;



            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

           

            public override void DraggingStarted(UIScrollView scrollView)
            {
                TextField.ResignFirstResponder();
            }
        }

       
    }
}