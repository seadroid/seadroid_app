using Foundation;
using System;
using System.ComponentModel;
using System.Threading;
using CoreGraphics;
using fifi.iOS.Helpers;
using UIKit;
using fifi.ViewModel;
using SeaDroid.iOS;

namespace fifi.iOS
{
    public partial class MasterPairingViewController : UIViewController
    {
        public MasterPairingViewController (IntPtr handle) : base (handle)
        {
        }

        #region Buttons    
      
        private UIButton _errorButton;
        private UIButton _ContinueButton;
        private MasterPairingViewModel _viewModel;
        private NSObject _NotifiyDone;

        #endregion

        public  bool StartedByMastersController { get; set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            if (_viewModel == null)
            {
                _viewModel = new MasterPairingViewModel();
            }
           


            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;

            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "Master Pairing",
                TextColor = UIColor.White
            };
            NavigationItem.TitleView = lblNavTitle;

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);

            _errorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor =  "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };
            _errorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _ContinueButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "9FFFFF".ToUiColor(),
                UserInteractionEnabled = true
            };

            _ContinueButton.Frame = new CGRect(0.0f, 0.0f, MasterSerialText.Frame.Size.Height, MasterSerialText.Frame.Size.Height);
            _ContinueButton.ContentMode = UIViewContentMode.Center;

            _errorButton.Frame = new CGRect(0.0f, 0.0f, MasterSerialText.Frame.Size.Height,
                MasterSerialText.Frame.Size.Height);
            _errorButton.ContentMode = UIViewContentMode.Center;
            MasterSerialText.RightViewMode = UITextFieldViewMode.Always;
            MasterSerialText.RightView = _errorButton;
            MasterSerialText.Layer.BorderWidth = 1f;
            MasterSerialText.Layer.BorderColor = UIColor.White.CGColor;

            MasterSerialText.EditingDidBegin += (sender, args) =>
            {
                MasterSerialText.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();

            };

            MasterSerialText.EditingChanged += (sender, args) =>
            {
                if (!VerifySerial()) return;;
               
            };

            MasterSerialText.EditingDidEndOnExit += (sender, args) =>
            {
                MasterSerialText.BackgroundColor = UIColor.Clear;

            };


            _ContinueButton.SetImage(UIImage.FromBundle("ic_continue"), UIControlState.Normal);
            _ContinueButton.TouchUpInside += (sender, args) =>
            {
                _viewModel.Serial = MasterSerialText.Text;
                _viewModel.PairMasterCommand.Execute(null);
            };
        }

        private void ViewModelOnPropertyChanged(object o, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (StartedByMastersController)
            {
                CreateBoatViewController MasterPairing = this.Storyboard.InstantiateViewController("CreateBoatViewController") as CreateBoatViewController;
                MasterPairing.StartedByMastersController = true;
                MasterPairing._viewModel = new CreateBoatViewModel();
                MasterPairing._viewModel.MasterSerial = MasterSerialText.Text;
                MasterPairing._viewModel.VerifyingAccount = false;

                _NotifiyDone = NSNotificationCenter.DefaultCenter.AddObserver(new Foundation.NSString("Canceld"), popview);
                NavigationController.PushViewController(MasterPairing, true);
            }
            else
            {
                CreateBoatViewController MasterPairing = this.Storyboard.InstantiateViewController("CreateBoatViewController") as CreateBoatViewController;
                MasterPairing._viewModel = new CreateBoatViewModel();
                MasterPairing._viewModel.MasterSerial = MasterSerialText.Text;
                NavigationController.PushViewController(MasterPairing, true);
            }
            
              
              
        }

        private void popview(NSNotification notificaiton)
        {
            NSNotificationCenter.DefaultCenter.RemoveObserver(_NotifiyDone);
            NavigationController.PopViewController(false);
        }

        private bool VerifySerial()
        {
            if (MasterSerialText.Text.Length > 0)
            {

                MasterSerialText.RightViewMode = UITextFieldViewMode.Always;
                MasterSerialText.RightView = _ContinueButton;
                return true;
            }

            MasterSerialText.RightViewMode = UITextFieldViewMode.Always;
            MasterSerialText.RightView = _errorButton;
            return false;
        }
    }
}