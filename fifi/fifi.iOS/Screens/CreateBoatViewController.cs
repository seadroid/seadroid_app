using Foundation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Acr.UserDialogs;
using CoreGraphics;
using fifi.iOS;
using fifi.iOS.Helpers;
using fifi.iOS.MenuControllers;
using fifi.ViewModel;
using SidebarNavigation;
using UIKit;
using Wapps.TOCrop;


namespace SeaDroid.iOS
{
    public partial class CreateBoatViewController : UIViewController
    {
        #region Buttons
        private UIButton _boatNameOkButton;
        private UIButton _boatNameErrorButton;
        private UIButton _modelOkButton;
        private UIButton _modelErrorButton;
        private UIButton _productionYearOkButton;
        private UIButton _productionYearErrorButton;
        private UIButton _lengthOkButton;
        private UIButton _lengthErrorButton;
        private UIButton _colorOkButton;
        private UIButton _colorErrorButton;
        private UIButton _registryNumberOkButton;
        private UIButton _registryNumberErrorButton;
        private UIButton _HarborOkButton;
        private UIButton _HarborErrorButton;
        private UIButton _boatTypeOkButton;
        private UIButton _boatTypeErrorButton;
        private UIButton _countryOkButton;
        private UIButton _countryErrorButton;     
        #endregion

        private int SelectedCountry;
        private int SelectedBoatType;
  
        private NSObject _willShowNotificationObserver;
        private NSObject _willHideNotificationObserver;
        private bool _Keyboardisshown;
        private nfloat _currentKeyboardFrame;
        private UIImagePickerController _imagePicker;
        private bool _imageChosen;
        public CreateBoatViewModel _viewModel;

        public bool StartedByMastersController { get; set; }

        public bool ImageChosen
        {
            get { return _imageChosen; }
            set
            {
                _imageChosen = value;
                if (!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;
                if (!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;
                if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;
                if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
                if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.UserInteractionEnabled = true;
            }
        }

        public CreateBoatViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "Create Boat",
                TextColor = UIColor.White
            };
            NavigationItem.TitleView = lblNavTitle;

            SaveButton.SetTitle("Missing Information", UIControlState.Normal);

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);

            ImageChosen = false;
            SaveButton.UserInteractionEnabled = false;
            SaveButton.BackgroundColor = UIColor.FromRGB(165, 55, 23);

            BoatImange.AddGestureRecognizer(new UITapGestureRecognizer(OnImageClick));
            BoatImange.UserInteractionEnabled = true;
            BoatImange.Layer.CornerRadius = BoatImange.Frame.Size.Height / 2;
            BoatImange.Layer.MasksToBounds = true;
            if (_viewModel == null)
            {
                _viewModel = new CreateBoatViewModel();
            }

            _imagePicker = new UIImagePickerController();

            SaveButton.TouchUpInside += SaveButtonOnTouchUpInside;

            #region TextFieldInit

            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
    
           

        

            UIBarButtonItem backButton = new UIBarButtonItem("Cancel", UIBarButtonItemStyle.Bordered, handleback);
            this.NavigationItem.LeftBarButtonItem = backButton;
            #region BorderInit
            TextBoatName.Layer.BorderWidth = 1f;
            TextBoatName.Layer.BorderColor = UIColor.White.CGColor;

            TextCountry.Layer.BorderWidth = 1f;
            TextCountry.Layer.BorderColor = UIColor.White.CGColor;

            TextBoatType.Layer.BorderWidth = 1f;
            TextBoatType.Layer.BorderColor = UIColor.White.CGColor;

            TextModel.Layer.BorderWidth = 1f;
            TextModel.Layer.BorderColor = UIColor.White.CGColor;

            TextProductionYear.Layer.BorderWidth = 1f;
            TextProductionYear.Layer.BorderColor = UIColor.White.CGColor;

            TextBoatLenght.Layer.BorderWidth = 1f;
            TextBoatLenght.Layer.BorderColor = UIColor.White.CGColor;

            TextBoatColor.Layer.BorderWidth = 1f;
            TextBoatColor.Layer.BorderColor = UIColor.White.CGColor;

            TextHarbor.Layer.BorderWidth = 1f;
            TextHarbor.Layer.BorderColor = UIColor.White.CGColor;

            TextRegister_No.Layer.BorderWidth = 1f;
            TextRegister_No.Layer.BorderColor = UIColor.White.CGColor;


            #endregion

            #region Button Init

            _boatTypeOkButton =new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _boatTypeOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _boatTypeErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _boatTypeErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _countryOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _countryOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _countryErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _countryErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);



            _boatNameOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _boatNameOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _boatNameErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _boatNameErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _modelOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _modelOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _modelErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _modelErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _productionYearOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _productionYearOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _productionYearErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _productionYearErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _lengthOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _lengthOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _lengthErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _lengthErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _colorOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _colorOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _colorErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _colorErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _registryNumberOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _registryNumberOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _registryNumberErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _registryNumberErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            _HarborOkButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "8ADB74".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _HarborOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _HarborErrorButton =
                new UIButton(UIButtonType.Custom)
                {
                    BackgroundColor = "F93334".ToUiColor(),
                    UserInteractionEnabled = false
                };
            _HarborErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

            #endregion

            #region InitButtonsInThextFields

            setButton(TextBoatName, _boatNameErrorButton);
            setButton(TextModel, _modelErrorButton);
            setButton(TextProductionYear, _productionYearErrorButton);
            setButton(TextBoatLenght, _lengthErrorButton);
            setButton(TextBoatColor, _colorErrorButton);
            setButton(TextRegister_No, _registryNumberErrorButton);
            setButton(TextHarbor, _HarborErrorButton);
            setButton(TextBoatType, _boatTypeErrorButton);
            setButton(TextCountry, _countryErrorButton);
            #endregion

            #region TextPropertyChangedInit


            TextBoatType.EditingDidBegin += (sender, args) =>
            {
                TextBoatType.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
                if (string.IsNullOrEmpty(TextBoatType.Text))
                {
                   TextBoatType.InsertText("Sail");
                    SelectedBoatType = 1;
                    if (!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;
                    if (!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;
                    if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;
                    if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
                    if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                    if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                    if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                    if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                    if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                    if(!ImageChosen) return;
                    SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                    SaveButton.SetTitle("Save", UIControlState.Normal);
                    SaveButton.UserInteractionEnabled = true;

                }
            };

            TextBoatType.EditingChanged += (sender, args) =>
            {
                if(!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;
                if(!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;                
                if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;
                if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
                if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                if (!ImageChosen) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.SetTitle("Save", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = true;
            };

            TextBoatType.EditingDidEnd += (sender, args) =>
            {
                TextBoatType.BackgroundColor = UIColor.Clear;
               
            };


            TextCountry.EditingDidBegin += (sender, args) =>
            {
                TextCountry.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
                if (string.IsNullOrEmpty(TextCountry.Text))
                {
                    TextCountry.InsertText("Denmark");
                    SelectedCountry = 1;
                    if (!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;
                    if (!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;
                    if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;
                    if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
                    if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                    if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                    if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                    if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                    if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                    if (!ImageChosen) return;
                    SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                    SaveButton.SetTitle("Save", UIControlState.Normal);
                    SaveButton.UserInteractionEnabled = true;
                }
            };

            TextCountry.EditingChanged += (sender, args) =>
            {
                if (!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;
                if (!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;               
                if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;
                if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
                if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                if (!ImageChosen) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.SetTitle("Save", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = true;
            };

            TextCountry.EditingDidEnd += (sender, args) =>
            {
                TextCountry.BackgroundColor = UIColor.Clear;
            };

            TextBoatName.EditingDidBegin += (sender, args) =>
            {
                TextBoatName.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            };

            TextBoatName.EditingChanged += (sender, args) =>
            {
                if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;
                if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
                if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                if (!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;
                if (!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;
                if (!ImageChosen) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.SetTitle("Save", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = true;
            };

            TextBoatName.EditingDidEnd += (sender, args) =>
            {
                TextBoatName.BackgroundColor = UIColor.Clear;
            };

            TextModel.EditingDidBegin += (sender, args) =>
            {
                TextModel.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            };

            TextModel.EditingChanged += (sender, args) =>
            {
                if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
               if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;           
                if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                if (!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;
                if (!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;
                if (!ImageChosen) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.SetTitle("Save", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = true;
            };

            TextModel.EditingDidEnd += (sender, args) =>
            {
                TextModel.BackgroundColor = UIColor.Clear;
            };
         
            TextProductionYear.EditingDidBegin += (sender, args) =>
            {
                TextProductionYear.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            };

            TextProductionYear.EditingChanged += (sender, args) =>
            {
                if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
                if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;              
                if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                if (!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;
                if (!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;
                if (!ImageChosen) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.SetTitle("Save", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = true;
            };

            TextProductionYear.EditingDidEnd += (sender, args) =>
            {
                TextProductionYear.BackgroundColor = UIColor.Clear;
            };
           

            TextBoatLenght.EditingDidBegin += (sender, args) =>
            {
                TextBoatLenght.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            };

            TextBoatLenght.EditingChanged += (sender, args) =>
            {
                if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
                if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;            
                if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                if (!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;
                if (!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;
                if (!ImageChosen) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.SetTitle("Save", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = true;
            };

            TextBoatLenght.EditingDidEnd += (sender, args) =>
            {
                TextBoatLenght.BackgroundColor = UIColor.Clear;
            };
        

            TextBoatColor.EditingDidBegin += (sender, args) =>
            {
                TextBoatColor.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            };

            TextBoatColor.EditingChanged += (sender, args) =>
            {
                if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
                if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;               
                if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                if (!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;
                if (!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;
                if (!ImageChosen) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.SetTitle("Save", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = true;
            };

            TextBoatColor.EditingDidEnd += (sender, args) =>
            {
                TextBoatColor.BackgroundColor = UIColor.Clear;
            };


            TextRegister_No.EditingDidBegin += (sender, args) =>
            {
                TextRegister_No.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            };

            TextRegister_No.EditingChanged += (sender, args) =>
            {
                if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
                if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;            
                if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                if (!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;
                if (!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;
                if (!ImageChosen) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                SaveButton.SetTitle("Save", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = true;
            };

            TextRegister_No.EditingDidEnd += (sender, args) =>
            {
                TextRegister_No.BackgroundColor = UIColor.Clear;
            };


            TextHarbor.EditingDidBegin += (sender, args) =>
            {
                TextHarbor.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
            };

            TextHarbor.EditingChanged += (sender, args) =>
            {
                if (!ValidateText(TextHarbor, _HarborErrorButton, _HarborOkButton)) return;
                if (!ValidateText(TextRegister_No, _registryNumberErrorButton, _registryNumberOkButton)) return;
                if (!ValidateText(TextBoatColor, _colorErrorButton, _colorOkButton)) return;
                if (!ValidateSmallInt(TextBoatLenght, _lengthErrorButton, _lengthOkButton)) return;
                if (!ValidateCheckIfYear(TextProductionYear, _productionYearErrorButton, _productionYearOkButton)) return;
                if (!ValidateText(TextModel, _modelErrorButton, _modelOkButton)) return;
                if (!ValidateText(TextBoatName, _boatNameErrorButton, _boatNameOkButton)) return;
                if (!ValidateText(TextCountry, _countryErrorButton, _countryOkButton)) return;
                if (!ValidateText(TextBoatType, _boatTypeErrorButton, _boatTypeOkButton)) return;
                if (!ImageChosen) return;
                SaveButton.BackgroundColor = UIColor.FromRGB(0, 177, 87);
                    SaveButton.SetTitle("Save", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = true;
            };

            TextHarbor.EditingDidEnd += (sender, args) =>
            {
                TextHarbor.BackgroundColor = UIColor.Clear;
            };

            #endregion

            #region SpinnerInit


            var LanguagePickerModel =
                new ListPickerModel(new List<string>
                {
                    "Denmark",
                    "Sweden",
                    "Norway",
                    "Findland",
                    "Germany",                                                    
                    "United Kingdom",
                });
            LanguagePickerModel.PropertyChanged += (sender, args) =>
            {
                TextCountry.Text = "";
                TextCountry.InsertText(LanguagePickerModel.selectedItem);
                SelectedCountry = LanguagePickerModel.SelectedItemRow + 1;
            };
            var LanguagePicker = new UIPickerView() { Model = LanguagePickerModel, BackgroundColor = "#80FFFFFF".ToUiColorAlpha()
            };
            TextCountry.InputView = LanguagePicker;

            var BoatTypePickerModel = new ListPickerModel(new List<string>
            {
                "Sail",
                "SpeedBoat",
                "Kayak",                
            });

            BoatTypePickerModel.PropertyChanged += (sender, args) =>
            {
                TextBoatType.Text = "";
                TextBoatType.InsertText(BoatTypePickerModel.selectedItem);
                SelectedBoatType  = BoatTypePickerModel.SelectedItemRow + 1;
            };
            var BoatTypePicker = new UIPickerView() {Model = BoatTypePickerModel, BackgroundColor = "#80FFFFFF".ToUiColorAlpha()
            };
            TextBoatType.InputView = BoatTypePicker;

            #endregion

            #endregion

          ;

            base.ViewDidLoad();
        }

        private void ViewModelOnPropertyChanged(object o, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (_viewModel.NavigateToNext)
            {
                var app = (AppDelegate)UIApplication.SharedApplication.Delegate;
                app.Window.RootViewController = new RootViewController();
            }


            if (StartedByMastersController)
            {
                if (_viewModel.BoatCreated)
                {
                    NavigationController.PopViewController(false);
                    NSNotificationCenter.DefaultCenter.PostNotificationName("Canceld", this);
                    NSNotificationCenter.DefaultCenter.PostNotificationName("RefreshMasters", this);
                }
                else
                {
                    NavigationController.PopViewController(true);
                    NSNotificationCenter.DefaultCenter.PostNotificationName("Canceld", this);
                }
               
            }
            else
            {
                var app = (AppDelegate)UIApplication.SharedApplication.Delegate;
                var loginViewController = UIStoryboard.FromName("Main", null)
                    .InstantiateViewController("loginViewController");
                app.Window.RootViewController = loginViewController;
            }
        }

        private void handleback(object sender, EventArgs e)
        {
            
           _viewModel.CancelMasterPairing.Execute(null);

           
        }

        private void SaveButtonOnTouchUpInside(object o, EventArgs eventArgs)
        {
            _viewModel.Boatname = TextBoatName.Text;
            _viewModel.Category_id = SelectedBoatType;
            _viewModel.Country_id = SelectedCountry;
            _viewModel.Model = TextModel.Text;
            _viewModel.Color = TextBoatColor.Text;
            _viewModel.BoatLength = TextBoatLenght.Text;
            _viewModel.Harbor = TextHarbor.Text;
            _viewModel.Registry_no = TextRegister_No.Text;
            _viewModel.ProductionYear = Convert.ToInt32(TextProductionYear.Text);
      
          

            _viewModel.CreateBoatCommand.Execute(null);

       
        }

        private void OnImageClick()
        {
            UIAlertController actionSheetAlert =
                UIAlertController.Create("Pick Boat Image", "", UIAlertControllerStyle.ActionSheet);


            actionSheetAlert.AddAction(UIAlertAction.Create("Camara", UIAlertActionStyle.Default, (action) =>
            {
                try
                {
                    _imagePicker.Delegate = new CameraDelegate(this);
                    _imagePicker.SourceType = UIImagePickerControllerSourceType.Camera;
                    NavigationController.PresentViewController(_imagePicker, true, null);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }

            }));

            actionSheetAlert.AddAction(UIAlertAction.Create("Gallery", UIAlertActionStyle.Default, (action) =>
            {
                try
                {
                    _imagePicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                    _imagePicker.MediaTypes = UIImagePickerController.AvailableMediaTypes((UIImagePickerControllerSourceType.PhotoLibrary));
                    _imagePicker.FinishedPickingMedia += ImagePickerOnFinishedPickingMedia;
                    _imagePicker.Canceled += ImagePickerOnCanceled;
                    NavigationController.PresentModalViewController(_imagePicker, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }

            }));

            actionSheetAlert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel,
                (action) => Console.WriteLine("Cancel button pressed.")));
            this.PresentViewController(actionSheetAlert, true, null);
        }

        private void setButton( UITextField textField, UIButton button)
        {
            button.Frame = new CGRect(0.0f, 0.0f, textField.Frame.Size.Height, textField.Frame.Size.Height);
            button.ContentMode = UIViewContentMode.Center;
            textField.RightViewMode = UITextFieldViewMode.Always;
            textField.RightView = button;
        }
     

        private bool ValidateText(UITextField text, UIButton ErrorButton, UIButton OkButton)
        {
            if (!string.IsNullOrEmpty(text.Text))
            {
                if (text.RightView != OkButton)
                {
                    OkButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    OkButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = OkButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                return true;
            }
            else
            {
                if (text.RightView != ErrorButton)
                {
                    ErrorButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    ErrorButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = ErrorButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                SaveButton.SetTitle("Missing Information", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = false;
                SaveButton.BackgroundColor = UIColor.FromRGB(165, 55, 23);
                return false;
            }
        }


        private bool ValidateNumber(UITextField text, UIButton ErrorButton, UIButton OkButton)
        {
            if (CheckIfNumber(text.Text) && !string.IsNullOrEmpty(text.Text))
            {
                if (text.RightView != OkButton)
                {
                    OkButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    OkButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = OkButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                return true;
            }
            else
            {
                if (text.RightView != ErrorButton)
                {
                    ErrorButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    ErrorButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = ErrorButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                SaveButton.SetTitle("Missing Information", UIControlState.Normal); 
                SaveButton.UserInteractionEnabled = false;
                SaveButton.BackgroundColor = UIColor.FromRGB(165, 55, 23);
            }
            return false;
        }

        private bool ValidateSmallInt(UITextField text, UIButton ErrorButton, UIButton OkButton)
        {
            if (CheckIfNumber(text.Text) && !string.IsNullOrEmpty(text.Text) && CheckIfSmallInt(Convert.ToInt32(text.Text)))
            {
                if (text.RightView != OkButton)
                {
                    OkButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    OkButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = OkButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                return true;
            }
            else
            {
                if (text.RightView != ErrorButton)
                {
                    ErrorButton.Frame = new CGRect(0.0f, 0.0f, text.Frame.Size.Height,
                        text.Frame.Size.Height);
                    ErrorButton.ContentMode = UIViewContentMode.Center;
                    text.RightView = ErrorButton;
                    text.RightViewMode = UITextFieldViewMode.Always;
                }
                SaveButton.SetTitle("Missing Information", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = false;
                SaveButton.BackgroundColor = UIColor.FromRGB(165, 55, 23);
            }
            return false;
        }

        private bool CheckIfNumber(string text)
        {
            foreach (char c in text)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }

        private bool CheckIfSmallInt(int number)
        {
            if (number < 32767)
            {
                return true;
            }
            return false;
        }

        private bool ValidateCheckIfYear(UITextField textfield, UIButton ErrorButton, UIButton OkButton)
        {
            if (!string.IsNullOrEmpty(textfield.Text) && Convert.ToInt32(textfield.Text) > 1900 && Convert.ToInt32(textfield.Text) <  (DateTime.Now.Year+1)) 
            {
                if (textfield.RightView != OkButton)
                {
                    OkButton.Frame = new CGRect(0.0f, 0.0f, textfield.Frame.Size.Height,
                        textfield.Frame.Size.Height);
                    OkButton.ContentMode = UIViewContentMode.Center;
                    textfield.RightView = OkButton;
                    textfield.RightViewMode = UITextFieldViewMode.Always;                  
                }
                return true;
            }
            else
            {
                if (textfield.RightView != ErrorButton)
                {
                    ErrorButton.Frame = new CGRect(0.0f, 0.0f, textfield.Frame.Size.Height,
                        textfield.Frame.Size.Height);
                    ErrorButton.ContentMode = UIViewContentMode.Center;
                    textfield.RightView = ErrorButton;
                    textfield.RightViewMode = UITextFieldViewMode.Always;
                }
                SaveButton.SetTitle("Missing Information", UIControlState.Normal);
                SaveButton.UserInteractionEnabled = false;
                SaveButton.BackgroundColor = UIColor.FromRGB(165, 55, 23);
            }
            return false;
        }

       

        #region KeyboardHandling

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            _willShowNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver( UIKeyboard.WillShowNotification, KeyBoardwillShow);
            _willHideNotificationObserver =
                NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, KeyBoardWillHide);
            base.ViewDidAppear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_willShowNotificationObserver);
            }

            if (_willHideNotificationObserver != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_willHideNotificationObserver);
            }
            base.ViewDidDisappear(animated);
        }


        private void KeyBoardwillShow(NSNotification notification)
        {
            if (!_Keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height - keyboardHeight;
                this.View.Frame = NewFrame;
                _currentKeyboardFrame = keyboardHeight;
                _Keyboardisshown = true;
            }
            else
            {
                var HeightDifference = UIKeyboard.BoundsFromNotification(notification).Height - _currentKeyboardFrame;
                _currentKeyboardFrame = UIKeyboard.BoundsFromNotification(notification).Height;
                var newFrame = this.View.Frame;
                newFrame.Height = newFrame.Height - HeightDifference;
                this.View.Frame = newFrame;
                _Keyboardisshown = true;
            }
        }


        private void KeyBoardWillHide(NSNotification notification)
        {
            if (_Keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, System.IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height + keyboardHeight;
                this.View.Frame = NewFrame;
                _Keyboardisshown = false;
            }
        }


        private void ImagePickerOnCanceled(object o, EventArgs eventArgs)
        {
           _imagePicker.DismissViewController(true, null);
        }

        private void ImagePickerOnFinishedPickingMedia(object o, UIImagePickerMediaPickedEventArgs e)
        {


            bool isImage = false;
            switch (e.Info[UIImagePickerController.MediaType].ToString())
            {
                case "public.image":
                    Console.WriteLine("Image selected");



                    isImage = true;
                    break;
                case "public.video":
                    var alert = new UIAlertView("Error", "Can not use video as profile picture", null, NSBundle.MainBundle.LocalizedString("OK", "OK"));
                    alert.Show();
                    break;
            }


            _imagePicker.DismissModalViewController(true);

            var CropVC = new TOCropViewController(TOCropViewCroppingStyle.Default, e.OriginalImage);
            CropVC.Delegate = new CropVCDelegate(this);
            CropVC.AspectRatioPreset = TOCropViewControllerAspectRatioPreset.Square;
            CropVC.AspectRatioLockEnabled = true;

            NavigationController.PresentViewController(CropVC, true, null);
        }


        class CropVCDelegate : TOCropViewControllerDelegate
        {
            private CreateBoatViewController ViewController;
            public CropVCDelegate(CreateBoatViewController viewController)
            {
                ViewController = viewController;
            }


            public override void DidCropImageToRect(TOCropViewController cropViewController, CGRect cropRect, nint angle)
            {
                cropViewController.PresentingViewController.DismissViewController(true, null);
                var myImage = cropViewController.FinalImage;
                myImage = new ImageCropper().ResizeImage(myImage, 500, 500);
                NSData imageData = myImage.AsPNG();
                string Base64image = imageData.GetBase64EncodedString(NSDataBase64EncodingOptions.None);
                var imagesize = System.Text.ASCIIEncoding.ASCII.GetByteCount(Base64image) / 1024;
                var size = 500;
                while (imagesize > 1024)
                {
                    myImage = new ImageCropper().ResizeImage(myImage, size, size);
                    imageData = myImage.AsPNG();
                    Base64image = imageData.GetBase64EncodedString(NSDataBase64EncodingOptions.None);
                    imagesize = System.Text.ASCIIEncoding.ASCII.GetByteCount(Base64image) / 1024;
                    size = size - 50;
                }
                ViewController._viewModel.Base64EncodedImage = Base64image;
                ViewController.BoatImange.Image = myImage;
                ViewController.ImageChosen = true;
                //ViewController._viewModel.imagedata = Convert.FromBase64String(Base64image);
                //ViewController.ChangePending = true;

            }
        }

        class CameraDelegate : UIImagePickerControllerDelegate
        {
            private CreateBoatViewController Controller;

            public CameraDelegate(CreateBoatViewController controller)
            {
                Controller = controller;
            }

            public override void FinishedPickingMedia(UIImagePickerController picker, NSDictionary info)
            {
                picker.DismissModalViewController(true);
                var image = info.ValueForKey(new NSString("UIImagePickerControllerOriginalImage")) as UIImage;

                var CropVC = new TOCropViewController(TOCropViewCroppingStyle.Default, image);
                CropVC.Delegate = new CropVCDelegate(Controller);
                CropVC.AspectRatioPreset = TOCropViewControllerAspectRatioPreset.Square;
                CropVC.AspectRatioLockEnabled = true;
                Controller.NavigationController.PresentViewController(CropVC, true, null);

            }
        }
        #endregion
    }
}