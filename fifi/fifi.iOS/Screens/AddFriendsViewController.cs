using Foundation;
using System;
using System.ComponentModel;
using CoreGraphics;
using fifi.iOS.Helpers;
using UIKit;
using SidebarNavigation;
using fifi.Model;
using fifi.ViewModel;
using SeaDroid.iOS.Helpers;

namespace fifi.iOS
{
    public partial class AddFriendsViewController : UIViewController
    {
        private FriendViewModel _viewModel;

        protected SidebarController SidebarController
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.SidebarController;

        //protected new NavigationController NavigationController
        //    => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.NavigationController;

        public override UIStoryboard Storyboard
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.Storyboard;


        public AddFriendsViewController(IntPtr handle) : base(handle)
        {
            NavigationItem.SetLeftBarButtonItem(
                new UIBarButtonItem(UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain,
                    (sender, args) => { SidebarController.ToggleMenu(); }), true);
        }

        public override void ViewDidUnload()
        {
            base.ViewDidUnload();
            _viewModel.PropertyChanged -= ViewModel_propertyChanged;
            
        }


        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            AddFriendsButotn.BackgroundColor = UIColor.FromRGB(206, 208, 206);
            _viewModel = new FriendViewModel();
            AddFriendsTableView.Source = new AddFriendsTableSource(_viewModel, this);
            _viewModel.PropertyChanged += ViewModel_propertyChanged;
            _viewModel.LoadAllUsersCommand.Execute(null);

            AddFriendsSearchText.AttributedPlaceholder = new NSAttributedString("Search", font: UIFont.FromName("HelveticaNeueLTDMC_18", 20.0f), strokeColor: UIColor.White, foregroundColor: UIColor.White);

            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "     Friends",
                Font = UIFont.BoldSystemFontOfSize(19.0f),
                TextColor = UIColor.White
            };
            NavigationItem.TitleView = lblNavTitle;

ImageResizer resizer = new ImageResizer();
var image = UIImage.FromBundle("neg_three_men");
var ThreeMen = resizer.ResizeImage(UIImage.FromBundle("neg_three_men"), (float)image.Size.Height / (float)2.5, (float)image.Size.Width / (float)2.5);

var ThreeMenButton = new UIBarButtonItem((UIImage.FromBundle("ThreeMen"))
	, UIBarButtonItemStyle.Plain
	, (sender, args) =>
	{

	});
ThreeMenButton.Enabled = false;
            

            ThreeMenButton.Image = ThreeMen;
            NavigationItem.SetRightBarButtonItem(ThreeMenButton, false);

            AddFriendsSearchText.ShouldReturn += sender =>
            {
                AddFriendsSearchText.ResignFirstResponder();
                return true;
            };

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);
        }

        public void ViewModel_propertyChanged(object sender, PropertyChangedEventArgs e)
        {
            AddFriendsTableView.ReloadData();
        }

        partial void FriendsButtonPressed(UIButton sender)
        {
            FriendsViewController FriendsView =
                this.Storyboard.InstantiateViewController("FriendsViewController") as FriendsViewController;
            this.NavigationController.PushViewController(FriendsView, false);
            this.SidebarController.CloseMenu(true);
        }

        partial void FriendRequestButtonPressed(UIButton sender)
        {
            FriendRequestsViewControlller FriendRequestView =
                this.Storyboard.InstantiateViewController("FriendRequestsViewControlller") as
                    FriendRequestsViewControlller;
            this.NavigationController.PushViewController(FriendRequestView, false);
            this.SidebarController.CloseMenu(true);
        }

        partial void AddFriendsButtonPressed(UIButton sender)
        {
            return;
        }

        partial void SearchButtonPressed(UIButton sender)
        {
            _viewModel.AddFriendsListSortText = AddFriendsSearchText.Text;
            _viewModel.SortAddFriendsListCommnad.Execute(null);
            AddFriendsSearchText.ResignFirstResponder();
        }

        internal class AddFriendsTableSource : UITableViewSource
        {
            private FriendViewModel _viewModel;
            private string CellIdentifier = "AddFriendsListCell";
            private AddFriendsViewController _controller;

            public AddFriendsTableSource(FriendViewModel viewModel, AddFriendsViewController controller)
            {
                _viewModel = viewModel;
                _controller = controller;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                AddFriendsListCell cell = tableView.DequeueReusableCell(CellIdentifier) as AddFriendsListCell;
                var item = _viewModel.NotFriends[indexPath.Row];

                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier) as AddFriendsListCell;
                }
                cell.name = item.firstname + " " + item.lastname;
                cell.user = item;
           

                if (!string.IsNullOrEmpty(item.image))
                {
                    cell.UserImage.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(new ImageUrlDownloader().GetImageBitmapFromUrl(item.image))));
                }
                else
                {
                    cell.UserImage.Image = UIImage.FromBundle("friends_ghost_image_blue");
                }
                cell.UserImage.Layer.CornerRadius = cell.UserImage.Frame.Size.Height / 2;
                cell.UserImage.Layer.MasksToBounds = true;

                cell.Add_Friend_button.TouchDown += (sender, args) =>
                {
                    cell.Add_Friend_button.BackgroundColor = UIColor.FromRGB(0, 98, 157);
                };

                cell.Add_Friend_button.TouchUpInside += (sender, args) =>
                {
                    cell.Add_Friend_button.BackgroundColor = UIColor.FromRGB(54, 145, 219);
                    _viewModel.user_id = cell.user.id.ToString();
                    _viewModel.user = cell.user;
                    _viewModel.AddFriendCommand.Execute(null);
                    _viewModel.NotFriends.Remove(cell.user);
                    _controller.AddFriendsTableView.ReloadData(); 
                                   
                };

                cell.Add_Friend_button.TouchUpOutside += (sender, args) =>
                {
                    cell.Add_Friend_button.BackgroundColor = UIColor.FromRGB(54, 145, 219);
                };
                return cell;
            }


            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                var Trip = _viewModel.NotFriends[indexPath.Row];
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return _viewModel.NotFriends.Count;
            }
        }
    }
}