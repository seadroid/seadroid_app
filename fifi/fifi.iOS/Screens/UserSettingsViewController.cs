using Foundation;
using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using CoreAnimation;
using CoreGraphics;
using fifi.iOS.Helpers;
using fifi.Utils.Helpers;
using UIKit;
using fifi.ViewModel;
using Plugin.SecureStorage;
using SidebarNavigation;
using Wapps.TOCrop;

namespace fifi.iOS
{
    public partial class UserSettingsViewController : UIViewController
    {

        private NSObject _willShowNotificationObserver;        
        private NSObject _willHideNotificationObserver;
        private NSIndexPath LastVisibleRow;
        private bool _Keyboardisshown;
        private nfloat _currentKeyboardFrame;
        private ToastIos _Toast;
        private UIImage CurrentImage;
        private UserSettingsViewModel _viewModel;
        private UIImagePickerController _imagePicker;
        private MenuController _menuController;
        protected SidebarController SidebarController
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.SidebarController;

        //protected new NavigationController NavigationController
        //    => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.NavigationController;

        public override UIStoryboard Storyboard
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.Storyboard;

        public UserSettingsViewController(IntPtr handle) : base(handle)
        {
            NavigationItem.SetLeftBarButtonItem(
                new UIBarButtonItem(UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain,
                    (sender, args) =>
                    {
                        if (ChangePending)
                        {
                            var alert = new UIAlertView("Unsaved Changes", "Do you want to save changes made?", null, NSBundle.MainBundle.LocalizedString("NO", "NO"), NSBundle.MainBundle.LocalizedString("YES", "YES"));
                            alert.Clicked += (object sender2, UIButtonEventArgs e) =>
                            {
                                bool answer = (e.ButtonIndex != alert.CancelButtonIndex);
                                if (answer)
                                {
                                   UpdateUser();
                                 
                                }
                                else
                                {
                                    TextLastname.Text = Settings.Current.UserLastname;
                                    TextFirstname.Text = Settings.Current.UserFirstname;
                                    User_Imange.Image = CurrentImage;


                                    ChangePending = false;
                                }
                                SidebarController.ToggleMenu();
                                
                            };
                            alert.Show();

                        }
                        else
                        {
                            SidebarController.ToggleMenu();
                        }
                       
                    }), true);
        }

        private UIButton _FirstnameOkButton;
        private UIButton _FirstnameErrorButton;
        private UIButton _LastnameOkButton;
        private UIButton _LastnameErrorButton;
        private UIButton _EmailOkButton;
        private UIButton _EmailErrorButton;

        public bool ChangePending;

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            _viewModel = new UserSettingsViewModel();
            User_Imange.AddGestureRecognizer(new UITapGestureRecognizer(OnTap));
            User_Imange.UserInteractionEnabled = true;
            User_Imange.Layer.CornerRadius = User_Imange.Frame.Size.Height / 2;
            User_Imange.Layer.MasksToBounds = true;
            ChangePending = false;
            _imagePicker = new UIImagePickerController();
            _Toast = new ToastIos();

            ScrollView.ScrollEnabled = false;

            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "User Settings",
                TextColor = UIColor.White
            };


            TextFirstname.Text = _viewModel.UserProfile.Firstname;
            TextLastname.Text = _viewModel.UserProfile.Lastname;
            if (_viewModel.imagedata !=null)
            {            
                NSData data = NSData.FromArray(_viewModel.imagedata);
                User_Imange.Image = UIImage.LoadFromData(data);
                CurrentImage = User_Imange.Image;
            }

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);

            _FirstnameErrorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };
            _FirstnameErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);


            _LastnameErrorButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "F93334".ToUiColor(),
                UserInteractionEnabled = false
            };
            _LastnameErrorButton.SetImage(UIImage.FromBundle("ic_clear"), UIControlState.Normal);

         


            _FirstnameOkButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _FirstnameOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

            _LastnameOkButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "8ADB74".ToUiColor(),
                UserInteractionEnabled = false
            };
            _LastnameOkButton.SetImage(UIImage.FromBundle("ic_done"), UIControlState.Normal);

           

            _FirstnameOkButton.Frame = new CGRect(0.0f, 0.0f, TextFirstname.Frame.Size.Height,
                TextFirstname.Frame.Size.Height);
            _FirstnameOkButton.ContentMode = UIViewContentMode.Center;
            TextFirstname.RightView = _FirstnameOkButton;
            TextFirstname.RightViewMode = UITextFieldViewMode.Always;
            TextFirstname.Layer.BorderWidth = 1f;
            TextFirstname.Layer.BorderColor = UIColor.White.CGColor;

            _LastnameOkButton.Frame = new CGRect(0.0f, 0.0f, TextLastname.Frame.Size.Height,
                TextLastname.Frame.Size.Height);
            _LastnameOkButton.ContentMode = UIViewContentMode.Center;
            TextLastname.RightView = _LastnameOkButton;
            TextLastname.RightViewMode = UITextFieldViewMode.Always;
            TextLastname.Layer.BorderWidth = 1f;
            TextLastname.Layer.BorderColor = UIColor.White.CGColor;

          

            _menuController = (MenuController)SidebarController.MenuAreaController;

            this.View.AddGestureRecognizer(new UITapGestureRecognizer(()=> {
                TextLastname.ResignFirstResponder();
                TextFirstname.ResignFirstResponder();
            }));
            
        }

        partial void SaveButtonTouchUpOutside(UIButton sender)
        {
            Save_Button.BackgroundColor = UIColor.FromRGB(0, 177, 87);

        }

        partial void SaveButtonTouchUpInside(UIButton sender)
		{
		  UpdateUser();
		  Save_Button.BackgroundColor = UIColor.FromRGB(0, 177, 87);
        }

		partial void SaveButtonTouchDown(UIButton sender)
		{
		    Save_Button.BackgroundColor = UIColor.FromRGB(0, 126, 41);
		}

      
        partial void TexLastNameDidEndEdit(UITextField sender)
        {
            sender.BackgroundColor = UIColor.Clear;
        }
     

		

        partial void TextLastNameDidChange(UITextField sender)
        {
            _viewModel.UserProfile.Lastname = TextLastname.Text;
            ChangePending = true;
            if (!ValidateLastname()) return;
		    if (!ValidateFirstname()) return;
		    Save_Button.BackgroundColor = UIColor.FromRGB(0, 177, 87);
		    Save_Button.UserInteractionEnabled = true;
        }

		partial void TextLastnameDidBeginEdit(UITextField sender)
		{
		    sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
        }

        partial void TextFirstnameDidEndEdit(UITextField sender)
		{
		    sender.BackgroundColor = UIColor.Clear;
        }

        partial void TextFirstnameDidChange(UITextField sender)
		{
		    _viewModel.UserProfile.Firstname = TextFirstname.Text;
		    ChangePending = true;
            if (!ValidateFirstname()) return;
            if (!ValidateLastname()) return;	  
		    Save_Button.BackgroundColor = UIColor.FromRGB(0, 177, 87);
		    Save_Button.UserInteractionEnabled = true;
        }

		partial void TextFirstnameDidBeginEdit(UITextField sender)
		{
		    sender.BackgroundColor = "#80FFFFFF".ToUiColorAlpha();
        }

        private void UpdateUser()
        {
            if (_viewModel.imagedata != null)
            {
                _viewModel.UploadImageCommand.Execute(null);
                _menuController._UserImage.Image = User_Imange.Image;
            }
            _viewModel.PatchUserProfileCommand.Execute(null);
            _menuController._Username.Text = TextFirstname.Text +" "+ TextLastname.Text;
            _Toast.ShowToast("Updated", this.View);
            CurrentImage = User_Imange.Image;
            ChangePending = false;
        }


        private bool ValidateFirstname()
        {
            if (!string.IsNullOrWhiteSpace(TextFirstname.Text) && TextFirstname.Text.Length > 1 && TextFirstname.Text.Length < 60)
            {
                if (TextFirstname.RightView != _FirstnameOkButton)
                {
                    _FirstnameOkButton.Frame = new CGRect(0.0f, 0.0f, TextFirstname.Frame.Size.Height,
                        TextFirstname.Frame.Size.Height);
                    _FirstnameOkButton.ContentMode = UIViewContentMode.Center;
                    TextFirstname.RightView = _FirstnameOkButton;
                    TextFirstname.RightViewMode = UITextFieldViewMode.Always;
                }
                return true;
            }
            if (TextFirstname.RightView != _FirstnameErrorButton)
            {
                _FirstnameErrorButton.Frame = new CGRect(0.0f, 0.0f, TextFirstname.Frame.Size.Height,
                    TextFirstname.Frame.Size.Height);
                _FirstnameErrorButton.ContentMode = UIViewContentMode.Center;
                TextFirstname.RightView = _FirstnameErrorButton;
                TextFirstname.RightViewMode = UITextFieldViewMode.Always;

            
            }
            Save_Button.UserInteractionEnabled = false;
            Save_Button.BackgroundColor = UIColor.FromRGB(165, 55, 23);
            return false;
        }

        private bool ValidateLastname()
        {
            if (!string.IsNullOrWhiteSpace(TextLastname.Text) && TextLastname.Text.Length > 0 && TextLastname.Text.Length < 60)
            {
                if (TextLastname.RightView != _LastnameOkButton)
                {
                    _LastnameOkButton.Frame = new CGRect(0.0f, 0.0f, TextLastname.Frame.Size.Height,
                        TextLastname.Frame.Size.Height);
                    _LastnameOkButton.ContentMode = UIViewContentMode.Center;
                    TextLastname.RightView = _LastnameOkButton;
                    TextLastname.RightViewMode = UITextFieldViewMode.Always;
                }
                return true;
            }
            if (TextLastname.RightView != _LastnameErrorButton)
            {
                _LastnameErrorButton.Frame = new CGRect(0.0f, 0.0f, TextLastname.Frame.Size.Height,
                    TextLastname.Frame.Size.Height);
                _LastnameErrorButton.ContentMode = UIViewContentMode.Center;
                TextLastname.RightView = _LastnameErrorButton;
                TextLastname.RightViewMode = UITextFieldViewMode.Always;
             
            }
            Save_Button.UserInteractionEnabled = false;
            Save_Button.BackgroundColor = UIColor.FromRGB(165, 55, 23);
            return false;
        }

      

        private async void OnTap()
        {
            UIAlertController actionSheetAlert = UIAlertController.Create("Change Profile Image", "", UIAlertControllerStyle.ActionSheet);

            actionSheetAlert.AddAction(UIAlertAction.Create("Camara", UIAlertActionStyle.Default, (action) =>
            {
                try
                {
                    _imagePicker.Delegate = new CameraDelegate(this);
                    _imagePicker.SourceType = UIImagePickerControllerSourceType.Camera;
                    NavigationController.PresentViewController(_imagePicker, true, null);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                 
                }
               
            }));

            actionSheetAlert.AddAction(UIAlertAction.Create("Gallery", UIAlertActionStyle.Default, (action) =>
            {
                try
                {
                    _imagePicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                    _imagePicker.MediaTypes =  UIImagePickerController.AvailableMediaTypes((UIImagePickerControllerSourceType.PhotoLibrary));
                    _imagePicker.FinishedPickingMedia += ImagePickerOnFinishedPickingMedia;
                    _imagePicker.Canceled += ImagePickerOnCanceled;
                    NavigationController.PresentModalViewController(_imagePicker, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
             
                }
               
            }));

            actionSheetAlert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel,
                (action) => Console.WriteLine("Cancel button pressed.")));
            this.PresentViewController(actionSheetAlert, true, null);
        }



        private void ImagePickerOnCanceled(object o, EventArgs eventArgs)
        {
            _imagePicker.DismissViewController(true, null);

        }

        private void ImagePickerOnFinishedPickingMedia(object o, UIImagePickerMediaPickedEventArgs e)
        {


            bool isImage = false;
            switch (e.Info[UIImagePickerController.MediaType].ToString())
            {
                case "public.image":
                    Console.WriteLine("Image selected");
                   
                    
                   
                    isImage = true;
                    break;
                case "public.video":
                    var alert = new UIAlertView("Error","Can not use video as profile picture", null, NSBundle.MainBundle.LocalizedString("OK", "OK"));
                    alert.Show();
                    break;
            }
        

            _imagePicker.DismissModalViewController(true);

            var CropVC = new TOCropViewController(TOCropViewCroppingStyle.Default, e.OriginalImage);
            CropVC.Delegate = new CropVCDelegate(this);
            CropVC.AspectRatioPreset = TOCropViewControllerAspectRatioPreset.Square;
            CropVC.AspectRatioLockEnabled = true;

            NavigationController.PresentViewController(CropVC, true, null);
        }


        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            _willShowNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(
                UIKeyboard.WillShowNotification, KeyBoardwillShow);         
            _willHideNotificationObserver =
                NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, KeyBoardWillHide);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_willShowNotificationObserver);
            }

            if (_willHideNotificationObserver != null)
            {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_willHideNotificationObserver);
            }
        }

        #region KeyBoardHandling

        private void KeyBoardwillShow(NSNotification notification)
        {
            ScrollView.ScrollEnabled = true;
            if (!_Keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height - keyboardHeight;
                this.View.Frame = NewFrame;
                _currentKeyboardFrame = keyboardHeight;             
                _Keyboardisshown = true;
            }
            else
            {
                var HeightDifference = UIKeyboard.BoundsFromNotification(notification).Height - _currentKeyboardFrame;
                _currentKeyboardFrame = UIKeyboard.BoundsFromNotification(notification).Height;
                var newFrame = this.View.Frame;
                newFrame.Height = newFrame.Height - HeightDifference;
                this.View.Frame = newFrame;
                _Keyboardisshown = true;
            }
        }

   

        private void KeyBoardWillHide(NSNotification notification)
        {
           ScrollView.SetContentOffset(CGPoint.Empty, false );
            ScrollView.ScrollEnabled = false;
            
            if (_Keyboardisshown)
            {
                UIView.BeginAnimations(string.Empty, System.IntPtr.Zero);
                var keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                var NewFrame = this.View.Frame;
                NewFrame.Height = NewFrame.Height + keyboardHeight;
                this.View.Frame = NewFrame;
                _Keyboardisshown = false;
            }
        }

        #endregion


        class CropVCDelegate : TOCropViewControllerDelegate
        {
            private UserSettingsViewController ViewController;
            public CropVCDelegate(UserSettingsViewController viewController)
            {
                ViewController = viewController;
            }


            public override void DidCropImageToRect(TOCropViewController cropViewController, CGRect cropRect, nint angle)
            {
                cropViewController.PresentingViewController.DismissViewController(true, null);
                var myImage = cropViewController.FinalImage;
                myImage = new ImageCropper().ResizeImage(myImage, 500, 500);
                NSData imageData = myImage.AsPNG();
                string Base64image = imageData.GetBase64EncodedString(NSDataBase64EncodingOptions.None);
                var imagesize = System.Text.ASCIIEncoding.ASCII.GetByteCount(Base64image) / 1024;
                var size = 500;
                while (imagesize > 1024)
                {
                    myImage = new ImageCropper().ResizeImage(myImage, size, size);
                    imageData = myImage.AsPNG();
                    Base64image = imageData.GetBase64EncodedString(NSDataBase64EncodingOptions.None);
                    imagesize = System.Text.ASCIIEncoding.ASCII.GetByteCount(Base64image) / 1024;
                    size = size - 50;
                }
                ViewController.User_Imange.Image = myImage;
                ViewController._viewModel.imagedata = Convert.FromBase64String(Base64image);
                ViewController.ChangePending = true;

            }
        }
        class CameraDelegate : UIImagePickerControllerDelegate
        {
            private UserSettingsViewController Controller;

           public CameraDelegate(UserSettingsViewController controller)
           {
               Controller = controller;
           }

            public override void FinishedPickingMedia(UIImagePickerController picker, NSDictionary info)
            {
                picker.DismissModalViewController(true);
                var image = info.ValueForKey(new NSString("UIImagePickerControllerOriginalImage")) as UIImage;

                var CropVC = new TOCropViewController(TOCropViewCroppingStyle.Default, image);
                CropVC.Delegate = new CropVCDelegate(Controller);
                CropVC.AspectRatioPreset = TOCropViewControllerAspectRatioPreset.Square;
                CropVC.AspectRatioLockEnabled = true;
                Controller.NavigationController.PresentViewController(CropVC, true, null);

            }
        }




    }
}
   