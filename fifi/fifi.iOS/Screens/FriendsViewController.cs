using Foundation;
using System;
using System.ComponentModel;
using CallKit;
using CoreGraphics;
using fifi.Client.DataObjects;
using fifi.iOS.Helpers;
using UIKit;
using SidebarNavigation;
using fifi.Model;
using fifi.Utils.Helpers;
using fifi.ViewModel;
using fifi.ViewModel;
using SeaDroid.iOS.Helpers;

namespace fifi.iOS
{
    public partial class FriendsViewController : UIViewController
    {
        private FriendViewModel _viewModel;

        protected SidebarController SidebarController
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.SidebarController;

        //protected new NavigationController NavigationController
        //    => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.NavigationController;

        public override UIStoryboard Storyboard
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.Storyboard;


        public FriendsViewController(IntPtr handle) : base(handle)

        {
     
        }

        public override void ViewDidUnload()
        {
            base.ViewDidUnload();
            _viewModel.PropertyChanged -= ViewModel_PropertyChanged;
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();

            NavigationItem.SetLeftBarButtonItem(
                new UIBarButtonItem(UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain,
                    (sender, args) => { SidebarController.ToggleMenu(); }), true);
        

            FriendsListSearchTextInput.AttributedPlaceholder = new NSAttributedString("Search", font: UIFont.FromName("HelveticaNeueLTDMC_18", 20.0f), strokeColor: UIColor.White, foregroundColor: UIColor.White);

            FriendsButton.BackgroundColor = UIColor.FromRGB(206, 208, 206);
            _viewModel = new FriendViewModel();
            FriendsListListView.Source = new FriendsTablesource(_viewModel, this);

            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "     Friends",
                Font = UIFont.BoldSystemFontOfSize(19.0f),
                TextColor = UIColor.White
            };
            NavigationItem.TitleView = lblNavTitle;
            ImageResizer resizer = new ImageResizer();
            var image = UIImage.FromBundle("neg_three_men");
            var ThreeMen = resizer.ResizeImage(UIImage.FromBundle("neg_three_men"), (float)image.Size.Height / (float)2.5, (float)image.Size.Width / (float)2.5);

            var ThreeMenButton = new UIBarButtonItem((UIImage.FromBundle("ThreeMen"))
                , UIBarButtonItemStyle.Plain
                , (sender, args) =>
                {

                });
            ThreeMenButton.Enabled = false;


            ThreeMenButton.Image = ThreeMen;
            NavigationItem.SetRightBarButtonItem(ThreeMenButton, false);



            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);

            _viewModel.PropertyChanged += ViewModel_PropertyChanged;

            _viewModel.LoadFriendsCommand.Execute(null);

            FriendsListSearchTextInput.ShouldReturn += sender =>
            {
                FriendsListSearchTextInput.ResignFirstResponder();
                return true;
            };

        }

       

        public void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            FriendsListListView.ReloadData();
        }

        public async void refresh(object sender, EventArgs e)
        {
            _viewModel.LoadFriendsCommand.Execute(null);
        }


        partial void FriendsButtonPressed(UIButton sender)
        {
            
        }

        partial void FriendRequestButtonPressed(UIButton sender)
        {
            FriendRequestsViewControlller FriendRequestView =
                this.Storyboard.InstantiateViewController("FriendRequestsViewControlller") as
                    FriendRequestsViewControlller;
            this.NavigationController.PushViewController(FriendRequestView, false);
            this.SidebarController.CloseMenu(true);
        }

        partial void AddFriendsButtonPressed(UIButton sender)
        {
            AddFriendsViewController AddFriendsView =
                this.Storyboard.InstantiateViewController("AddFriendsViewController") as AddFriendsViewController;
            this.NavigationController.PushViewController(AddFriendsView, false);
            this.SidebarController.CloseMenu(true);
        }

        partial void SearchButtonPressed(UIButton sender)
        {
            _viewModel.FriendsListSortText = FriendsListSearchTextInput.Text;
            _viewModel.SortFriendsListCommand.Execute(null);
            FriendsListSearchTextInput.ResignFirstResponder();
        }

        internal class FriendsTablesource : UITableViewSource
        {
            private FriendViewModel _viewModel;
            private string CellIdentifier = "FriendsListCell";
            private FriendsViewController _controller;

            public FriendsTablesource(FriendViewModel viewModel, FriendsViewController controller)
            {
                _viewModel = viewModel;
                _controller = controller;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                FriendsListCell cell = tableView.DequeueReusableCell(CellIdentifier) as FriendsListCell;
                var item = _viewModel.Friends[indexPath.Row];

                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier) as FriendsListCell;
                }

                cell.name = item.firstname + " " + item.lastname;
                if (!string.IsNullOrEmpty(item.image))
                {
                    cell.Image.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(new ImageUrlDownloader().GetImageBitmapFromUrl(item.image))));
                }
                else
                {
                    cell.Image.Image = UIImage.FromBundle("friends_ghost_image_blue");
                }
                cell.Image.Layer.CornerRadius = cell.Image.Frame.Size.Height / 2;
                cell.Image.Layer.MasksToBounds = true;
                return cell;


               
            }


            public override NSIndexPath WillSelectRow(UITableView tableView, NSIndexPath indexPath)
            {
                _viewModel.user_id = _viewModel.Friends[indexPath.Row].id.ToString();
                _viewModel.user = _viewModel.Friends[indexPath.Row];
                return indexPath;
            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                _viewModel.user_id = _viewModel.Friends[indexPath.Row].id.ToString();
                _viewModel.user = _viewModel.Friends[indexPath.Row];

                try
                {
                    var ChatView = _controller.Storyboard.InstantiateViewController("FriendChatViewController") as FriendChatViewController;

                    ChatView.Friend = _viewModel.Friends[indexPath.Row];

                    _controller.NavigationController.PushViewController(ChatView, true);
                }

                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                //var alert = new UIAlertView("Remove Friend?", "Do you want to remove " + _viewModel.Friends[indexPath.Row].firstname + " " + _viewModel.Friends[indexPath.Row].lastname + " from your friends list?", null, NSBundle.MainBundle.LocalizedString("NO", "NO"), NSBundle.MainBundle.LocalizedString("YES", "YES"));
                //alert.Clicked += (object sender, UIButtonEventArgs e) =>
                //{
                //    bool answer = (e.ButtonIndex != alert.CancelButtonIndex);
                //    if (answer)
                //    {
                //        _viewModel.DeleteFriendCommand.Execute(null);
                //        _viewModel.LoadFriendsCommand.Execute(null);
                //    }
                //};
                //alert.Show();

            }



            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return _viewModel.Friends.Count;
            }


            
        }
    }
}