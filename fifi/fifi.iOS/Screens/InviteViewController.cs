using Foundation;
using System;
using System.ComponentModel;
using CoreGraphics;
using fifi.iOS.Helpers;
using fifi.iOS.MenuControllers;
using fifi.ViewModel;
using SidebarNavigation;
using UIKit;

namespace fifi.iOS
{
    public partial class InviteViewController : UIViewController
    {
        private UIButton _continueButton;
        private InviteViewModel _viewmodel;
        private UIButton _errorButton;


        protected SidebarController SidebarController
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.SidebarController;

        //protected new NavigationController NavigationController
        //    => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.NavigationController;

        public override UIStoryboard Storyboard
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.Storyboard;




        public InviteViewController (IntPtr handle) : base (handle)
        {
            NavigationItem.SetLeftBarButtonItem(
                new UIBarButtonItem(UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain,
                    (sender, args) => { SidebarController.ToggleMenu(); }), true);
        }

		partial void TextInviteEmailDidChange(UITextField sender)
		{
		    ValidateEmail();
		}

        public override void ViewDidLoad()
        {

            base.ViewDidLoad();

            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "Invite friend",
                TextColor = UIColor.White
            };
            NavigationItem.TitleView = lblNavTitle;

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);

            _errorButton = new UIButton(UIButtonType.Custom)
           {
               BackgroundColor = "F93334".ToUiColor(),
               UserInteractionEnabled = false
           };
           _errorButton.SetImage(UIImage.FromBundle("ic_clear"),UIControlState.Normal );

            _continueButton = new UIButton(UIButtonType.Custom)
            {
                BackgroundColor = "9FFFFF".ToUiColor(),
                UserInteractionEnabled = true
            };
            _continueButton.SetImage(UIImage.FromBundle("ic_continue"),UIControlState.Normal );

            _continueButton.TouchUpInside += (sender, e) =>
            {
                ((UIButton)sender).BackgroundColor = "9FFFFF".ToUiColor();
            };

            _continueButton.TouchDown += (sender, e) =>
            {
                ((UIButton)sender).BackgroundColor = "608CFF".ToUiColor();
                 // ins�t api kald
            };

            Text_Email_Invite.Layer.BorderWidth = 1f;
            Text_Email_Invite.Layer.BorderColor = UIColor.White.CGColor;

            _errorButton.Frame = new CGRect(0.0f, 0.0f, Text_Email_Invite.Frame.Size.Height,
                Text_Email_Invite.Frame.Size.Height);
            _errorButton.ContentMode = UIViewContentMode.Center;
            Text_Email_Invite.RightViewMode = UITextFieldViewMode.Always;
            Text_Email_Invite.RightView = _errorButton;

            Text_Email_Invite.ShouldReturn += field =>
            {
                Text_Email_Invite.ResignFirstResponder();
                return true;
            };

            _viewmodel = new InviteViewModel();
            _viewmodel.GetUserUnitsCommand.Execute(null);
            _viewmodel.PropertyChanged += ViewmodelOnPropertyChanged;

        }

        private void ViewmodelOnPropertyChanged(object o, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (_viewmodel.InviteAllowed)
            {
                var alert = new UIAlertView("Cant Invite", "you need a Master paired to your accout, to invite new users", null, NSBundle.MainBundle.LocalizedString("OK", "OK"));
                alert.Clicked += (object s, UIButtonEventArgs e) =>
                {
                    InvokeOnMainThread(() =>
                    {
                        var app = (AppDelegate)UIApplication.SharedApplication.Delegate;
                        //var viewController =
                        //    UIStoryboard.FromName("Main", null).InstantiateViewController("mainViewController") as
                        //        UINavigationController;
                        app.Window.RootViewController = new RootViewController();
                    });
                };
                alert.Show();
            }
        }

        private bool ValidateEmail()
        {
            if (Text_Email_Invite.Text.IsEmail())
            {
                _continueButton.Frame = new CGRect(0.0f, 0.0f, Text_Email_Invite.Frame.Size.Height,
                    Text_Email_Invite.Frame.Size.Height);
                _continueButton.ContentMode = UIViewContentMode.Center;
                Text_Email_Invite.RightViewMode = UITextFieldViewMode.Always;
                Text_Email_Invite.RightView = _continueButton;
                return true;
            }
            return false;
        }
    }
}