﻿using Foundation;
using System;
using System.Diagnostics;
using System.Drawing;
using CoreLocation;
using fifi.iOS.MenuControllers;
using CoreGraphics;
using CoreImage;
using UIKit;
using MapKit;
using System.Threading.Tasks;
using fifi.Client.DataObjects;
using fifi.iOS.Annotations;
using fifi.iOS.CustomControls;
using fifi.iOS.Helpers;
using fifi.Model;
using fifi.Utils.Helpers;
using fifi.ViewModel;
using Mapbox;
using SeaDroid.iOS.Helpers;
using Splat;

namespace fifi.iOS
{
    public partial class MapsViewController : BaseController
    {
        private NSObject _notifyAnnotationClicked;
        private NSObject _notifyAnnotastionDeselected;
        private ToastIos toast;
        private MapsViewModel _viewModel;
        private bool busy;
        private double MyLatitude;
        private double MyLongitude;
        private CustomMap _map;

        public MapsViewController(IntPtr handle) : base(handle)
        {
        }


        public override void ViewDidLoad()
        {
            toast = new ToastIos();
            base.ViewDidLoad();
            _viewModel = new MapsViewModel();
            NavigationItem.Title = string.Empty;
      
            _map = new CustomMap(View.Bounds)
            {
                CenterCoordinate = new CLLocationCoordinate2D(56.2639, 9.5018),
                ZoomEnabled = true,
            };

            MarkerMenu.Hidden = true;
            Closed_markermenu.Hidden = true;
            _notifyAnnotationClicked = NSNotificationCenter.DefaultCenter.AddObserver(new Foundation.NSString("MarkerSelected"), AnnotationClicked);
            _notifyAnnotastionDeselected = NSNotificationCenter.DefaultCenter.AddObserver(new Foundation.NSString("MarkerDeselected") ,AnnotationDeSelected);
            
            close_markermenu_button.TouchDown += (sender, args) =>
            {
                MarkerMenu.Hidden = true;
                Closed_markermenu.Hidden = false;
            };

            open_markermenu_button.TouchDown += (sender, args) =>
            {
                MarkerMenu.Hidden = false;
                Closed_markermenu.Hidden = true;
            };

            UIApplication app = UIApplication.SharedApplication;
            var statusBarHeight = app.StatusBarFrame.Size.Height;
            UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
            statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
            this.NavigationController.NavigationBar.AddSubview(statusbar);

            View.InsertSubview(_map, 0);


            //setting up the Alertview
            ivAlert.AddGestureRecognizer(new UITapGestureRecognizer(ShowHelpDialog) {NumberOfTapsRequired = 1});
            ivAlert.UserInteractionEnabled = true;
            UIImage CloseAlertImage = UIImage.FromBundle("ic_clear").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
            close_alert_button.SetImage(CloseAlertImage, UIControlState.Normal);     
            close_alert_button.TintColor = UIColor.FromRGB(237, 33, 35);
           
            close_alert_button.TouchDown += (sender, args) =>
            {
                alertView.Hidden = true;
            };


            // setting up the navigationbar buttons 
            if (Settings.Current.Mapmode == string.Empty)
            {
                Settings.Current.Mapmode = "Solo";
            }
            ImageResizer resizer = new ImageResizer();
            GlobalButton.Image = (UIImage.FromBundle("neg_global"));
            var ImageSize = GlobalButton.Image;
            SoloButton.Title = null;
            SoloButton.Image = resizer.ResizeImage(UIImage.FromBundle("neg_one_man"), 
                (float) ImageSize.Size.Height / (float) 1.75, (float) ImageSize.Size.Width / (float) 1.75);
            FriendsButton.Title = null;
            FriendsButton.Image = resizer.ResizeImage(UIImage.FromBundle("neg_three_men"),
                (float) ImageSize.Size.Height / (float) 1.75, (float) ImageSize.Size.Width / (float) 1.75);
            GlobalButton.Title = null;
            GlobalButton.Image = resizer.ResizeImage(UIImage.FromBundle("neg_global"),
                (float) ImageSize.Size.Height / (float) 1.75, (float) ImageSize.Size.Width / (float) 1.75);

            SoloButton.Clicked += (sender, args) =>
            {
                InvokeOnMainThread((() =>
                {
                    SoloButton.TintColor = UIColor.FromRGB(158, 255, 255);
                    FriendsButton.TintColor = UIColor.White;
                    GlobalButton.TintColor = UIColor.White;
                }));

                toast.ShowToast("Solo", this.View);
                Settings.Current.Mapmode = "Solo";
                UpdateMap();
            };


            FriendsButton.Clicked += (sender, args) =>
            {
                InvokeOnMainThread((() =>
                {
                    SoloButton.TintColor = UIColor.White;
                    FriendsButton.TintColor = UIColor.FromRGB(158, 255, 255);
                    GlobalButton.TintColor = UIColor.White;
                }));
                toast.ShowToast("Friends", this.View);

                Settings.Current.Mapmode = "Friends";
                UpdateMap();
            };
            
            GlobalButton.Clicked += (sender, args) =>
            {
                InvokeOnMainThread((() =>
                {
                    SoloButton.TintColor = UIColor.White;
                    FriendsButton.TintColor = UIColor.White;
                    GlobalButton.TintColor = UIColor.FromRGB(158, 255, 255);
                }));
                toast.ShowToast("Global", this.View);
                Settings.Current.Mapmode = "Global";
                UpdateMap();
            };

            switch (Settings.Current.Mapmode)
            {
                case "Solo":
                    SoloButton.TintColor = UIColor.FromRGB(158, 255, 255);
                    FriendsButton.TintColor = UIColor.White;
                    GlobalButton.TintColor = UIColor.White;
                    break;
                case "friends":
                    SoloButton.TintColor = UIColor.White;
                    FriendsButton.TintColor = UIColor.FromRGB(158, 255, 255);
                    GlobalButton.TintColor = UIColor.White;

                    break;
                case "Global":
                    SoloButton.TintColor = UIColor.White;
                    FriendsButton.TintColor = UIColor.White;
                    GlobalButton.TintColor = UIColor.FromRGB(158, 255, 255);
                    break;
            }
        }

        partial void AddFriendButtonTouchedUpOutside(UIButton sender)
        {
            
        }

        partial void AddFriendButtonTouchedUpInside(UIButton sender)
        {
           _viewModel.SendFriendRequestCommand.Execute(null);
            AddFriendButton.SetImage(UIImage.FromBundle("neg_bluebox_req_sent.png"), UIControlState.Normal);
        }

        partial void AddFriendButtonTouchedDown(UIButton sender)
        {
            
        }



        // loading in the data from the selected annotation and changing the view
        private void AnnotationClicked(NSNotification notificaiton)
        {
            ActiveBoat currentboat;
            _viewModel.user_id = _map.SelectedAnnotations[0].GetTitle();
            try
            {                
                foreach (var boat in _viewModel.ActiveBoats)
                {
                    if (Convert.ToInt16(_map.SelectedAnnotations[0].GetTitle()) == boat.User_id)
                    {
                      currentboat = boat;
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }         
                UIView speedProgress = new UIView();
                speedProgress.Frame = new CGRect(0, 0, Background_speedline.Frame.Width /3, Background_speedline.Frame.Height);
                UIView currentSpeed = new UIView();
                currentSpeed.Frame = new CGRect((speedProgress.Frame.Width ), Background_speedline.Frame.Height - maxbar.Frame.Height, maxbar.Frame.Width, maxbar.Frame.Height);
                currentSpeed.BackgroundColor = maxbar.BackgroundColor;
                speedProgress.BackgroundColor = UIColor.FromRGB(111, 207, 240);

            var IsNotFriend = true;
            foreach (var FriendRequests in _viewModel.sentFriendRequests.data)
            {
                if (Convert.ToInt16(_map.SelectedAnnotations[0].GetTitle()) == FriendRequests.id)
                {
                    AddFriendButton.SetImage(UIImage.FromBundle("neg_bluebox_req_sent.png"), UIControlState.Normal);
                    IsNotFriend = false;
                }
            }
            foreach (var friends in _viewModel.friends.data)
            {
                if (_map.SelectedAnnotations[0].GetTitle() == friends.id.ToString())
                {
                    AddFriendButton.SetImage(UIImage.FromBundle("neg_bluebox_req_sent.png"), UIControlState.Normal);
                    IsNotFriend = false;
                }
            }
            if (_map.SelectedAnnotations[0].GetTitle() == Settings.Current.UserId)
            {
                AddFriendButton.SetImage(UIImage.FromBundle("neg_bluebox_req_sent.png"), UIControlState.Normal);
                IsNotFriend = false;
            }
            if (IsNotFriend)
            {
                AddFriendButton.SetImage(UIImage.FromBundle("neg_bluebox_send_req.png"), UIControlState.Normal);
            }

            

                Text_currentspeed.Lines = 1;      
                var Textsize = Text_currentspeed.Text.StringSize(Text_currentspeed.Font);
                Text_currentspeed.Center = new CGPoint(currentSpeed.Frame.X +(Textsize.Width/2) , Text_currentspeed.Center.Y);
                Background_speedline.AddSubview(speedProgress);
                Background_speedline.Add(currentSpeed);
                Background_speedline.BringSubviewToFront(speedProgress);   
                Background_speedline.BringSubviewToFront(currentSpeed);
                Closed_markermenu.Hidden = false;
                   
                
        }

        private void AnnotationDeSelected(NSNotification notification)
        {
            Closed_markermenu.Hidden = true;
        }


        private void Activevessels_changed(object sender,
            System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateMap();
        }

        private void UpdateMap()
        {
            string user_id = null;
            try
            {
                Debug.WriteLine("Changing map markers");

                if (busy)
                {
                    Debug.WriteLine("View is busy, map update canceled");
                    return;
                }
                InvokeOnMainThread((() => { _map.RemoveAnnotations(_map.Annotations); }));


                busy = true;

                foreach (var boat in _viewModel.ActiveBoats)
                {
                    if (boat.User_id == Convert.ToInt16(Settings.Current.UserId))
                    {
                        MyLatitude = double.Parse(boat.Latitude);
                        MyLongitude = double.Parse(boat.Longitude);
                        user_id = boat.User_id.ToString();
                    }
                }

                 if (user_id != null)
                 {
                    InvokeOnMainThread((() =>  {_map.AddAnnotation(new BoatAnnotation(user_id, new CLLocationCoordinate2D(MyLatitude, MyLongitude)));}));
                }
              

                busy = false;


                switch (Settings.Current.Mapmode)
                {
                    case "Solo":

                        busy = false;
                        break;

                    case "Friends":
                        foreach (var boat in _viewModel.ActiveBoats)
                        {
                            foreach (var friend in _viewModel.friends.data)
                            {
                                if (boat.User_id == friend.id)
                                {
                                    var friendLatitude = double.Parse(boat.Latitude);
                                    var friendLongitude = double.Parse(boat.Longitude);
                                    var friend_id = boat.User_id;
                                    InvokeOnMainThread(
                                    (() =>
                                    {
                                        _map.AddAnnotation(new FriendAnnotation(friend_id.ToString(),
                                            new CLLocationCoordinate2D(friendLatitude, friendLongitude)));
                                    }));
                                }
                            }
                        }

                        busy = false;
                        break;
                    case "Global":

                        foreach (var boat in _viewModel.ActiveBoats)
                        {
                            bool IsFriend = false;
                            foreach (var friend in _viewModel.friends.data)
                            {
                                if (boat.User_id == friend.id)
                                {
                                    var friendLatitude = double.Parse(boat.Latitude);
                                    var friendLongitude = double.Parse(boat.Longitude);
                                    var friend_id = boat.User_id;
                                    InvokeOnMainThread(
                                    (() =>
                                    {
                                        _map.AddAnnotation(new FriendAnnotation(friend_id.ToString(),
                                            new CLLocationCoordinate2D(friendLatitude, friendLongitude)));
                                    }));
                                    IsFriend = true;
                                }
                            }
                            if (!IsFriend && boat.User_id != Convert.ToInt16(Settings.Current.UserId))
                            {
                                var globaluserlatitude = double.Parse(boat.Latitude);
                                var globaluserlongitude = double.Parse(boat.Longitude);
                                var global_id = boat.User_id;
                                InvokeOnMainThread(
                                (() =>
                                {
                                    _map.AddAnnotation(new GlobalAnnotation(global_id.ToString(),
                                        new CLLocationCoordinate2D(globaluserlatitude, globaluserlongitude)));
                                }));
                            }
                        }
                        busy = false;
                        break;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            _viewModel.ActiveBoats.CollectionChanged += Activevessels_changed;
            _viewModel.StartLoadvesselsCommand.Execute(null);
            _viewModel.LoadSentFriendRequestsCommand.Execute(null);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            _viewModel.ActiveBoats.CollectionChanged -= Activevessels_changed;
            _viewModel.StopLoadVesselsCommand.Execute(null);
        }

        private void ShowHelpDialog(UITapGestureRecognizer obj)
        {
            if (alertView.Hidden)
            {
                alertView.Hidden = false;
            }
            else
            {
                alertView.Hidden = true;
                btnSubmit.BackgroundColor = "#C93913".ToUiColor();
                txtTitle.Text = string.Empty;
                txtDescription.Text = string.Empty;
                btnSubmit.Enabled = true;
                btnSubmit.SetTitle("Submit", UIControlState.Normal);
            }
        }

        partial void BtnSubmitOnDown(NSObject sender)
        {
            btnSubmit.BackgroundColor = "#a03913".ToUiColor();
        }

        partial void BtnSubmitOnUp(NSObject sender)
        {
            txtDescription.ResignFirstResponder();
            btnSubmit.BackgroundColor = "#ed2124".ToUiColor();
            btnSubmit.SetTitle("Message Sent", UIControlState.Normal);
            btnSubmit.Enabled = false;
        }

        partial void BtnSubmitDragOutside(NSObject sender)
        {
            btnSubmit.BackgroundColor = "#C93919".ToUiColor();
        }

        private bool TextFieldShouldReturn(UITextField textField)
        {
            var nextTag = textField.Tag + 1;
            var nextResponder = textField.Superview.ViewWithTag(nextTag) as UIResponder;
            if (nextResponder != null)
            {
                nextResponder.BecomeFirstResponder();
            }
            else
            {
                textField.ResignFirstResponder();
            }

            return false;
        }

   
    }
}