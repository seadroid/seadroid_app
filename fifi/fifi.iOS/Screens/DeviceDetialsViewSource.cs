using System;
using CoreLocation;
using fifi.ViewModel;
using Foundation;
using Mapbox.Maps;
using UIKit;
using MapView = Mapbox.MapView;

namespace fifi.iOS
{
    internal class DeviceDetialsViewSource : UITableViewSource, IOnMapReadyCallback
    {
        DeviceDetailsViewModel _viewmodel;
        string CellIdentifier = "DeviceDetailsCell";
      
        DeviceDetailsViewController _controller;

        public DeviceDetialsViewSource(DeviceDetailsViewModel viewmodel, DeviceDetailsViewController controller)
        {
            _viewmodel = viewmodel;
            _controller = controller;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _viewmodel.Master.Trips.Count;
        }





        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
			
            DeviceDetialsCell cell = tableView.DequeueReusableCell(CellIdentifier) as DeviceDetialsCell;
            var item = _viewmodel.Master.Trips[indexPath.Row];


            //---- if there are no cells to reuse, create a new one
            if (cell == null)
            {
                cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier) as DeviceDetialsCell;
            }

            cell.Number = item.Id.ToString();
            cell.Start = item.StartedAt;
            cell.End = item.EndedAt;

            var mapView = new MapView(cell.MapUiView.Bounds)
            {
                StyleURL = NSUrl.FromString("mapbox://styles/otbardram/cirol0jn3001lgxm79yh61kzu"),
                CenterCoordinate = new CLLocationCoordinate2D(56.2639, 9.5018),
                ZoomLevel = 5
            };
            cell.MapUiView.InsertSubview(mapView, 0);
            
            

            return cell;
        }

        public async override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            var Trip = _viewmodel.Master.Trips[indexPath.Row];			                  
        }

        protected void UpdateMapContents()
        {
	        
        }

        public void OnMapReady(MapboxMap map)
        {
	        
        }
    }
}