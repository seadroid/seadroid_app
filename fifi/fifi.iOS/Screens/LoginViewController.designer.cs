// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("LoginViewController")]
    partial class LoginViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton CreateUserButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgBackground { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgLogo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView tableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField textEmail { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField textPassword { get; set; }

        [Action ("CreateUserTouchDown:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void CreateUserTouchDown (UIKit.UIButton sender);

        [Action ("TextFieldEmailChangedEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFieldEmailChangedEditing (UIKit.UITextField sender);

        [Action ("TextFieldEmailDidBeginEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFieldEmailDidBeginEditing (UIKit.UITextField sender);

        [Action ("TextFieldEmailDidEndEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFieldEmailDidEndEditing (UIKit.UITextField sender);

        [Action ("TextFieldPasswordChangedEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFieldPasswordChangedEditing (UIKit.UITextField sender);

        [Action ("TextFieldPasswordDidBeginEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFieldPasswordDidBeginEditing (UIKit.UITextField sender);

        [Action ("TextFieldPasswordDidEndEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextFieldPasswordDidEndEditing (UIKit.UITextField sender);

        void ReleaseDesignerOutlets ()
        {
            if (CreateUserButton != null) {
                CreateUserButton.Dispose ();
                CreateUserButton = null;
            }

            if (imgBackground != null) {
                imgBackground.Dispose ();
                imgBackground = null;
            }

            if (imgLogo != null) {
                imgLogo.Dispose ();
                imgLogo = null;
            }

            if (tableView != null) {
                tableView.Dispose ();
                tableView = null;
            }

            if (textEmail != null) {
                textEmail.Dispose ();
                textEmail = null;
            }

            if (textPassword != null) {
                textPassword.Dispose ();
                textPassword = null;
            }
        }
    }
}