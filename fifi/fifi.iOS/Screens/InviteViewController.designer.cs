// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("InviteViewController")]
    partial class InviteViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Text_Email_Invite { get; set; }

        [Action ("TextInviteEmailDidChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void TextInviteEmailDidChange (UIKit.UITextField sender);

        void ReleaseDesignerOutlets ()
        {
            if (Text_Email_Invite != null) {
                Text_Email_Invite.Dispose ();
                Text_Email_Invite = null;
            }
        }
    }
}