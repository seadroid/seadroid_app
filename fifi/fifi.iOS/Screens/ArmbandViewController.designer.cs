// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SeaDroid.iOS
{
    [Register ("ArmbandViewController")]
    partial class ArmbandViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView ArmBandTable { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ArmBandTable != null) {
                ArmBandTable.Dispose ();
                ArmBandTable = null;
            }
        }
    }
}