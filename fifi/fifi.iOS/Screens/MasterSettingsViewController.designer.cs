// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SeaDroid.iOS
{
    [Register ("MasterSettingsViewController")]
    partial class MasterSettingsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView BoatImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField BoatnameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField ColorTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CountryTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField HarborTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField RegistryNumberTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SaveButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BoatImage != null) {
                BoatImage.Dispose ();
                BoatImage = null;
            }

            if (BoatnameTextField != null) {
                BoatnameTextField.Dispose ();
                BoatnameTextField = null;
            }

            if (ColorTextField != null) {
                ColorTextField.Dispose ();
                ColorTextField = null;
            }

            if (CountryTextField != null) {
                CountryTextField.Dispose ();
                CountryTextField = null;
            }

            if (HarborTextField != null) {
                HarborTextField.Dispose ();
                HarborTextField = null;
            }

            if (RegistryNumberTextField != null) {
                RegistryNumberTextField.Dispose ();
                RegistryNumberTextField = null;
            }

            if (SaveButton != null) {
                SaveButton.Dispose ();
                SaveButton = null;
            }
        }
    }
}