// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("FriendRequestsViewControlller")]
    partial class FriendRequestsViewControlller
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AddFriendsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView FriendRequestRecivedTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FriendRequestsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView FriendRequestSentTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView FriendRequestsRecivedBar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView FriendRequestsSentBar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FriendsButton { get; set; }

        [Action ("AddFriendsButtonPressed:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void AddFriendsButtonPressed (UIKit.UIButton sender);

        [Action ("FriendRequestButton_TouchedUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void FriendRequestButton_TouchedUpInside (UIKit.UIButton sender);

        [Action ("FriendsButtonPressed:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void FriendsButtonPressed (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (AddFriendsButton != null) {
                AddFriendsButton.Dispose ();
                AddFriendsButton = null;
            }

            if (FriendRequestRecivedTableView != null) {
                FriendRequestRecivedTableView.Dispose ();
                FriendRequestRecivedTableView = null;
            }

            if (FriendRequestsButton != null) {
                FriendRequestsButton.Dispose ();
                FriendRequestsButton = null;
            }

            if (FriendRequestSentTableView != null) {
                FriendRequestSentTableView.Dispose ();
                FriendRequestSentTableView = null;
            }

            if (FriendRequestsRecivedBar != null) {
                FriendRequestsRecivedBar.Dispose ();
                FriendRequestsRecivedBar = null;
            }

            if (FriendRequestsSentBar != null) {
                FriendRequestsSentBar.Dispose ();
                FriendRequestsSentBar = null;
            }

            if (FriendsButton != null) {
                FriendsButton.Dispose ();
                FriendsButton = null;
            }
        }
    }
}