using Foundation;
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading.Tasks;
using CoreGraphics;
using fifi.iOS;
using fifi.iOS.Helpers;
using fifi.Model;
using SidebarNavigation;
using UIKit;
using fifi.ViewModel;
using Plugin.BLE.iOS;
using SeaDroid.iOS.Helpers;

namespace SeaDroid.iOS
{
    public partial class ArmbandViewController : UITableViewController
    {
        protected SidebarController SidebarController
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.SidebarController;

        //protected new NavigationController NavigationController
        //    => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.NavigationController;

        public override UIStoryboard Storyboard
            => (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.Storyboard;

        private ArmbandViewModel _viewModel;
        public ArmbandDetailsController PairingDetailsView;
        private PairingController PairingView;
        private UIBarButtonItem _addArmband;

        public ArmbandViewController(IntPtr handle) : base(handle)
        {
            NavigationItem.SetLeftBarButtonItem(
                new UIBarButtonItem(UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain,
                    (sender, args) => { SidebarController.ToggleMenu(); }), true);
        }

        public NSObject _notifyEditDone;
        public NSObject _notifyParingDone;


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            #region NavBar

            var lblNavTitle = new UILabel(new CGRect(0, 40, 320, 40))
            {
                TextAlignment = UITextAlignment.Left,
                Text = "Armbands",
                TextColor = UIColor.White
            };
            NavigationItem.TitleView = lblNavTitle;

            ImageResizer resizer = new ImageResizer();
            _addArmband = new UIBarButtonItem((UIImage.FromBundle("plus_icon"))
                , UIBarButtonItemStyle.Plain
                , (sender, args) =>
                {
                    PairingController ArmbandPairing =
                        this.Storyboard.InstantiateViewController("PairingController") as PairingController;
                    NavigationController.PushViewController(ArmbandPairing, true);
                    _notifyParingDone = NSNotificationCenter.DefaultCenter.AddObserver(new NSString("PairingDone"), HandlePairingDone);
                    
                });
            NavigationItem.SetRightBarButtonItem(_addArmband, true);
            var ImageSize = _addArmband.Image;

            _addArmband.Image = resizer.ResizeImage(UIImage.FromBundle("plus_icon"),
                (float) ImageSize.Size.Height / (float) 2.5, (float) ImageSize.Size.Width / (float) 2.5);

            #endregion


            _viewModel = new ArmbandViewModel();

            _viewModel.Armbands.CollectionChanged += ArmbandsOnCollectionChanged;

            ArmBandTable.Source = new ArmbandViewSource(_viewModel, this);

            _viewModel.GetArmbandsCommand.Execute(null);
            _viewModel.SearchForDevicesCommand.Execute(null);
            
            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            _viewModel.SoftStopSearchingForDevicesCommand.Execute(null);

        }
       

        private void ViewModelOnPropertyChanged(object o, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            ArmBandTable.ReloadData();
        }


        public void ArmbandClicked()
        {
            _viewModel.HardStopSearchingForDevicesCommand.Execute(null);
            Task.Delay(100);
            _notifyEditDone = NSNotificationCenter.DefaultCenter.AddObserver(new Foundation.NSString("Done"), HandleEddittingDone);
            NavigationController.PushViewController(PairingDetailsView, true);
            
        }

        public void HandlePairingDone(NSNotification notification)
        {
            _viewModel.GetArmbandsCommand.Execute(null);
        }

        public void HandleEddittingDone(NSNotification notificaiton)
        {
            NSNotificationCenter.DefaultCenter.RemoveObserver(_notifyEditDone);
            _viewModel._SelectedArmband = PairingDetailsView._viewModel.Wristband;
            _viewModel.UpdateArmband.Execute(null);
        }

        public void reload()
        {
            ArmBandTable.ReloadData();
        }

        private void ArmbandsOnCollectionChanged(object o,
            NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            ArmBandTable.ReloadData();
        }
    }

    internal class ArmbandViewSource : UITableViewSource
    {
        private ArmbandViewModel _viewModel;
        string CellIdentifier = "ArmbandCell";
        private ArmbandViewController _controller;
        private nint spacing = 5;

        public ArmbandViewSource(ArmbandViewModel viewModel, ArmbandViewController controller)
        {
            _viewModel = viewModel;
            _controller = controller;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return 1;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return _viewModel.Armbands.Count;
        }

        public override nfloat GetHeightForFooter(UITableView tableView, nint section)
        {
            return spacing;
        }

        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            return spacing;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            ArmbandCell cell = tableView.DequeueReusableCell(CellIdentifier) as ArmbandCell;
            var item = _viewModel.Armbands[indexPath.Section];

            //---- if there are no cells to reuse, create a new one
            if (cell == null)
            {
                cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier) as ArmbandCell;
            }

            cell.Name = item.firstname + " " + item.lastname;

            cell.BatteryLevelText = string.Empty;
            cell.BatteryImage.TintColor = UIColor.White;
            cell.SignalImage.TintColor = UIColor.White;


            if (!string.IsNullOrEmpty(item.Battery))
            {

                cell.BatteryLevelText = item.Battery + "%";
                cell.BatteryImage.Image = cell.BatteryImage.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
                cell.SignalImage.Image = cell.SignalImage.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);


                if (Convert.ToInt32(item.Battery) <= 20)
                {
                    var image = cell.BatteryImage.Image;
                    cell.BatteryImage.TintColor = "#cf3619".ToUiColor();

                }
                if (Convert.ToInt32(item.Battery) > 20 && Convert.ToInt32(item.Battery) <= 30)
                {
                    cell.BatteryImage.TintColor = "#ffe70f".ToUiColor();
                    
                }
                if (Convert.ToInt32(item.Battery) > 30)
                {
                    cell.BatteryImage.TintColor = "#00b157".ToUiColor();

                }

                cell.SignalImage.TintColor = "#00b157".ToUiColor();
            }


            return cell;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            _controller.PairingDetailsView =
                _controller.Storyboard
                    .InstantiateViewController("ArmbandDetailsController") as ArmbandDetailsController;
            _controller.PairingDetailsView._viewModel = new PairingDetailsViewModel()
            {
                Wristband = _viewModel.Armbands[indexPath.Section],
                BluetoothDeviceId = _viewModel.Armbands[indexPath.Section].serial
            };

            _controller.ArmbandClicked();
           
        }
    }
}