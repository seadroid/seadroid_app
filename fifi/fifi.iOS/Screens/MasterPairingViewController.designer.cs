// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace fifi.iOS
{
    [Register ("MasterPairingViewController")]
    partial class MasterPairingViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField MasterSerialText { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (MasterSerialText != null) {
                MasterSerialText.Dispose ();
                MasterSerialText = null;
            }
        }
    }
}