using Foundation;
using System;
using UIKit;
using fifi.ViewModel;
using MapKit;
using fifi.iOS.CustomControls;
using CoreGraphics;
using CoreLocation;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using CoreImage;
using fifi.Client.DataObjects;
using fifi.Helpers;
using fifi.iOS;
using fifi.iOS.Helpers;
namespace fifi.iOS
{
	
    public partial class DeviceTripViewController : UIViewController
    {
		public PastTripDetailsViewModel _viewmodel;

        public DeviceTripViewController (IntPtr handle) : base (handle) {}



		public override void ViewDidLoad()
		{
			StartPictureFrame.Image = UIImage.FromBundle("ic_start_point");
			EndPictureFrame.Image = UIImage.FromBundle("ic_end_point");
			StartDate.Text = _viewmodel.Trip.StartedAt;
			EndDate.Text = _viewmodel.Trip.EndedAt;
			if (EndDate.Text == null)
			{
				EndDate.Text = "Not Ended";
			}

			Altitude_Units.Text = "Altitude";
			Altitude.Text = _viewmodel.Altitude;
			Speed_Units.Text = _viewmodel.SpeedUnits;
			Speed.Text = _viewmodel.Speed;
			WindSpeed_Units.Text = _viewmodel.WindSpeedUnits;
			WindSpeed.Text = _viewmodel.WindSpeed;
			Windheading_units.Text = _viewmodel.WindHeadingUnits;
			WindHeading.Text = _viewmodel.WindHeading;

		    UIApplication app = UIApplication.SharedApplication;
		    var statusBarHeight = app.StatusBarFrame.Size.Height;
		    UIView statusbar = new UIView(new CGRect(0, -statusBarHeight, UIScreen.MainScreen.Bounds.Width, statusBarHeight));
		    statusbar.BackgroundColor = UIColor.FromRGB(0, 173, 239);
		    this.NavigationController.NavigationBar.AddSubview(statusbar);


            #region map



            var mapView = new CustomMap(MapViewFrame.Bounds)
			{
				CenterCoordinate = new CLLocationCoordinate2D(56.2639, 9.5018),
			};
			mapView.ZoomEnabled = true;
			MapViewFrame.InsertSubview(mapView, 0);

		    if (_viewmodel.Trip.Coordinates.Count > 0)
		    {
		         _viewmodel.Trip.Coordinates = _viewmodel.Trip.Coordinates.OrderBy(x => Convert.ToDateTime(x.recorded_at)).ThenBy(x => Convert.ToDateTime(x.recorded_at)).ToList();

                var start =
                    _viewmodel.Trip.Coordinates.FirstOrDefault(
		                c =>
		                    c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
		                    c.Longitude.Where(char.IsDigit).Any(x => x != '0'));
		        var end =
		            _viewmodel.Trip.Coordinates.LastOrDefault(
		                c =>
		                    c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
		                    c.Longitude.Where(char.IsDigit).Any(x => x != '0'));

		        var startLatitude = double.Parse(start.Latitude);
		        var startLongitude = double.Parse(start.Longitude);

		        var endLatitude = double.Parse(end.Latitude);
		        var endLongitude = double.Parse(end.Longitude);
		        ;
		        mapView.AddAnnotations(new StartAnnotation("startMarker", new CLLocationCoordinate2D(startLatitude, startLongitude)));
		        mapView.AddAnnotation(new EndAnnotation("endmarker", new CLLocationCoordinate2D(endLatitude, endLongitude)));
		        var boat = new BoatAnnotation("Boat", new CLLocationCoordinate2D(startLatitude, startLongitude));
		        mapView.AddAnnotation(boat);
		        var points = _viewmodel.Trip.Coordinates.Where(
		                c =>
		                    c.Latitude.Where(char.IsDigit).Any(x => x != '0') &&
		                    c.Longitude.Where(char.IsDigit).Any(x => x != '0'))
		            .Select(
		                s =>
		                    new CLLocationCoordinate2D(double.Parse(s.Latitude),
		                        double.Parse(s.Longitude))).ToArray();
		        var Polyline = MKPolyline.FromCoordinates(points);

		        mapView.AddOverlay(Polyline);
		        mapView.SetVisibleMapRect(Polyline.BoundingMapRect, true);


		        #endregion
		        var stepvalue = 1.0;
		        CoordinatSlider.MinValue = 0;
		        CoordinatSlider.MaxValue = points.Length - 1;
		        CoordinatSlider.Value = (float)1.0;
		        CoordinatSlider.ValueChanged += (sender, e) =>
		        {
		            var newstep = Math.Round(CoordinatSlider.Value / stepvalue);
		            CoordinatSlider.Value = (float) newstep;
		            var newCenterCoordinat =
		                new CLLocationCoordinate2D(points[(int) newstep].Latitude, points[(int) newstep].Longitude);
		            mapView.SetCenterCoordinate(newCenterCoordinat, true);
		            _viewmodel.CurrentPosition = _viewmodel.Trip.Coordinates[(int) newstep];
		            Speed.Text = _viewmodel.Speed;
		            WindSpeed.Text = _viewmodel.WindSpeed;
		            Altitude.Text = _viewmodel.Altitude;
		            WindHeading.Text = _viewmodel.WindHeading;
		            foreach (var annotation in mapView.Annotations)
		            {
		                var title = annotation.GetTitle();
		                if (title == "Boat")
		                {         
                            mapView.RemoveAnnotation(annotation);
		                    var newboat = new BoatAnnotation("Boat", newCenterCoordinat);
		                    mapView.AddAnnotation(newboat);


                        }
		            }
		        };



		    };
             

		}
    }
}