﻿using System;
using System.Drawing;
using UIKit;

namespace SeaDroid.iOS.Helpers
{
    public class ImageResizer
    {
        public UIImage ResizeImage(UIImage sourceimage, float maxwidth, float maxheight)
        {
            var sourcesize = sourceimage.Size;
            var MaxResizeFactur = Math.Max(maxwidth / sourcesize.Width, maxheight / sourcesize.Height);
            if (MaxResizeFactur > 1) return sourceimage;
            var width = MaxResizeFactur * sourcesize.Width;
            var height = MaxResizeFactur * sourcesize.Height;
            UIGraphics.BeginImageContextWithOptions(new SizeF((float)width, (float)height), false, 2.0f);
            sourceimage.Draw(new RectangleF(0, 0, (float)width, (float)height));
            var resultimage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            return resultimage;
        }
    }
}