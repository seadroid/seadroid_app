﻿using System.Diagnostics;
using PubnubApi;

namespace SeaDroid.iOS.Helpers
{
    public class IosPubnubLog : IPubnubLog
    {
        private string logFilePath = "";

        public IosPubnubLog()
        {
            // Get folder path may vary based on environment
            string folder = System.IO.Directory.GetCurrentDirectory(); //For console
            //string folder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // For iOS
            System.Diagnostics.Debug.WriteLine(folder);
            logFilePath = System.IO.Path.Combine(folder, "pubnubmessaging.log");
            Trace.Listeners.Add(new TextWriterTraceListener(logFilePath));            
            
        }

        public void WriteToLog(string log)
        {
            Debug.WriteLine(log);         
            Trace.WriteLine(log);         
            Trace.Flush();
        }
    }
}