﻿using fifi.Client.DataObjects;
using UIKit;

namespace fifi.iOS.Helpers
{
    public class IosFlag
    {
        public UIImage flagImage;
        public string code;
        public string PhoneCode;

        public IosFlag(UIImage flagImage, string code, string phoneCode)
        {
            this.flagImage = flagImage;
            this.code = code;
            PhoneCode = phoneCode;
        }
    }
}