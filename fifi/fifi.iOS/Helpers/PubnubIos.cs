﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using fifi.Annotations;
using fifi.Client.DataObjects;
using fifi.Helpers;
using fifi.iOS;
using fifi.Utils.Helpers;
using MvvmHelpers;
using Newtonsoft.Json;
using PubnubApi;
using UIKit;

namespace SeaDroid.iOS.Helpers
{
    public sealed class PubnubIos
    {
        private static volatile PubnubIos instance;
        private static object syncRoot = new object();
        private SubscribeCallbackExt listenerSubscribeCallack;
        private ChatMessage _chatMessage;

        public ObservableRangeCollection<ChatMessage> LatestMessages;

        public bool HasLoadedLastMessages;


        private static Pubnub pubnubInstance;

        private int NumberOfCycledChannels;

        public ChatMessage ChatMessage
        {
            get { return _chatMessage; }
            set
            {
                _chatMessage = value;
                OnPropertyChanged();
            }
        }


        private PubnubIos()
        {
            LatestMessages = new ObservableRangeCollection<ChatMessage>();
            HasLoadedLastMessages = false;


            if (Settings.Current.PubnubUIID != string.Empty)
            {
                pubnubInstance = new Pubnub((new PNConfiguration()
                {
                    SubscribeKey = "sub-c-18d884ae-51a6-11e7-abef-0619f8945a4f",
                    PublishKey = "pub-c-160f1838-1dca-4952-8ffc-43c7f7d12374",
                    Uuid = Settings.Current.PubnubUIID,
                    LogVerbosity = PNLogVerbosity.BODY,
                    PubnubLog = new IosPubnubLog()
                }));
            }

            else
            {
                pubnubInstance = new Pubnub((new PNConfiguration()
                {
                    SubscribeKey = "sub-c-18d884ae-51a6-11e7-abef-0619f8945a4f",
                    PublishKey = "pub-c-160f1838-1dca-4952-8ffc-43c7f7d12374",
                    LogVerbosity = PNLogVerbosity.BODY,
                    PubnubLog = new IosPubnubLog()
                }));


                Settings.Current.PubnubUIID = pubnubInstance.PNConfig.Uuid;
            }
            StartPubnub();
        }


        private void StartPubnub()
        {
            listenerSubscribeCallack = new SubscribeCallbackExt(
                (pubnubObj, message) =>
                {
                    // Handle new message stored in message.Message 

                    var Payload = JsonConvert.DeserializeObject<MobilePayload>((string) message.Message);

                    var base64EncodedBytes = System.Convert.FromBase64String(Convert.ToString(Payload.full_message["Text"]));
                    var text = System.Text.Encoding.UTF8.GetString(base64EncodedBytes, 0, base64EncodedBytes.Length);
                    var ChatMessage = new ChatMessage(Convert.ToString(Payload.full_message["Id"]), Convert.ToString(Payload.full_message["Username"]), text, Convert.ToString(Payload.full_message["Channel"]), Convert.ToDateTime(Payload.full_message["Date"]), 0);
                   

                    var NewLatestMessages = new List<ChatMessage>();
                    NewLatestMessages.Add(ChatMessage);

                    foreach (var lastestmessage in LatestMessages)
                    {
                        if (lastestmessage.Channel != ChatMessage.Channel)
                        {
                            NewLatestMessages.Add(lastestmessage);
                        }
                    }
                    LatestMessages.ReplaceRange(NewLatestMessages);
                },
                (pubnubObj, presence) =>
                {
                    // handle incoming presence data 
                },
                (pubnubObj, status) =>
                {
                    // the status object returned is always related to subscribe but could contain
                    // information about subscribe, heartbeat, or errors
                    // use the PNOperationType to switch on different options
                    switch (status.Operation)
                    {
                        // let's combine unsubscribe and subscribe handling for ease of use
                        case PNOperationType.PNSubscribeOperation:
                            Debug.WriteLine(status.AffectedChannels);


                            break;


                        case PNOperationType.PNUnsubscribeOperation:
                            Debug.WriteLine(status.AffectedChannels);

                            // note: subscribe statuses never have traditional
                            // errors, they just have categories to represent the
                            // different issues or successes that occur as part of subscribe
                            switch (status.Category)
                            {
                                case PNStatusCategory.PNConnectedCategory:
                                    // this is expected for a subscribe, this means there is no error or issue whatsoever
                                    break;
                                case PNStatusCategory.PNReconnectedCategory:
                                    // this usually occurs if subscribe temporarily fails but reconnects. This means
                                    // there was an error but there is no longer any issue
                                    break;
                                case PNStatusCategory.PNDisconnectedCategory:
                                    // this is the expected category for an unsubscribe. This means there
                                    // was no error in unsubscribing from everything
                                    var tasd = 1;
                                    break;
                                case PNStatusCategory.PNUnexpectedDisconnectCategory:
                                    // this is usually an issue with the internet connection, this is an error, handle appropriately
                                    break;
                                case PNStatusCategory.PNAccessDeniedCategory:
                                    // this means that PAM does allow this client to subscribe to this
                                    // channel and channel group configuration. This is another explicit error
                                    break;
                                default:
                                    foreach (var channel in status.AffectedChannels)
                                    {
                                        var a = channel;
                                    }
                                    var b = status.ErrorData.Information;
                                    var c = status.ErrorData.Throwable;

                                    // More errors can be directly specified by creating explicit cases for other
                                    // error categories of `PNStatusCategory` such as `PNTimeoutCategory` or `PNMalformedFilterExpressionCategory` or `PNDecryptionErrorCategory`
                                    break;
                            }
                            break;
                        case PNOperationType.PNHeartbeatOperation:
                            // heartbeat operations can in fact have errors, so it is important to check first for an error.
                            if (status.Error)
                            {
                                // There was an error with the heartbeat operation, handle here
                            }
                            else
                            {
                                // heartbeat operation was successful
                            }
                            break;
                        default:
                            // Encountered unknown status type
                            break;
                    }
                });

            pubnubInstance.AddListener(listenerSubscribeCallack);
        }


        public void SubcribeForPushNotifications(string device_id)
        {
            var channels = pubnubInstance.GetSubscribedChannels().ToArray();

            pubnubInstance.AddPushNotificationsOnChannels()
                .PushType(PNPushType.APNS)
                .Channels(channels)
                .DeviceId("6d9b6cd16fcd16344f856d940d17e9c5c3900c81251ff9a1034227cec8ed6234")
                .Async(new PubnubIos.DemoPushAddChannel());
        }

        public void UnSubscribeFromPushNotifications(string device_id)
        {
            var channels = pubnubInstance.GetSubscribedChannels().ToArray();
            device_id = device_id.Replace("<", "").Replace(">", "").Replace(" ", "");
            pubnubInstance.RemovePushNotificationsFromChannels().PushType(PNPushType.APNS).Channels(channels)
                .DeviceId("6d9b6cd16fcd16344f856d940d17e9c5c3900c81251ff9a1034227cec8ed6234").Async(new DemoPushRemoveChannel());
        }


        public void GetSubcribedPushChannels(string deive_id)
        {
            pubnubInstance.AuditPushChannelProvisions()
                .DeviceId(deive_id)
                .PushType(PNPushType.APNS)
                .Async(new PubnubIos.DemoPushListProvisionChannel());
        }

        public void Subcribe(string Channel)
        {
            pubnubInstance.Subscribe<string>().Channels(new string[] {Channel}).Execute();
        }


        public void ClearLatestMessages()
        {
            LatestMessages = new ObservableRangeCollection<ChatMessage>();
        }

        public void SubcribeToFriendChat(int user_id, int friend_id)
        {
            var channels = pubnubInstance.GetSubscribedChannels();
            var ChannelsExist = false;

            if (channels == null)
            {
                channels = new List<string>();
            }

            if (user_id > friend_id)
            {
                foreach (var channel in channels)
                {
                    if (channel == user_id + "-" + friend_id)
                    {
                        ChannelsExist = true;
                    }
                }

                if (!ChannelsExist)
                {
                    pubnubInstance.Subscribe<string>().Channels(new string[] {user_id + "-" + friend_id}).Execute();
                }
            }
            else
            {
                foreach (var channel in channels)
                {
                    if (channel == friend_id + "-" + user_id)
                    {
                        ChannelsExist = true;
                    }
                }
                if (!ChannelsExist)
                {
                    pubnubInstance.Subscribe<string>().Channels(new string[] {friend_id + "-" + user_id}).Execute();
                }
            }
        }


        public bool CheckConnection()
        {
            var sucess = false;
            pubnubInstance.Time()
                .Async(new PNTimeResultExt(
                    (result, status) =>
                    {
                        if (status.Error)
                        {
                            sucess = false;
                        }
                        else
                        {
                            sucess = true;
                        }
                    }
                ));
            return sucess;
        }


        public List<string> GetSubcribedChannels()
        {
            List<string> channels = pubnubInstance.GetSubscribedChannels();
            return channels;
        }

        public bool SendMessage(ChatMessage message)
        {
            var base64message = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(message.Text));
            message.Text = base64message;

            try
            {
                Dictionary<string, object> apnsData = new Dictionary<string, object>();
                apnsData.Add("aps", new Dictionary<string, object>()
                {
                    {"alert", "Apns Test"},
                    {"badge", 2}
                });


                MobilePayload payload = new MobilePayload();
                payload.pn_apns = apnsData;
                payload.full_message = new Dictionary<string, object>()
                {
                    { "Text", message.Text},
                    { "Channel", message.Channel},
                    { "Date", message.Date},
                    { "Username", message.Username},
                    { "Id", message.Sender_id }

                };
                payload.pn_debug = true;

                pubnubInstance.Publish()
                    .Message(payload)
                    .Channel(message.Channel)
                    .ShouldStore(true)
                    .UsePOST(false)
                    .Async(new PNPublishResultExt(
                        (result, status) =>
                        {
                            if (status.Error)
                            {
                                Debug.WriteLine("error happened while publishing: " +
                                                pubnubInstance.JsonPluggableLibrary.SerializeToJsonString(status));
                            }
                            else
                            {
                                Debug.WriteLine("publish worked! timetoken: " + result.Timetoken.ToString());
                            }
                        }
                    ));
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
           
        }


        public void UnsubcribeAll()
        {
            pubnubInstance.UnsubscribeAll<string>();
        }

        public void Unsubscribe(string channel)
        {
            pubnubInstance.Unsubscribe<string>().Channels(new string[] {channel}).Execute();
        }

        public void UnSubcribefromFriendChat(int user_id, int friend_id)
        {
            if (user_id > friend_id)
            {
                pubnubInstance.Unsubscribe<string>().Channels(new string[] {user_id + "-" + friend_id}).Execute();
            }
            else
            {
                pubnubInstance.Unsubscribe<string>().Channels(new string[] {friend_id + "-" + user_id}).Execute();
            }
        }

        public void GetLastestMessageFromSubcribedChannels()
        {
            List<ChatMessage> ChatMessages = new List<ChatMessage>();

            var channels = pubnubInstance.GetSubscribedChannels();

            if (channels.Count > NumberOfCycledChannels)
            {
                pubnubInstance.History().Channel(channels[NumberOfCycledChannels]).Count(1).Async(
                    new PNHistoryResultExt((result, status) =>
                    {
                        if (!status.Error)
                        {
                            if (result.Messages.Count > 0)
                            {
                                var message = (Dictionary<string, object>) result.Messages[0].Entry;
                                var base64EncodedBytes = Convert.FromBase64String(Convert.ToString(message["Text"]));
                                var text = System.Text.Encoding.UTF8.GetString(base64EncodedBytes, 0,
                                    base64EncodedBytes.Length);

                                var NewMessage = new ChatMessage(Convert.ToString(message["Sender_id"]),
                                    Convert.ToString(message["Username"]), text, Convert.ToString(message["Channel"]),
                                    (DateTime) (message["Date"]), result.StartTimeToken);

                                ChatMessages.Add(NewMessage);


                                foreach (var m in LatestMessages)
                                {
                                    if (m.Channel != NewMessage.Channel)
                                    {
                                        ChatMessages.Add(m);
                                    }
                                }

                                LatestMessages.ReplaceRange(ChatMessages);

                                NumberOfCycledChannels = NumberOfCycledChannels + 1;

                                GetLastestMessageFromSubcribedChannels();
                            }
                            else
                            {
                                NumberOfCycledChannels = NumberOfCycledChannels + 1;
                                GetLastestMessageFromSubcribedChannels();
                            }
                        }
                    }));
            }
            else
            {
                NumberOfCycledChannels = 0;
                HasLoadedLastMessages = true;
            }
        }


        public void PullHistory(string channel, ObservableRangeCollection<ChatMessage> Messagelist)
        {
            if (Messagelist.Count <= 0)
            {
                bool firstmeessage = true;
                List<ChatMessage> messages = new List<ChatMessage>();
                var sucess = false;
                pubnubInstance.History()
                    .Channel(channel) // where to fetch history from
                    .Count(15) // how many items to fetch
                    .Async(new PNHistoryResultExt(
                        (result, status) =>
                        {
                            if (!status.Error)
                            {
                                foreach (var message in result.Messages)
                                {
                                    try
                                    {
                                        var m = (Dictionary<string, object>) message.Entry;

                                        if (firstmeessage)
                                        {
                                            var ChatMessage = new ChatMessage(Convert.ToString(m["Sender_id"]),
                                                Convert.ToString(m["Username"]), System.Text.Encoding.UTF8.GetString(
                                                    Convert.FromBase64String(Convert.ToString(m["Text"])),
                                                    0, Convert.FromBase64String(Convert.ToString(m["Text"])).Length),
                                                Convert.ToString("Channel"),
                                                (DateTime) (m["Date"]), result.StartTimeToken);

                                            messages.Add(ChatMessage);
                                            firstmeessage = false;
                                        }
                                        else
                                        {
                                            var ChatMessage = new ChatMessage(Convert.ToString(m["Sender_id"]),
                                                Convert.ToString(m["Username"]), System.Text.Encoding.UTF8.GetString(
                                                    Convert.FromBase64String(Convert.ToString(m["Text"])),
                                                    0, Convert.FromBase64String(Convert.ToString(m["Text"])).Length),
                                                Convert.ToString("Channel"),
                                                (DateTime) (m["Date"]), message.Timetoken);

                                            messages.Add(ChatMessage);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Debug.WriteLine(e);
                                    }
                                }
                                Messagelist.AddRange(messages);
                            }
                        }
                    ));
            }
            else
            {
                List<ChatMessage> messages = new List<ChatMessage>();
                bool firstmeessage = true;
                pubnubInstance.History()
                    .Channel(channel) // where to fetch history from
                    .Count(5).Start(Messagelist[0].TimeToken) // how many items to fetch
                    .Async(new PNHistoryResultExt(
                        (result, status) =>
                        {
                            if (!status.Error)
                            {
                                foreach (var message in result.Messages)
                                {
                                    try
                                    {
                                        var m = (Dictionary<string, object>) message.Entry;

                                        if (firstmeessage)
                                        {
                                            var ChatMessage = new ChatMessage(Convert.ToString(m["Sender_id"]),
                                                Convert.ToString(m["Username"]), System.Text.Encoding.UTF8.GetString(
                                                    Convert.FromBase64String(Convert.ToString(m["Text"])),
                                                    0, Convert.FromBase64String(Convert.ToString(m["Text"])).Length),
                                                Convert.ToString("Channel"),
                                                (DateTime) (m["Date"]), result.StartTimeToken);

                                            messages.Add(ChatMessage);
                                            firstmeessage = false;
                                        }
                                        else
                                        {
                                            var ChatMessage = new ChatMessage(Convert.ToString(m["Sender_id"]),
                                                Convert.ToString(m["Username"]), System.Text.Encoding.UTF8.GetString(
                                                    Convert.FromBase64String(Convert.ToString(m["Text"])),
                                                    0, Convert.FromBase64String(Convert.ToString(m["Text"])).Length),
                                                Convert.ToString("Channel"),
                                                (DateTime) (m["Date"]), message.Timetoken);

                                            messages.Add(ChatMessage);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Debug.WriteLine(e);
                                    }
                                }
                                messages.AddRange(Messagelist);
                                if (messages.Count > Messagelist.Count)
                                {
                                    Messagelist.ReplaceRange(messages);
                                }
                            }
                        }
                    ));
            }
        }


        public static PubnubIos Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null) instance = new PubnubIos();
                    }
                }
                return instance;
            }
        }

        public void ResetSingleton()
        {
            pubnubInstance.Destroy();
            pubnubInstance.RemoveListener(listenerSubscribeCallack);
            instance = new PubnubIos();
        }

        public void SetUUID(string uuid)
        {
            pubnubInstance.ChangeUUID(uuid);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

      

        internal class DemoPushAddChannel : PNCallback<PNPushAddChannelResult>
        {
            public override void OnResponse(PNPushAddChannelResult result, PNStatus status)
            {
                var statustext = status.AffectedChannels.ToString();
                var resulttext = result.ToString();

                Instance.GetSubcribedPushChannels(
                    "4e71acc275a8eeb400654d923724c073956661455697c92ca6c5438f2c19aa7b");
                //pubnubInstance.PNConfig.PubnubLog.WriteToLog("test2");
                //pubnubInstance.PNConfig.PubnubLog.WriteToLog(result.ToString() + status);
            }
        }

        internal class DemoPushListProvisionChannel : PNCallback<PNPushListProvisionsResult>
        {
            public override void OnResponse(PNPushListProvisionsResult result, PNStatus status)
            {
                var statustext = status.ToString();
                var resulttext = result.ToString();
                Instance.SendMessage(new ChatMessage("2", "test", "test", "3-2", DateTime.Now.ToUniversalTime(), 0));
            }
        }

        internal class DemoPushRemoveChannel : PNCallback<PNPushRemoveChannelResult>
        {
            public override void OnResponse(PNPushRemoveChannelResult result, PNStatus status)
            {
            }

        }
    }

    public class MobilePayload
    {
        public Dictionary<string, object> pn_apns;
        public Dictionary<string, object> pn_gcm;
        public Dictionary<string, object> pn_mpns;
        public Dictionary<string, object> full_message;
        public bool pn_debug;
    }
}