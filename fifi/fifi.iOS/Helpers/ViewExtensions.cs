using System;
using System.Linq;
using CoreGraphics;
using UIKit;

namespace fifi.iOS.Helpers
{
    public static class ViewExtensions
    {
        /// <summary>
        /// Find the first responder in the <paramref name="view"/>'s subview hierarchy
        /// </summary>
        /// <param name="view">
        /// A <see cref="UIView"/>
        /// </param>
        /// <returns>
        /// A <see cref="UIView"/> that is the first responder or null if there is no first responder
        /// </returns>
        public static UIView FindFirstResponder(this UIView view)
        {
            if (view.IsFirstResponder)
            {
                return view;
            }
            return
                view.Subviews.Select(subview => subview.FindFirstResponder())
                    .FirstOrDefault(firstResponder => firstResponder != null);
        }

        public static double GetViewRelativeBottom(this UIView view, UIView rootView)
        {
            var viewRelativeCoordinates = rootView.ConvertPointFromView(view.Frame.Location, view);
            var activeViewRoundedY = Math.Round(viewRelativeCoordinates.Y, 2);

            return activeViewRoundedY + view.Frame.Height;
        }

        /// <summary>
        /// Determines if the <see cref="UIView"/> is overlapped by the keyboard
        /// </summary>
        /// <param name="activeView">
        /// 
        /// </param>
        /// <param name="rootView"></param>
        /// <param name="keyboardFrame"></param>
        /// <returns></returns>
        public static bool IsKeyboardOverlapping(this UIView activeView, UIView rootView, CGRect keyboardFrame)
        {
            var activeViewBottom = activeView.GetViewRelativeBottom(rootView);
            var pageHeight = rootView.Frame.Height;
            var keyboardHeight = keyboardFrame.Height;

            return activeViewBottom >= pageHeight - keyboardHeight;
        }
    }
}