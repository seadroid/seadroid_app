﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Foundation;
using UIKit;

namespace fifi.iOS.Helpers
{
    public static class ExtensionMethods
    {
        #region Color

        public static UIColor ToUiColor(this string hexString)
        {
            hexString = hexString.Replace("#", "");

            if (hexString.Length == 3)
            {
                hexString = hexString + hexString;
            }

            if (hexString.Length != 6)
            {
                throw new Exception("Invalid hex string");
            }

            var red = int.Parse(hexString.Substring(0, 2), NumberStyles.AllowHexSpecifier);
            var green = int.Parse(hexString.Substring(2, 2), NumberStyles.AllowHexSpecifier);
            var blue = int.Parse(hexString.Substring(4, 2), NumberStyles.AllowHexSpecifier);

            return UIColor.FromRGB(red, green, blue);
        }

        public static UIColor ToUiColorAlpha(this string hexString)
        {
            hexString = hexString.Replace("#", "");

            if (hexString.Length > 8)
            {
                throw new Exception("Invalid hex string with alpha");
            }

            var alpha = int.Parse(hexString.Substring(0, 2), NumberStyles.AllowHexSpecifier);
            var red = int.Parse(hexString.Substring(2, 2), NumberStyles.AllowHexSpecifier);
            var green = int.Parse(hexString.Substring(4, 2), NumberStyles.AllowHexSpecifier);
            var blue = int.Parse(hexString.Substring(6, 2), NumberStyles.AllowHexSpecifier);

            return UIColor.FromRGBA(red, green, blue, alpha);
        }

        #endregion

        #region Email

        public static bool IsEmail(this string email)
        {
            var emailRegex =
                @"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$";
            var regex = new Regex(emailRegex, RegexOptions.IgnoreCase);
            return regex.IsMatch(email);
        }

        #endregion

        #region Animations

        public static void FadeIn(this UIView view, double duration, float delay)
        {
            UIView.Animate(duration, delay, UIViewAnimationOptions.CurveEaseIn, () => { view.Alpha = 1; }, () => { });
        }

        #endregion

        #region Degrees to Radians

        public static nfloat ToRadians(this float val)
        {
            return (NMath.PI / 180.0f) * val;
        }

        #endregion
    }
}