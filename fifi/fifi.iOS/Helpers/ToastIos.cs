﻿using System;
using System.Drawing;
using UIKit;

namespace fifi.iOS.Helpers
{
    public class ToastIos: UIView
    {
        public void ShowToast(String message, UIView view)
        {
            UIView residualView = view.ViewWithTag(1989);
            if (residualView != null)
                residualView.RemoveFromSuperview();
                         
            var viewBack = new UIImageView(ResizeImage(UIImage.FromBundle("IosToast"),80, 80));
                                 
            viewBack.Tag = 1989;
            
            UILabel lblMsg = new UILabel(new CoreGraphics.CGRect(60, 20, 80, 40));
            lblMsg.Lines = 2;
            lblMsg.Text = message;
            lblMsg.TextColor = UIColor.White;
            lblMsg.TextAlignment = UITextAlignment.Center;       
            viewBack.AddSubview(lblMsg);
            view.AddSubview(viewBack);
            viewBack.Center = new CoreGraphics.CGPoint(view.Center.X, view.Center.Y*1.75);
            UIView.BeginAnimations("Toast");       
            UIView.SetAnimationDuration(3.0f);
            viewBack.Alpha = 0.0f;
            UIView.CommitAnimations();
        }

        public UIImage ResizeImage(UIImage sourceimage, float maxwidth, float maxheight)
        {

            var sourcesize = sourceimage.Size;
            var MaxResizeFactur = Math.Max(maxwidth / sourcesize.Width, maxheight / sourcesize.Height);
            if (MaxResizeFactur > 1) return sourceimage;
            var width = MaxResizeFactur * sourcesize.Width;
            var height = MaxResizeFactur * sourcesize.Height;
            UIGraphics.BeginImageContextWithOptions(new SizeF((float)width, (float)height), false, 2.0f);
            sourceimage.Draw(new RectangleF(0, 0, (float)width, (float)height));
            var resultimage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            return resultimage;
        }
    }
}