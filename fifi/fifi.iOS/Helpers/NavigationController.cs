﻿using Foundation;
using System;
using UIKit;

namespace fifi.iOS
{
    public partial class NavigationController : UINavigationController
    {
        public NavigationController (IntPtr handle) : base (handle)
        {
        }

        public NavigationController()
        {
        }
    }
}