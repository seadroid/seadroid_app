﻿using System;
using System.Collections.Generic;
using UIKit;

namespace fifi.iOS.Helpers
{
    public class ListPickerModel : UIPickerViewModel
    {       
            public EventHandler PropertyChanged;
            public string selectedItem;
            private List<string> _items;
            public int SelectedItemRow;

            public ListPickerModel(List<string> items)
            {
                _items = items;
            }

            public override nint GetComponentCount(UIPickerView pickerView)
            {
                return 1;
            }

            public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
            {
                return _items.Count;
            }

            public override string GetTitle(UIPickerView pickerView, nint row, nint component)
            {
                return _items[(int)row];
            }

        public override void Selected(UIPickerView pickerView, nint row, nint component)
        {
            selectedItem = _items[(int) row];
            SelectedItemRow = (int) row;
            PropertyChanged?.Invoke(null, null);
        }
    }
}