﻿using System;
using System.Net;
using Foundation;

namespace fifi.iOS.Helpers
{  
    
        public class ImageUrlDownloader
        {
            private string base64imageBytes;
            public string GetImageBitmapFromUrl(string url)
            {
                try
                {
                    using (var webClient = new WebClient())
                    {
                        var imageBytes = webClient.DownloadData(url);
                        base64imageBytes = Convert.ToBase64String(imageBytes);
                    }
                    return base64imageBytes;
            }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return string.Empty;
                }
               
            }
        }
    }

