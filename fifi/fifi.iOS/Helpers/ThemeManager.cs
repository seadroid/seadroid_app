﻿using UIKit;

namespace fifi.iOS.Helpers
{
    public class ThemeManager
    {
        public static void ApplyTheme()
        {
            // Status bar
            UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.LightContent;

            // Navigation bar
            UINavigationBar.Appearance.BarTintColor = "#000000".ToUiColor();
            UINavigationBar.Appearance.TintColor = UIColor.White;

            UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes
            {
                Font = UIFont.FromName("Avenir-Medium", 17f),
                TextColor = UIColor.White
            });

            // Navigation bar buttons
            UIBarButtonItem.Appearance.SetTitleTextAttributes(new UITextAttributes
            {
                Font = UIFont.FromName("Avenir-Medium", 17f),
                TextColor = UIColor.White
            }, UIControlState.Normal);

            // TabBar
            UITabBar.Appearance.TintColor = "#000000".ToUiColor();
            UITabBarItem.Appearance.SetTitleTextAttributes(new UITextAttributes
            {
                Font = UIFont.FromName("Avenir-Book", 10f)
            }, UIControlState.Normal);
        }
    }
}